import React from "react"
import './style.css'
import BG_IMAGE from "./../../asset/img/login/BG_IMAGE.png"
import LOGO_KTB from "./../../asset/img/login/LOGO_KTB.png"
import SUBMITICON from './../../asset/img/login/SUBMIT_ICON.svg'

import clsx from 'clsx'

import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';

import styled from 'styled-components';
import { TextField } from '@material-ui/core';

import CircularProgress from '@material-ui/core/CircularProgress';

import { useNavigate } from "react-router-dom";

// api login
import auth from 'src/apis/auth'
import ChangeFirstPassword from 'src/apis/firstPassword'

// action
import { TOKEN } from 'src/stores/actions'
import { USER } from 'src/stores/actions'

// Parse JWT Token
import JWT from 'jsonwebtoken'

// Commons
import PasswordChecker from 'src/commons/passwordChecker'

// icon
import VisibilityOffIcon from '@material-ui/icons/Visibility'
import VisibilityIcon from '@material-ui/icons/VisibilityOff'

const UsernameTextField = styled(TextField)`
  .MuiOutlinedInput-root {
    &.Mui-focused fieldset {
      border: 2px solid #337AB7 !important;
    }
  }
  .MuiOutlinedInput-input {
    padding: 13px 47px 13px 16px;
    font-size: 18px;
  }
`;

const PasswordTextField = styled(TextField)`
  .MuiOutlinedInput-root {
    &.Mui-focused fieldset {
      border: 2px solid #337AB7 !important;
    }
  }
  .MuiOutlinedInput-input {
    padding: 13px 47px 13px 16px;
    font-size: 18px;
  }
`;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  InputControl: {
    width: '447px',
    backgroundColor: "#F9FDFF",
    borderRadius: "8px",
    caretColor: "#337AB7"
    // border: 2px solid #337AB7 !important;
  },
  username: {
    marginTop: "20px"
  },
  password: {
    marginTop: "20px"
  },
  iconSubmit: {
    width: 30,
    height: 30,
  },
}));

function Login (props) {
  const classes = useStyles();
  let history = useNavigate();

  // state
  const [ store ] = React.useState(props.store)
  const [Username, setUsername ] = React.useState("") 
  const [Password, setPassword ] = React.useState("")
  // const [Username, setUsername ] = React.useState("admin") 
  // const [Password, setPassword ] = React.useState("Admin1234567890_")
  const [isPasswordVisible, setIsPasswordVisible] = React.useState(false)
  const [circleStatus, setcircleStatus ] = React.useState(false)

  const [isFirstLogIn, setIsFirstLogIn] = React.useState(false)
  const [newPassword, setNewPassword] = React.useState("")
  const [confirmNewPassword, setConfirmNewPassword] = React.useState("")
  const [isNewPasswordVisible, setIsNewPasswordVisible] = React.useState(false)
  const [isConfirmPasswordVisible, setIsConfirmPasswordVisible] = React.useState(false)
  const [ AddPassError, setAddPassError ] = React.useState(false)
  const [ AddPassCond, setAddPassCond ] = React.useState([])
  const [ Token, setToken ] = React.useState(null)

  const ref = React.createRef()

  React.useEffect(() => {

  }, [])

  const Login = async () => {
    setcircleStatus(true)
    setTimeout(async () => {
      const res = await auth({
        "query": `{
          Login(username: "`+ Username +`", password: "`+ Password +`") {
            token
          }
        }`
      })
      if (res.data.data.Login !== "") {
        var user = JWT.decode(res.data.data.Login.token)
        if (user.first_time) {
          setToken(res.data.data.Login.token)
          setIsFirstLogIn(true)
          setcircleStatus(false)
        } else {
          store.dispatch(USER(user))
          store.dispatch(TOKEN(res.data.data.Login.token))
          history("/dashboard/status")
          setcircleStatus(true)
        }
      } else {
        alert(res.data.errors[0].message)
        setcircleStatus(false)
      }
    }, 200);
  }

  const handleChangePasswordFirstTime = async () => {
    let anyPasswordError = PasswordChecker(newPassword)
    if (anyPasswordError.length === 0) {
      if (newPassword === confirmNewPassword) {
        setAddPassError(false)
        setAddPassCond([])
        const res = await ChangeFirstPassword({
          query: `
            mutation{
              FirstTimeLogin (newPassword: "${newPassword}") {
                  result
              }
            }
          `
        }, Token)
        if (res.data.data.FirstTimeLogin.result) {
          alert("เปลี่ยนรหัสผ่านสำเร็จ กรุณา Log in ด้วยรหัสผ่านใหม่")
          setIsFirstLogIn(false)
          setNewPassword("")
          setConfirmNewPassword("")
          setAddPassError(false)
          setAddPassCond([])
          setUsername("")
          setPassword("")
          setIsPasswordVisible(false)
          setIsNewPasswordVisible(false)
          setIsConfirmPasswordVisible(false)
        } else {
          if (res.data.errors[0].message === "MATCHED OLD PASSWORD") {
            alert("ไม่สามารถใช้รหัสผ่านเก่าได้")
          } else {
            alert("ไม่สามารถเปลี่ยนรหัสผ่านได้")
          }
        }
      } else {
        setAddPassError(true)
        setAddPassCond([5])
      }
    } else {
      setAddPassError(true)
      setAddPassCond(anyPasswordError)
    }
  }

  return (
    <div className="container-fluid">
      <img className="fixed-bg-image" alt="BG_IMAGE" src={BG_IMAGE}></img>
      <div className="form-login">
        <div className="logo-ktb">
          <img alt="LOGO_KTB" src={LOGO_KTB}></img>
        </div>

        <div className="flex-from-loading">

            {/* loading */}
            <div className="loading-space" style={{
              display: "block"
            }}>
              <CircularProgress color={ "primary" } />
            </div>


            {/* user and pass */}
            <div style={{display: isFirstLogIn ? 'none' : 'block'}}>
              <div className="username-ktb">
                
                <FormControl variant="outlined" >
                  <UsernameTextField 
                    className={clsx(classes.InputControl, classes.username)}
                    type="text"
                    variant="outlined" 
                     
                    labelwidth={0}
                    placeholder="ใส่ชื่อผู้ใช้ของคุณ"
                    value={Username}
                    onChange={(e) => {
                      if (e.target.value.slice(-1) !== ' ') {
                        if (/\s/.test(e.target.value)) {  // Incase of string has spaces
                          setUsername(e.target.value.split(/\s+/).join(''))
                        } else {
                          setUsername(e.target.value)
                        }
                      }
                    }}
                  />
                </FormControl>
                
              </div>
              <div className="password-ktb">
              
              <FormControl variant="outlined" >
                <PasswordTextField 
                  className={clsx(classes.InputControl, classes.password)}
                  type={isPasswordVisible ? "text" : "password"}
                  variant="outlined" 
                  labelwidth={0}
                  placeholder="ใส่รหัสผ่านของคุณ" 
                  value={Password}
                  onChange={(e) => {
                    if (e.target.value.slice(-1) !== ' ') {
                      if (/\s/.test(e.target.value)) {  // Incase of string has spaces
                        setPassword(e.target.value.split(/\s+/).join(''))
                      } else {
                        setPassword(e.target.value)
                      }
                    }
                  }}
                  onKeyDown={(e) => {
                    if (e.key === "Enter"){
                      // api login
                      Login()
                    }
                  }}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          edge="end"
                          onClick={() => {
                            setIsPasswordVisible(!isPasswordVisible)
                          }}
                        >
                          {
                            isPasswordVisible ?
                            <VisibilityOffIcon style={{color: "#BABABA"}} /> :
                            <VisibilityIcon style={{color: "#BABABA"}} />
                          }
                        </IconButton>
                        <IconButton
                          aria-label="login"
                          edge="end"
                          ref={ref}
                          onClick={() => {
                            // api login
                            Login()
                          }}
                        >
                          <img src={SUBMITICON} alt="SUBMITICON" className={classes.iconSubmit}></img>
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
              </FormControl>

              </div>
            </div>

            <div style={{display: isFirstLogIn ? 'block' : 'none'}}>
              <div style={{
                color: "white",
                fontSize: "18px",
                textAlign: "center"
              }}>
                คุณได้ทำการ Log in เป็นครั้งแรก กรุณาเปลี่ยนรหัสผ่าน
              </div>
              <div className="password-ktb">
                <FormControl variant="outlined" >
                  <PasswordTextField 
                    className={clsx(classes.InputControl, classes.password)}
                    type={isNewPasswordVisible ? "text" : "password"}
                    variant="outlined" 
                    error={AddPassError}
                    labelwidth={0}
                    placeholder="ใส่รหัสผ่านใหม่ของคุณ" 
                    value={newPassword}
                    onChange={(e) => {
                      if (e.target.value.slice(-1) !== ' ') {
                        if (/\s/.test(e.target.value)) {  // Incase of string has spaces
                          setNewPassword(e.target.value.split(/\s+/).join(''))
                        } else {
                          setNewPassword(e.target.value)
                        }
                      }
                    }}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            edge="end"
                            onClick={() => {
                              setIsNewPasswordVisible(!isNewPasswordVisible)
                            }}
                          >
                            {
                              isNewPasswordVisible ?
                              <VisibilityOffIcon style={{color: "#BABABA"}} /> :
                              <VisibilityIcon style={{color: "#BABABA"}} />
                            }
                          </IconButton>
                        </InputAdornment>
                      ),
                    }}
                  />
                </FormControl>
                <div style={{
                  margin: "5px 0px 0px 20px"
                }}>
                  <ul style={{ padding: '0', textAlign: 'left', color: 'white' }}>
                    <li style={{display: AddPassCond.includes(0) ? 'list-item' : 'none' }}>
                      รหัสผ่านต้องมีความยาวมากกว่า 8 ตัวอักษร
                    </li>
                    <li style={{display: AddPassCond.includes(1) ? 'list-item' : 'none' }}>
                      รหัสผ่านต้องประกอบไปด้วยตัวพิมพ์ใหญ่อย่างน้อย 1 ตัว
                    </li>
                    <li style={{display: AddPassCond.includes(2) ? 'list-item' : 'none' }}>
                      รหัสผ่านต้องประกอบไปด้วยตัวพิมพ์เล็กอย่างน้อย 1 ตัว
                    </li>
                    <li style={{display: AddPassCond.includes(3) ? 'list-item' : 'none' }}>
                      รหัสผ่านต้องประกอบไปด้วยตัวเลขอย่างน้อย 1 ตัว
                    </li>
                    <li style={{display: AddPassCond.includes(4) ? 'list-item' : 'none' }}>
                      รหัสผ่านต้องประกอบไปด้วยอักขระพิเศษอย่างน้อย 1 ตัว
                    </li>
                  </ul>
                </div>
              </div>
              <div className="password-ktb">
                <FormControl variant="outlined" >
                  <PasswordTextField 
                    className={clsx(classes.InputControl, classes.password)}
                    type={isConfirmPasswordVisible ? "text" : "password"}
                    variant="outlined" 
                    error={AddPassError}
                    labelwidth={0}
                    placeholder="ยืนยันรหัสผ่านใหม่ของคุณ" 
                    value={confirmNewPassword}
                    onChange={(e) => {
                      if (e.target.value.slice(-1) !== ' ') {
                        if (/\s/.test(e.target.value)) {  // Incase of string has spaces
                          setConfirmNewPassword(e.target.value.split(/\s+/).join(''))
                        } else {
                          setConfirmNewPassword(e.target.value)
                        }
                      }
                    }}
                    onKeyDown={(e) => {
                      if (e.key === "Enter"){
                        handleChangePasswordFirstTime()
                      }
                    }}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            edge="end"
                            onClick={() => {
                              setIsConfirmPasswordVisible(!isConfirmPasswordVisible)
                            }}
                          >
                            {
                              isConfirmPasswordVisible ?
                              <VisibilityOffIcon style={{color: "#BABABA"}} /> :
                              <VisibilityIcon style={{color: "#BABABA"}} />
                            }
                          </IconButton>
                          <IconButton
                            aria-label="toggle password visibility"
                            edge="end"
                            ref={ref}
                            onClick={() => {
                              handleChangePasswordFirstTime()
                            }}
                          >
                            <img src={SUBMITICON} alt="SUBMITICON" className={classes.iconSubmit}></img>
                          </IconButton>
                        </InputAdornment>
                      ),
                    }}
                  />
                </FormControl>
                <div style={{
                  margin: "5px 0px 0px 20px"
                }}>
                  <ul style={{ padding: '0', textAlign: 'left', color: 'white' }}>
                    <li style={{display: AddPassCond.includes(5) ? 'list-item' : 'none' }}>
                      รหัสผ่าน และยืนยันรหัสผ่านไม่ตรงกัน
                    </li>
                  </ul>
                </div>
              </div>
              <div style={{
                margin: "5px 0px 0px 0px",
                textDecoration: "underline",
                color: "white",
                cursor: "pointer"
              }}
              onClick={() => {
                setIsFirstLogIn(false)
                setNewPassword("")
                setConfirmNewPassword("")
                setAddPassError(false)
                setAddPassCond([])
                setUsername("")
                setPassword("")
                setIsPasswordVisible(false)
                setIsNewPasswordVisible(false)
                setIsConfirmPasswordVisible(false)
              }}>
                {'<< กลับไปยังหน้า Log in'}
              </div>
            </div>
              
            {/* loading */}
            <div className="loading-space" style={{
              display: "block"
            }}>
              <CircularProgress color={ ((circleStatus) ? "secondary": "primary") } />
            </div>

        </div>

      </div>
      {/* <div className="version"> V2.0. All Rights Reserved. </div> */}
    </div>
  )
}

export default Login
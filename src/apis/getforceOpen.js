
import axios from 'axios'
import sconfig from './config'

// eslint-disable-next-line
export default (Token, id) => {
  return axios({
    method: 'get',
    url: sconfig.url +'/device/'+id+'/forceOpen',
    headers: { 
      'Authorization': 'Bearer ' + Token, 
    },
  })
}

import axios from 'axios'
import sconfig from './config'

// eslint-disable-next-line
export default (data, Token) => {
  return axios({
    method: 'post',
    url: sconfig.url +'/employee/createEmployee',
    headers: { 
      'Authorization': 'Bearer ' + Token, 
    },
    data : data
  })
}
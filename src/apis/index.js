import axios from 'axios'
import sconfig from './config'

// eslint-disable-next-line
export default (token) => {
  var api = axios.create({
    baseURL: sconfig.url
  })
  
  api.defaults.headers.common['Authorization'] = 'Bearer ' + token

  // api.defaults.headers.common['Content-Length'] =  "100"
  // api.defaults.headers.common['Content-Type'] = "multipart/form-data;"
  // api.defaults.headers.common['Host'] = "http://104.154.199.48:13210/employee/createEmployee"


  return api
}
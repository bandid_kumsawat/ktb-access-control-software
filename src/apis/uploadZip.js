
import axios from 'axios'
import sconfig from './config'

// eslint-disable-next-line
export default (data, Token) => {
  console.log(Token)
  console.log(data)
  return axios({
    method: 'post',
    url: sconfig.url +'/employee/uploadzip',
    headers: { 
      'Authorization': 'Bearer ' + Token, 
    },
    data : data
  })
}
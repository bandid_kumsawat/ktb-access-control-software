
import axios from 'axios'
import sconfig from './config'

// eslint-disable-next-line
export default (data, Token, id) => {
  return axios({
    method: 'patch',
    url: sconfig.url +'/device/'+id+'/forceOpen',
    headers: { 
      'Authorization': 'Bearer ' + Token, 
    },
    data: data
  })
}

import axios from 'axios'
import sconfig from './config'

// eslint-disable-next-line
export default (data, Token, id) => {
  return axios({
    method: 'patch',
    url: sconfig.url +'/employee/' + id,
    headers: { 
      'Authorization': 'Bearer ' + Token, 
    },
    data : data
  })
}

import server from '.'

// eslint-disable-next-line
export default (data, token) => {
  return server(token).post('/graph/admin/access', data)
}
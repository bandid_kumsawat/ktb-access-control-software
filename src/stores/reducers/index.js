import initialState from './state'

//eslint-disable-next-line
export default (state = initialState, action) => {
  switch (action.type) {
    case 'TOKEN':
      return Object.assign({}, state, {
        AccessToken: action.payload
      })
    case "DEVICE": 
      return Object.assign({}, state, {
        Device: action.payload
      })
    case "LOG": 
      return Object.assign({}, state, {
        Log: action.payload
      })
    case "GROUP": 
      return Object.assign({}, state, {
        Group: action.payload
      })
    case "USER": 
      return Object.assign({}, state, {
        User: action.payload
      })
    case "LISTUSER": 
      return Object.assign({}, state, {
        ListUser: action.payload
      })
    case "EMPLOYEE":
      return Object.assign({}, state, {
        Employee: action.payload
      })
    case "SEARCHDEVICE":
      return Object.assign({}, state, {
        SearchDevice: action.payload
      })
    case "SEARCHEMPLOYEE":
      return Object.assign({}, state, {
        SearchEmployee: action.payload
      })
    case "ADDGROUP":
      return Object.assign({}, state, {
        AddGroup: action.payload
      })
    case "EDITGROUP":
      return Object.assign({}, state, {
        EditGroup: action.payload
      })
    case "Event":
      return Object.assign({}, state, {
        Event: action.payload
      })
    case "EventStatus":
      return Object.assign({}, state, {
        EventStatus: action.payload
      })
    default:
      return state
  }
}

// eslint-disable-next-line
export default {
  AccessToken: undefined,
  User: undefined,
  ListUser: undefined,
  Device: undefined,
  Log: undefined,
  Group: undefined,
  Employee: undefined,
  SearchDevice: undefined,
  SearchEmployee: undefined,
  AddGroup: undefined,
  EditGroup: undefined,
  Event: undefined,
  EventStatus: {
    init: false,
    new: 0,
    old: 0
  }
}
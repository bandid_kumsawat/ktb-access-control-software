export const TOKEN = (payload) => {
  return {
    type : "TOKEN",
    payload
  }
}
export const DEVICE = (payload) => {
  return {
    type: "DEVICE",
    payload
  }
}
export const LOG = (payload) => {
  return {
    type: "LOG",
    payload
  }
}

export const GROUP = (payload) => {
  return {
    type: "GROUP",
    payload
  }
}

export const USER = (payload) => {
  return {
    type: "USER",
    payload
  }
}

export const LISTUSER = (payload) => {
  return {
    type: "LISTUSER",
    payload
  }
}

export const EMPLOYEE = (payload) => {
  return {
    type: "EMPLOYEE",
    payload
  }
}

export const SEARCHDEVICE = (payload) => {
  return {
    type: "SEARCHDEVICE",
    payload
  }
}

export const SEARCHEMPLOYEE = (payload) => {
  return {
    type: "SEARCHEMPLOYEE",
    payload
  }
}

export const ADDGROUP = (payload) => {
  return {
    type: "ADDGROUP",
    payload
  }
}

export const EDITGROUP = (payload) => {
  return {
    type: "EDITGROUP",
    payload
  }
}

export const Event = (payload) => {
  return {
    type: "Event",
    payload
  }
}

export const EventStatus = (payload) => {
  return {
    type: "EventStatus",
    payload
  }
}
import axios from 'axios'
import sconfig from './config'

// eslint-disable-next-line
export default (token) => {
  var api = axios.create({
    baseURL: sconfig.url
  })
  api.defaults.headers.common['Authorization'] = 'Bearer ' + token
  return api
}
import React from 'react'
import ReplayIcon from '@material-ui/icons/Replay';
import IconButton from '@material-ui/core/IconButton'

import { 
  EMPLOYEE
} from "src/stores/actions"

// deplay apis
import getEmployeeByGroup from 'src/apis/getEmployeeByGroup'

function ReloadData (props) {
  const { onBackdrop, group_id } = props
  const [ store ] = React.useState(props.store)

  const updateEmployee = async () => {
    const Token = store.getState().AccessToken
    onBackdrop(true)
    const res = await getEmployeeByGroup({
      query: `
      {
        Employee (groups: [`+group_id+`]) {
          results {
              employee_id
              name
              lastname
              position
              mifare_id
              employee_code
              picture_url
              email
              groups {
                  group_id
                  name
              }
              gates {
                  gate_id,
                  name_gate,
                  begin_time,
                  end_time,
              }
              employee_gates {
                  gate_id
                  override_time
                  begin_time
                  end_time
              }
          }
          count
        }     
      }
        
      `,
    }, Token)
    if (res.data.errors === undefined){
      store.dispatch(EMPLOYEE(res.data.data.Employee.results))
    } 
    onBackdrop(false)
  }

  return <IconButton
    onClick={() => {
      updateEmployee()
    }}
    style={{backgroundColor: "#01a6e6", width: "44px", height: "44px", margin: "12px"}}
  >
    <ReplayIcon style={{color: "#fff"}}/>
  </IconButton>
}

export default ReloadData
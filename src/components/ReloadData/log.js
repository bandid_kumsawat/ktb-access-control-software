import React from 'react'
import ReplayIcon from '@material-ui/icons/Replay';
import IconButton from '@material-ui/core/IconButton'

import { 
  LOG,
  SEARCHEMPLOYEE
} from "src/stores/actions"

// deplay apis
import getLog from 'src/apis/getLog'

function ReloadData (props) {
  const { onBackdrop } = props
  const [ store ] = React.useState(props.store)

  const handleSearchEmployee = async () => {
    onBackdrop(true)
    const Token = store.getState().AccessToken
    var query = store.getState().SearchEmployee
    query.search.offset = 0
    query.search.limit = 10
    store.dispatch(SEARCHEMPLOYEE(query))

    var str_device = ""
    if (query.search.device !== undefined){
      var AccessDoorArrFiltered = query.search.device.filter(function(x) {
        return x !== undefined && x !== "undefined";
      });
      str_device = JSON.stringify(AccessDoorArrFiltered.map((item) => {
        return Number(item)
      }))
    }
    if (query.flag){
      const res_log = await getLog({
        query: `
          {
            GetRecord (
                `+((query.search.name !== "") ? `name: "`+query.search.name+`",`: ``)+`
                `+((query.search.card !== "") ? `mifareId: "`+query.search.card+`",`: ``)+`
                `+((query.search.startdate !== "") ? `beginDate: "`+query.search.startdate+`",`: ``)+`
                `+((query.search.enddate !== "") ? `endDate: "`+query.search.enddate+`",`: ``)+`
                `+((query.search.starttime !== "") ? `beginTime: "`+query.search.starttime+`",`: ``)+`
                `+((query.search.endtime !== "") ? `endTime: "`+query.search.endtime+`",`: ``)+`
  
                `+((query.search.device !== "" && query.search.device !== undefined) ? `enterGate: `+str_device+`,`: ``)+`
                `+((query.search.device !== "" && query.search.device !== undefined) ? `exitGate: `+str_device+`,`: ``)+`
                `+((query.search.offset !== "") ? `offset: `+query.search.offset+`,`: ``)+`
                `+((query.search.limit !== "") ? `limit: `+query.search.limit+`,`: ``)+`
              ) {
                results {
                  record_id
                  enter_time
                  enter_method
                  exit_method
                  enter_gate {
                      gate_id
                      name
                      begin_time
                      end_time
                  }
                  exit_time
                  exit_gate {
                      gate_id
                      name
                      begin_time
                      end_time
                  }
                  is_exited
                  employee {
                      employee_id
                      name
                      lastname
                      position
                      mifare_id
                      picture_url
                      face_id
                  }
                }
                count
              }
          }
        `
      }, Token)
      store.dispatch(LOG(res_log.data.data.GetRecord))
    }else {
      const res_log = await getLog({
        query: `
          {
            GetRecord (
                `+((query.search.offset !== "") ? `offset: `+query.search.offset+`,`: ``)+`
                `+((query.search.limit !== "") ? `limit: `+query.search.limit+`,`: ``)+`
              ) {
                results {
                  record_id
                  enter_time
                  enter_method
                  exit_method
                  enter_gate {
                      gate_id
                      name
                      begin_time
                      end_time
                  }
                  exit_time
                  exit_gate {
                      gate_id
                      name
                      begin_time
                      end_time
                  }
                  is_exited
                  employee {
                      employee_id
                      name
                      lastname
                      position
                      mifare_id
                      picture_url
                      face_id
                  }
                }
                count
              }
          }
        `
      }, Token)
      store.dispatch(LOG(res_log.data.data.GetRecord))
    }
    onBackdrop(false)
  }


  return <IconButton
    onClick={() => {
      handleSearchEmployee()
    }}
    style={{backgroundColor: "#01a6e6", width: "44px", height: "44px",  margin: "12px"}}
  >
    <ReplayIcon style={{color: "#fff"}}/>
  </IconButton>
}

export default ReloadData
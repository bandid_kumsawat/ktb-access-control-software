import React from 'react'
import styled from 'styled-components';

import LOGO_KTB from 'src/asset/img/sidebar/LOGO_KTB.png'

// common lib
import parseToday from 'src/commons/parseToday'

import DEVICESTATUS from "src/asset/img/sidebar/STATUS_DEVICE.svg"
import DEVICESETING from "src/asset/img/sidebar/SETING_DEVICE.svg"
import USERSETTING from "src/asset/img/sidebar/USERSETTING.svg"
import ADMIN from "src/asset/img/sidebar/ADMIN.svg"
import LOGIMG from 'src/asset/img/log/log.svg'

import { makeStyles } from '@material-ui/core/styles';

import { useNavigate } from "react-router-dom";

import { useLocation } from 'react-router-dom'

import { 
  DEVICE,
  LOG,
  GROUP,
  LISTUSER,
  Event,
  SEARCHEMPLOYEE
} from "src/stores/actions"

// deplay apis
import getUser from 'src/apis/getUser'
import getDevice from 'src/apis/getDevice'
import getGroup from 'src/apis/getGroup';
import getEvent from 'src/apis/getEvent';
import getLog from 'src/apis/getLog'

const DashboardSidebarRoot = styled('div')`
  background-color: #01A6E6;
  height: 100vh;
  z-index: 1200;
  width: 15vw;
`;

const LogoKTB = styled('div')`
  margin-top: 10%;
  margin-right: auto;
  margin-bottom: 25%;
  margin-left: 10%;
`;

const LogoKTBImg = styled('img')`
  width: 67.5%;
  height: 5.5%;
`;

const Menu = styled('div')`
  // width: calc(100% - 8px + 8px);
  color: #fff;
  padding-left: 28px;
  padding-top: 15px;
  padding-bottom: 15px;
  &:hover {
    // background-color: #ffffff11;
  }
`;

const KTBAccessIcon = styled('img')`
  width: 13%;
`;

const MounsePostion = styled('div')`
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const useStyles = makeStyles((theme) => ({
  active: {
    width: "calc(100% - 8px - 20px - 8px)",
    borderInlineStart: "8px solid #FBAB2Dff",
    backgroundColor: "#004fa5ff",
  },
  inactive: {
    width: "calc(100% - 8px - 20px - 8px)",
    borderInlineStart: "8px solid #FBAB2D00",
    backgroundColor: "#004fa500",
  },
  space: {
    paddingLeft: 10,
    width: "180px"
  }
}));

var init_start_date = 1

function DashboardSidebar (props) {
  let history = useNavigate();
  const location = useLocation();
  const classes = useStyles()
  
  const [ store ] = React.useState(props.store)
  const [ UserInfo, setUserInfo ] = React.useState({})

  React.useEffect(() => {
    const loadUserInfo = () => {
      var userinfo = store.getState().User
      setUserInfo(Object.assign({}, userinfo))
    }
    
    var interval_ = setInterval(() => {
      loadUserInfo()
    }, 100)

    return () => {
      clearInterval(interval_)
    }

  }, [store])

  const getActiveStatus = () => {
    if (location.pathname === '/dashboard/status') {
      return classes.active
    } else {
      return classes.inactive
    }
  }
  const getActiveSetting = () => {
    if (location.pathname === '/dashboard/setting') {
      return classes.active
    } else {
      return classes.inactive
    }
  }
  const getActiveLog = () => {
    if (location.pathname === '/dashboard/log') {
      return classes.active
    } else {
      return classes.inactive
    }
  }
  const getActiveUser = () => {
    if (location.pathname === '/dashboard/user' || location.pathname.split("/").length === 5) {
      return classes.active
    } else {
      return classes.inactive
    }
  }
  const getActiveAdmin = () => {
    if (location.pathname === '/dashboard/admin') {
      return classes.active
    } else {
      return classes.inactive
    }
  }

  const reload_data = async () => {
    const Token = store.getState().AccessToken
    const res_event = await getEvent({
      query: `
        {
            Event {
                results {
                    event_id
                    type
                    time
                    topic
                    message
                }
                count
            }
        }
      `
    }, Token)
    store.dispatch(Event(res_event.data.data.Event))
    const res_device = await getDevice({
      query: `
      {
          GetDevice {
              results {
                  device_id
                  gate_id
                  device_status_id
                  device_config_id
                  serial_number
                  tablet_enter_serial_number
                  tablet_exit_serial_number
                  create_at
                  update_at
                  gate {
                      gate_id
                      name_gate
                      begin_time
                      end_time
                      create_at
                      update_at
                  }
                  device_config {
                      device_config_id
                      buzzer_enabled
                      door_sensor_enabled
                      break_glass_enabled
                      buzzer_delay
                      two_factor_enabled
                  }
                  device_status {
                      buzzer
                      iot_box_status
                      tablet_enter_status
                      tablet_exit_status
                      door_status
                      break_glass
                  }
              }
              count
          }
      }
      `
    }, Token)
    store.dispatch(DEVICE(res_device.data.data.GetDevice.results))

    const res_groups = await getGroup({
      query: `
        query {
          Group {
            group_id
            name
            gates {
                gate_id
                name
                begin_time
                end_time
            }
            employees {
                employee_id
                name
                lastname
                position
                mifare_id
                picture_url
                face_id
            }
            group_gates {
                gate_id
                group_id
                override_time
                begin_time
                end_time
            }
          }
      }
      `
    }, Token)
    store.dispatch(GROUP(res_groups.data.data.Group))

    // load user data
    const res_getUser = await getUser({
      query: `
        {
            GetUserAdmin (orderBy:"create_at" , orderType:"desc"){
              results {
                user_id
                role {
                    role_name
                }
                username
                name
                email
                employee_code
                create_at
                update_at
                last_active
                update_password_at
                is_locked
              }
            }
        }
      `
    }, Token)
    store.dispatch(LISTUSER(res_getUser.data.data.GetUserAdmin.results))
  }

  const handleSearchEmployee = async () => {
    const Token = store.getState().AccessToken
    var query = store.getState().SearchEmployee
    query.search.offset = 0
    query.search.limit = 10
    store.dispatch(SEARCHEMPLOYEE(query))

    var str_device = ""
    if (query.search.device !== undefined){
      var AccessDoorArrFiltered = query.search.device.filter(function(x) {
        return x !== undefined && x !== "undefined";
      });
      str_device = JSON.stringify(AccessDoorArrFiltered.map((item) => {
        return Number(item)
      }))
    }
    if (query.flag){
      const res_log = await getLog({
        query: `
          {
            GetRecord (
                `+((query.search.name !== "") ? `name: "`+query.search.name+`",`: ``)+`
                `+((query.search.surname !== "") ? `employee_code: "`+query.search.surname+`",`: ``)+`
                `+((query.search.card !== "") ? `mifareId: "`+query.search.card+`",`: ``)+`
                `+((query.search.startdate !== "") ? `beginDate: "`+query.search.startdate+`",`: ``)+`
                `+((query.search.enddate !== "") ? `endDate: "`+query.search.enddate+`",`: ``)+`
                `+((query.search.starttime !== "") ? `beginTime: "`+query.search.starttime+`",`: ``)+`
                `+((query.search.endtime !== "") ? `endTime: "`+query.search.endtime+`",`: ``)+`
  
                `+((query.search.device !== "" && query.search.device !== undefined) ? `enterGate: `+str_device+`,`: ``)+`
                `+((query.search.device !== "" && query.search.device !== undefined) ? `exitGate: `+str_device+`,`: ``)+`
                `+((query.search.offset !== "") ? `offset: `+query.search.offset+`,`: ``)+`
                `+((query.search.limit !== "") ? `limit: `+query.search.limit+`,`: ``)+`
              ) {
                results {
                  record_id
                  enter_time
                  enter_method
                  exit_method
                  enter_gate {
                      gate_id
                      name
                      begin_time
                      end_time
                  }
                  exit_time
                  exit_gate {
                      gate_id
                      name
                      begin_time
                      end_time
                  }
                  is_exited
                  employee {
                      employee_id
                      name
                      lastname
                      position
                      mifare_id
                      picture_url
                      face_id
                  }
                }
                count
              }
          }
        `
      }, Token)
      store.dispatch(LOG(res_log.data.data.GetRecord))
    }else {
      const res_log = await getLog({
        query: `
          {
            GetRecord (
                `+((query.search.offset !== "") ? `offset: `+query.search.offset+`,`: ``)+`
                `+((query.search.limit !== "") ? `limit: `+query.search.limit+`,`: ``)+`
              ) {
                results {
                  record_id
                  enter_time
                  enter_method
                  exit_method
                  enter_gate {
                      gate_id
                      name
                      begin_time
                      end_time
                  }
                  exit_time
                  exit_gate {
                      gate_id
                      name
                      begin_time
                      end_time
                  }
                  is_exited
                  employee {
                      employee_id
                      name
                      lastname
                      position
                      mifare_id
                      picture_url
                      face_id
                  }
                }
                count
              }
          }
        `
      }, Token)
      store.dispatch(LOG(res_log.data.data.GetRecord))
    }
  }

  return (
    <DashboardSidebarRoot>
      <LogoKTB>
        <LogoKTBImg alt="logo-ktb" src={LOGO_KTB} />
      </LogoKTB>

      <Menu className={getActiveStatus()}>
        <MounsePostion onClick={() => {
          reload_data()
          history("/dashboard/status")
        }}>
          <KTBAccessIcon src={DEVICESTATUS} alt="DEVICESTATUS" />
          <span className={classes.space}>สถานะอุปกรณ์</span>
        </MounsePostion>
      </Menu>


      <Menu className={getActiveSetting()}>
        <MounsePostion onClick={() => {
          reload_data()
          history("/dashboard/setting")
        }}>
          <KTBAccessIcon src={DEVICESETING} alt="DEVICESETING" />
          <span className={classes.space}>ตั้งค่าอุปกรณ์</span>
        </MounsePostion>
      </Menu>

      <Menu className={getActiveLog()}>
        <MounsePostion onClick={() => {
          reload_data()
          handleSearchEmployee()
          history("/dashboard/log")
        }}>
          <KTBAccessIcon src={LOGIMG} alt="LOGIMG" />
          <span className={classes.space}>ประวัติการเข้าออก</span>
        </MounsePostion>
      </Menu>


      <Menu className={getActiveUser()}>
        <MounsePostion onClick={() => {
          reload_data()
          history("/dashboard/user")
        }}>
          <KTBAccessIcon src={USERSETTING} alt="USERSETTING" />
          <span className={classes.space}>ตั้งค่าบัญชีผู้ใช้</span>
        </MounsePostion>
      </Menu>

      {
        ((UserInfo.perms === undefined) ? null: ((UserInfo.perms === "admin") ? <Menu className={getActiveAdmin()}>
          <MounsePostion onClick={() => {
            reload_data()
            history("/dashboard/admin")
          }}>
            <KTBAccessIcon src={ADMIN} alt="ADMIN" />
            <span className={classes.space}>ระบบแอดมิน</span>
          </MounsePostion>
        </Menu>: null))
      }

        {/* <Menu>
          <MounsePostion onClick={() => {
            window.location.href = "/"
          }}>
            <KTBAccessIcon />
            <span className={classes.space} style={{color: 'red'}}>ออกจากระบบ</span>
          </MounsePostion>
        </Menu> */}

    </DashboardSidebarRoot>
  )
}

export default DashboardSidebar
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: 300,
  },
  online: {
    color: "#11AB42",
  },
  offline: {
    color: "#CC1503"
  },
  paper: {
    marginRight: theme.spacing(2),
  },
}));


export default useStyles
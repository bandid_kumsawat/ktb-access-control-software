import React from 'react';
import styled from 'styled-components';

import REMOVESEARCH from "src/asset/img/log/REMOVESEARCH.svg"

import getLog from 'src/apis/getLog'

import DeleteForeverRoundedIcon from '@material-ui/icons/DeleteForeverRounded';

import Backdrop from '@material-ui/core/Backdrop' 
import CircularProgress from '@material-ui/core/CircularProgress'

import { makeStyles } from '@material-ui/core/styles';

import parseToday from 'src/commons/parseToday'

import { 
  SEARCHEMPLOYEE,
  LOG
} from "src/stores/actions"

import TableLog from "src/components/TablePaging/Log/TableLog"

const StyleBackdrop = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff"
  }
}));

const StutusContrainer = styled('div')`
  padding: 20px 20px 20px 30px;
`

const UserSearchLog = styled('div')`
  width: 100%;
  display: flex;
  align-content: center;
  align-items: center;
  padding-bottom: 20px;
`
const TagLabel = styled('div')`
  margin: 10px 10px 10px 0px;
  font-size: 18px;
  display: flex;
  align-items: center;
`
const TagSearch = styled('div')`
  font-size: 18px;
  display: flex;
  align-items: center;
  padding: 5px 20px;
  border-radius: 18px;
  align-content: center;
  border-radius: 20px;
  background-color : #fff;
  border: 2px solid #cccccc;
  margin-left: 8px;
`
const CancelTagSearch = styled('div')`
  font-size: 18px;
  display: flex;
  align-items: center;
  padding: 5px 20px;
  border-radius: 18px;
  align-content: center;
  border-radius: 20px;
  background-color : #fff;
  border: 2px solid red;
  color: red;
  margin-left: 8px;
`
const RemoveTagSearch = styled('div')`
  margin-left: 5px;
  display: flex;
  align-content: center;
  justify-content: center;
  align-items: center;
`

export default function Log(props) {
  const navbarclasses = StyleBackdrop()
  
  const [ store ] = React.useState(props.store)
  const [ Search, setSearch ] = React.useState({})
  const [ Token ] = React.useState(store.getState().AccessToken)
  const [ device, setdevice ] = React.useState(store.getState().Device)

  const [ OpenBackdrop, setOpenBackdrop ] = React.useState(false)

  React.useEffect(() => {
    const updateLog = () => {
      if (store.getState().Log !== undefined && store.getState().SearchEmployee !== undefined && store.getState().Device !== undefined){
        setSearch(Object.assign({}, store.getState().SearchEmployee))
        setdevice(store.getState().Device)
      }
    }
    updateLog()
    const interval_ = setInterval(() => {
      updateLog()
    }, 200);

    return () => {
      clearInterval(interval_)
    }
  }, [store])

  const formatDate = (date) => {
    let listDate = date.split('-')
    return `${listDate[1]}/${listDate[2]}/${listDate[0]}`
  }

  const SeactchByTag = () => {
    if (Search !== undefined){
      if (Search.flag){
        return (
          <UserSearchLog>
            <TagLabel> ผลลัพธ์การค้นหา :</TagLabel>

            { ((Search.search.name !== "") ? <TagSearch>ชื่อ: {Search.search.name}  </TagSearch>: null) }
            { ((Search.search.surname !== "") ? <TagSearch>รหัสพนักงาน: {Search.search.surname}  </TagSearch>: null) }
            { ((Search.search.card !== "") ? <TagSearch>Card ID: {Search.search.card}  </TagSearch>: null) }
            { ((Search.search.starttime !== "") ? <TagSearch>เริ่มต้น: {formatDate(Search.search.startdate) + " " + Search.search.starttime}  </TagSearch>: null) }
            { ((Search.search.endtime !== "") ? <TagSearch>สิ้นสุด: { formatDate(Search.search.enddate) + " " + Search.search.endtime}  </TagSearch>: null) }
            {
              (Search.search.device !== undefined ? ((Search.search.device[0] !== null && Search.search.device[0] !== undefined) ? <TagSearch>ประตู: {
                Search.search.device.map((item) => {
                  var name = ""
                  device.forEach(element => {
                    if (Number(element.gate_id) === Number(item)){
                      name = element.gate.name_gate
                    }
                  });
                  return name + "," 
                }) 
              }  </TagSearch>: null): null)
            }

            <CancelTagSearch>ยกเลิกการค้นหา<RemoveTagSearch onClick={async () => {
              setOpenBackdrop(true)
              var query = Search
              query.flag = false
              
              // query.search.startdate = parseToday(400)
              // query.search.enddate = parseToday(0)

              // query.search.starttime = "00:00"
              // query.search.endtime = "23:59"

              // startdate: parseToday(init_start_date),
              // enddate: parseToday(0),
              // starttime: "00:00",
              // endtime: "23:59",

              query.search.name = ""
              query.search.surname = ""
              query.search.card = ""
              query.search.device = undefined
              store.dispatch(SEARCHEMPLOYEE(query))
              // store.dispatch(LOG(undefined))

              // `+((query.search.startdate !== "") ? `beginDate: "`+query.search.startdate+`",`: ``)+`
              // `+((query.search.enddate !== "") ? `endDate: "`+query.search.enddate+`",`: ``)+`
              // `+((query.search.starttime !== "") ? `beginTime: "`+query.search.starttime+`",`: ``)+`
              // `+((query.search.endtime !== "") ? `endTime: "`+query.search.endtime+`",`: ``)+`

              const res_log = await getLog({
                query: `
                {
                    GetRecord (
        
                      `+((query.search.offset !== "") ? `offset: `+query.search.offset+`,`: ``)+`
                      `+((query.search.limit !== "") ? `limit: `+query.search.limit+`,`: ``)+`

                    ) {
                        results {
                          record_id
                          enter_time
                          enter_method
                          exit_method
                          enter_gate {
                              gate_id
                              name
                              begin_time
                              end_time
                          }
                          exit_time
                          exit_gate {
                              gate_id
                              name
                              begin_time
                              end_time
                          }
                          is_exited
                          employee {
                              name
                              employee_id
                              lastname
                              position
                              mifare_id
                              picture_url
                              face_id
                          }
                        }
                        count
                    }
                }
                `
              }, Token)
              store.dispatch(LOG(res_log.data.data.GetRecord))
              // store.dispatch(LOG(undefined))
              setOpenBackdrop(false)
              }} >
                {/* <img src={REMOVESEARCH} alt={REMOVESEARCH} />  */}
                <DeleteForeverRoundedIcon style={{color: "red", fontSize: "18px", cursor: "pointer"}}/>
              </RemoveTagSearch></CancelTagSearch>
          </UserSearchLog>
        )
      } else {
        return (
          <UserSearchLog>
            <TagLabel> ผลลัพธ์การค้นหา :</TagLabel>
            {/* <TagSearch>ข้อมูลทั้งหมด<RemoveTagSearch></RemoveTagSearch> </TagSearch> */}
            <TagSearch>ข้อมูลทั้งหมด</TagSearch>
          </UserSearchLog>
        )
      }
    } else {
      return null
    }
  }

  return (

    <StutusContrainer>
      {
        // (Search.flag ? SeactchByTag(): null)
        SeactchByTag()
      }
      <TableLog store={store} />

      <Backdrop className={navbarclasses.backdrop} open={OpenBackdrop}>
        <CircularProgress color="inherit" />
      </Backdrop>

    </StutusContrainer>

  );
}


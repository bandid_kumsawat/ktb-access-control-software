import TableCell from '@material-ui/core/TableCell';
import { withStyles } from '@material-ui/core/styles';

const CustomeTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: "#CDCDCD66",
    color: "#000000",
    fontSize: '18px',
  },
  body: {
    fontSize: 18,
  },
}))(TableCell);

export default CustomeTableCell
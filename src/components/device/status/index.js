import React from 'react';
import styled from 'styled-components';

import PropTypes from 'prop-types';
import clsx from 'clsx';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';

import DOOR from 'src/asset/img/door/Door.svg'
import { IconButton } from '@material-ui/core';

import MeetingRoomIcon from '@material-ui/icons/MeetingRoom';

import parseTimeForce from 'src/commons/parseTimeForce'

//Test TimePicker
// import TimePicker from 'src/components/TimePicker';
// setting style 
import TimeStyles from 'src/components/TimePicker/style'
// time format
import Time from 'src/components/TimePicker/time.json'
// icon
import UpTime from "src/asset/img/timepicker/up.svg";
import DownTime from "src/asset/img/timepicker/down.svg";

import ListItemText from '@material-ui/core/ListItemText';

// matial ui icon
// import RouterIcon from '@material-ui/icons/Router';

// dialog main component
import Dialog from '@material-ui/core/Dialog';

// dialog update Device component
import CustomeDialogContentManageDV from 'src/components/material/dialog/MangeDevice/CustomeDialogContent'
import CustomeDialogTitleManageDV from 'src/components/material/dialog/MangeDevice/CustomeDialogTitle'
import BodyManageDV from "src/components/material/dialog/MangeDevice/content/Body"
import ActionManageDV from "src/components/material/dialog/MangeDevice/content/Action"

import BodyForm from 'src/components/material/dialog/MangeDevice/content/Body/Form'
import BodyLabel from 'src/components/material/dialog/MangeDevice/content/Body/Label'
import BodySpace from 'src/components/material/dialog/MangeDevice/content/Body/Space'

import InputFromId from 'src/components/material/dialog/MangeDevice/content/Form/InputDelayTimeDoor'

import Container from 'src/components/TimePicker/Container'

// button component
import Choice1Btn from 'src/components/material/button/Choice1Btn'
import Choice2Btn from 'src/components/material/button/Choice2Btn'

// menu component
import CustomeMenu from 'src/components/material/menu/CustomeMenu'
import CustomeMenuItem from 'src/components/material/menu/CustomeMenuItem'

// setting style 
import useStyles from './style'
import "src/components/TablePaging/Device/TableDevice.css"

// import POINT from 'src/asset/img/status/POINT.svg'
// import { IconButton } from '@material-ui/core';

import { withStyles } from "@material-ui/styles"

import getForceOpen from 'src/apis/getforceOpen'
import setForceOpenApi from 'src/apis/setforceOpen'

// From Select
import FormControl from "@material-ui/core/FormControl"
import Select from "@material-ui/core/Select"
import InputBase from '@material-ui/core/InputBase'
import MenuItem from '@material-ui/core/MenuItem'
const BootstrapInput = withStyles((theme) => ({
  root: {
    'label + &': {
      // marginTop: theme.spacing(3),
    },
  },
  input: {
    textAlign: "left",
    borderRadius: 4,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #ccc',
    fontSize: 16,
    padding: '10px 47px 13px 12px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:focus': {
      borderRadius: 4,
      // borderColor: '#80bdff',
      // boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
      border: '1px solid #337AB7',
    },
  },
}))(InputBase);

const StutusContrainer = styled('div')`
  padding: 20px 20px 20px 30px;
`

var intervalTime_ = Object

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

function TablePagingTableHead(props) {
  const [ headCells ] = React.useState(props.headCells)
  const { classes, order, orderBy, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        <TableCell align={'left'} style={{width: 34, height: 34}} />
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={'left'}
            // padding={headCell.disablePadding ? 'none' : 'default'}
            // sortDirection={orderBy === headCell.id ? order : false}
            style={{fontSize: 18, whiteSpace: "nowrap"}}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
              style={{fontSize: 18}}
            >
              {headCell.label}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

TablePagingTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
  headCells: PropTypes.array.isRequired,
};

var t1 = "00"
var t2 = "00"

export default function Status(props) {

  const classes = useStyles();

  const [ store ] = React.useState(props.store)
  const [ Token ] = React.useState(store.getState().AccessToken)
  const [ anchorEl, setAnchorEl ] = React.useState(null);

  const [order, setOrder] = React.useState('desc');
  const [orderBy, setOrderBy] = React.useState('name');
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [ rows, setrows] = React.useState(undefined)

  const [ ForceOpenTime , setForceOpenTime] = React.useState([0, 0, 0])

  const headCells = [
    { id: 'name', numeric: false, disablePadding: false, label: 'ตำแหน่งประตู' },
    { id: 'Tablets1', numeric: true, disablePadding: false, label: 'Tablets Enter' },
    { id: 'Tablets2', numeric: true, disablePadding: false, label: 'Tablets Exit' },
    { id: 'iot_box_status', numeric: true, disablePadding: false, label: 'IoT Box' },
    { id: 'door_status', numeric: true, disablePadding: false, label: 'Door Sensor' },
    { id: 'break_glass', numeric: true, disablePadding: false, label: 'Break Glass' },
    { id: 'buzzer', numeric: true, disablePadding: false, label: 'Buzzer' },
  ];

  const [ openDialogMangeDV, setopenDialogMangeDV ] = React.useState(false)
  const [ ForceOpen, setForceOpen ] = React.useState(false)

  const [ isTimeDelay, setisTimeDelay ] = React.useState(false)

  const classestime = TimeStyles()
  const [ timeDelay , setTimeDelay ] = React.useState("00:00")
  const [ Unit, setUnit ] = React.useState("min")
  const [ Delay, setDelay ] = React.useState(60)
  const [ Delay_, setDelay_ ] = React.useState(60)
  const [ openMenuTime, setOpenMenuTime ] = React.useState(false)
  const [ anchorElTime, setanchorElTime ] = React.useState(null)
  const [ Hours, setHours ] = React.useState("00") // true
  const [ Minutes, setMinutes ] = React.useState("00") // false

  const [ deviceID, setdeviceID ] = React.useState() // false

  const [ forceConfig, setforceConfig ] = React.useState(undefined)

  const [ ShowLabel, setShowLabel ] = React.useState(true)

  const ScorllHours = React.createRef();
  const ScorllMinutes = React.createRef();

  React.useEffect(() => {
    const setDevice = () => {
      var devices = store.getState().Device
      var newdevices = []
      if (devices !== undefined) {
        devices.forEach((row) => {
          if (row.gate_id !== undefined) {
            newdevices.push( {
              id: row.gate_id,
              device: row.device_id,
              name: row.gate.name_gate,
              Tablets1: row.device_status.tablet_enter_status,
              Tablets2: row.device_status.tablet_exit_status,
              iot_box_status: row.device_status.iot_box_status,
              door_status: (row.device_config.door_sensor_enabled ? row.device_status.door_status: "disabled"),
              break_glass: (row.device_config.break_glass_enabled ? row.device_status.break_glass: "disabled"),
              buzzer: (row.device_config.buzzer_enabled ? row.device_status.buzzer: "disabled"),
            } )
          }
        })
        setrows(Object.assign([], newdevices))
      }
    }
    var interval_ = setInterval(() => {
      setDevice()
    }, 200);
    return () => {
      clearInterval(interval_)
    }
  }, [store])

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleCloseDialogMangeDV = () => {
    setopenDialogMangeDV(false);
  };

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = rows.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleCloseopenMenuTime = () => {
    setanchorElTime(null);
    setOpenMenuTime(false)
  }

  const handleGetForceDevice = async (id) => {
    setdeviceID(id)
    setUnit("min")
    var res = await getForceOpen(Token, id)
    setForceOpen(res.data.enabled)
    setDelay(res.data.delay / 60)
    // setDelay_(res.data.delay / 60)
    setisTimeDelay(res.data.delay_enabled)
    setforceConfig(Object.assign({}, res.data))
    if (res.data.delay > 3600){
      setUnit("hr")
      setDelay_(res.data.delay / 60 / 60)
    } else {
      setDelay_(res.data.delay / 60)
      setUnit("min")
    }

    // call api
    intervalTime_ = setInterval(() => {
        if (res.data.enabled){
          
          setForceOpenTime(Object.assign([], parseTimeForce(res.data.until)))
          var CheckTiemleft = parseTimeForce(res.data.until)
          if (CheckTiemleft[1] === 0 && CheckTiemleft[2] === 0 && CheckTiemleft[3] === 0){
            setisTimeDelay(false)

            if (res.data.delay_enabled){
              setForceOpen(false)
            }else {
              setForceOpen(true)
            }

            setForceOpenTime(Object.assign([], [0, 0, 0, 0]))
            clearInterval(intervalTime_)
          }

        } else {
          
          setForceOpenTime(Object.assign([], [0, 0, 0, 0]))

          setisTimeDelay(false)
          setForceOpen(false)

          clearInterval(intervalTime_)
        }
    }, 10);

    setopenDialogMangeDV(true)
  }

  const handleSetForceConfig = async () => {
    // console.log(isTimeDelay)
    // console.log(Unit)
    var query = {
      "enabled": ForceOpen,
      "delay_enabled": isTimeDelay,
      "delay": (Unit === "min" ? Delay_ * 60: Delay_ * 60 * 60)
    }
    const res = await setForceOpenApi(query, Token, deviceID)
    clearInterval(intervalTime_)
    setopenDialogMangeDV(false)
  }

  const notFoundData = (colSpan) => {
    return <TableRow >
      <TableCell colSpan={colSpan} style={{textAlign: "center"}}>ไม่พบข้อมูล</TableCell >
    </TableRow>
  }

  return (
    <StutusContrainer>
      
      {
        ((rows === undefined) ? "ไม่พบข้อมูล": 
          <Paper className={classes.paper}>
            <TableContainer className={classes.container} id="table-containner-custom-scroll-log">
              <Table
                className={classes.table}
                stickyHeader 
                aria-label="sticky table"
              >
                <TablePagingTableHead
                  classes={classes}
                  numSelected={selected.length}
                  order={order}
                  orderBy={orderBy}
                  onSelectAllClick={handleSelectAllClick}
                  onRequestSort={handleRequestSort}
                  rowCount={rows.length}
                  headCells={headCells}
                />
                <TableBody>
                  {
                    (rows.length === 0 ? notFoundData(8) : 
                    stableSort(rows, getComparator(order, orderBy))
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row, index) => {
    
                      return (
    
                        <TableRow
                          hover
                          onClick={() => console.log(row.id)}
                          tabIndex={-1}
                          key={row.id}
                        >
                          <TableCell style={{width: 34, height: 34}} >
                            <IconButton
                              style={{
                                width: "10",
                                height: "10"
                              }}
                              aria-controls="customized-menu"
                              aria-haspopup="true"
                              variant="contained"
                              color="primary"
                              onClick={(e) => {
                                handleGetForceDevice(row.device)
                              }}
                            >
                              {/* <img src={DOOR} alt={DOOR} /> */}
                              <MeetingRoomIcon color="primary" style={{fontSize: "20px"}}/>
                            </IconButton>
                          </TableCell>
                          <TableCell align="left" className={classes.sizeFont} style={{whiteSpace: "nowrap"}}>
                            {
                              row.name
                            }
                          </TableCell>
                          <TableCell align="left" className={clsx(classes.sizeFont, ( (row.Tablets1 === "online") ? classes.online: classes.offline ))}>{
                            (row.Tablets1 === "disabled" ? "-": ( (row.Tablets1 === "online") ? "Online": "Offline" ))
                          }</TableCell>
                          <TableCell align="left" className={clsx(classes.sizeFont, ( (row.Tablets2 === "online") ? classes.online: classes.offline ))}>{
                            ( (row.Tablets2 === "disabled") ? "-": ( (row.Tablets2 === "online") ? "Online": "Offline" ) )
                          }</TableCell>
                          <TableCell align="left" className={clsx(classes.sizeFont, ( (row.iot_box_status === "online") ? classes.online: classes.offline ))}>{
                            ((row.iot_box_status === "disabled") ? "-": ((row.iot_box_status === "online") ? "Online": "Offline" ) )
                          }</TableCell>
                          <TableCell align="left" className={clsx(classes.sizeFont, (row.door_status === "disabled" ? classes.disabled: ( (row.door_status === "open") ? classes.active: classes.inactive )) )}>{
                            ( (row.door_status === "disabled") ? "-": ( (row.door_status === "open") ? "Open": "Close" ) )
                          }</TableCell>
                          <TableCell align="left" className={clsx(classes.sizeFont, (row.break_glass === "disabled" ? classes.disabled : ( (row.break_glass) ? classes.glass_emergency: classes.glass_normal ) ) )}>
                          {( (row.break_glass === "disabled") ? "-": ( (row.break_glass) ? "Emergency": "Normal" )  ) }
                          </TableCell>
                          <TableCell align="left" className={clsx(classes.sizeFont, (row.buzzer === "disabled" ? classes.disabled : ( (row.buzzer === "normal") ? classes.buzzer_normal: classes.buzzer_alert ) ) )}>
                            {( (row.buzzer === "disabled") ? "-": ( (row.buzzer === "normal") ? "Normal": "Alert" ) ) }
                          </TableCell>
                        </TableRow>
                      );
                    }))
                  }
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              rowsPerPageOptions={[5, 10, 25, 50, 100, 1000]}
              component="div"
              count={rows.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
            />
          </Paper>
        ) 
      }
      <CustomeMenu
        id="customized-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <CustomeMenuItem>
          <ListItemText primary="อัพเดตซอฟต์แวร์" />
        </CustomeMenuItem>
      </CustomeMenu>

      {/* Dialog for mange device */}
      <Dialog
        disableBackdropClick={true}
        fullWidth={false}
        maxWidth={"xl"}
        open={openDialogMangeDV}
        onClose={handleCloseDialogMangeDV}
        aria-labelledby="update-dialog-MangeDV"
      >
        <CustomeDialogTitleManageDV 
        id="update-dialog-MangeDV"
        children={<div style={{
          fontSize: 28,
          textAlign: "center",
          width: 400
        }}></div>}
        ></CustomeDialogTitleManageDV>

        <CustomeDialogContentManageDV>
          <BodyManageDV style={{
            paddingTop: "30px",
          }}>
            <BodySpace style={{
              padding: "10px 50px 0px 10px"
            }}></BodySpace>
            <BodyLabel style={{
              padding: "10px 170px 0px 20px",
              textAlign: "left"
            }}>Force Open</BodyLabel>
            <BodyForm style={{
              padding: "10px 10px 0px 10px"
            }}>
              <Choice2Btn style={{
                width: 250, 
                height: 60,
                background: ForceOpen ? '' : '#999999',
                borderColor: ForceOpen ? '' : '#999999'
              }}
              onClick={() => {
                setForceOpen(!ForceOpen)
                setisTimeDelay(!ForceOpen)
                // setForceOpenTime(!ForceOpen)
              }}
              >
                {ForceOpen ? 'Active' : 'Inactive'}
              </Choice2Btn>
            </BodyForm>
            <BodySpace style={{
              padding: "10px 10px 0px 50px"
            }}></BodySpace>
          </BodyManageDV>
          
          {
            (ForceOpen ? <BodyManageDV style={{
              paddingTop: "10px",
            }}>
              <BodySpace style={{
                padding: "10px 50px 0px 10px"
              }}></BodySpace>
              <BodyForm style={{
                padding: "10px 10px 0px 300px"
              }}>
                <Checkbox color={"primary"} checked={isTimeDelay} onChange={(e) => {
                  setisTimeDelay(!isTimeDelay)
                }} />
                <BodyLabel style={{
                  textAlign: "left",
                  width: "180px"
                }}>Time Delay</BodyLabel>
              </BodyForm>
              <BodySpace style={{
                padding: "10px 10px 0px 10px"
              }}></BodySpace>
            </BodyManageDV>: null)
          }

          <BodyManageDV style={{
            paddingTop: "0px",
            display: isTimeDelay ? "flex" : "none"
          }}>
            <BodySpace style={{
              padding: "0px 10px 0px 10px"
            }}></BodySpace>
            <BodyForm style={{
              padding: "0px 60px 0px 355px",
              justifyContent: "space-between"
            }}>
              <InputFromId 
                // className={classes.InputIdControl}
                variant="outlined" 
                labelwidth={0}
                type={"number"}
                placeholder="Delay" 
                value={(Unit === "min" ? Delay_: Delay_)}
                fullWidth={true}
                autoComplete='off'
                style={{width: "120px", height: "50px",}}
                onChange={(e) => {
                  var isInt = function(n) { return parseInt(n) === n };
                  if (Unit === "min"){
                    if (Number(e.target.value) > (60 * 24) || Number(e.target.value) < 0 || !isInt(Number(e.target.value))){
                      alert("ใส่เวลาได้ไม่เกิน 60 นาทีและเวลาเป็นจำนวนเต็ม")
                    }else {
                      setDelay_(e.target.value)
                      setDelay((Unit === "min" ? e.target.value: e.target.value * 60))
                    }
                  }else {
                    if (Number(e.target.value) > 24 || Number(e.target.value) < 0 || !isInt(Number(e.target.value))){
                      alert("ใส่เวลาได้ไม่เกิน 24 ชั่งโมงและเวลาเป็นจำนวนเต็ม")
                    }else {
                      setDelay_(e.target.value)
                      setDelay((Unit === "min" ? e.target.value: e.target.value * 60))
                    }
                  }
                }}
              />

              {/* <FormControl variant="outlined" className={classes.formControl}> */}
                <Select
                  labelId="demo-simple-select-outlined-label"
                  id="demo-simple-select-outlined"
                  input={<BootstrapInput />}
                  style={{width: "120px", height: "50px"}}
                  value={Unit}
                  onChange={(e) => {
                    setUnit(e.target.value)
                    // var ddd = Delay_
                    // if (Unit === "hr"){
                    //   ddd = ddd * 60
                    // } else {
                    //   ddd = ddd / 60
                    // }
                    // setDelay_(ddd)
                    setDelay_(0)
                    setDelay((Unit === "min" ? 0: 0 * 60))

                  }}
                >
                  <MenuItem value={"min"}>{ "นาที" }</MenuItem>
                  <MenuItem value={"hr"}>{ "ชั่วโมง" }</MenuItem>
                </Select>
              {/* </FormControl> */}



            </BodyForm>

            <BodySpace style={{
              padding: "10px 10px 0px 10px"
            }}></BodySpace>
          </BodyManageDV>
          {
            (ShowLabel ? (ForceOpenTime !== undefined ? 
              ( isTimeDelay ?    
                (ForceOpenTime[1] <= 0 && ForceOpenTime[2] <= 0 && ForceOpenTime[3] <= 0 ? 

                <div style={{paddingtop: "20px", marginTop: "20px",color: "#acacac"}}> ระบบควบคุมประตูถูกปิดแล้ว </div> : 
                <div style={{paddingTop: "20px"}}>ระบบควบคุมประตูจะกลับมาทำงานปกติใน {(ForceOpenTime[1] < 0 ? 0 : ForceOpenTime[1])} ชม. {(ForceOpenTime[2] < 0 ? 0 : ForceOpenTime[2])} นาที {(ForceOpenTime[3] < 0 ? 0 : ForceOpenTime[3])} วินาที</div>)    :  /*<div style={{paddingtop: "20px", marginTop: "20px",color: "#acacac"}}> ระบบควบคุมประตูถูกปิดแล้ว </div>*/ null ): null) : null)
          }

          <ActionManageDV>
            <div style={{
              display: "flex",
              justifyContent: "space-between",
              width: "100%",
              padding: "30px calc(10% + 30px) 0px calc(10% + 30px)"
            }}>
              <Choice1Btn 
                style={{
                  width: 250, 
                  height: 60,
                }} 
                onClick={() => {
                  clearInterval(intervalTime_)
                  setopenDialogMangeDV(false)
                  // setisTimeDelay(false)
                  setTimeDelay("60")
                }}
              >ยกเลิก</Choice1Btn>
              
              <Choice2Btn style={{
                width: 250, 
                height: 60
              }}
              onClick={() => {
                handleSetForceConfig()
              }}
              >ยืนยัน</Choice2Btn>
            </div>
          </ActionManageDV>

          <CustomeMenu
          anchorEl={anchorElTime}
          keepMounted
          open={openMenuTime}
          onClose={handleCloseopenMenuTime}
          >

                                                                              <Container  >

                                                                                <div style={{
                                                                                  display: "flex",
                                                                                  alignContent: "center",
                                                                                  alignItems: "center",
                                                                                  justifyContent: "center",
                                                                                }}>

                                                                                  <div style={{
                                                                                    width: 40,
                                                                                    height: 30,
                                                                                    display: "flex",
                                                                                    alignContent: "center",
                                                                                    alignItems: "center",
                                                                                    justifyContent: "center",
                                                                                    marginBottom: "10px"
                                                                                  }}>
                                                                                    <IconButton onClick={(e) => {
                                                                                      var tNumber = t1
                                                                                      var elmnt = document.getElementById("scroll-hours-from-" + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));
                                                                                      elmnt.scrollIntoView();
                                                                                      if (Number(t1) > 0){
                                                                                        t1--
                                                                                        setHours(((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()))
                                                                                        setTimeDelay(
                                                                                          ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                                                                                          ":" + 
                                                                                          ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                                                                                        )
                                                                                      }
                                                                                    }}>
                                                                                      <img src={UpTime} alt="UpTime" />
                                                                                    </IconButton>
                                                                                  </div>
                                                                                  <div style={{
                                                                                    width: 40,
                                                                                  }}></div>
                                                                                  <div style={{
                                                                                    width: 40,
                                                                                    height: 30,
                                                                                    display: "flex",
                                                                                    alignContent: "center",
                                                                                    alignItems: "center",
                                                                                    justifyContent: "center",
                                                                                    marginBottom: "10px"
                                                                                  }}>
                                                                                    <IconButton onClick={() => {
                                                                                      console.log(t2)
                                                                                      var tNumber = t2
                                                                                      var elmnt = document.getElementById("scroll-minutese-from-" + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));
                                                                                      elmnt.scrollIntoView();
                                                                                      if (Number(t2) > 0){
                                                                                        t2--
                                                                                        setMinutes(((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString()))
                                                                                        setTimeDelay(
                                                                                          ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                                                                                          ":" + 
                                                                                          ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                                                                                        )

                                                                                      }
                                                                                    }}>
                                                                                      <img src={UpTime} alt="UpTime" />
                                                                                    </IconButton>
                                                                                  </div>
                                                                                </div>

                                                                                <div style={{
                                                                                  display: "flex"
                                                                                }}>
                                                                                  <div style={{
                                                                                    height: "200px",
                                                                                    width: "40px",
                                                                                    overflow: "scroll",
                                                                                  }} 
                                                                                  ref={ScorllHours}
                                                                                  className="remove-scroll"
                                                                                  >
                                                                                    {
                                                                                      Time.Hours.map((item, index) => {
                                                                                        return (
                                                                                          <div
                                                                                              id={"scroll-hours-from-" + Number(index)}
                                                                                              key={index}
                                                                                              style={{
                                                                                              width: "40px",
                                                                                              height: "40px",
                                                                                              display: "flex",
                                                                                              alignContent: "center",
                                                                                              justifyContent: "center",
                                                                                            }}
                                                                                            >
                                                                                              <IconButton 
                                                                                                onClick={() => {
                                                                                                  setHours(item)
                                                                                                  t1 = item
                                                                                                  setTimeDelay(
                                                                                            ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                                                                                            ":" + 
                                                                                            ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                                                                                          )
                                                                                                console.log("select time")
                                                                                              }}
                                                                                              className={((item === Hours) ? classestime.Select: classestime.Unselect)}
                                                                                            >{item}</IconButton>
                                                                                          </div>  
                                                                                        )
                                                                                      })
                                                                                    }
                                                                                  </div>
                                                                                  <div style={{
                                                                                    height: "200px",
                                                                                    width: "40px",
                                                                                    fontSize: "28px",
                                                                                    display: "flex",
                                                                                    alignContent: "center",
                                                                                    alignItems: "center",
                                                                                    justifyContent: "center",
                                                                                  }} >:</div>
                                                                                  <div style={{
                                                                                    height: "200px",
                                                                                    width: "40px",
                                                                                    overflow: "scroll",
                                                                                  }} 
                                                                                  ref={ScorllMinutes}
                                                                                  className="remove-scroll"
                                                                                  >
                                                                                    {
                                                                                      Time.Minutese.map((item, index) => {
                                                                                        return (
                                                                                          <div
                                                                                            key={index}
                                                                                            id={"scroll-minutese-from-" + Number(index)}
                                                                                            style={{
                                                                                            width: "40px",
                                                                                            height: "40px",
                                                                                            display: "flex",
                                                                                            alignContent: "center",
                                                                                            justifyContent: "center",
                                                                                          }}>
                                                                                            <IconButton 
                                                                                              className={((item === Minutes) ? classestime.Select: classestime.Unselect)} 
                                                                                              onClick={() => {
                                                                                                setMinutes(item)
                                                                                                t2 = item
                                                                                                setTimeDelay(
                                                                                          ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                                                                                          ":" + 
                                                                                          ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                                                                                        )
                                                                                              }}
                                                                                            >{item}</IconButton>
                                                                                          </div>  
                                                                                        )
                                                                                      })
                                                                                    }
                                                                                  </div>
                                                                                </div>

                                                                                <div style={{
                                                                                  display: "flex",
                                                                                  alignContent: "center",
                                                                                  alignItems: "center",
                                                                                  justifyContent: "center",
                                                                                }}>

                                                                                  <div style={{
                                                                                    width: 40,
                                                                                    height: 30,
                                                                                    display: "flex",
                                                                                    alignContent: "center",
                                                                                    alignItems: "center",
                                                                                    justifyContent: "center",
                                                                                    marginTop: "10px"
                                                                                  }}>
                                                                                    <IconButton onClick={() => {
                                                                                      var tNumber = t1
                                                                                      var elmnt = document.getElementById("scroll-hours-from-" + ((Number(tNumber) !== 23) ? (Number(tNumber) + 1): Number(tNumber)));
                                                                                      elmnt.scrollIntoView();
                                                                                      if (Number(t1) < 23){
                                                                                        t1++
                                                                                        setHours(((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()))
                                                                                        setTimeDelay(
                                                                                          ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                                                                                          ":" + 
                                                                                          ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                                                                                        )
                                                                                      }
                                                                                    }}>
                                                                                      <img src={DownTime} alt="DownTime" />
                                                                                    </IconButton>
                                                                                  </div>
                                                                                  <div style={{
                                                                                    width: 40,
                                                                                  }}></div>
                                                                                  <div style={{
                                                                                    width: 40,
                                                                                    height: 30,
                                                                                    display: "flex",
                                                                                    alignContent: "center",
                                                                                    alignItems: "center",
                                                                                    justifyContent: "center",
                                                                                    marginTop: "10px"
                                                                                  }}>
                                                                                    <IconButton onClick={() => {
                                                                                      var tNumber = t2
                                                                                      var elmnt = document.getElementById("scroll-minutese-from-" + ((Number(tNumber) !== 59) ? (Number(tNumber) + 1): Number(tNumber)));
                                                                                      elmnt.scrollIntoView();
                                                                                      if (Number(t2) < 59){
                                                                                        t2++
                                                                                        setMinutes(((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString()))
                                                                                        setTimeDelay(
                                                                                          ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                                                                                          ":" + 
                                                                                          ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                                                                                        )
                                                                                      }
                                                                                    }}>
                                                                                      <img src={DownTime} alt="DownTime" />
                                                                                    </IconButton>
                                                                                  </div>
                                                                                </div>

                                                                              </Container>

          </CustomeMenu>
        </CustomeDialogContentManageDV>

      </Dialog>

    </StutusContrainer>

  );
}
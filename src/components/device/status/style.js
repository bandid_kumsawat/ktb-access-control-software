import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  online: {
    color: "#529915",
  },
  offline: {
    color: "gray"
  },
  InputIdControl: {
    width: '100%',
    backgroundColor: "#F9FDFF",
    borderRadius: "8px",
    caretColor: "#337AB7",
    marginTop: "20px"
  },
  root: {
    width: '100%',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  container: {
    // "full height" - "height navbar" - "space footter" - "padding container top" - "padding container bottom" - "height result search"
    maxHeight: "calc(100vh - 8vh - 30px - 40px - 40px - 70px - 20px)",
  },
  sizeFont: {
    fontSize: 18
  },
  active: {
    color: "#529915"
  },
  inactive: {
    color: "gray"
  },
  inactive_status: {
    color: "#a2a2a2"
  },
  glass_normal: {
    color: "#529915"
  },
  glass_emergency: {
    color: "#ff4b2b"
  },
  buzzer_normal: {
    color: "#529915"
  },
  buzzer_alert: {
    color: "#ffbf00"
  },
  disabled: {
    color: "#000"
  }
}));


export default useStyles
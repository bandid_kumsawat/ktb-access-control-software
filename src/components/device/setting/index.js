import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Paper from '@material-ui/core/Paper';
import styled from 'styled-components';
import Checkbox from '@material-ui/core/Checkbox';

// Confirm dialog
import Success from 'src/components/ComfirmAction/Success'
import Unsuccess from 'src/components/ComfirmAction/Unsuccess'
import Confirm from 'src/components/ComfirmAction/Confirm'  
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";

import "src/components/TablePaging/Device/TableDevice.css"

// commons lib
import parseTimetoFulltime from 'src/commons/parseTimetoFulltime'

import ListItemText from '@material-ui/core/ListItemText';

import { withStyles, makeStyles } from "@material-ui/styles"

import EditIcon from '@material-ui/icons/Edit';

// matial ui icon
// import RouterIcon from '@material-ui/icons/Router';

// apis
import UpdateDvice from 'src/apis/updateDevice'
import GetDevice from 'src/apis/getDevice'
import RevoceDevice from "src/apis/removeDevice"

// store
import { 
  DEVICE,
} from "src/stores/actions"

import { 
  SEARCHDEVICE,
} from "src/stores/actions"

// svg image
import POINT from 'src/asset/img/status/POINT.svg'
import Edit from 'src/asset/img/door/Edit.svg'
import Exit from 'src/asset/img/setting/EXIT.svg'
import REMOVESEARCH from "src/asset/img/log/REMOVESEARCH.svg"

import { IconButton } from '@material-ui/core';

// dialog main component
import Dialog from '@material-ui/core/Dialog';

// dialog update firmware component
import CustomeDialogContentUpdateFWTextUpdateFW from "src/components/material/dialog/UpdateFirmware/CustomeDialogContent"
import CustomeDialogContentUpdateFW from 'src/components/material/dialog/UpdateFirmware/CustomeDialogContent'
import CustomeDialogTitleUpdateFW from 'src/components/material/dialog/UpdateFirmware/CustomeDialogTitle'
import CustomeExitDialogUpdateFW from 'src/components/material/dialog/UpdateFirmware/CustomeExitDialog'

// dialog update Device component
import CustomeDialogContentManageDV from 'src/components/material/dialog/MangeDevice/CustomeDialogContent'
import CustomeDialogTitleManageDV from 'src/components/material/dialog/MangeDevice/CustomeDialogTitle'
import CustomeExitDialogManageDV from 'src/components/material/dialog/MangeDevice/CustomeExitDialog'
import ActionManageDV from "src/components/material/dialog/MangeDevice/content/Action"
import BodyManageDV from "src/components/material/dialog/MangeDevice/content/Body"
// import HeaderManageDV from "src/components/material/dialog/MangeDevice/content/Header"
// import BodyManageDVAddDevice from 'src/components/material/dialog/MangeDevice/content/Header/Adddevice'
// import BodyManageDVAddEmployer from 'src/components/material/dialog/MangeDevice/content/Header/AddEmployer'
import BodyForm from 'src/components/material/dialog/MangeDevice/content/Body/Form'
import BodyLabel from 'src/components/material/dialog/MangeDevice/content/Body/Label'
import BodySpace from 'src/components/material/dialog/MangeDevice/content/Body/Space'
import InputFromId from 'src/components/material/dialog/MangeDevice/content/Form/InputId'
import Fromto from 'src/components/material/dialog/MangeDevice/content/Body/Fromto'

// button component
import Choice1Btn from 'src/components/material/button/Choice1Btn'
import Choice2Btn from 'src/components/material/button/Choice2Btn'

// menu component
import CustomeMenu from 'src/components/material/menu/CustomeMenu'
import CustomeMenuItem from 'src/components/material/menu/CustomeMenuItem'

// TimePicker
// sub Component
import Container from 'src/components/TimePicker/Container'
// Pseudo Style
import "src/components/TimePicker/style.css"
// setting style 
import TimeStyles from 'src/components/TimePicker/style'
// time format
import Time from 'src/components/TimePicker/time.json'
// icon
import UpTime from "src/asset/img/timepicker/up.svg";
import DownTime from "src/asset/img/timepicker/down.svg";

// setting style 
import useStyles from 'src/components/device/setting/style'

import SecretKey from 'src/components/GlobalDialog/SecretKey'

// select box
import FormControl from "@material-ui/core/FormControl"
import Select from "@material-ui/core/Select"
import InputBase from '@material-ui/core/InputBase'
import MenuItem from '@material-ui/core/MenuItem'
const BootstrapInput = withStyles((theme) => ({
  root: {
    'label + &': {
      marginTop: theme.spacing(3),
    },
  },
  input: {
    textAlign: "left",
    borderRadius: 4,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #ccc',
    fontSize: 16,
    padding: '10px 47px 13px 12px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:focus': {
      borderRadius: 4,
      // borderColor: '#80bdff',
      // boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
      border: '1px solid #337AB7',
    },
  },
}))(InputBase);


var t1 = "00"
var t2 = "00"

var t3 = "00"
var t4 = "00"

var start = ""
var stop = ""

const StutusContrainer = styled('div')`
  padding: 20px 20px 20px 30px;
`

const UserSearchLog = styled('div')`
  width: 100%;
  display: flex;
  align-content: center;
  align-items: center;
  padding-bottom: 20px;
`
const TagLabel = styled('div')`
  margin: 10px 10px 10px 0px;
  font-size: 18px;
  display: flex;
  align-items: center;
`
const TagSearch = styled('div')`
  font-size: 18px;
  display: flex;
  align-items: center;
  padding: 5px 20px;
  border-radius: 18px;
  align-content: center;
  border-radius: 20px;
  background-color : #fff;
  border: 2px solid #cccccc;
  margin-left: 8px;
`

const RemoveTagSearch = styled('div')`
  margin-left: 5px;
`

const CancelTagSearch = styled('div')`
  font-size: 18px;
  display: flex;
  align-items: center;
  padding: 5px 20px;
  border-radius: 18px;
  align-content: center;
  border-radius: 20px;
  background-color : #fff;
  border: 2px solid red;
  color: red;
  margin-left: 8px;
`
const FromAllTime = styled('div')`
  display: flex;
  justify-content: flex-start;
  align-content: center;
  padding-left: 20px;
`
const FromAllTimeLabel = styled('div')`
  display: flex;
  align-content: center;
  justify-content: flex-start;
  align-items: center;
  padding-right: 10px;
`
const ComfirmAction = styled("div")`
  display: flex;
  align-content: center;
  justify-content: space-around;
  margin: 20px;
`
const useStylesNavbar = makeStyles((theme) => ({
  Visible: {
    display: "block",
  },
  InVisible: {
    display: "none",
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff"
  }
}));

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

function TablePagingTableHead(props) {
  const [ headCells ] = React.useState(props.headCells)
  const { classes, order, orderBy, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        <TableCell align={'left'} style={{width: 34, height: 34}} />
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={'left'}
            // padding={headCell.disablePadding ? 'none' : 'default'}
            // sortDirection={orderBy === headCell.id ? order : false}
            style={{fontSize: 18, whiteSpace: "nowrap"}}
          >
            {/* {headCell.label} */}
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
              style={{fontSize: 18}}
            >
              {headCell.label}
            </TableSortLabel>
          </TableCell>
        ))}
        <TableCell align={'left'} style={{width: 34, height: 34}} />
        {/* <TableCell align={'left'} style={{width: 34, height: 34}} /> */}
      </TableRow>
    </TableHead>
  );
}

TablePagingTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
  headCells: PropTypes.array.isRequired,
};

export default function Status(props) {

  const classes = useStyles();
  const classestime = TimeStyles()
  const navbarclasses = useStylesNavbar()

  const ScorllHoursFrom = React.createRef();
  const ScorllMinuteseFrom = React.createRef();
  const ScorllHoursTo = React.createRef();
  const ScorllMinuteseTo = React.createRef();

  const [ store ] = React.useState(props.store)

  const [anchorElUpdateSoftware, setanchorElUpdateSoftware] = React.useState(null);
  const [anchorElTimeFrom, setanchorElTimeFrom] = React.useState(null);
  const [anchorElTimeTo, setanchorElTimeTo] = React.useState(null);

  const [ openDialogFW, setopenDialogFW ] = React.useState(false)
  const [ openDialogMangeDV, setopenDialogMangeDV ] = React.useState(false)

  const [ deviceID, setdeviceID ] = React.useState('')
  const [ deviceName, setdeviceName ] = React.useState('')

  const [ openMenu, setopenMenu ] = React.useState(false)
  const [ openMenuTimeFrom, setopenMenuTimeFrom ] = React.useState(false)
  const [ openMenuTimeTo, setopenMenuTimeTo ] = React.useState(false)

  const [ HoursFrom, setHoursFrom ] = React.useState("00") // true
  const [ HoursTo, setHoursTo ] = React.useState("00") // true
  const [ MinuteseFrom, setMinuteseFrom ] = React.useState("00") // false
  const [ MinuteseTo, setMinuteseTo ] = React.useState("00") // false

  const [ TimeFrom , setTimeFrom ] = React.useState("00:00")
  const [ TimeTo , setTimeTo ] = React.useState("00:00")

  const [ EditSerialTabletsEnter, setEditSerialTabletsEnter ] = React.useState("")
  const [ EditSerialTabletsExit, setEditSerialTabletsExit ] = React.useState("")
  const [ EditSerialIoTBox, setEditSerialIoTBox ] = React.useState("")
  const [ EditDoorName, setEditDoorName ] = React.useState("")
  const [ EditStartTime, setEditStartTime ] = React.useState("")
  const [ EditStopTime, setEditStopTime ] = React.useState("")
  const [ EditDelayBuzzer, setEditDelayBuzzer ] = React.useState(60)
  const [ EditAllTime, setEditAllTime ] = React.useState(false)
  const [ EditFactor, setEditFactor ] = React.useState(false)
  const [ EditDoorSensor, setEditDoorSensor ] = React.useState(true)
  const [ EditBreakGlass, setEditBreakGlass ] = React.useState(true)
  const [ EditBuzzer, setEditBuzzer ] = React.useState(true)

  // Validation Edit Device
  const [ EditSerialNumberIoTBoxValidation, setEditSerialNumberIoTBoxValidation ] = React.useState(false)
  const [ EditTabletsEnterValidation, setEditTabletsEnterValidation ] = React.useState(false)
  const [ EditTabletsExitValidation, setEditTabletsExitValidation ] = React.useState(false)
  const [ EditDoorNameValidation, setEditDoorNameValidation ] = React.useState(false)
  const [ EditDelayValidation, setEditDelayValidation ] = React.useState(false)
  const [ EditTimetoValidation , setEditTimetoValidation ] = React.useState(false)
  const [ EditTimefromValidation, setEditTimefromValidation ] = React.useState(false)


  const [ rows, setrows] = React.useState(undefined)
  const [order, setOrder] = React.useState('desc');
  const [orderBy, setOrderBy] = React.useState('name');
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const [ openDialogComfirm, setopenDialogComfirm ] = React.useState(false)
  const [ TitleAction, setTitleAction ] = React.useState("")
  const [ OpenBackdrop, setOpenBackdrop ] = React.useState(false)
  const [ openDialogComfirmStatus, setopenDialogComfirmStatus ] = React.useState(true)
  // const [ isOpenPersonalProfile, setIsOpenPersonalProfile ] = React.useState(false)
  const [ MessageError, setMessageError ] = React.useState("")

  const [ openDialogComfirmDeleteDevice, setopenDialogComfirmDeleteDevice ] = React.useState(false)

  const headCells = [
    { id: 'name', numeric: false, disablePadding: false, label: 'ตำแหน่งประตู' },
    { id: 'door_sensor_enabled', numeric: true, disablePadding: false, label: 'Door Sensor' },
    { id: 'break_glass_enabled', numeric: true, disablePadding: false, label: 'Break Glass' },
    { id: 'buzzer_enabled', numeric: true, disablePadding: false, label: 'Buzzer' },
    { id: 'buzzer_delay', numeric: true, disablePadding: false, label: 'Buzzer Delay' },
    { id: 'two_factor_enabled', numeric: true, disablePadding: false, label: 'Factor' },
  ];

  // const [ TestTop, setTestTop ] = React.useState(-10000)
  // const [ Testleft, setTestleft ] = React.useState(-10000)

  const initTimePicker = (from_, to_) => {
    try{
      // 0000-01-01T00:00:53Z
      // from_ = "0000-01-01T00:00:53Z"
      // to_ = "0000-01-01T00:00:53Z"
      from_ = start
      to_ = stop
      console.log("initTimePicker")
      var fh = from_.split("T")[1].split(":")[0]
      var fm = from_.split("T")[1].split(":")[1]
      var th = to_.split("T")[1].split(":")[0]
      var tm = to_.split("T")[1].split(":")[1]

      var from = new Date();
      from.setHours(fh)
      from.setMinutes(fm)
      var to = new Date();
      to.setHours(th)
      to.setMinutes(tm)
      var tNumber = 0
      var elmnt = Object

      // For From
      tNumber = from.getHours()
      t1 = tNumber
      elmnt = document.getElementById("scroll-hours-from-" + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));
      elmnt.scrollIntoView();
      setHoursFrom((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString())

      tNumber = from.getMinutes()
      t2 = tNumber
      elmnt = document.getElementById("scroll-minutese-from-" + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));
      elmnt.scrollIntoView();
      setMinuteseFrom((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
  
      // For To
      tNumber = to.getHours()
      t3 = tNumber
      elmnt = document.getElementById("scroll-hours-to-" + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));
      elmnt.scrollIntoView();
      setHoursTo((Number(t3) < 10) ? ("0" + Number(t3).toString()): Number(t3).toString())
      tNumber = to.getMinutes()
      t4 = tNumber
      elmnt = document.getElementById("scroll-minutese-to-" + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));
      elmnt.scrollIntoView();
      setMinuteseTo((Number(t4) < 10) ? ("0" + Number(t4).toString()): Number(t4).toString())

      // intiStateTime()
    } catch (e) {
      console.log(e)
    }
  }

  const IniterTime = (id, from , to) => {
    start = from
    stop = to
    
    var fh = from.split("T")[1].split(":")[0]
    var fm = from.split("T")[1].split(":")[1]
    var th = to.split("T")[1].split(":")[0]
    var tm = to.split("T")[1].split(":")[1]

    // eslint-disable-next-line
    var from = new Date();
    // eslint-disable-next-line
    from.setHours(fh)
    // eslint-disable-next-line
    from.setMinutes(fm)
    // eslint-disable-next-line
    var to = new Date();
    // eslint-disable-next-line
    to.setHours(th)
    // eslint-disable-next-line
    to.setMinutes(tm)
    
    var h1 = from.getHours()
    var m1 = from.getMinutes()
    var h2 = to.getHours()
    var m2 = to.getMinutes()
    
    setTimeFrom(((Number(h1) < 10) ? ("0" + Number(h1).toString()): Number(h1).toString()) + ":" + ((Number(m1) < 10) ? ("0" + Number(m1).toString()): Number(m1).toString()) )
    setTimeTo(((Number(h2) < 10) ? ("0" + Number(h2).toString()): Number(h2).toString()) + ":" + ((Number(m2) < 10) ? ("0" + Number(m2).toString()): Number(m2).toString()) )

    setHoursFrom((Number(h1) < 10) ? ("0" + Number(h1).toString()): Number(h1).toString())
    setMinuteseFrom((Number(m1) < 10) ? ("0" + Number(m1).toString()): Number(m1).toString())
    setHoursTo((Number(h2) < 10) ? ("0" + Number(h2).toString()): Number(h2).toString())
    setMinuteseTo((Number(m2) < 10) ? ("0" + Number(m2).toString()): Number(m2).toString())

    t1 = h1
    t2 = m1
    t3 = h2
    t4 = m1

  }

  // const intiStateTime = () => {
    
  //   if (EditStartTime !== "" && EditStopTime !== ""){
      

  //     var fh = EditStartTime.split("T")[1].split(":")[0]
  //     var fm = EditStartTime.split("T")[1].split(":")[1]
  //     var th = EditStopTime.split("T")[1].split(":")[0]
  //     var tm = EditStopTime.split("T")[1].split(":")[1]

  //     var from = new Date();
  //     from.setHours(fh)
  //     from.setMinutes(fm)
  //     var to = new Date();
  //     to.setHours(th)
  //     to.setMinutes(tm)
      
  //     var h1 = from.getHours()
  //     var m1 = from.getMinutes()
  //     var h2 = to.getHours()
  //     var m2 = to.getMinutes()
      
  //     setTimeFrom(((Number(h1) < 10) ? ("0" + Number(h1).toString()): Number(h1).toString()) + ":" + ((Number(m1) < 10) ? ("0" + Number(m1).toString()): Number(m1).toString()) )
  //     setTimeTo(((Number(h2) < 10) ? ("0" + Number(h2).toString()): Number(h2).toString()) + ":" + ((Number(m2) < 10) ? ("0" + Number(m2).toString()): Number(m2).toString()) )
  
  //     setHoursFrom((Number(h1) < 10) ? ("0" + Number(h1).toString()): Number(h1).toString())
  //     console.log(((Number(h1) < 10) ? ("0" + Number(h1).toString()): Number(h1).toString()))
  //     setMinuteseFrom((Number(m1) < 10) ? ("0" + Number(m1).toString()): Number(m1).toString())
  //     console.log(((Number(m1) < 10) ? ("0" + Number(m1).toString()): Number(m1).toString()))
  //     setHoursTo((Number(h2) < 10) ? ("0" + Number(h2).toString()): Number(h2).toString())
  //     console.log(((Number(h2) < 10) ? ("0" + Number(h2).toString()): Number(h2).toString()))
  //     setMinuteseTo((Number(m2) < 10) ? ("0" + Number(m2).toString()): Number(m2).toString())
  //     console.log(((Number(m2) < 10) ? ("0" + Number(m2).toString()): Number(m2).toString()))
  
  //     t1 = h1
  //     t2 = m1
  //     t3 = h2
  //     t4 = m1

  //   } else {
  //     // eslint-disable-next-line
  //     var from = new Date("2021-01-11T18:50:00.644+00:00")
  //     // eslint-disable-next-line
  //     var to = new Date("2021-02-11T04:30:00.644+00:00")
      
  //     // eslint-disable-next-line
  //     var h1 = from.getHours()
  //     // eslint-disable-next-line
  //     var m1 = from.getMinutes()
  //     // eslint-disable-next-line
  //     var h2 = to.getHours()
  //     // eslint-disable-next-line
  //     var m2 = to.getMinutes()
      
  //     setTimeFrom(((Number(h1) < 10) ? ("0" + Number(h1).toString()): Number(h1).toString()) + ":" + ((Number(m1) < 10) ? ("0" + Number(m1).toString()): Number(m1).toString()) )
  //     setTimeTo(((Number(h2) < 10) ? ("0" + Number(h2).toString()): Number(h2).toString()) + ":" + ((Number(m2) < 10) ? ("0" + Number(m2).toString()): Number(m2).toString()) )
  
  //     setHoursFrom((Number(h1) < 10) ? ("0" + Number(h1).toString()): Number(h1).toString())
  //     console.log(((Number(h1) < 10) ? ("0" + Number(h1).toString()): Number(h1).toString()))
  //     setMinuteseFrom((Number(m1) < 10) ? ("0" + Number(m1).toString()): Number(m1).toString())
  //     console.log(((Number(m1) < 10) ? ("0" + Number(m1).toString()): Number(m1).toString()))
  //     setHoursTo((Number(h2) < 10) ? ("0" + Number(h2).toString()): Number(h2).toString())
  //     console.log(((Number(h2) < 10) ? ("0" + Number(h2).toString()): Number(h2).toString()))
  //     setMinuteseTo((Number(m2) < 10) ? ("0" + Number(m2).toString()): Number(m2).toString())
  //     console.log(((Number(m2) < 10) ? ("0" + Number(m2).toString()): Number(m2).toString()))
  
  //     t1 = h1
  //     t2 = m1
  //     t3 = h2
  //     t4 = m1
  //   }

  // }
  const setDevice = () => {
    var devices = store.getState().Device
    var newdevices = []
    if (devices !== undefined) {
      devices.forEach((row) => {
        if (row.device_id !== undefined) {
          newdevices.push( {
            id: row.device_id,
            name: row.gate.name_gate,
            Tablets1: row.device_status.tablet_enter_status,
            Tablets2: row.device_status.tablet_exit_status,
            iot_box_status: row.device_status.iot_box_status,
            door_status: row.device_status.door_status,
            break_glass: row.device_status.break_glass,
            buzzer: row.device_status.buzzer,
            gate: row.gate
          } )
        }
      })
      setrows(Object.assign([], newdevices))
    }
  }
  React.useEffect(() => {
    const runintiStateTime = () => {
      if (EditStartTime !== "" && EditStopTime !== ""){
      

        var fh = EditStartTime.split("T")[1].split(":")[0]
        var fm = EditStartTime.split("T")[1].split(":")[1]
        var th = EditStopTime.split("T")[1].split(":")[0]
        var tm = EditStopTime.split("T")[1].split(":")[1]
  
        var from = new Date();
        from.setHours(fh)
        from.setMinutes(fm)
        var to = new Date();
        to.setHours(th)
        to.setMinutes(tm)
        
        var h1 = from.getHours()
        var m1 = from.getMinutes()
        var h2 = to.getHours()
        var m2 = to.getMinutes()
        
        setTimeFrom(((Number(h1) < 10) ? ("0" + Number(h1).toString()): Number(h1).toString()) + ":" + ((Number(m1) < 10) ? ("0" + Number(m1).toString()): Number(m1).toString()) )
        setTimeTo(((Number(h2) < 10) ? ("0" + Number(h2).toString()): Number(h2).toString()) + ":" + ((Number(m2) < 10) ? ("0" + Number(m2).toString()): Number(m2).toString()) )
    
        setHoursFrom((Number(h1) < 10) ? ("0" + Number(h1).toString()): Number(h1).toString())
        setMinuteseFrom((Number(m1) < 10) ? ("0" + Number(m1).toString()): Number(m1).toString())
        setHoursTo((Number(h2) < 10) ? ("0" + Number(h2).toString()): Number(h2).toString())
        setMinuteseTo((Number(m2) < 10) ? ("0" + Number(m2).toString()): Number(m2).toString())
    
        t1 = h1
        t2 = m1
        t3 = h2
        t4 = m1
  
      } else {
        // eslint-disable-next-line
        var from = new Date("2021-01-11T18:50:00.644+00:00")
        // eslint-disable-next-line
        var to = new Date("2021-02-11T04:30:00.644+00:00")
        
        // eslint-disable-next-line
        var h1 = from.getHours()
        // eslint-disable-next-line
        var m1 = from.getMinutes()
        // eslint-disable-next-line
        var h2 = to.getHours()
        // eslint-disable-next-line
        var m2 = to.getMinutes()
        
        setTimeFrom(((Number(h1) < 10) ? ("0" + Number(h1).toString()): Number(h1).toString()) + ":" + ((Number(m1) < 10) ? ("0" + Number(m1).toString()): Number(m1).toString()) )
        setTimeTo(((Number(h2) < 10) ? ("0" + Number(h2).toString()): Number(h2).toString()) + ":" + ((Number(m2) < 10) ? ("0" + Number(m2).toString()): Number(m2).toString()) )
    
        setHoursFrom((Number(h1) < 10) ? ("0" + Number(h1).toString()): Number(h1).toString())
        setMinuteseFrom((Number(m1) < 10) ? ("0" + Number(m1).toString()): Number(m1).toString())
        setHoursTo((Number(h2) < 10) ? ("0" + Number(h2).toString()): Number(h2).toString())
        setMinuteseTo((Number(m2) < 10) ? ("0" + Number(m2).toString()): Number(m2).toString())
    
        t1 = h1
        t2 = m1
        t3 = h2
        t4 = m1
      }
    }
    const setDevice_ = () => {
      var devices = store.getState().Device
      var newdevices = []
      if (devices !== undefined) {
        devices.forEach((row) => {
          if (row.device_id !== undefined) {
            newdevices.push( {
              id: row.device_id,
              name: row.gate.name_gate,
              break_glass_enabled: row.device_config.break_glass_enabled,
              buzzer_delay: row.device_config.buzzer_delay,
              buzzer_enabled: row.device_config.buzzer_enabled,
              door_sensor_enabled: row.device_config.door_sensor_enabled,
              two_factor_enabled: row.device_config.two_factor_enabled,
              gate: row.gate
            } )
          }
        })
        setrows(Object.assign([], newdevices))
      }
    }
    runintiStateTime()
    setDevice_()
    const interval_ = setInterval(() => {
      setDevice_()
    }, 200);

    return () => {
      clearInterval(interval_)
    }
  }, [store, EditStartTime, EditStopTime])

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = rows.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleClick = (event) => {
    // console.log(event.currentTarget)
    // console.log((event.currentTarget).offsetTop)
    // console.log((event.currentTarget).offsetLeft)
    // setTestTop((event.currentTarget).offsetTop + 40)
    // setTestleft((event.currentTarget).offsetLeft)
    setanchorElUpdateSoftware(event.currentTarget);
    setopenMenu(true)
  };

  const handleClose = () => {
    setanchorElUpdateSoftware(null);
    setopenMenu(false)
  };

  const handleCloseopenMenuTimeFrom = () => {
    setanchorElTimeFrom(null);
    setopenMenuTimeFrom(false)
  }

  const handleCloseopenMenuTimeTo = () => {
    setanchorElTimeTo(null);
    setopenMenuTimeTo(false)
  }

  const handleCloseDialogFW = () => {
    setopenDialogFW(false);
  };
  const handleCloseDialogMangeDV = () => {
    setopenDialogMangeDV(false);
  };

  const initUpdateDevice = (device_id) => {
    const device = store.getState().Device
    if (device !== undefined){
      device.forEach((item) => {
        if (device_id === item.device_id){
          setEditSerialTabletsEnter( item.tablet_enter_serial_number )
          setEditSerialTabletsExit( item.tablet_exit_serial_number )
          setEditSerialIoTBox( item.serial_number )
          setEditDoorName( item.gate.name_gate )
          setEditStartTime( item.gate.begin_time )
          setEditStopTime( item.gate.end_time )
          setEditDelayBuzzer( item.device_config.buzzer_delay )
          var allb = item.gate.begin_time.split("T")[1] // 00:00:000Z
          var alle = item.gate.end_time.split("T")[1] // 23:59:000Z
          setEditAllTime( false )
          // setEditAllTime( (allb === "00:00:00Z" && alle === "23:59:00Z") )
          // ละก็ 1 factor or 2 factor
          // ถ้าเลือก 1 factor ส่งค่า false
          // 2 factor ส่งค่า true ละกันครั
          setEditFactor( item.device_config.two_factor_enabled )
          setEditDoorSensor( item.device_config.door_sensor_enabled )
          setEditBreakGlass( item.device_config.break_glass_enabled )
          setEditBuzzer( item.device_config.buzzer_enabled )
          
        }
      })      
      setopenDialogMangeDV(true)
    }
  }

  const handleUpdateDevice = async () => {
    const Token = store.getState().AccessToken
    
    var id = deviceID
    var tablets_enter = EditSerialTabletsEnter
    var tablets_exit = EditSerialTabletsExit
    var iot_box = EditSerialIoTBox
    var doorname = EditDoorName
    var from = TimeFrom
    var to = TimeTo
    var alltime = EditAllTime
    var door = EditDoorSensor
    var glass = EditBreakGlass
    var buzzer = EditBuzzer
    var delay = EditDelayBuzzer
    var factor = EditFactor

    var check = true

    if (new Date(`2020-02-02 ${from}`).getTime() >= new Date(`2020-02-02 ${to}`).getTime() ){
      check = false
      setEditTimetoValidation(true)
      setEditTimefromValidation(true)
    } else {
      setEditTimetoValidation(false)
      setEditTimefromValidation(false)
    }

    if (tablets_enter === ""){
      check = false
      setEditTabletsEnterValidation(true)
    } else {
      setEditTabletsEnterValidation(false)
    }

    if (tablets_exit === ""){
      check = false
      setEditTabletsExitValidation(true)
    } else {
      setEditTabletsExitValidation(false)
    }

    if (iot_box === ""){
      check = false
      setEditSerialNumberIoTBoxValidation(true)
    } else {
      setEditSerialNumberIoTBoxValidation(false)
    }

    if (doorname === ""){
      check = false
      setEditDoorNameValidation(true)
    } else {
      setEditDoorNameValidation(false)
    }
    if (delay === "0" || delay === "" || Number(delay) < 0){
      check = false
      setEditDelayValidation(true)
    }else {
      setEditDelayValidation(false)
    }

    if (
      check
    ) {
      const checkIfDuplicateExists = (w) => {
          return new Set(w).size !== w.length 
      }
      if (!checkIfDuplicateExists([tablets_enter, tablets_exit, iot_box])){
        setopenDialogMangeDV(false)
        setOpenBackdrop(true)
        var query =`
          mutation {
            EditDevice(
              deviceId: ${Number(id)},
              gateName: "${doorname}", 
              iotSerialNumber: "${iot_box}", 
              tabletEnterSerialNumber: "${tablets_enter}",
              tabletExitSerialNumber: "${tablets_exit}",
              doorSensor: ${door}, 
              breakGlass: ${glass}, 
              beginTime: "${ (alltime ? parseTimetoFulltime(new Date(), "00:00"): parseTimetoFulltime(new Date(), from))}",
              endTime: "${(alltime ? parseTimetoFulltime(new Date(), "23:59"): parseTimetoFulltime(new Date(), to))}",
              buzzerEnabled: ${buzzer},
              buzzerDelay: ${delay},
              twoFactor: ${factor}
            ) {
              result
            }
          }
        `
        console.log(query)
        const res = await UpdateDvice({
          query: query, 
        }, Token)
        if (res.data.data.EditDevice.result){
          const res_device = await GetDevice({
            query: `
            {
                GetDevice {
                  results {
                    device_id
                    gate_id
                    device_status_id
                    device_config_id
                    serial_number
                    tablet_enter_serial_number
                    tablet_exit_serial_number
                    create_at
                    update_at
                    gate {
                        gate_id
                        name_gate
                        begin_time
                        end_time
                        create_at
                        update_at
                    }
                    device_config {
                        device_config_id
                        buzzer_enabled
                        door_sensor_enabled
                        break_glass_enabled
                        buzzer_delay
                        two_factor_enabled
                    }
                    device_status {
                        buzzer
                        iot_box_status
                        tablet_enter_status
                        tablet_exit_status
                        door_status
                        break_glass
                    }
                  }
                }
            }
            `
          }, Token)
          store.dispatch(DEVICE(res_device.data.data.GetDevice.results))
          setOpenBackdrop(false)
          setTitleAction(`แก้ไขอุปกรณ์ ${doorname}`)
          setopenDialogComfirm(true)
          setopenDialogComfirmStatus(true)
          setDevice()
        } else {
          setOpenBackdrop(false)
          setTitleAction(`ไม่สามารถแก้ไขอุปกรณ์ ${doorname}`)
          setopenDialogComfirm(true)
          setopenDialogComfirmStatus(false)
          setMessageError(res.data.errors[0].message)
          setDevice()
        }
      } else {
        setEditTabletsEnterValidation(true)
        setEditTabletsExitValidation(true)
        setEditSerialNumberIoTBoxValidation(true)
        setOpenBackdrop(false)
        // setTitleAction(`ไม่สามารถแก้ไขอุปกรณ์ ${doorname}`)
        setTitleAction("Serial Number ไม่สามารถซ้ำกันได้")
        setopenDialogComfirm(true)
        setopenDialogComfirmStatus(false)
        setMessageError("")
        setDevice()
      }
    }
  }

  const handleRemoveDevice = async () => {

    setopenDialogMangeDV(false)
    setOpenBackdrop(true)
    
    const Token = store.getState().AccessToken
    
    const res = await RevoceDevice({
      query: `
        mutation{
          RemoveDevice(deviceId: ${Number(deviceID)}) {
              result
          }
      }
      `
    }, Token)
    if (res.data.errors === undefined && res.data.data.RemoveDevice.result){
      const res_device = await GetDevice({
        query: `
          {
            GetDevice {
              results {
                device_id
                gate_id
                device_status_id
                device_config_id
                serial_number
                tablet_enter_serial_number
                tablet_exit_serial_number
                create_at
                update_at
                gate {
                    gate_id
                    name_gate
                    begin_time
                    end_time
                    create_at
                    update_at
                }
                device_config {
                    device_config_id
                    buzzer_enabled
                    door_sensor_enabled
                    break_glass_enabled
                    buzzer_delay
                    two_factor_enabled
                }
                device_status {
                    buzzer
                    iot_box_status
                    tablet_enter_status
                    tablet_exit_status
                    door_status
                    break_glass
                }
              }
            }
          }
        `
      }, Token)
      store.dispatch(DEVICE(res_device.data.data.GetDevice.result))
      setOpenBackdrop(false)
      setTitleAction(`ลบอุปกรณ์`)
      setopenDialogComfirm(true)
      setopenDialogComfirmStatus(true)
      setDevice()
    } else {
      alert(res.data.errors[0].message)
    }
    handleCloseDialogMangeDV()

  }

  // const setTimeDate = () => {
  //   if (SelectHoMin){
  //     setTimeFrom(
  //       ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
  //       ":" + 
  //       ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
  //     )
  //     console.log("..setTimeFrom...")
  //     console.log(t1 + ":" + t2)
  //   } else {
  //     setTimeTo(
  //       ((Number(t3) < 10) ? ("0" + Number(t3).toString()): Number(t3).toString()) + 
  //       ":" + 
  //       ((Number(t4) < 10) ? ("0" + Number(t4).toString()): Number(t4).toString())
  //     )
  //     console.log("...setTimeTo...")
  //     console.log(t1 + ":" + t2)
  //   }
  // }
  const [ Search, setSearch ] = React.useState({})

  React.useEffect(() => {
    const LoadData = () => {
      setSearch(Object.assign({}, store.getState().SearchDevice))
    }
    var interval_ = setInterval(() => {
      LoadData()
    }, 200);
    return () => {
      clearInterval(interval_)
    }
  }, [store])

  const SearchByTag = () => {
    if (Search !== undefined){
      if (Search.flag){
        return (
          <UserSearchLog>
            <TagLabel> ผลลัพธ์การค้นหา :</TagLabel>

            { ((Search.search.device !== "") ? <TagSearch>Serial Number: {Search.search.device} <RemoveTagSearch>{/* <img src={REMOVESEARCH} alt={REMOVESEARCH} /> */} </RemoveTagSearch> </TagSearch>: null) }
            
            <CancelTagSearch>ยกเลิกการค้นหา <RemoveTagSearch style={{cursor: 'pointer'}} onClick={async () => {
              const Token = store.getState().AccessToken
              const res_device = await GetDevice({
                query: `
                {
                  GetDevice {
                    results {
                        device_id
                        gate_id
                        device_status_id
                        device_config_id
                        serial_number
                        tablet_enter_serial_number
                        tablet_exit_serial_number
                        create_at
                        update_at
                        gate {
                            gate_id
                            name_gate
                            begin_time
                            end_time
                            create_at
                            update_at
                        }
                        device_config {
                            device_config_id
                            buzzer_enabled
                            door_sensor_enabled
                            break_glass_enabled
                            buzzer_delay
                            two_factor_enabled
                        }
                        device_status {
                            buzzer
                            iot_box_status
                            tablet_enter_status
                            tablet_exit_status
                            door_status
                            break_glass
                        }
                    }
                    count
                  }
                }
                `
              }, Token)
              store.dispatch(DEVICE(res_device.data.data.GetDevice.results))

              var searche = Search
              searche.flag = false
              searche.search.device = ""
              store.dispatch(SEARCHDEVICE(searche))
            }} ><img src={REMOVESEARCH} alt={REMOVESEARCH} /></RemoveTagSearch></CancelTagSearch>
          </UserSearchLog>
        )
      } else {
        return (
          <UserSearchLog>
            <TagLabel> ผลลัพธ์การค้นหา :</TagLabel>
            <TagSearch>ข้อมูลทั้งหมด<RemoveTagSearch></RemoveTagSearch> </TagSearch>
          </UserSearchLog>
        )
      }
    } else {
      return null
    }
  }

  const notFoundData = (colSpan) => {
    return <TableRow >
      <TableCell colSpan={colSpan} style={{textAlign: "center"}}>ไม่พบข้อมูล</TableCell >
    </TableRow>
  }

  return (

    <StutusContrainer>
      { SearchByTag() }
      {
        ((rows === undefined) ? "ไม่พบข้อมูล": 
          <Paper className={classes.paper}>
            <TableContainer className={classes.container} id="table-containner-custom-scroll-device">
              <Table
                className={classes.table}
                stickyHeader 
                aria-label="sticky table"
              >
                <TablePagingTableHead
                  classes={classes}
                  numSelected={selected.length}
                  order={order}
                  orderBy={orderBy}
                  onSelectAllClick={handleSelectAllClick}
                  onRequestSort={handleRequestSort}
                  rowCount={rows.length}
                  headCells={headCells}
                />
                <TableBody>
                  {
                    (rows.length === 0 ? notFoundData(8) : stableSort(rows, getComparator(order, orderBy))
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row, index) => {
    
                      return (
    
                        <TableRow
                          hover
                          onClick={() => console.log(row.id)}
                          tabIndex={-1}
                          key={row.id}
                        >
                          <TableCell aling="left" >
                            <IconButton style={{
                              width: "10",
                              height: "10",
                              display: (store.getState().User.perms === "admin") ? "flex" : "none"
                            }}
                            aria-controls="customized-menu"
                            aria-haspopup="true"
                            variant="contained"
                            color="primary"
                            onClick={(e) => {
                              handleClick(e)
                              setdeviceID(row.id)
                              setdeviceName(row.name)
                            }}
                            >
                              <img src={POINT} alt="POINT" />
                            </IconButton>
                          </TableCell>
                          <TableCell align="left" className={classes.sizeFont} style={{whiteSpace: "nowrap"}}>
                            {
                              row.name
                            }
                          </TableCell>

                          <TableCell align="left" className={clsx(classes.sizeFont, ( (row.door_sensor_enabled === true) ? classes.active: classes.inactive_status ))}>{
                            ((row.door_sensor_enabled === true) ? "Enable": "Disabled" )
                          }</TableCell>

                          <TableCell align="left" className={clsx(classes.sizeFont, ( (row.break_glass_enabled === true) ? classes.active: classes.inactive_status ))}>{
                            ( (row.break_glass_enabled === true) ? "Enable": "Disabled" )
                          }</TableCell>

                          <TableCell align="left" className={clsx(classes.sizeFont, ( (row.buzzer_enabled === true) ? classes.active: classes.inactive_status ))}>{
                            ( (row.buzzer_enabled === true) ? "Enable": "Disabled" )
                          }</TableCell>

                          <TableCell align="left" className={clsx(classes.sizeFont)}>{
                            row.buzzer_delay + " วินาที"
                          }</TableCell>

                            <TableCell align="left" className={clsx(classes.sizeFont)}>{
                            ( (row.two_factor_enabled === !true) ? "1 Factor": "2 Factors" )
                          }</TableCell>

                          <TableCell align="left">
                            <IconButton style={{
                              width: "10",
                              height: "10",
                            }}
                            aria-controls="customized-menu" 
                            aria-haspopup="true"
                            variant="contained"
                            color="secondary"
                            onClick={(e) => {
                              setdeviceID(row.id)
                              setdeviceName(row.name)
                              initUpdateDevice(row.id)
                              IniterTime(row.id, row.gate.begin_time, row.gate.end_time)
                              // setopenDialogMangeDV(true)
                            }}
                            >
                              {/* <img src={Edit} alt="Edit" /> */}
                              <EditIcon color="primary" style={{fontSize: "18px"}}/>
                            </IconButton>
                          </TableCell>

                          {/* <TableCell>
                            <SecretKey store={store} />
                          </TableCell> */}
                        </TableRow>
                      );
                    }))
                  }
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              rowsPerPageOptions={[5, 10, 25, 50, 100, 1000]}
              component="div"
              count={rows.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
            />
          </Paper>
        ) 
      }
      
      {/* <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="customized table">
          <TableHead>
            <TableRow>
              <CustomeTableCell align="left" style={{ width: 10 }} ></CustomeTableCell>
              <CustomeTableCell align="left">ตำแหน่งประตู</CustomeTableCell>
              <CustomeTableCell align="center">สถานะ Tablets1</CustomeTableCell>
              <CustomeTableCell align="center">สถานะ Tablets2</CustomeTableCell>
              <CustomeTableCell align="center">สถานะ IoT</CustomeTableCell>
              <CustomeTableCell align="center">Door sensor</CustomeTableCell>
              <CustomeTableCell align="center">Break glass</CustomeTableCell>
              <CustomeTableCell align="left"></CustomeTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {
              ((device !== null) ? device.map((row) => (
                <CustomeTableRow key={row.device_id} onClick={(e) => {
                }}>
                  <CustomeTableCell align="center">
                    <IconButton style={{
                      width: "10",
                      height: "10"
                    }}
                    aria-controls="customized-menu"
                    aria-haspopup="true"
                    variant="contained"
                    color="primary"
                    onClick={(e) => {
                      handleClick(e)
                      setdeviceID(row.device_id)
                    }}
                    >
                      <img src={POINT} alt="POINT" />
                    </IconButton>
                  </CustomeTableCell>
                  <CustomeTableCell align="left">
                    
                    {row.gate.name_gate}
                    
                  </CustomeTableCell>
                  <CustomeTableCell align="center" className={((row.device_status.tablet_status === "online") ? classes.online: classes.offline)}>
                    
                    {((row.device_status.tablet_status === "online") ? "Online": "Offline")}
                    
                  </CustomeTableCell>
                  <CustomeTableCell align="center" className={((row.device_status.tablet_status === "online") ? classes.online: classes.offline)}>
                    
                    {((row.device_status.tablet_status === "online") ? "Online": "Offline")}
                    
                  </CustomeTableCell>
                  <CustomeTableCell align="center" className={((row.device_status.iot_box_status === "online") ? classes.online: classes.offline)}>
                    
                    {((row.device_status.iot_box_status === "online") ? "Online": "Offline")}
                    
                  </CustomeTableCell>
                  <CustomeTableCell align="center" className={((row.device_status.door_status === "close") ? classes.online: classes.offline)}>
                    
                    {((row.device_status.door_status === "close") ? "Open": "Close")}
                    
                  </CustomeTableCell>
                  <CustomeTableCell align="center" className={((row.device_status.break_glass) ? classes.online: classes.offline)}>
                    
                    {((row.device_status.break_glass) ? "Online": "Offline")}
                    
                  </CustomeTableCell>
  
                  <CustomeTableCell>
                    <IconButton style={{
                      width: "10",
                      height: "10",
                    }}
                    aria-controls="customized-menu"
                    aria-haspopup="true"
                    variant="contained"
                    color="secondary"
                    onClick={(e) => {
                      setdeviceID(row.device_id)
                      initUpdateDevice(row.device_id)
                      IniterTime(row.device_id, row.gate.begin_time, row.gate.end_time)
                      setopenDialogMangeDV(true)
                    }}
                    >
                      <img src={Edit} alt="Edit" />
                    </IconButton>
                  </CustomeTableCell>
  
  
                </CustomeTableRow>
              )): null)
            }
          </TableBody>
        </Table>
      </TableContainer> */}

      {/* Dialog for Update Software */}
      <Dialog
        disableBackdropClick={true}
        fullWidth={false}
        maxWidth={"xl"}
        open={openDialogFW}
        onClose={handleCloseDialogFW}
        aria-labelledby="update-dialog-fw-fw"
      >
        <CustomeExitDialogUpdateFW>
          <IconButton onClick={() => {
            setopenDialogFW(false)
          }}>
            <img src={Exit} alt="Exit" />
          </IconButton>
        </CustomeExitDialogUpdateFW>
        <CustomeDialogTitleUpdateFW id="update-dialog-fw-fw" style={{textAlign: "center"}}>อัพเดตซอฟต์แวร์?</CustomeDialogTitleUpdateFW>
        <CustomeDialogContentUpdateFW>
          <CustomeDialogContentUpdateFWTextUpdateFW>
            คุณแน่ใจว่าจะอัพเดตซอฟต์แวร์ของ {deviceName} ใช่หรือไม่?
          </CustomeDialogContentUpdateFWTextUpdateFW>
          <div style={{
            display: "flex",
            justifyContent: "space-between"
          }}>
            <Choice1Btn 
              onClick={() => {
                setopenDialogFW(false)
              }}
              style={{
                width: 250, 
                height: 60,
                marginRight: 20
              }}
            >ไว้คราวหลัง</Choice1Btn>
            <Choice2Btn style={{
              width: 250, 
              height: 60,
              marginLeft: 20
            }}
            onClick={() => {
              setopenDialogFW(false)
            }}
            >อัพเดตตอนนี้</Choice2Btn>
          </div>
        </CustomeDialogContentUpdateFW>
      </Dialog>





      {/* Dialog for ManageDevice */}
      <Dialog
        disableBackdropClick={true}
        fullWidth={false}
        maxWidth={"xl"}
        open={openDialogMangeDV}
        onClose={handleCloseDialogMangeDV}
        aria-labelledby="update-dialog-MangeDV"
      >
        <CustomeExitDialogManageDV>
          <IconButton onClick={() => {
            setopenDialogMangeDV(false)
          }}>
            <img src={Exit} alt="Exit" />
          </IconButton>
        </CustomeExitDialogManageDV>
        <CustomeDialogTitleManageDV 
        id="update-dialog-MangeDV"
        children={<div style={{
          fontSize: 28,
          textAlign: "center",
          width: 400
        }}>แก้ไขข้อมูล</div>}
        ></CustomeDialogTitleManageDV>
        <CustomeDialogContentManageDV>
        
        {/* Serial No.Tablets Enter */}
        <BodyManageDV style={{
          paddingTop: "30px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>Serial No.Tablets Enter</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>
            
            <InputFromId 
              className={classes.InputIdControl}
              variant="outlined" 
              error={EditTabletsEnterValidation}
              labelwidth={0}
              placeholder="Serial Number Tablets Enter" 
              fullWidth={true}
              autoComplete='off'
              value={EditSerialTabletsEnter}
              onChange={(e) => {
                setEditSerialTabletsEnter(e.target.value)
              }}
            />

          </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>

        {/* Serial No.Tablets Exit */}
        <BodyManageDV style={{
          paddingTop: "0px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>Serial No.Tablets Exit</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>
            
            <InputFromId 
              className={classes.InputIdControl}
              variant="outlined" 
              error={EditTabletsExitValidation}
              labelwidth={0}
              placeholder="Serial Number Tablets Exit" 
              fullWidth={true}
              autoComplete='off'
              value={EditSerialTabletsExit}
              onChange={(e) => {
                setEditSerialTabletsExit(e.target.value)
              }}
            />

          </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>

        {/* Serial No.IoT Box */}
        <BodyManageDV style={{
          paddingTop: "0px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>Serial No.IoT Box</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>
            
            <InputFromId 
              className={classes.InputIdControl}
              variant="outlined" 
              error={EditSerialNumberIoTBoxValidation}
              labelwidth={0}
              placeholder="Serial Number IoT Box" 
              fullWidth={true}
              autoComplete='off'
              value={EditSerialIoTBox}
              onChange={(e) => {
                setEditSerialIoTBox(e.target.value)
              }}
            />

          </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>

        {/* Door name */}
        <BodyManageDV style={{
          paddingTop: "5px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>ตำแหน่งประตู</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>

            <InputFromId 
              className={classes.InputIdControl}
              variant="outlined" 
              error={EditDoorNameValidation}
              labelwidth={0}
              placeholder="กรอกตำแหน่งประตู" 
              fullWidth={true}
              autoComplete='off'
              value={EditDoorName}
              onChange={(e) => {
                setEditDoorName(e.target.value)
              }}
            />

          </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>



        <BodyManageDV style={{
          paddingTop: "5px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>ช่วงเวลาเปิดปิดประตู</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>
            <Fromto>จาก</Fromto>
            <InputFromId 
              className={classes.InputIdControl}
              variant="outlined" 
              labelwidth={0}
              placeholder="00:00" 
              value={TimeFrom}
              fullWidth={true}
              autoComplete='off'
              disabled={EditAllTime}
              error={EditTimefromValidation}
              onClick={(e) => {
                if (!EditAllTime){
                  setanchorElTimeFrom(e.currentTarget);
                  setopenMenuTimeFrom(true)
                  // intiStateTime()
                  initTimePicker(EditStartTime, EditStopTime)
                  // console.log(e.clientX)
                  // console.log(e.clientY)
                  // console.log((e.nativeEvent.layerX))
                  // // console.log((e.currentTarget))
                  // setTestTop(e.clientY - e.nativeEvent.layerY + 47.38)
                  // setTestleft(e.clientX - e.nativeEvent.layerX)
                }
              }}
            />
            <Fromto>ถึง</Fromto>
            <InputFromId 
              className={classes.InputIdControl}
              variant="outlined" 
              labelwidth={0}
              placeholder="00:00" 
              value={TimeTo}
              fullWidth={true}
              autoComplete='off'
              disabled={EditAllTime}
              error={EditTimetoValidation}
              onClick={(e) => {
                if (!EditAllTime){
                  setanchorElTimeTo(e.currentTarget);
                  setopenMenuTimeTo(true)
                  // intiStateTime()
                  initTimePicker(EditStartTime, EditStopTime)
                }
              }}
            />
          </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>

          {/* select all time */}
          <BodyManageDV style={{
            
          }}>
          <BodySpace style={{
            
          }}></BodySpace>
          <BodyLabel style={{
            textAlign: "left"
          }}></BodyLabel>
          <BodyForm style={{
            
          }}>

          <FromAllTime>
            <Checkbox color={"primary"} checked={EditAllTime} onChange={(e) => {
              setEditAllTime(e.target.checked )
            }} />
            <FromAllTimeLabel>
              All Time
            </FromAllTimeLabel>
          </FromAllTime>

          </BodyForm>
          <BodySpace style={{
            
          }}></BodySpace>
          </BodyManageDV>









        {/* Door Sensor */}
        <BodyManageDV style={{
          paddingTop: "5px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>Door Sensor</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>

            {/* {
              (StatusDoorSensor ? <span style={{color: "#CDCDCD"}}>Active</span> : <span style={{color: "#CDCDCD"}}>Inactive</span>)
            } */}

            <FormControl variant="outlined" className={classes.formControl}>
              {
                <Select
                  labelId="demo-simple-select-outlined-label"
                  id="demo-simple-select-outlined"
                  input={<BootstrapInput />}
                  value={EditDoorSensor}
                  onChange={(e) => {
                    setEditDoorSensor(e.target.value)
                  }}
                >
                  <MenuItem value={true}>Enable</MenuItem>
                  <MenuItem value={false}>Disable</MenuItem>
                </Select>
              }
            </FormControl>

          </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>







        {/* Break Glass */}
        <BodyManageDV style={{
          paddingTop: "5px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>Break Glass</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>

            {/* {
              (StatusBreakGlass ? <span style={{color: "#CDCDCD"}}>Active</span> : <span style={{color: "#CDCDCD"}}>Inactive</span>)
            } */}
            <FormControl variant="outlined" className={classes.formControl}>
            {
              <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                input={<BootstrapInput />}
                value={EditBreakGlass}
                onChange={(e) => {
                  setEditBreakGlass(e.target.value)
                }}
              >
                <MenuItem value={true}>Enable</MenuItem>
                <MenuItem value={false}>Disable</MenuItem>
              </Select>
            }
            </FormControl>
          </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>







        <BodyManageDV style={{
          paddingTop: "5px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>Buzzer</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>
            <FormControl variant="outlined" className={classes.formControl}>
            {
              <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                input={<BootstrapInput />}
                value={EditBuzzer}
                onChange={(e) => {
                  setEditBuzzer(e.target.value)
                }}
              >
                <MenuItem value={true}>Enable</MenuItem>
                <MenuItem value={false}>Disable</MenuItem>
              </Select>
            }
            </FormControl>
          </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>

        {EditBuzzer ?
                  
          <BodyManageDV style={{
              
          }}>
          <BodySpace style={{
            
          }}></BodySpace>
          <BodyLabel style={{
            textAlign: "left"
          }}></BodyLabel>
          <BodyForm style={{
            paddingTop: "10px"
          }}>

          <FromAllTime>
            <FromAllTimeLabel>
              Delay
            </FromAllTimeLabel>
          </FromAllTime>

          <InputFromId 
            className={classes.InputIdControl}
            variant="outlined" 
            type="number"
            labelwidth={0}
            error={EditDelayValidation}
            fullWidth={true}
            placeholder="กรอก Delay"
            autoComplete='off'
            style={{width: "100%"}}
            value={EditDelayBuzzer}
            onChange={(e)=> {
              var value = e.target.value
              if (value === "0" || Number(value) <= 0){
                alert("กรุณาใส่ delay ให้ถูกต้อง")
              }else {
                if (e.target.value == 0) {
                  setEditDelayBuzzer(e.target.value)
                } else {
                  setEditDelayBuzzer(e.target.value.replace(/^0+/, ''))
                }
              }
            }}
          />
          <FromAllTime>
            <FromAllTimeLabel>
              วินาที
            </FromAllTimeLabel>
          </FromAllTime>

          </BodyForm>
          <BodySpace style={{
            
          }}></BodySpace>
          </BodyManageDV>
        :
          null
        }

        {/* Edit Factor */}
        <BodyManageDV style={{
          paddingTop: "5px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>Factor</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>
            <FormControl variant="outlined" className={classes.formControl}>
              <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                input={<BootstrapInput />}
                value={EditFactor}
                onChange={(e) => {
                  setEditFactor(e.target.value)
                }}
              >
                <MenuItem value={false}>1 Factor Authentication</MenuItem>
                <MenuItem value={true}>2 Factors Authentication</MenuItem>
              </Select>
            </FormControl>
          </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>

        <ActionManageDV>
          <div style={{
            display: "flex",
            justifyContent: "space-between",
            width: "100%",
            padding: "20px calc(10% + 30px) 0px calc(10% + 30px)"
          }}>
            <Choice1Btn 
              style={{
                width: 250, 
                height: 60,
                borderColor: "red",
                color: "red"
              }} 
              onClick={() => {
                // handleRemoveDevice()
                setopenDialogMangeDV(false)
                setopenDialogComfirmDeleteDevice(true)
              }}
            >ลบ</Choice1Btn>
            
            <Choice2Btn style={{
              width: 250, 
              height: 60
            }}
            onClick={() => {
              handleUpdateDevice()
            }}
            >บันทึกการเปลี่ยนแปลง</Choice2Btn>
          </div>
        </ActionManageDV>

        <CustomeMenu
          anchorEl={anchorElTimeFrom}
          keepMounted
          open={openMenuTimeFrom}
          onClose={handleCloseopenMenuTimeFrom}
        >

                                                                            <Container  >

                                                                              <div style={{
                                                                                display: "flex",
                                                                                alignContent: "center",
                                                                                alignItems: "center",
                                                                                justifyContent: "center",
                                                                              }}>

                                                                                <div style={{
                                                                                  width: 40,
                                                                                  height: 30,
                                                                                  display: "flex",
                                                                                  alignContent: "center",
                                                                                  alignItems: "center",
                                                                                  justifyContent: "center",
                                                                                  marginBottom: "10px"
                                                                                }}>
                                                                                  <IconButton onClick={(e) => {
                                                                                    var tNumber = t1
                                                                                    var elmnt = document.getElementById("scroll-hours-from-" + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));
                                                                                    elmnt.scrollIntoView();
                                                                                    if (Number(t1) > 0){
                                                                                      t1--
                                                                                      setHoursFrom(((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()))
                                                                                      setTimeFrom(
                                                                                        ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                                                                                        ":" + 
                                                                                        ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                                                                                      )
                                                                                    }
                                                                                  }}>
                                                                                    <img src={UpTime} alt="UpTime" />
                                                                                  </IconButton>
                                                                                </div>
                                                                                <div style={{
                                                                                  width: 40,
                                                                                }}></div>
                                                                                <div style={{
                                                                                  width: 40,
                                                                                  height: 30,
                                                                                  display: "flex",
                                                                                  alignContent: "center",
                                                                                  alignItems: "center",
                                                                                  justifyContent: "center",
                                                                                  marginBottom: "10px"
                                                                                }}>
                                                                                  <IconButton onClick={() => {
                                                                                    console.log(t2)
                                                                                    var tNumber = t2
                                                                                    var elmnt = document.getElementById("scroll-minutese-from-" + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));
                                                                                    elmnt.scrollIntoView();
                                                                                    if (Number(t2) > 0){
                                                                                      t2--
                                                                                      setMinuteseFrom(((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString()))
                                                                                      setTimeFrom(
                                                                                        ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                                                                                        ":" + 
                                                                                        ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                                                                                      )

                                                                                    }
                                                                                  }}>
                                                                                    <img src={UpTime} alt="UpTime" />
                                                                                  </IconButton>
                                                                                </div>
                                                                              </div>

                                                                              <div style={{
                                                                                display: "flex"
                                                                              }}>
                                                                                <div style={{
                                                                                  height: "200px",
                                                                                  width: "40px",
                                                                                  overflow: "scroll",
                                                                                }} 
                                                                                ref={ScorllHoursFrom}
                                                                                className="remove-scroll"
                                                                                >
                                                                                  {
                                                                                    Time.Hours.map((item, index) => {
                                                                                      return (
                                                                                        <div
                                                                                            id={"scroll-hours-from-" + Number(index)}
                                                                                            key={index}
                                                                                            style={{
                                                                                            width: "40px",
                                                                                            height: "40px",
                                                                                            display: "flex",
                                                                                            alignContent: "center",
                                                                                            justifyContent: "center",
                                                                                          }}
                                                                                          >
                                                                                            <IconButton 
                                                                                              onClick={() => {
                                                                                                setHoursFrom(item)
                                                                                                t1 = item
                                                                                                setTimeFrom(
                                                                                          ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                                                                                          ":" + 
                                                                                          ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                                                                                        )
                                                                                              console.log("select time")
                                                                                            }}
                                                                                            className={((item === HoursFrom) ? classestime.Select: classestime.Unselect)}
                                                                                          >{item}</IconButton>
                                                                                        </div>  
                                                                                      )
                                                                                    })
                                                                                  }
                                                                                </div>
                                                                                <div style={{
                                                                                  height: "200px",
                                                                                  width: "40px",
                                                                                  fontSize: "28px",
                                                                                  display: "flex",
                                                                                  alignContent: "center",
                                                                                  alignItems: "center",
                                                                                  justifyContent: "center",
                                                                                }} >:</div>
                                                                                <div style={{
                                                                                  height: "200px",
                                                                                  width: "40px",
                                                                                  overflow: "scroll",
                                                                                }} 
                                                                                ref={ScorllMinuteseFrom}
                                                                                className="remove-scroll"
                                                                                >
                                                                                  {
                                                                                    Time.Minutese.map((item, index) => {
                                                                                      return (
                                                                                        <div
                                                                                          key={index}
                                                                                          id={"scroll-minutese-from-" + Number(index)}
                                                                                          style={{
                                                                                          width: "40px",
                                                                                          height: "40px",
                                                                                          display: "flex",
                                                                                          alignContent: "center",
                                                                                          justifyContent: "center",
                                                                                        }}>
                                                                                          <IconButton 
                                                                                            className={((item === MinuteseFrom) ? classestime.Select: classestime.Unselect)} 
                                                                                            onClick={() => {
                                                                                              setMinuteseFrom(item)
                                                                                              t2 = item
                                                                                              setTimeFrom(
                                                                                        ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                                                                                        ":" + 
                                                                                        ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                                                                                      )
                                                                                            }}
                                                                                          >{item}</IconButton>
                                                                                        </div>  
                                                                                      )
                                                                                    })
                                                                                  }
                                                                                </div>
                                                                              </div>

                                                                              <div style={{
                                                                                display: "flex",
                                                                                alignContent: "center",
                                                                                alignItems: "center",
                                                                                justifyContent: "center",
                                                                              }}>

                                                                                <div style={{
                                                                                  width: 40,
                                                                                  height: 30,
                                                                                  display: "flex",
                                                                                  alignContent: "center",
                                                                                  alignItems: "center",
                                                                                  justifyContent: "center",
                                                                                  marginTop: "10px"
                                                                                }}>
                                                                                  <IconButton onClick={() => {
                                                                                    var tNumber = t1
                                                                                    var elmnt = document.getElementById("scroll-hours-from-" + ((Number(tNumber) !== 23) ? (Number(tNumber) + 1): Number(tNumber)));
                                                                                    elmnt.scrollIntoView();
                                                                                    if (Number(t1) < 23){
                                                                                      t1++
                                                                                      setHoursFrom(((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()))
                                                                                      setTimeFrom(
                                                                                        ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                                                                                        ":" + 
                                                                                        ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                                                                                      )
                                                                                    }
                                                                                  }}>
                                                                                    <img src={DownTime} alt="DownTime" />
                                                                                  </IconButton>
                                                                                </div>
                                                                                <div style={{
                                                                                  width: 40,
                                                                                }}></div>
                                                                                <div style={{
                                                                                  width: 40,
                                                                                  height: 30,
                                                                                  display: "flex",
                                                                                  alignContent: "center",
                                                                                  alignItems: "center",
                                                                                  justifyContent: "center",
                                                                                  marginTop: "10px"
                                                                                }}>
                                                                                  <IconButton onClick={() => {
                                                                                    var tNumber = t2
                                                                                    var elmnt = document.getElementById("scroll-minutese-from-" + ((Number(tNumber) !== 59) ? (Number(tNumber) + 1): Number(tNumber)));
                                                                                    elmnt.scrollIntoView();
                                                                                    if (Number(t2) < 59){
                                                                                      t2++
                                                                                      setMinuteseFrom(((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString()))
                                                                                      setTimeFrom(
                                                                                        ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                                                                                        ":" + 
                                                                                        ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                                                                                      )
                                                                                    }
                                                                                  }}>
                                                                                    <img src={DownTime} alt="DownTime" />
                                                                                  </IconButton>
                                                                                </div>
                                                                              </div>

                                                                            </Container>

        </CustomeMenu>


        <CustomeMenu
          anchorEl={anchorElTimeTo}
          keepMounted
          open={openMenuTimeTo}
          onClose={handleCloseopenMenuTimeTo}
        >

                                                                              <Container  >

                                                                              <div style={{
                                                                                display: "flex",
                                                                                alignContent: "center",
                                                                                alignItems: "center",
                                                                                justifyContent: "center",
                                                                              }}>

                                                                                <div style={{
                                                                                  width: 40,
                                                                                  height: 30,
                                                                                  display: "flex",
                                                                                  alignContent: "center",
                                                                                  alignItems: "center",
                                                                                  justifyContent: "center",
                                                                                  marginBottom: "10px"
                                                                                }}>
                                                                                  <IconButton 
                                                                                    onClick={() => {
                                                                                      console.log(t3)
                                                                                      var tNumber = t3
                                                                                      var elmnt = document.getElementById("scroll-hours-to-" + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));                                                                                      elmnt.scrollIntoView();
                                                                                      if (Number(t3) > 0){
                                                                                        t3--
                                                                                        setHoursTo(((Number(t3) < 10) ? ("0" + Number(t3).toString()): Number(t3).toString()))
                                                                                        setTimeTo(
                                                                                          ((Number(t3) < 10) ? ("0" + Number(t3).toString()): Number(t3).toString()) + 
                                                                                          ":" + 
                                                                                          ((Number(t4) < 10) ? ("0" + Number(t4).toString()): Number(t4).toString())
                                                                                        )
                                                                                      }
                                                                                    }}
                                                                                  >
                                                                                    <img src={UpTime} alt="UpTime" />
                                                                                  </IconButton>
                                                                                </div>
                                                                                <div style={{
                                                                                  width: 40,
                                                                                }}></div>
                                                                                <div style={{
                                                                                  width: 40,
                                                                                  height: 30,
                                                                                  display: "flex",
                                                                                  alignContent: "center",
                                                                                  alignItems: "center",
                                                                                  justifyContent: "center",
                                                                                  marginBottom: "10px"
                                                                                }}>
                                                                                  <IconButton
                                                                                    onClick={() => {
                                                                                      console.log(t4)
                                                                                      var tNumber = t4
                                                                                      var elmnt = document.getElementById("scroll-minutese-to-" + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));
                                                                                      elmnt.scrollIntoView();
                                                                                      if (Number(t4) > 0){
                                                                                        t4--
                                                                                        setMinuteseTo(((Number(t4) < 10) ? ("0" + Number(t4).toString()): Number(t4).toString()))
                                                                                        setTimeTo(
                                                                                          ((Number(t3) < 10) ? ("0" + Number(t3).toString()): Number(t3).toString()) + 
                                                                                          ":" + 
                                                                                          ((Number(t4) < 10) ? ("0" + Number(t4).toString()): Number(t4).toString())
                                                                                        )
                                                                                      }
                                                                                    }}
                                                                                  >
                                                                                    <img src={UpTime} alt="UpTime" />
                                                                                  </IconButton>
                                                                                </div>
                                                                              </div>

                                                                              <div style={{
                                                                                display: "flex"
                                                                              }}>
                                                                                <div style={{
                                                                                  height: "200px",
                                                                                  width: "40px",
                                                                                  overflow: "scroll",
                                                                                }} 
                                                                                ref={ScorllHoursTo}
                                                                                className="remove-scroll"
                                                                                >
                                                                                  {
                                                                                    Time.Hours.map((item, index) => {
                                                                                      return (
                                                                                        <div
                                                                                        id={"scroll-hours-to-" + Number(index)}
                                                                                        key={index}
                                                                                        style={{
                                                                                          width: "40px",
                                                                                          height: "40px",
                                                                                          display: "flex",
                                                                                          alignContent: "center",
                                                                                          justifyContent: "center",
                                                                                        }}
                                                                                        >
                                                                                          <IconButton 
                                                                                            onClick={() => {
                                                                                              setHoursTo(item)
                                                                                              t3 = item
                                                                                              setTimeTo(
                                                                                                ((Number(t3) < 10) ? ("0" + Number(t3).toString()): Number(t3).toString()) + 
                                                                                                ":" + 
                                                                                                ((Number(t4) < 10) ? ("0" + Number(t4).toString()): Number(t4).toString())
                                                                                              )
                                                                                            }}
                                                                                            className={((item === HoursTo) ? classestime.Select: classestime.Unselect)}
                                                                                          >{item}</IconButton>
                                                                                        </div>  
                                                                                      )
                                                                                    })
                                                                                  }
                                                                                </div>
                                                                                <div style={{
                                                                                  height: "200px",
                                                                                  width: "40px",
                                                                                  fontSize: "28px",
                                                                                  display: "flex",
                                                                                  alignContent: "center",
                                                                                  alignItems: "center",
                                                                                  justifyContent: "center",
                                                                                }} >:</div>
                                                                                <div style={{
                                                                                  height: "200px",
                                                                                  width: "40px",
                                                                                  overflow: "scroll",
                                                                                }} 
                                                                                ref={ScorllMinuteseTo}
                                                                                className="remove-scroll"
                                                                                >
                                                                                  {
                                                                                    Time.Minutese.map((item, index) => {
                                                                                      return (
                                                                                        <div
                                                                                        id={"scroll-minutese-to-" + Number(index)}
                                                                                          key={index}
                                                                                          style={{
                                                                                          width: "40px",
                                                                                          height: "40px",
                                                                                          display: "flex",
                                                                                          alignContent: "center",
                                                                                          justifyContent: "center",
                                                                                        }}>
                                                                                          <IconButton 
                                                                                            className={((item === MinuteseTo) ? classestime.Select: classestime.Unselect)} 
                                                                                            onClick={() => {
                                                                                              setMinuteseTo(item)
                                                                                              t4 = item
                                                                                              setTimeTo(
                                                                                                ((Number(t3) < 10) ? ("0" + Number(t3).toString()): Number(t3).toString()) + 
                                                                                                ":" + 
                                                                                                ((Number(t4) < 10) ? ("0" + Number(t4).toString()): Number(t4).toString())
                                                                                              )
                                                                                            }}
                                                                                          >{item}</IconButton>
                                                                                        </div>  
                                                                                      )
                                                                                    })
                                                                                  }
                                                                                </div>
                                                                              </div>

                                                                              <div style={{
                                                                                display: "flex",
                                                                                alignContent: "center",
                                                                                alignItems: "center",
                                                                                justifyContent: "center",
                                                                              }}>

                                                                                <div style={{
                                                                                  width: 40,
                                                                                  height: 30,
                                                                                  display: "flex",
                                                                                  alignContent: "center",
                                                                                  alignItems: "center",
                                                                                  justifyContent: "center",
                                                                                  marginTop: "10px"
                                                                                }}>
                                                                                  <IconButton
                                                                                    onClick={() => {
                                                                                      var tNumber = t3
                                                                                      var elmnt = document.getElementById("scroll-hours-to-" + ((Number(tNumber) !== 23) ? (Number(tNumber) + 1): Number(tNumber)));
                                                                                      elmnt.scrollIntoView();
                                                                                      if (Number(t3) < 23){
                                                                                        t3++
                                                                                        setHoursTo(((Number(t3) < 10) ? ("0" + Number(t3).toString()): Number(t3).toString()))
                                                                                        setTimeTo(
                                                                                          ((Number(t3) < 10) ? ("0" + Number(t3).toString()): Number(t3).toString()) + 
                                                                                          ":" + 
                                                                                          ((Number(t4) < 10) ? ("0" + Number(t4).toString()): Number(t4).toString())
                                                                                        )
                                                                                      }
                                                                                    }}
                                                                                  >
                                                                                    <img src={DownTime} alt="DownTime" />
                                                                                  </IconButton>
                                                                                </div>
                                                                                <div style={{
                                                                                  width: 40,
                                                                                }}></div>
                                                                                <div style={{
                                                                                  width: 40,
                                                                                  height: 30,
                                                                                  display: "flex",
                                                                                  alignContent: "center",
                                                                                  alignItems: "center",
                                                                                  justifyContent: "center",
                                                                                  marginTop: "10px"
                                                                                }}>
                                                                                  <IconButton
                                                                                    onClick={() => {
                                                                                      console.log(t4)
                                                                                      var tNumber = t4
                                                                                      var elmnt = document.getElementById("scroll-minutese-to-" + ((Number(tNumber) !== 59) ? (Number(tNumber) + 1): Number(tNumber)));
                                                                                      elmnt.scrollIntoView();
                                                                                      if (Number(t4) < 59){
                                                                                        t4++
                                                                                        setMinuteseTo(((Number(t4) < 10) ? ("0" + Number(t4).toString()): Number(t4).toString()))
                                                                                        setTimeTo(
                                                                                          ((Number(t3) < 10) ? ("0" + Number(t3).toString()): Number(t3).toString()) + 
                                                                                          ":" + 
                                                                                          ((Number(t4) < 10) ? ("0" + Number(t4).toString()): Number(t4).toString())
                                                                                        )
                                                                                      }
                                                                                    }}
                                                                                  >
                                                                                    <img src={DownTime} alt="DownTime" />
                                                                                  </IconButton>
                                                                                </div>
                                                                              </div>

                                                                            </Container>

        </CustomeMenu>

        </CustomeDialogContentManageDV>
      </Dialog>
          
      <CustomeMenu
        id="customized-menu"
        anchorEl={anchorElUpdateSoftware}
        keepMounted
        open={openMenu}
        onClose={handleClose}
      >
        <CustomeMenuItem onClick={() => {
          setHoursFrom(null)
          setopenMenu(false)
          setopenDialogFW(true)
        }}>
          <ListItemText primary="อัพเดตซอฟต์แวร์" />
        </CustomeMenuItem>
      </CustomeMenu>
      

      

      <Dialog
        disableBackdropClick={true}
        fullWidth={false}
        maxWidth={"xl"}
        open={openDialogComfirmDeleteDevice}
        aria-labelledby="update-dialog-MangeDV"
      >
          <Confirm Message={<div>คุณต้องการลบ <span style={{color: "#01A6E6"}}>{deviceName}</span> หรือไม่</div>} title={"ลบอุปกรณ์"}/>

          <ComfirmAction>

          <ComfirmAction>
          <Choice2Btn 
              style={{
                width: 380, 
                height: 60,
                marginRight: 20
              }}
              onClick={() => {
                setopenDialogComfirmDeleteDevice(false)
                handleRemoveDevice()
              }}
            >
              ยืนยัน
            </Choice2Btn>

            <Choice2Btn 
              style={{
                width: 380, 
                height: 60,
                backgroundColor: "#fff",
                color: "#01A6E6",
                marginLeft: 20
              }}
              onClick={() => {
                setopenDialogComfirmDeleteDevice(false)
              }}
            >
              ยกเลิก
            </Choice2Btn>
          </ComfirmAction>
            
          </ComfirmAction>

      </Dialog>

      <Dialog
          disableBackdropClick={true}
          fullWidth={false}
          maxWidth={"xl"}
          open={openDialogComfirm}
          aria-labelledby="update-dialog-MangeDV"
        >
            {
              (openDialogComfirmStatus ? <Success title={TitleAction}/> : <Unsuccess title={TitleAction} msgerror={MessageError}/>)
            }
            <ComfirmAction>

              {
                (openDialogComfirmStatus ? <Choice2Btn 
                  style={{
                    width: 800, 
                    height: 60
                  }}
                  onClick={() => {
                    setopenDialogComfirm(false)
                  }}
                >
                  ยืนยัน
                </Choice2Btn> : <ComfirmAction>
                  <Choice2Btn 
                    style={{
                      width: 800, 
                      height: 60,
                      backgroundColor: "#e46f6f",
                      border: "red"
                    }}
                    onClick={() => {
                      setopenDialogComfirm(false)
                    }}
                  >
                    ยืนยัน
                  </Choice2Btn>
                </ComfirmAction>)
              }
              
            </ComfirmAction>

        </Dialog>



      <Backdrop className={navbarclasses.backdrop} open={OpenBackdrop}>
        <CircularProgress color="inherit" />
      </Backdrop>
      
    </StutusContrainer>

  );
}
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: 300,
  },
  online: {
    color: "#11AB42",
  },
  offline: {
    color: "#CC1503"
  },
  paper: {
    marginRight: theme.spacing(2),
  },
  active_: {
    borderBottom: "5px solid #01A6E6",
    color: "#01a6e6"
  },
  formControl: {
    width: "100%",
  },
  formControlDate: {
    padding: 0,
    width: "100%",
  },
  container: {
    // "full height" - "height navbar" - "space footter" - "padding container top" - "padding container bottom" - "height result search"
    maxHeight: "calc(100vh - 8vh - 30px - 40px - 40px - 70px - 20px)",
  },
  sizeFont: {
    fontSize: 18
  },
  active: {
    color: "#11AB42"
  },
  inactive: {
    color: "#CC1503"
  },
  inactive_status: {
    color: "#a9a9a9"
  }
}));


export default useStyles
import React from 'react'
import styled from 'styled-components'
import StyledMenu from 'src/components/material/menu/CustomeMenuEvent'
import StyledMenuItem from 'src/components/material/menu/CustomeMenuItem'

import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import TablePagination from '@material-ui/core/TablePagination';
import TableFooter from '@material-ui/core/TableFooter';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';

import ReplayIcon from '@material-ui/icons/Replay';

// image 
import UNNORMAL from 'src/asset/img/navbar/UNNORMAL.svg'
import NORMAL from 'src/asset/img/navbar/NORMAL.svg'

import { makeStyles, useTheme } from '@material-ui/core/styles';

import parseDate from 'src/commons/parseDate'
import parseTime from 'src/commons/parseTime'

import getEvent from 'src/apis/getEvent';

import IconButton from '@material-ui/core/IconButton'

import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';

const PaggingContainer = styled('div')`
  text-align: center;
  width: 100%;
  color: rgb(1, 166, 230);
  text-decoration-line: underline;
  cursor: pointer;
`

const useStyles1 = makeStyles((theme) => ({
  root: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5),
  },
}));

const useStyles2 = makeStyles({
  table: {
    minWidth: 500,
  },
});

function TablePaginationActions(props) {
  const classes = useStyles1();
  const theme = useTheme();
  const { count, page, rowsPerPage, onChangePage, SelectProps } = props;

  const handleFirstPageButtonClick = (event) => {
    onChangePage(event, 0, false);
  };

  const handleBackButtonClick = (event) => {
    onChangePage(event, page - 1, false);
  };

  const handleNextButtonClick = (event) => {
    onChangePage(event, page + 1, false);
  };

  const handleLastPageButtonClick = (event) => {
    onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1), false);
  };

  const handleFirstPagePagging = (event) => {
    onChangePage(event, 0, true);
  }

  return (
    <div className={classes.root}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
      <IconButton 
        onClick={handleFirstPagePagging} 
      >
        <ReplayIcon />
      </IconButton>
    </div>
  );
}

const CallEvent = async (limit, offset, token) => {
  let res
  try {
    res = await getEvent({
      query: `
          {
              Event (limit: ${limit}, offset: ${offset}) {
                  results {
                      event_id
                      type
                      time
                      topic
                      message
                  }
                  count
              }
          }
        `
      }, token
    )
  } catch (res_e) {
    res = res_e.response
  }
  return res.data.data.Event
}

/**
 * props: -> 
 *  store
 *  anchorElAlert
 *  handleCloseAlert
 */
function Event (props) {
  const { store, anchorElAlert, handleCloseAlert } = props

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = async (event, newPage, all) => {
    if (all){
      setPage(newPage);
      setRowsPerPage(10);
      const Token = store.getState().AccessToken
      if (Token !== undefined){
        var event = await CallEvent(10, 0 * 10, Token)
        setEvent(event)
      }
    } else {
      setPage(newPage);
      console.log(rowsPerPage, newPage * rowsPerPage)
      const Token = store.getState().AccessToken
      if (Token !== undefined){
        var event = await CallEvent(rowsPerPage, newPage * rowsPerPage, Token)
        setEvent(event)
      }
    }
  };

  const handleChangeRowsPerPage = async (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    const Token = store.getState().AccessToken
    if (Token !== undefined){
      var event = await CallEvent(parseInt(event.target.value, 10), 0, Token)
      setEvent(event)
    }
    setPage(0);
  };

  const [ Event, setEvent ] = React.useState({
    results: [],
    count: 0
  })

  React.useEffect(() => {
    const fn = async () => {
      const Token = store.getState().AccessToken
      if (Token !== undefined){
        var event = await CallEvent(10, 0, Token)
        setEvent(event)
      }
    }
    fn()
  }, [store])

  return <StyledMenu
    id="customized-menu"
    anchorEl={anchorElAlert}
    keepMounted
    open={Boolean(anchorElAlert)}
    onClose={handleCloseAlert}
  >

    <Table>
      <TableBody>
        <TableRow style={{
          display: "flex",
          justifyContent: "flex-end",
          paddingRight: "20px"
        }}>
          
        <TablePaginationActions 
          count={Event.count}
          page={page}
          rowsPerPage={rowsPerPage}
          onChangePage={handleChangePage}
          SelectProps={{
            inputProps: { 'aria-label': 'rows per page' },
            native: true,
          }}
        />

          {/* <TablePagination
            rowsPerPageOptions={[5, 10, 25, 50, 100, 1000]}
            colSpan={3}
            count={Event.count}
            rowsPerPage={rowsPerPage}
            page={page}
            labelRowsPerPage={"Event Per Page"}
            SelectProps={{
              inputProps: { 'aria-label': 'rows per page' },
              native: true,
            }}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
            ActionsComponent={TablePaginationActions}
            style={{paddingRight: "20px"}}
          /> */}

        </TableRow>
        <TableRow>
        {
          (Event !== undefined ? Event.results.map((item) => {
            return <StyledMenuItem>
              <ListItemIcon>
                {
                  (item.type === "alert" ? <img alt="UNNORMAL" src={UNNORMAL} />: <img alt="NORMAL" src={NORMAL} />)
                }
              </ListItemIcon>
              <ListItemText >
                <div style={{fontSize: 18}}>{item.topic}</div>
                <div style={{
                  display: "flex",
                  justifyContent: "space-between"
                }}>
                  <div style={{fontSize: 16}}>{item.message}</div>
                  <div style={{
                    paddingLeft: 90,
                    paddingRight: 20
                  }}>{
                    parseDate(item.time) + " " + parseTime(item.time)
                  }</div>
                </div>
              </ListItemText>
            </StyledMenuItem>
          }): null)
        }
        </TableRow>
        <TableRow>

        </TableRow>
      </TableBody>
    </Table>

  </StyledMenu>
}

export default Event
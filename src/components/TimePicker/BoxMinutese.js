import styled from 'styled-components';

export default styled("div")`
  width: calc(300px - 10px );
  height: 380px;
  padding: 10px;
`
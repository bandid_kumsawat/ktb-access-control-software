import React from 'react'
import IconButton from '@material-ui/core/IconButton';

// sub Component
import Container from 'src/components/TimePicker/Container'

// Pseudo Style
import "src/components/TimePicker/style.css"

// setting style 
import TimeStyles from 'src/components/TimePicker/style'

// time format
import Time from 'src/components/TimePicker/time.json'

// icon
import UpTime from "src/asset/img/timepicker/up.svg";
import DownTime from "src/asset/img/timepicker/down.svg";

import InputFromId from 'src/components/material/dialog/MangeDevice/content/Form/InputId'

// menu component
import CustomeMenu from 'src/components/material/menu/CustomeMenu'

let t1 = "00"
let t2 = "00"

export default function TimePicker(props) {
  
  const classestime = TimeStyles()

  const ScorllHoursFrom = React.createRef();
  const ScorllMinuteseFrom = React.createRef();

  // const [ store ] = React.useState(props.store)
  const [ keyPicker ] = React.useState(props.keyPicker)
  const [ PrickerValue ] = React.useState(props.PrickerValue)

  const [anchorElTimeFrom, setanchorElTimeFrom] = React.useState(null);

  const [ openMenuTimeFrom, setopenMenuTimeFrom ] = React.useState(false)

  const [ HoursFrom, setHoursFrom ] = React.useState("00") // true
  const [ MinuteseFrom, setMinuteseFrom ] = React.useState("00") // false

  const [ TimeFrom , setTimeFrom ] = React.useState("00:00")

  const initTimePicker = () => {
    try{
      var tNumber = "0"
      var elmnt = Object
      
      // For From
      tNumber = t1
      elmnt = document.getElementById("scroll-hours-from-" + keyPicker + "-" + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));
      elmnt.scrollIntoView();
      setHoursFrom((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString())

      tNumber = t2
      elmnt = document.getElementById("scroll-minutese-from-" + keyPicker + "-" + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));
      elmnt.scrollIntoView();
      setMinuteseFrom((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())

    } catch (e) {
      console.log(e)
    }
  }

  const intiStateTime = () => {
    var from = new Date(PrickerValue)
    
    var h1 = from.getHours()
    var m1 = from.getMinutes()
    
    setTimeFrom(((Number(h1) < 10) ? ("0" + Number(h1).toString()): Number(h1).toString()) + ":" + ((Number(m1) < 10) ? ("0" + Number(m1).toString()): Number(m1).toString()) )

    setHoursFrom((Number(h1) < 10) ? ("0" + Number(h1).toString()): Number(h1).toString())
    console.log(((Number(h1) < 10) ? ("0" + Number(h1).toString()): Number(h1).toString()))
    setMinuteseFrom((Number(m1) < 10) ? ("0" + Number(m1).toString()): Number(m1).toString())
    console.log(((Number(m1) < 10) ? ("0" + Number(m1).toString()): Number(m1).toString()))

    t1 = h1
    t2 = m1
  }

  React.useEffect(() => {
    intiStateTime()
  }, [])

  const handleCloseopenMenuTimeFrom = () => {
    setanchorElTimeFrom(null);
    setopenMenuTimeFrom(false)
  }
  
  return (
    <div>

      <InputFromId 
        variant="outlined" 
         
        labelwidth={0}
        placeholder="00:00" 
        value={TimeFrom}
        fullWidth={true}
        autoComplete='off'
        onClick={(e) => {
          setanchorElTimeFrom(e.currentTarget);
          setopenMenuTimeFrom(true)
          initTimePicker()
        }}
      />
      <CustomeMenu
        anchorEl={anchorElTimeFrom}
        keepMounted
        open={openMenuTimeFrom}
        onClose={handleCloseopenMenuTimeFrom}
      >

      
        <Container id={keyPicker} key={keyPicker} >

          <div style={{
            widtH: 120,
            display: "flex",
            alignContent: "center",
            alignItems: "center",
            justifyContent: "center",
          }}>

            <div style={{
              width: 40,
              height: 30,
              display: "flex",
              alignContent: "center",
              alignItems: "center",
              justifyContent: "center",
              marginBottom: "10px"
            }}>
              <IconButton onClick={(e) => {
                var tNumber = t1
                var elmnt = document.getElementById("scroll-hours-from-" + keyPicker + "-" + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));
                elmnt.scrollIntoView();
                if (Number(t1) > 0){
                  t1--
                  setHoursFrom(((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()))
                  setTimeFrom(
                    ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                    ":" + 
                    ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                  )
                }
              }}>
                <img src={UpTime} alt="UpTime" />
              </IconButton>
            </div>
            <div style={{
              width: 40,
            }}></div>
            <div style={{
              width: 40,
              height: 30,
              display: "flex",
              alignContent: "center",
              alignItems: "center",
              justifyContent: "center",
              marginBottom: "10px"
            }}>
              <IconButton onClick={() => {
                console.log(t2)
                var tNumber = t2
                var elmnt = document.getElementById("scroll-minutese-from-" + keyPicker + "-" + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));
                elmnt.scrollIntoView();
                if (Number(t2) > 0){
                  t2--
                  setMinuteseFrom(((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString()))
                  setTimeFrom(
                    ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                    ":" + 
                    ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                  )

                }
              }}>
                <img src={UpTime} alt="UpTime" />
              </IconButton>
            </div>
            
          </div>

          <div style={{
            display: "flex"
            }}>
            <div style={{
              height: "200px",
              width: "40px",
              overflow: "scroll",
            }} 
            ref={ScorllHoursFrom}
            className="remove-scroll"
            >
              {
                Time.Hours.map((item, index) => {
                  return (
                    <div
                        id={"scroll-hours-from-" + keyPicker + "-" + Number(index)}
                        key={index}
                        style={{
                        width: "40px",
                        height: "40px",
                        display: "flex",
                        alignContent: "center",
                        justifyContent: "center",
                      }}
                      >
                        <IconButton 
                          onClick={() => {
                            setHoursFrom(item)
                            t1 = item
                            setTimeFrom(
                              ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                              ":" + 
                              ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                            )
                          console.log("select time")
                        }}
                        className={((item === HoursFrom) ? classestime.Select: classestime.Unselect)}
                      >{item}</IconButton>
                    </div>  
                  )
                })
              }
            </div>
            <div style={{
              height: "200px",
              width: "40px",
              fontSize: "28px",
              display: "flex",
              alignContent: "center",
              alignItems: "center",
              justifyContent: "center",
            }} >:</div>
            <div style={{
            height: "200px",
            width: "40px",
            overflow: "scroll",
          }} 
          ref={ScorllMinuteseFrom}
          className="remove-scroll"
          >
            {
              Time.Minutese.map((item, index) => {
                return (
                  <div
                    key={index}
                    id={"scroll-minutese-from-" + keyPicker + "-" + Number(index)}
                    style={{
                    width: "40px",
                    height: "40px",
                    display: "flex",
                    alignContent: "center",
                    justifyContent: "center",
                  }}>
                    <IconButton 
                      className={((item === MinuteseFrom) ? classestime.Select: classestime.Unselect)} 
                      onClick={() => {
                        setMinuteseFrom(item)
                        t2 = item
                        setTimeFrom(
                          ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                          ":" + 
                          ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                        )
                      }}
                    >{item}</IconButton>
                  </div>  
                )
              })
            }
          </div>
          </div>

          <div style={{
            display: "flex",
            alignContent: "center",
            alignItems: "center",
            justifyContent: "center",
            }}>

            <div style={{
              width: 40,
              height: 30,
              display: "flex",
              alignContent: "center",
              alignItems: "center",
              justifyContent: "center",
              marginTop: "10px"
            }}>
              <IconButton onClick={() => {
                var tNumber = t1
                var elmnt = document.getElementById("scroll-hours-from-" + keyPicker + "-" + ((Number(tNumber) !== 23) ? (Number(tNumber) + 1): Number(tNumber)));
                elmnt.scrollIntoView();
                if (Number(t1) < 23){
                  t1++
                  setHoursFrom(((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()))
                  setTimeFrom(
                    ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                    ":" + 
                    ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                  )
                }
              }}>
                <img src={DownTime} alt="DownTime" />
              </IconButton>
            </div>
            <div style={{
              width: 40,
            }}></div>
            <div style={{
            width: 40,
            height: 30,
            display: "flex",
            alignContent: "center",
            alignItems: "center",
            justifyContent: "center",
            marginTop: "10px"
          }}>
            <IconButton onClick={() => {
              var tNumber = t2
              var elmnt = document.getElementById("scroll-minutese-from-" + keyPicker + "-" + ((Number(tNumber) !== 59) ? (Number(tNumber) + 1): Number(tNumber)));
              elmnt.scrollIntoView();
              if (Number(t2) < 59){
                t2++
                setMinuteseFrom(((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString()))
                setTimeFrom(
                  ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                  ":" + 
                  ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                )
              }
            }}>
              <img src={DownTime} alt="DownTime" />
            </IconButton>
          </div>
          </div>

        </Container>
        
      </CustomeMenu>

    </div>
  )  
}


// import React from 'react'
// import IconButton from '@material-ui/core/IconButton';

// // sub Component
// import Container from 'src/components/TimePicker/Container'

// // Pseudo Style
// import "src/components/TimePicker/style.css"

// // setting style 
// import TimeStyles from 'src/components/TimePicker/style'

// // time format
// import Time from 'src/components/TimePicker/time.json'

// // icon
// import UpTime from "src/asset/img/timepicker/up.svg";
// import DownTime from "src/asset/img/timepicker/down.svg";

// import InputFromId from 'src/components/material/dialog/MangeDevice/content/Form/InputId'

// // menu component
// import CustomeMenu from 'src/components/material/menu/CustomeMenu'

// let t1 = "00"
// let t2 = "00"

// export default function TimePicker(props) {
  
//   const classestime = TimeStyles()

//   const ScorllHoursFrom = React.createRef();
//   const ScorllMinuteseFrom = React.createRef();

//   // const [ store ] = React.useState(props.store)
//   const [ keyPicker ] = React.useState(props.keyPicker)
//   const [ PrickerValue ] = React.useState(props.PrickerValue)

//   const [anchorElTimeFrom, setanchorElTimeFrom] = React.useState(null);

//   const [ openMenuTimeFrom, setopenMenuTimeFrom ] = React.useState(false)

//   const [ HoursFrom, setHoursFrom ] = React.useState("00") // true
//   const [ MinuteseFrom, setMinuteseFrom ] = React.useState("00") // false

//   const [ TimeFrom , setTimeFrom ] = React.useState("00:00")

//   const initTimePicker = () => {
//     try{
//       var tNumber = "0"
//       var elmnt = Object
      
//       // For From
//       tNumber = t1
//       elmnt = document.getElementById("scroll-hours-from-" + keyPicker + "-" + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));
//       elmnt.scrollIntoView();
//       setHoursFrom(Object.assign([], (Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()))

//       tNumber = t2
//       elmnt = document.getElementById("scroll-minutese-from-" + keyPicker + "-" + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));
//       elmnt.scrollIntoView();
//       setMinuteseFrom(Object.assign([], (Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString()))

//     } catch (e) {
//       console.log(e)
//     }
//   }

//   const intiStateTime = () => {
//     var from = new Date(PrickerValue)
    
//     var h1 = from.getHours()
//     var m1 = from.getMinutes()
    
//     setTimeFrom(Object.assign([], ((Number(h1) < 10) ? ("0" + Number(h1).toString()): Number(h1).toString()) + ":" + ((Number(m1) < 10) ? ("0" + Number(m1).toString()): Number(m1).toString()) ))

//     setHoursFrom(Object.assign([],(Number(h1) < 10) ? ("0" + Number(h1).toString()): Number(h1).toString()))
//     console.log(((Number(h1) < 10) ? ("0" + Number(h1).toString()): Number(h1).toString()))
//     setMinuteseFrom(Object.assign([],(Number(m1) < 10) ? ("0" + Number(m1).toString()): Number(m1).toString()))
//     console.log(((Number(m1) < 10) ? ("0" + Number(m1).toString()): Number(m1).toString()))

//     t1 = h1
//     t2 = m1
//   }

//   React.useEffect(() => {
//     intiStateTime()
//   }, [])

//   const handleCloseopenMenuTimeFrom = () => {
//     setanchorElTimeFrom(null);
//     setopenMenuTimeFrom(false)
//   }
  
//   return (
//     <div>

//       <InputFromId 
//         variant="outlined" 
//          
//         labelwidth={0}
//         placeholder="00:00" 
//         value={TimeFrom}
//         fullWidth={true}
//         autoComplete='off'
//         onClick={(e) => {
//           setanchorElTimeFrom(e.currentTarget);
//           setopenMenuTimeFrom(true)
//           initTimePicker()
//         }}
//       />
//       <CustomeMenu
//         anchorEl={anchorElTimeFrom}
//         keepMounted
//         open={openMenuTimeFrom}
//         onClose={handleCloseopenMenuTimeFrom}
//       >

      
//         <Container  >

//           <div style={{
//             widtH: 120,
//             display: "flex",
//             alignContent: "center",
//             alignItems: "center",
//             justifyContent: "center",
//           }}>

//             <div style={{
//               width: 40,
//               height: 30,
//               display: "flex",
//               alignContent: "center",
//               alignItems: "center",
//               justifyContent: "center",
//               marginBottom: "10px"
//             }}>
//               <IconButton onClick={(e) => {
//                 var tNumber = t1
//                 var elmnt = document.getElementById("scroll-hours-from-" + keyPicker + "-" + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));
//                 elmnt.scrollIntoView();
//                 if (Number(t1) > 0){
//                   t1--
//                   setHoursFrom(Object.assign([], ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString())))
//                   setTimeFrom(
//                     Object.assign([], 
//                     ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
//                     ":" + 
//                     ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString()))
//                   )
//                 }
//               }}>
//                 <img src={UpTime} alt="UpTime" />
//               </IconButton>
//             </div>
//             <div style={{
//               width: 40,
//             }}></div>
//             <div style={{
//               width: 40,
//               height: 30,
//               display: "flex",
//               alignContent: "center",
//               alignItems: "center",
//               justifyContent: "center",
//               marginBottom: "10px"
//             }}>
//               <IconButton onClick={() => {
//                 console.log(t2)
//                 var tNumber = t2
//                 var elmnt = document.getElementById("scroll-minutese-from-" + keyPicker + "-" + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));
//                 elmnt.scrollIntoView();
//                 if (Number(t2) > 0){
//                   t2--
//                   setMinuteseFrom(Object.assign([], ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())))
//                   setTimeFrom(
//                     Object.assign(
//                       ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
//                       ":" + 
//                       ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString()))
//                     )

//                 }
//               }}>
//                 <img src={UpTime} alt="UpTime" />
//               </IconButton>
//             </div>
            
//           </div>

//           <div style={{
//             display: "flex"
//             }}>
//             <div style={{
//               height: "200px",
//               width: "40px",
//               overflow: "scroll",
//             }} 
//             ref={ScorllHoursFrom}
//             className="remove-scroll"
//             >
//               {
//                 Time.Hours.map((item, index) => {
//                   return (
//                     <div
//                         id={"scroll-hours-from-" + keyPicker + "-" + Number(index)}
//                         key={index}
//                         style={{
//                         width: "40px",
//                         height: "40px",
//                         display: "flex",
//                         alignContent: "center",
//                         justifyContent: "center",
//                       }}
//                       >
//                         <IconButton 
//                           onClick={() => {
//                             setHoursFrom(Object.assign([], item))
//                             t1 = item
//                             setTimeFrom(
//                               Object.assign([], 
//                               ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
//                               ":" + 
//                               ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString()))
//                             )
//                           console.log("select time")
//                         }}
//                         className={((item === HoursFrom) ? classestime.Select: classestime.Unselect)}
//                       >{item}</IconButton>
//                     </div>  
//                   )
//                 })
//               }
//             </div>
//             <div style={{
//               height: "200px",
//               width: "40px",
//               fontSize: "28px",
//               display: "flex",
//               alignContent: "center",
//               alignItems: "center",
//               justifyContent: "center",
//             }} >:</div>
//             <div style={{
//             height: "200px",
//             width: "40px",
//             overflow: "scroll",
//           }} 
//           ref={ScorllMinuteseFrom}
//           className="remove-scroll"
//           >
//             {
//               Time.Minutese.map((item, index) => {
//                 console.log(MinuteseFrom)
//                 return (
//                   <div
//                     key={index}
//                     id={"scroll-minutese-from-" + keyPicker + "-" + Number(index)}
//                     style={{
//                     width: "40px",
//                     height: "40px",
//                     display: "flex",
//                     alignContent: "center",
//                     justifyContent: "center",
//                   }}>
//                     <IconButton 
//                       className={((item === MinuteseFrom) ? classestime.Select: classestime.Unselect)} 
//                       onClick={() => {
//                         setMinuteseFrom(Object.assign([], item))
//                         t2 = item
//                         setTimeFrom(
//                           Object.assign([], 
//                           ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
//                           ":" + 
//                           ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString()))
//                         )
//                       }}
//                     >{item}</IconButton>
//                   </div>  
//                 )
//               })
//             }
//           </div>
//           </div>

//           <div style={{
//             display: "flex",
//             alignContent: "center",
//             alignItems: "center",
//             justifyContent: "center",
//             }}>

//             <div style={{
//               width: 40,
//               height: 30,
//               display: "flex",
//               alignContent: "center",
//               alignItems: "center",
//               justifyContent: "center",
//               marginTop: "10px"
//             }}>
//               <IconButton onClick={() => {
//                 var tNumber = t1
//                 var elmnt = document.getElementById("scroll-hours-from-" + keyPicker + "-" + ((Number(tNumber) !== 23) ? (Number(tNumber) + 1): Number(tNumber)));
//                 elmnt.scrollIntoView();
//                 if (Number(t1) < 23){
//                   t1++
//                   setHoursFrom(Object.assign([], ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString())))
//                   setTimeFrom(
//                     Object.assign([], 
//                       ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
//                       ":" + 
//                       ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
//                     )
//                   )
//                 }
//               }}>
//                 <img src={DownTime} alt="DownTime" />
//               </IconButton>
//             </div>
//             <div style={{
//               width: 40,
//             }}></div>
//             <div style={{
//             width: 40,
//             height: 30,
//             display: "flex",
//             alignContent: "center",
//             alignItems: "center",
//             justifyContent: "center",
//             marginTop: "10px"
//           }}>
//             <IconButton onClick={() => {
//               var tNumber = t2
//               var elmnt = document.getElementById("scroll-minutese-from-" + keyPicker + "-" + ((Number(tNumber) !== 59) ? (Number(tNumber) + 1): Number(tNumber)));
//               elmnt.scrollIntoView();
//               if (Number(t2) < 59){
//                 t2++
//                 setMinuteseFrom(Object.assign([], ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())))
//                 setTimeFrom(
//                   Object.assign([], 
//                     ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
//                     ":" + 
//                     ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
//                   )
//                 )
//               }
//             }}>
//               <img src={DownTime} alt="DownTime" />
//             </IconButton>
//           </div>
//           </div>

//         </Container>
        
//       </CustomeMenu>

//     </div>
//   )  
// }
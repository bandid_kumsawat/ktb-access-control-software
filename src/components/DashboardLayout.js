import React from 'react'
import { Outlet, useNavigate} from 'react-router-dom';

// common lib
import parseToday from 'src/commons/parseToday'

import { 
  DEVICE,
  LOG,
  GROUP,
  LISTUSER,
  SEARCHDEVICE,
  SEARCHEMPLOYEE,
  ADDGROUP,
  EDITGROUP,
  Event,
  EventStatus
} from "src/stores/actions"

// mock apis
// import device from "src/apis_test_dev/device"
// import log from "src/apis_test_dev/log"
// import groups from "src/apis_test_dev/groups"

// deplay apis
import getUser from 'src/apis/getUser'
import getDevice from 'src/apis/getDevice'
import getGroup from 'src/apis/getGroup';
import getLog from 'src/apis/getLog';
import getEvent from 'src/apis/getEvent';

import DashboardNavbar from './DashboardNavbar';
import DashboardSidebar from './DashboardSidebar';

import styled from 'styled-components';

const DashboardLayoutRoot = styled('div')`
  background-color: #F9FDFF;
  display: flex;
  height: 100%;
  width: 100%;
`

var init_start_date = 1
var TimeForUserActive = Object
var ActiveUseronWeb = 1000 * 60 * 15

const DashobardFlexNavOutlet = styled('div')``

function DashboardLayout (props) {

  const [ store ] = React.useState(props.store)
  const history = useNavigate()

  React.useEffect(() => {
    // console.log("set timeout { onLoad }")
    TimeForUserActive = setTimeout(() => {
      // console.log("go to login 2")
      window.location.href = "/"
    }, ActiveUseronWeb);

    // inactive 15 min goto login
    window.addEventListener('mouseover', () => {
      // console.log("clear timeout { mouseover }")
      clearTimeout(TimeForUserActive)
      // set for user mouseover anytime
      // console.log("set timeout { mouseover }")
      TimeForUserActive = setTimeout(() => {
        console.log("go to login 1")
        window.location.href = "/"
      }, ActiveUseronWeb);
    } );
    window.addEventListener('mouseleave', () => {
      // set for user mouseleave my app
      // console.log("clear timeout { mouseleave }")
      clearTimeout(TimeForUserActive)
      // console.log("set timeout { mouseleave }")
      TimeForUserActive = setTimeout(() => {
        console.log("go to login 3")
        window.location.href = "/"
      }, ActiveUseronWeb);
    } );

    const initSearch = async () => {
      const Token = store.getState().AccessToken
      var searchd = store.getState().SearchDevice
      var searche = store.getState().SearchEmployee
      if (searchd === undefined || searche === undefined){
        store.dispatch(SEARCHDEVICE({
          flag: false, 
          search: {
            device: "",
          }
        },))
        store.dispatch(SEARCHEMPLOYEE({
          flag: false, 
          search: {
            name: "",
            surname: "",
            card:"",
            startdate: parseToday(init_start_date),
            enddate: parseToday(0),
            starttime: "00:00",
            endtime: "23:59",
            device: [undefined],
            offset: 0,
            limit: 10,
            orderBy: "",
            orderType: "",
          }
        },))

      }

      if (store.getState().SearchEmployee !== undefined){
        var query = {
          flag: false, 
          search: {
            name: "",
            surname: "",
            card:"",
            startdate: parseToday(init_start_date),
            enddate: parseToday(0),
            starttime: "00:00",
            endtime: "23:59",
            device: [undefined],
            offset: 0,
            limit: 10,            
            orderBy: "desc",
            orderType: "date_in",
          }
        }
        // `+((query.search.startdate !== "") ? `beginDate: "`+query.search.startdate+`",`: ``)+`
        // `+((query.search.enddate !== "") ? `endDate: "`+query.search.enddate+`",`: ``)+`
        // `+((query.search.starttime !== "") ? `beginTime: "`+query.search.starttime+`",`: ``)+`
        // `+((query.search.endtime !== "") ? `endTime: "`+query.search.endtime+`",`: ``)+`



        // `+((query.search.orderBy !== "") ? `orderType: "`+query.search.orderBy+`",`: ``)+`
        // `+((query.search.orderType !== "") ? `orderBy: "`+query.search.orderType+`",`: ``)+`

        const res_log = await getLog({
          query: `
            {
                GetRecord (
                  `+((query.search.offset !== "") ? `offset: `+query.search.offset+`,`: ``)+`
                  `+((query.search.limit !== "") ? `limit: `+query.search.limit+`,`: ``)+`
                ) {
                  results {
                    record_id
                    enter_time
                    enter_method
                    exit_method
                    enter_gate {
                        gate_id
                        name
                        begin_time
                        end_time
                    }
                    exit_time
                    exit_gate {
                        gate_id
                        name
                        begin_time
                        end_time
                    }
                    is_exited
                    employee {
                        employee_id
                        name
                        lastname
                        position
                        mifare_id
                        picture_url
                        face_id
                    }
                  }
                  count
                }
            }
          `
        }, Token)
        store.dispatch(LOG(res_log.data.data.GetRecord))
      }

    }
    const initGroupPage = () => {
      var addgroup = store.getState().AddGroup
      var editgroup = store.getState().EditGroup
      if (addgroup === undefined || editgroup === undefined){
        store.dispatch(ADDGROUP({
          name: "",
          device: [],
          start: "",
          end: ""
        }))
        store.dispatch(EDITGROUP({
          name: "",
          device: [],
          start: "",
          end: ""
        }))
      }
    }
    initGroupPage()
    initSearch()

    // Token for Auth
    const Token = store.getState().AccessToken
    const User = store.getState().User
    if ((Token === undefined && User === undefined)){
      history("/")
      return
    }
    const loadDateFromAPIS = async () => {

      const res_event = await getEvent({
        query: `
          {
              Event {
                  results {
                      event_id
                      type
                      time
                      topic
                      message
                  }
                  count
              }
          }
        `
      }, Token)
      var EventStatus_ = store.getState().EventStatus
      if (EventStatus_.init){
        EventStatus_.new = res_event.data.data.Event.count
        store.dispatch(EventStatus(EventStatus_))
      } else {
        EventStatus_.new = res_event.data.data.Event.count
        EventStatus_.old = res_event.data.data.Event.count
        EventStatus_.init = true
        store.dispatch(EventStatus(EventStatus_))
      }
      store.dispatch(Event(res_event.data.data.Event))
      if (store.getState().SearchDevice !== undefined) {
        if (store.getState().SearchDevice.flag === false) {
          const res_device = await getDevice({
            query: `
            {
                GetDevice {
                    results {
                        device_id
                        gate_id
                        device_status_id
                        device_config_id
                        serial_number
                        tablet_enter_serial_number
                        tablet_exit_serial_number
                        create_at
                        update_at
                        gate {
                            gate_id
                            name_gate
                            begin_time
                            end_time
                            create_at
                            update_at
                        }
                        device_config {
                            device_config_id
                            buzzer_enabled
                            door_sensor_enabled
                            break_glass_enabled
                            buzzer_delay
                            two_factor_enabled
                        }
                        device_status {
                            buzzer
                            iot_box_status
                            tablet_enter_status
                            tablet_exit_status
                            door_status
                            break_glass
                        }
                    }
                    count
                }
            }
            `
          }, Token)
          store.dispatch(DEVICE(res_device.data.data.GetDevice.results))
        }
      }

      const res_groups = await getGroup({
        query: `
          query {
            Group {
              group_id
              name
              gates {
                  gate_id
                  name
                  begin_time
                  end_time
              }
              employees {
                  employee_id
                  name
                  lastname
                  position
                  mifare_id
                  picture_url
                  face_id
              }
              group_gates {
                  gate_id
                  group_id
                  override_time
                  begin_time
                  end_time
              }
            }
        }
        `
      }, Token)
      store.dispatch(GROUP(res_groups.data.data.Group))

      // load user data
      const res_getUser = await getUser({
        query: `
          {
              GetUserAdmin (orderBy:"create_at" , orderType:"desc"){
                results {
                  user_id
                  role {
                      role_name
                  }
                  username
                  name
                  email
                  employee_code
                  create_at
                  update_at
                  last_active
                  update_password_at
                  is_locked
                }
              }
          }
        `
      }, Token)
      store.dispatch(LISTUSER(res_getUser.data.data.GetUserAdmin.results))

    }

    loadDateFromAPIS()

    var interval_ = setInterval(() => {
      loadDateFromAPIS()
    }, 2000);

    // check token expire
    // setTimeout(() => {
    //   window.location.href = "/"
    // }, store.getState().User.exp / 1000)

    return () => {
      clearInterval(interval_)
    }

  }, [store, history])

  return (
    <DashboardLayoutRoot>
      <DashboardSidebar store={ store }/>
      <DashobardFlexNavOutlet>
        <DashboardNavbar store={ store }/>
        <Outlet />
      </DashobardFlexNavOutlet>
    </DashboardLayoutRoot>
  )
}

export default DashboardLayout;

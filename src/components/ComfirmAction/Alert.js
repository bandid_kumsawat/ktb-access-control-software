import React from 'react'
import styled from 'styled-components'
import ErrorIcon from '@material-ui/icons/Error';

const Header = styled("div")`
  width: 900px;
  height: 40px;
  margin-top: 20px;
  display: flex;
  align-content: center;
  justify-content: center;
  align-items: center;
  font-size: 30px;
`
const Body = styled("div")`
  display: flex;
  align-content: center;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  margin-top: 20px;
}
`
const Container = styled("div")`
`
const Message = styled("div")`
  color: #000;
  font-size: 30px;
`
export default function Alert(props){
  
  return (
    <Container>
      <Header></Header>
      <Body>
        <Message>{props.Message}</Message>
      </Body>
    </Container>
  )
}
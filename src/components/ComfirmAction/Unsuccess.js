import React from 'react'
import styled from 'styled-components'
import ErrorIcon from '@material-ui/icons/Error';

const Header = styled("div")`
  width: 900px;
  height: 100px;
  margin-top: 20px;
  display: flex;
  align-content: center;
  justify-content: center;
  align-items: center;
  font-size: 50px;
`
const Body = styled("div")`
  display: flex;
  align-content: center;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  margin-top: 20px;
  background-color: #ffe6e6;
}
`
const Container = styled("div")`
`
const Message = styled("div")`
  color: red;
  font-size: 30px;
`
export default function Unsuccess(props){
  
  return (
    <Container>
      <Header>{props.title}</Header>
      <Body>
        <ErrorIcon style={{fontSize: 150, color: "#e46f6f"}}/>
        <Message>Unsuccessful</Message>
        <div>{props.msgerror}</div>
      </Body>
    </Container>
  )
}
import React from 'react'
import styled from 'styled-components'
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';

const Header = styled("div")`
  width: 900px;
  height: 100px;
  margin-top: 20px;
  display: flex;
  align-content: center;
  justify-content: center;
  align-items: center;
  font-size: 40px;
  margin: 20px;
  text-align: center;
`
const Body = styled("div")`
  display: flex;
  align-content: center;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  margin-top: 20px;
  background-color: #f3ffea;
}
`
const Container = styled("div")`
`
const Message = styled("div")`
  color: green;
  font-size: 30px;
`
export default function Success(props){
  
  return (
    <Container>
      <Header>{props.title}</Header>
      <Body>
        <CheckCircleOutlineIcon style={{fontSize: 150, color: "green"}}/>
        <Message>Successfull</Message>
      </Body>
    </Container>
  )
}
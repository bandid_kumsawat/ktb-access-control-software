import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import styled from 'styled-components';

import TableSortLabel from "@material-ui/core/TableSortLabel"

// Confirm dialog
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";

import { 
  SEARCHEMPLOYEE,
  LOG
} from "src/stores/actions"

import CommonGetRecord from 'src/commons/getRecord'

import "./TableLog.css"

// commons lib
import parseTime from 'src/commons/parseTime'
import parseDate from 'src/commons/parseDate'
import parseTimeUTC from 'src/commons/parseTimeUTC'

const useStylesTableSearch = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff"
  }
}));

const ContentName = styled('div')`
  display: flex;
  align-items: center;
  align-content: center;
`
const PaddingName = styled('div')`
  padding: 0px 0px 0px 5px;
`

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

function TablePagingTableHead(props) {
  const [ headCells ] = React.useState(props.headCells)
  const { classes, order, orderBy, onRequestSort } = props;

  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {/* <TableCell align={'left'} style={{width: "10px"}} /> */}
        {headCells.map((headCell) => (
          ((headCell.id === "enter_in" || 
            headCell.id === "enter_out" || 
            headCell.id === "date_in" || 
            headCell.id === "date_out"
          ) ? <TableCell
            key={headCell.id}
            align={'left'}
            padding={headCell.disablePadding ? 'none' : 'default'}
            sortDirection={orderBy === headCell.id ? order : false}
            style={{fontSize: 18, whiteSpace: "nowrap"}}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
              style={{fontSize: 18}}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>: <TableCell style={{fontSize: 18, whiteSpace: "nowrap"}}> { headCell.label } </TableCell>)
        ))}
      </TableRow>
    </TableHead>
  );
}

TablePagingTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
  headCells: PropTypes.array.isRequired,
};

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  container: {
    // "full height" - "height navbar" - "space footter" - "padding container top" - "padding container bottom" - "height result search"
    maxHeight: "calc(100vh - 8vh - 30px - 40px - 40px - 70px - 20px)",
  },
  sizeFont: {
    fontSize: 18
  }
}));

export default function TableLog(props) {
  const [ store ] = React.useState(props.store)
  const classes = useStyles();
  const classestablelog = useStylesTableSearch()
  const [order, setOrder] = React.useState('desc');
  const [orderBy, setOrderBy] = React.useState('date_in');
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [ Token ] = React.useState(store.getState().AccessToken)
  const [ rows, setrows] = React.useState(undefined)
  const [ ParamSearch, setParamSearch ] = React.useState(undefined)
  const [ RowsTotal, setRowsTotal ] = React.useState(0)
  
  const [ OpenBackdrop, setOpenBackdrop ] = React.useState(false)

  const headCells = [
    { id: 'name', numeric: true, disablePadding: false, label: 'ชื่อ-นามสกุล' },
    { id: 'position', numeric: true, disablePadding: false, label: 'ตำแหน่ง' },
    { id: 'device_in', numeric: true, disablePadding: false, label: 'ประตูที่เข้า' },
    { id: 'device_out', numeric: true, disablePadding: false, label: 'ประตูที่ออก' },
    { id: 'enter_in', numeric: true, disablePadding: false, label: 'วิธีเข้า' },
    { id: 'enter_out', numeric: true, disablePadding: false, label: 'วิธีออก' },
    { id: 'date_in', numeric: true, disablePadding: false, label: 'วันที่เข้า' },
    { id: 'time_in', numeric: true, disablePadding: false, label: 'เวลาเข้า' },
    { id: 'date_out', numeric: true, disablePadding: false, label: 'วันที่ออก' },
    { id: 'time_out', numeric: true, disablePadding: false, label: 'เวลาออก' }
  ];

  const saveToState = async (search) => {
    setOpenBackdrop(true)
    const res = await CommonGetRecord(search, Token)
    console.log(res)
    store.dispatch(LOG(res.data.data.GetRecord))
    setOpenBackdrop(false)
  }

  React.useEffect(() => {
    // setPage((store.getState().SearchEmployee === undefined ? 0 : store.getState().SearchEmployee.search.offset))
    // setRowsPerPage((store.getState().SearchEmployee === undefined ? 10 : store.getState().SearchEmployee.search.limit))
    const LoadData = async () => {
      var logg = store.getState().Log
      var search = store.getState().SearchEmployee
      var newlogg = []

      if (logg !== undefined && logg !== null && search !== undefined){
        
        var limit = search.search.limit
        var page = search.search.offset / limit

        setOrder(search.search.orderBy)
        setOrderBy(search.search.orderType)

        setPage(page)
        setRowsPerPage(limit)

        setParamSearch(search)
        setRowsTotal(logg.count)
        logg.results.forEach((row, index) => {
          if (row.employee.employee_id !== 0) {
            newlogg.push( {
              id: row.record_id,
              emp_id: row.employee.employee_id,
              name: row.employee.name + " " + row.employee.lastname,
              position: (row.employee.position === "" ? "-" : row.employee.position),
              device_in: (row.enter_gate.name === "" ? "-": row.enter_gate.name),
              device_out: (row.exit_gate.name === "" ? "-": row.exit_gate.name),
              enter_in: (row.enter_method === ""? "-": row.enter_method),
              enter_out: (row.exit_method === "" ? "-" : row.exit_method),
              // format time 0001-01-01T00:00:00Z
              date_in: (row.enter_time === "0001-01-01T00:00:00Z" ? "-": parseDate(row.enter_time)),
              time_in: (row.enter_time === "0001-01-01T00:00:00Z"? "-": parseTimeUTC(row.enter_time)),
              date_out: (row.exit_time === "0001-01-01T00:00:00Z" ? "-": parseDate(row.exit_time)),
              time_out: (row.exit_time === "0001-01-01T00:00:00Z" ? "-": parseTimeUTC(row.exit_time)),
            } )
          } else {
            newlogg.push( {
              id: "-" + index,
              emp_id:  "-",
              name:  "-",
              position:  "-",
              device_in:  "-",
              device_out:  "-",
              enter_in:  "-",
              enter_out:  "-",
              date_in:  "-",
              time_in:  "-",
              date_out:  "-",
              time_out:  "-",
            } )
          }
        })
        
      }
      setrows(Object.assign([], newlogg))
    }
    LoadData()
    var interval_ = setInterval(() => {
      LoadData()
    }, 200);
    return () => {
      clearInterval(interval_)
    }
  }, [store])

  const handleRequestSort = (event, property) => {
    console.log(orderBy)
    console.log(property)
    const isAsc = orderBy === property && order === 'asc';
    // setOrder(isAsc ? 'desc' : 'asc');
    // setOrderBy(property);
    var ps = ParamSearch
    ps.search.orderBy = (isAsc ? 'desc' : 'asc')

    ps.search.orderType = property
    saveToState(ps)
    store.dispatch(SEARCHEMPLOYEE(ps))
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = rows.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleChangePage = async (event, newPage) => {
    var offset = newPage * parseInt(rowsPerPage, 10)
    var limit = newPage * parseInt(rowsPerPage, 10) + parseInt(rowsPerPage, 10) - offset
    console.log(`Change page > offset = ${offset}, limit = ${limit}`)
    var ps = ParamSearch
    ps.search.offset = offset
    ps.search.limit = limit
    saveToState(ps)
    store.dispatch(SEARCHEMPLOYEE(ps))
    setPage(newPage);
  };

  const handleChangeRowsPerPage = async (event) => {
    var offset = 0 * parseInt(event.target.value, 10)
    var limit = 0 * parseInt(event.target.value, 10) + parseInt(event.target.value, 10) - offset
    console.log(`Change rows > offset = ${offset}, limit = ${limit}`)
    var ps = ParamSearch
    ps.search.offset = offset
    ps.search.limit = limit
    saveToState(ps)
    store.dispatch(SEARCHEMPLOYEE(ps))
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const notFoundData = (colSpan) => {
    return <TableRow >
      <TableCell colSpan={colSpan} style={{textAlign: "center"}}>ไม่พบข้อมูล</TableCell >
    </TableRow>
  }

  return ((rows === undefined) ? "ไม่พบข้อมูล": (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <TableContainer className={classes.container} id="table-containner-custom-scroll-log">
          <Table
            className={classes.table}

            // aria-labelledby="tableTitle"
            // size={'medium'}
            // aria-label="TablePaging table"

            stickyHeader 
            aria-label="sticky table"
          >
            <TablePagingTableHead
              classes={classes}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
              headCells={headCells}
            />
            <TableBody>
              {
                (rows.length === 0 ? notFoundData(10) : stableSort(rows, getComparator(order, orderBy))
                .map((row, index) => {
                  return (
                    <TableRow
                      hover
                      onClick={() => console.log(row.id)}
                      role="checkbox"
                      tabIndex={-1}
                      key={row.id}
                    >
                      {/* <TableCell  /> */}
                      <TableCell align="left">

                        <ContentName>
                          {/* <img src={`${server.url}/employee/${row.id}/picture?authorization=${Token}`} alt="UNSELECTEMPY" width={32} height={32} style={{ borderRadius: "30px" }}/> */}
                          <PaddingName className={classes.sizeFont} style={{whiteSpace: "nowrap"}}>{row.name}</PaddingName>
                        </ContentName>
                        
                        </TableCell>
                      <TableCell align="left" className={classes.sizeFont} style={{whiteSpace: "nowrap"}}>
                        {row.position}
                      </TableCell>
                      <TableCell align="left" className={classes.sizeFont} style={{whiteSpace: "nowrap"}}>{row.device_in}</TableCell>
                      <TableCell align="left" className={classes.sizeFont} style={{whiteSpace: "nowrap"}}>{row.device_out}</TableCell>

                      <TableCell align="left" className={classes.sizeFont} style={{whiteSpace: "nowrap"}}>{row.enter_in}</TableCell>
                      <TableCell align="left" className={classes.sizeFont} style={{whiteSpace: "nowrap"}}>{row.enter_out}</TableCell>


                      <TableCell align="left" className={classes.sizeFont}>{row.date_in}</TableCell>
                      <TableCell align="left" className={classes.sizeFont}>{row.time_in}</TableCell>
                      <TableCell align="left" className={classes.sizeFont}>{row.date_out}</TableCell>
                      <TableCell align="left" className={classes.sizeFont}>{row.time_out}</TableCell>
                    </TableRow>
                  );
                }))
              }
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25, 50, 100, 1000]}
          component="div"
          count={RowsTotal}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>

      <Backdrop className={classestablelog.backdrop} open={OpenBackdrop}>
        <CircularProgress color="inherit" />
      </Backdrop>
      
    </div>
  ))
}


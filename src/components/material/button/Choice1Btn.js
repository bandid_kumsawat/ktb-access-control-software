import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';

const Choice1Btn = withStyles({
  root: {
    boxShadow: 'none',
    textTransform: 'none',
    fontSize: 16,
    padding: '6px 12px',
    border: '1px solid',
    lineHeight: 1.5,
    backgroundColor: '#fff',
    borderColor: '#000',
    width: 160,
  },
})(Button);

export default Choice1Btn




// '&:hover': {
//   backgroundColor: '#0069d9',
//   borderColor: '#0062cc',
//   boxShadow: 'none',
// },
// '&:active': {
//   boxShadow: 'none',
//   backgroundColor: '#0062cc',
//   borderColor: '#005cbf',
// },
// '&:focus': {
//   boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
// },
import TextField from '@material-ui/core/TextField'
import styled from 'styled-components';

const InputId = styled(TextField)`
  .MuiOutlinedInput-root {
    width: 100%;
    &.Mui-focused fieldset {
      border: 2px solid #337AB7 !important;
    }
  }
  .MuiOutlinedInput-input {
    padding: 13px 47px 13px 16px;
    font-size: 18px;
  }
`;

export default InputId
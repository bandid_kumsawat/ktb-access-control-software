import styled from 'styled-components';

const Frome = styled('div')`
  width: 100%;
  display: flex;
  align-items: center;
  margin-top: 20px;
`

export default Frome;
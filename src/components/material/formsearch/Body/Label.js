import styled from 'styled-components';

const Label = styled('div')`
  width: 25%;
  font-size: 18px;
  display: flex;
  align-items: center;
`

export default Label;
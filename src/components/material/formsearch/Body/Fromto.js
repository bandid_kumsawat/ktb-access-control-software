import styled from 'styled-components';

const FromTo = styled('div')`
  padding: 10px;
  display: flex;
  justify-content: flex-start;
`

export default FromTo;
import styled from 'styled-components';

const CustomeExitDialog = styled('div')`
  display: flex;
  justify-content: flex-end;
`

export default CustomeExitDialog;
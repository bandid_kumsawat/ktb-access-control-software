import DialogTitle from '@material-ui/core/DialogTitle';
import { withStyles } from '@material-ui/core/styles';

const CustomeDialogTitle = withStyles({
  root: {
    padding: "0px 180px 0px 180px",
    fontSize: "28px !important"
  }
})(DialogTitle)


export default CustomeDialogTitle
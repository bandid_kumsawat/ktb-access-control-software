import styled from 'styled-components';

const AddDevice = styled('div')`
  width: 50%;
  font-size: 18px;
`

export default AddDevice;
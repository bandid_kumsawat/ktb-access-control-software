import TextField from '@material-ui/core/TextField'
import styled from 'styled-components';

const InputId = styled(TextField)`
  .MuiOutlinedInput-root {
    margin-top: 3px;
    &.Mui-focused fieldset {
      border: 2px solid #337AB7 !important;
    }
  }
  .MuiOutlinedInput-input {
    padding: 10px 24px 12px 12px;
    font-size: 18px;
  }
`;

export default InputId
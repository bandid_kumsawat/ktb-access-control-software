import styled from 'styled-components';

const Action = styled('div')`
  display: flex;
  justify-content: center;
`

export default Action;
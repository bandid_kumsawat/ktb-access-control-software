import DialogContent from '@material-ui/core/DialogContent';
import { withStyles } from '@material-ui/core/styles';

const CustomeDialogContent = withStyles({
  root: {
    padding: "10px 0px 30px 0px",
    textAlign: "center"
  }
})(DialogContent)

export default CustomeDialogContent 
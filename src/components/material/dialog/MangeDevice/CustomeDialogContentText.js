import DialogContentText from '@material-ui/core/DialogContentText';
import { withStyles } from '@material-ui/core/styles';

const CustomeDialogContentText = withStyles({
  root: {
    textAlign: "center"
  }
})(DialogContentText)


export default CustomeDialogContentText
import DialogContent from '@material-ui/core/DialogContent';
import { withStyles } from '@material-ui/core/styles';

const CustomeDialogContent = withStyles({
  root: {
    padding: "30px 50px 30px 50px",
  }
})(DialogContent)

export default CustomeDialogContent 
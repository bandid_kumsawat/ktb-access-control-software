import Menu from '@material-ui/core/Menu';
import { withStyles } from '@material-ui/core/styles';

import "./style.css"

const CustomeMenu = withStyles({
  // root: {
  //   zIndex: 2000,
  //   paddingTop: '0px',
  //   paddingButton: '0px',
  // },
  paper: {
    // border: '1px solid #d3d4d5',
    boxShadow: "0px 5px 5px -3px rgb(0 0 0 / 20%), 0px 8px 10px 1px rgb(0 0 0 / 14%), 0px 3px 14px 2px rgb(0 0 0 / 12%)",
    // height: "450px"
  },
})((props) => (
  <Menu
    id={"menu-scoll-bor-custom"}
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'left',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'left',
    }}
    {...props}
  />
));

export default CustomeMenu
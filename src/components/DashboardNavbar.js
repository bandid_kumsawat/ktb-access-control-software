import React, { useState } from 'react'
import { useParams, useNavigate } from 'react-router-dom';
import styled from 'styled-components';
import clsx from 'clsx'

import EventComponent from 'src/components/Event'

import ReloadDataLog from 'src/components/ReloadData/log'
import ReloadDataEmp from 'src/components/ReloadData/employee'

import SearchEmployee from 'src/components/search/employee'
import SearchDevice from 'src/components/search/device'
import Checkbox from '@material-ui/core/Checkbox'
import Radio from '@material-ui/core/Radio'

import parseDate from 'src/commons/parseDate'
import parseTime from 'src/commons/parseTime'

import UploadZip from 'src/apis/uploadZip'

// commons lib
import parseTimetoFulltime from 'src/commons/parseTimetoFulltime'

// Confirm dialog
import Success from 'src/components/ComfirmAction/Success'
import Unsuccess from 'src/components/ComfirmAction/Unsuccess'
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import Confirm from 'src/components/ComfirmAction/Confirm'  

// image 
import ADD from 'src/asset/img/navbar/ADD.svg'
import BELL_ALERT from 'src/asset/img/navbar/BELL_ALERT.svg'
import BELL_NO_ALERT from 'src/asset/img/navbar/BELL_NO_ALERT.svg'

import SEARCH from 'src/asset/img/navbar/SEARCH.svg'
import USER_PEOPLE from 'src/asset/img/user/user_ktb.jpg'
import EXIT from 'src/asset/img/search/EXIT.svg'
import UNSELECTEMPY from "src/asset/img/employee/noemployer.svg"
import EXPORTEMPTY from "src/asset/img/employee/exportemployee.svg"
import ADDGROUP from "src/asset/img/sidebar/ADDGROUP.svg"
import REMOVEDOORACCESS from "src/asset/img/sidebar/REMOVEDOORACCESS.svg"
import Exit from 'src/asset/img/setting/EXIT.svg'


import IconButton from '@material-ui/core/IconButton';

import MenuItem from '@material-ui/core/MenuItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';

import { withStyles, makeStyles } from '@material-ui/core/styles';

import { useLocation } from 'react-router-dom'

// dialog update Device component
import Dialog from '@material-ui/core/Dialog'
import CustomeDialogContentManageDV from 'src/components/material/dialog/MangeDevice/CustomeDialogContent'
import CustomeDialogTitleManageDV from 'src/components/material/dialog/MangeDevice/CustomeDialogTitle'
import CustomeExitDialogManageDV from 'src/components/material/dialog/MangeDevice/CustomeExitDialog'
import ActionManageDV from "src/components/material/dialog/MangeDevice/content/Action"
import BodyManageDV from "src/components/material/dialog/MangeDevice/content/Body"
import BodyForm from 'src/components/material/dialog/MangeDevice/content/Body/Form'
import BodyLabel from 'src/components/material/dialog/MangeDevice/content/Body/Label'
import BodySpace from 'src/components/material/dialog/MangeDevice/content/Body/Space'
import InputFromId from 'src/components/material/dialog/MangeDevice/content/Form/InputId'
import Fromto from 'src/components/material/dialog/MangeDevice/content/Body/Fromto'
import InputAdornment from '@material-ui/core/InputAdornment';
import Tooltip from '@material-ui/core/Tooltip';

import { EMPLOYEE } from 'src/stores/actions'
// button component
import Choice1Btn from 'src/components/material/button/Choice1Btn'
import Choice2Btn from 'src/components/material/button/Choice2Btn'

// Search Form
import CustomeContentBody from 'src/components/material/formsearch/Body/Form'
import CustomeContentSpace from 'src/components/material/formsearch/Body/Space'
import CustomeContentButton from 'src/components/material/button/Choice1Btn'

// setting style 
import useStyles from './device/setting/style'

// apis 
import AddAdmin from 'src/apis/addUser'
import getAdmin from 'src/apis/getUser'
import AddDeivce from 'src/apis/addDevice'
import getDevice from 'src/apis/getDevice';
import addGroup from 'src/apis/addGroup'
import getGroup from 'src/apis/getGroup'
import getLog from 'src/apis/getLog';
import getMyProfile from 'src/apis/getMyProfile'
import getEmployeeByGroup from 'src/apis/getEmployeeByGroup'

// store listuser
import { LISTUSER,DEVICE, GROUP, SEARCHEMPLOYEE, SEARCHDEVICE, EventStatus} from 'src/stores/actions'

// menu component
import CustomeMenu from 'src/components/material/menu/CustomeMenu'
import CustomeMenuItem from 'src/components/material/menu/CustomeMenuItem'

// TimePicker
import TimePicker from 'src/components/GlobalDialog/TimePicker';
// sub Component
import Container from 'src/components/TimePicker/Container'
// Pseudo Style
import "src/components/TimePicker/style.css"
// setting style import TimeStyles from 'src/components/TimePicker/style'
import TimeStyles from 'src/components/TimePicker/style'
// time format
import Time from 'src/components/TimePicker/time.json'
// icon
import UpTime from "src/asset/img/timepicker/up.svg";
import DownTime from "src/asset/img/timepicker/down.svg";
import VisibilityOffIcon from '@material-ui/icons/Visibility'
import VisibilityIcon from '@material-ui/icons/VisibilityOff'

import { 
  LOG,
} from "src/stores/actions"

import ChangePassword from "src/apis/changePassword"

// Commons
import PasswordChecker from 'src/commons/passwordChecker'

// new concept dialog material
import AddGroup from 'src/components/GlobalDialog/AddGroup'

import StyledMenu from 'src/components/material/menu/CustomeMenu'
import StyledMenuItem from 'src/components/material/menu/CustomeMenuItem'

// From Select
import FormControl from "@material-ui/core/FormControl"
import Select from "@material-ui/core/Select"
import InputBase from '@material-ui/core/InputBase'
import addEmployee from 'src/apis/addEmployee';
import { isNonNullChain, isNullishCoalesce } from 'typescript';
const BootstrapInput = withStyles((theme) => ({
  root: { 
    'label + &': {
      marginTop: theme.spacing(3),
    },
  },
  input: {
    textAlign: "left",
    borderRadius: 4,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #ccc',
    fontSize: 16,
    padding: '10px 47px 13px 12px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:focus': {
      borderRadius: 4,
      // borderColor: '#80bdff',
      // boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
      border: '1px solid #337AB7',
    },
  },
}))(InputBase);


const useStylesNavbar = makeStyles((theme) => ({
  Visible: {
    display: "block",
  },
  InVisible: {
    display: "none",
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff"
  }
}));

const ComfirmAction = styled("div")`
  display: flex;
  align-content: center;
  justify-content: space-around;
  margin: 20px;
`

const TitleAndExite = styled("div")`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 20px;
`

const Title = styled("div")` 
  font-size: 40px;
`

const DashboardNavbarRoot = styled('div')`
  background-color: #FFFFFF;
  width: 85vw;
  height: 8vh;
  display: flex;
  flex-wrap: nowrap;
  align-items: center;
  padding: 10px 0px 5px 0px;
`;
const DashboardMenuName = styled('div')`
  background-color: #FFFFFF;
  width: 50%;
  height: 100%;
  display: flex;
  justify-content: flex-start;
  flex-wrap: nowrap;
  align-items: center;
`;

const DashboardMenuButton = styled('div')`
  background-color: #FFFFFF;
  width: 50%;
  height: 100%;
  display: flex;
  justify-content: flex-end;
  flex-wrap: nowrap;
  align-items: center;
`;

const MenuName = styled('span')`
  font-size: 28px;
  padding-left: 30px;
`;

const Icon = styled('img')`
  width: 100%;
  height: 100%;
`

const Deivider = styled('div')`
  border: 1px solid #337AB7;
  margin-right: 15px;
  margin-left: 15px;
  height: 50%;
`

const CustomeBlackdrop = styled("div")`
  height: 100vh;
  width: 100vw;
  position: fixed;
  right: 0px;
  top: 0px;
  background-color: #666666bf;
  z-index: 1998;
`

const SearchingForm = styled("div")`
  width: 600px;
  height: 100vh;
  background-color: #F9FDFF;
  position: fixed;
  top: 0px;
  right: 0px;
  z-index: 1999;
`

const FromAllTime = styled('div')`
  display: flex;
  justify-content: flex-start;
  align-content: center;
  padding-left: 20px;
`
const FromAllTimeLabel = styled('div')`
  display: flex;
  align-content: center;
  justify-content: flex-start;
  align-items: center;
  padding-right: 10px;
`

const CustomTooltip = withStyles({
  tooltip: {
    color: 'black',
    backgroundColor: 'white',
    boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)",
    textAlign: "center",
    maxWidth: "none"
  },
  arrow: {
    color: 'white',
  },
})(Tooltip);

var t1 = "00"
var t2 = "00"

var t3 = "00"
var t4 = "00"



function DashboardNavbar (props) {
  const location = useLocation();
  const classestime = TimeStyles()

  const classes = useStyles();
  const navbarclasses = useStylesNavbar()
  
  const [SEventStatus, setSEventStatus] = React.useState({
    init: false,
    new: 0,
    old: 0
  })

  const ScorllHoursFrom = React.createRef();
  const ScorllMinuteseFrom = React.createRef();
  const ScorllHoursTo = React.createRef();
  const ScorllMinuteseTo = React.createRef();

  const inputFile = React.createRef()
  const inputFileZip = React.createRef()

  const [ Event, setEvent ] = React.useState(undefined)

  const [anchorElAlert, setanchorElAlert ] = React.useState(null)
  const [ openDialogComfirmUpload, setopenDialogComfirmUpload ] = React.useState(false)

  const [ store ] = useState(props.store)

  const [ openDialogMangeDV, setopenDialogMangeDV ] = React.useState(false)
  const [ openDialogMangeAdmin, setopenDialogMangeAdmin ] = React.useState(false)
  const [ openDialogAddGroup, setopenDialogAddGroup ] = React.useState(false)
  const [ openDialogAddUserToGroup, setopenDialogAddUserToGroup ] = React.useState(false)

  const [ AddGroupNameValidation, setAddGroupNameValidation ] = React.useState(false)

  const [ TimeFromValidation, setTimeFromValidation ] = React.useState(false)
  const [ TimeToValidation, setTimeToValidation ] = React.useState(false)

  const [ BackDropOpen, setBackDropOpen ] = React.useState(false)
  const [ GlobalSearchOpen, setGlobalSearchOpen ] = React.useState(false)

  const [ AddName, setAddName ] = React.useState("")
  const [ AddEmployeeCode, setAddEmployeeCode ] = React.useState("")
  const [ AddEmail, setAddEmail ] = React.useState("")
  const [ AddUserLevel, setAddUserLevel ] = React.useState(1001)
  const [ AddUsername, setAddUsername ] = React.useState("")
  const [ AddPassword, setAddPassword ] = React.useState("")
  const [ AddConfirmPass, setAddConfirmPass ] = React.useState("")
  const [ AddPassError, setAddPassError ] = React.useState(false)
  const [ AddPassCond, setAddPassCond ] = React.useState([])

  const [ AddNameValidation, setAddNameValidation ] = React.useState(false)
  const [ AddEmployeeCodeValidation, setAddEmployeeCodeValidation ] = React.useState(false)
  const [ AddEmailValidation, setAddEmailValidation ] = React.useState(false)
  const [ AddUsernameValidation, setAddUsernameValidation ] = React.useState(false)
  const [ AddPasswordValidation, setAddPasswordValidation ] = React.useState(false)
  const [ AddConfirmPassValidation, setAddConfirmPassValidation ] = React.useState(false)

  const [ isAddPasswordVisible, setIsAddPasswordVisible ] = React.useState(false)
  const [ isAddConfirmPassVisible, setIsAddConfirmPassVisible ] = React.useState(false)

  const [ Token, setToken ] = React.useState('')

  const [ ImageFile, setImageFile ] = React.useState(null) 

  const [ HoursFrom, setHoursFrom ] = React.useState("00") // true
  const [ HoursTo, setHoursTo ] = React.useState("00") // true
  const [ MinuteseFrom, setMinuteseFrom ] = React.useState("00") // false
  const [ MinuteseTo, setMinuteseTo ] = React.useState("00") // false

  const [ TimeFrom , setTimeFrom ] = React.useState("00:00")
  const [ TimeTo , setTimeTo ] = React.useState("00:00")

  const [anchorElTimeFrom, setanchorElTimeFrom] = React.useState(null);
  const [anchorElTimeTo, setanchorElTimeTo] = React.useState(null);

  const [ openMenuTimeFrom, setopenMenuTimeFrom ] = React.useState(false)
  const [ openMenuTimeTo, setopenMenuTimeTo ] = React.useState(false)

  const [ AddSerialTabletsEnter, setAddSerialTabletsEnter ] = React.useState("")
  const [ AddSerialTabletsExit, setAddSerialTabletsExit ] = React.useState("")
  const [ AddSerialIoTBox, setAddSerialIoTBox ] = React.useState("")
  const [ AddDoorName, setAddDoorName ] = React.useState("")
  const [ AddBuzzer, setAddBuzzer ] = React.useState(true)
  const [ AddCheckAlltime, setAddCheckAlltime ] = React.useState(false)
  const [ AddDelayBuzzerEnable, setAddDelayBuzzerEnable ] = React.useState(60)
  const [ AddBreakGlass, setAddBreakGlass ] = React.useState(true) 
  const [ AddDoorSensor , setAddDoorSensor ] = React.useState(true) 
  const [ AddFactor, setAddFactor ] = React.useState(true) 

  // Validation Add Device
  const [ AddSerialNumberIoTBoxValidation, setAddSerialNumberIoTBoxValidation ] = React.useState(false)
  const [ AddTabletsEnterValidation, setAddTabletsEnterValidation ] = React.useState(false)
  const [ AddTabletsExitValidation, setAddTabletsExitValidation ] = React.useState(false)
  const [ AddDoorNameValidation, setAddDoorNameValidation ] = React.useState(false)
  const [ AddTimeFromValidation, setAddTimeFromValidation ] = React.useState(false)
  const [ AddTimeToValidation, setAddTimeToValidation ] = React.useState(false)
  const [ AddDelayValidation, setAddDelayValidation ] = React.useState(false)


  // const [ AddStartTime, setAddStartTime ] = React.useState("")
  // const [ AddStopTime, setAddStopTime ] = React.useState("")

  const [ DeviceList, setDeviceList ] = React.useState([])
  const [ GroupDeviceList, setGroupDeviceList ] = React.useState([])

  // add group
  const [ GroupName, setGroupName ] = React.useState("") 
  const [ AccessDoorArr, setAccessDoorArr ] = React.useState([{
    doorID: undefined,
    isTimeInOut: false,
    timeBegin: "00:00",
    timeEnd: "23:59",
    isAllTime: false
  }])

  const [ ZipFileName, setZipFileName ] = React.useState("")
  const [ ZipFileValue, setZipFileValue ] = React.useState(null)

  const [ AddEmployeeName, setAddEmployeeName ] = React.useState("") 
  const [ AddEmployeeSurname, setAddEmployeeSurname ] = React.useState("") 
  const [ AddEmployeePostion, setAddEmployeePostion ] = React.useState("") 
  const [ AddEmployeeCard, setAddEmployeeCard ] = React.useState("") 
  const [ AddEmployeeProfile, setAddEmployeeProfile ] = React.useState(null) 
  const [ AddEmployeeEmail, setAddEmployeeEmail ] = React.useState("") 
  const [ AddEmployeePresonalID, setAddEmployeePresonalID ] = React.useState("") 

  const [ AddEmployeeNameValidation, setAddEmployeeNameValidation ] = React.useState(false) 
  const [ AddEmployeeSurnameValidation, setAddEmployeeSurnameValidation ] = React.useState(false) 
  const [ AddEmployeePostionValidation, setAddEmployeePostionValidation ] = React.useState(false) 
  const [ AddEmployeeCardValidation, setAddEmployeeCardValidation ] = React.useState(false) 
  const [ AddEmployeeProfileValidation, setAddEmployeeProfileValidation ] = React.useState(false) 
  const [ AddEmployeeEmailValidation, setAddEmployeeEmailValidation ] = React.useState(false) 
  const [ AddEmployeePresonalIDValidation, setAddEmployeePresonalIDValidation ] = React.useState(false) 

  const [ anchorElLogout, setanchorElLogout  ]  = React.useState(null)
  const [ openLogout, setopenLogout ] = React.useState(false)


  const [ openDialogComfirm, setopenDialogComfirm ] = React.useState(false)
  const [ TitleAction, setTitleAction ] = React.useState("")
  const [ OpenBackdrop, setOpenBackdrop ] = React.useState(false)
  const [ openDialogComfirmStatus, setopenDialogComfirmStatus ] = React.useState(true)
  const [ isOpenPersonalProfile, setIsOpenPersonalProfile ] = React.useState(false)
  const [ MessageError, setMessageError ] = React.useState("")


  const [ profileData, setProfileData ] = React.useState({})

  const [ isOpenChangePassword, setIsOpenChangePassword ] = React.useState(false)
  const [ oldPasswordChange, setOldPasswordChange ] = React.useState("")
  const [ newPasswordChange, setNewPasswordChange ] = React.useState("")
  const [ confirmNewPasswordChange, setConfirmNewPasswordChange ] = React.useState("")

  const [ isOldPasswordChangeVisible, setIsOldPasswordChangeVisible ] = React.useState(false)
  const [ isNewPasswordChangeVisible, setIsNewPasswordChangeVisible ] = React.useState(false)
  const [ isConfirmNewPasswordChangeVisible, setIsConfirmNewPasswordChangeVisible ] = React.useState(false)

  // for add device to employee
  const [ AccessDoorArrValidation, setAccessDoorArrValidation ] = React.useState(false)
  const [ SelectAll, setSelectAll ] = React.useState(false)

  const [ mustChangePass, setMustChangePass ] = React.useState(false)

  const history = useNavigate()

  const initTimePicker = (from_, to_) => {
    try{
      var from = new Date(from_);
      var to = new Date(to_);
      var tNumber = 0
      var elmnt = Object
      
      // For From
      tNumber = from.getHours()
      t1 = tNumber
      elmnt = document.getElementById("scroll-hours-from-" + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));
      elmnt.scrollIntoView();
      setHoursFrom((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString())

      tNumber = from.getMinutes()
      t2 = tNumber
      elmnt = document.getElementById("scroll-minutese-from-" + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));
      elmnt.scrollIntoView();
      setMinuteseFrom((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
  
      // For To
      tNumber = to.getHours()
      t3 = tNumber
      elmnt = document.getElementById("scroll-hours-to-" + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));
      elmnt.scrollIntoView();
      setHoursTo((Number(t3) < 10) ? ("0" + Number(t3).toString()): Number(t3).toString())
      tNumber = to.getMinutes()
      t4 = tNumber
      elmnt = document.getElementById("scroll-minutese-to-" + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));
      elmnt.scrollIntoView();
      setMinuteseTo((Number(t4) < 10) ? ("0" + Number(t4).toString()): Number(t4).toString())
    } catch (e) {
      console.log(e)
    }
  }

  const intiStateTime = () => {
    var from = new Date("2021-01-11T00:00:00.000+00:00")
    var to = new Date("2021-02-11T00:00:00.000+00:00")
    
    var h1 = from.getHours()
    var m1 = from.getMinutes()
    var h2 = to.getHours()
    var m2 = to.getMinutes()
    
    setTimeFrom(((Number(h1) < 10) ? ("0" + Number(h1).toString()): Number(h1).toString()) + ":" + ((Number(m1) < 10) ? ("0" + Number(m1).toString()): Number(m1).toString()) )
    setTimeTo(((Number(h2) < 10) ? ("0" + Number(h2).toString()): Number(h2).toString()) + ":" + ((Number(m2) < 10) ? ("0" + Number(m2).toString()): Number(m2).toString()) )

    setHoursFrom((Number(h1) < 10) ? ("0" + Number(h1).toString()): Number(h1).toString())
    setMinuteseFrom((Number(m1) < 10) ? ("0" + Number(m1).toString()): Number(m1).toString())
    setHoursTo((Number(h2) < 10) ? ("0" + Number(h2).toString()): Number(h2).toString())
    setMinuteseTo((Number(m2) < 10) ? ("0" + Number(m2).toString()): Number(m2).toString())

    t1 = h1
    t2 = m1
    t3 = h2
    t4 = m1
  }

  const handleCloseLogout = () => {
    setopenLogout(false)
  }

  React.useEffect(() => {
    const storeState = () => {

      setSEventStatus(Object.assign({}, store.getState().EventStatus))

      setEvent(store.getState().Event)

      setToken(store.getState().AccessToken)
      var list = []
      var device = store.getState().Device
      if (device !== undefined){
        device.forEach((item, index) => {
          list.push({
            device_id: item.device_id,
            device_name: item.gate.name_gate
          })
        })
      }
      setDeviceList(list)
    }
    var interval_ = setInterval(() => {
      storeState()
    }, 10000);
    intiStateTime()
    storeState()
    return () => {
      clearInterval(interval_)
    }
  }, [store])

  React.useEffect(() => {
    if (getGroupId() !== undefined) {
      if (store.getState().Group !== undefined) {
        let list = []
        let g_id = parseInt(getGroupId())
        let availableGate = store.getState().Group.filter(obj => obj.group_id === g_id)
        availableGate = availableGate[0].gates
        availableGate.forEach((item, index) => {
          list.push({
            device_id: item.gate_id,
            device_name: item.name
          })
        })
        setGroupDeviceList(list)
      }
    }
  }, [location.pathname])

  React.useEffect(() => {
    const fetchUserProfile = async () => {
      const res = await getMyProfile({
        query: `
          {
            GetProfile {
                user_id
                role {
                    role_name
                }
                username
                name
                email
                employee_code
                create_at
                update_at
                last_active
                update_password_at
            }
          }
        `
      }, Token)
      setProfileData(res.data.data.GetProfile)
    }
    fetchUserProfile()

  }, [Token])

  // Check if password must be updated
  React.useEffect(() => {
    if (store.getState().User !== undefined) {
      if (store.getState().User.update_password) {
        setIsOpenPersonalProfile(true)
        setMustChangePass(true)
      }
    }
  }, [store.getState().User])

  // Redirect to log in when user level attempts admin URL
  React.useEffect(() => {
    if (store.getState().User !== undefined) {
      if (location.pathname === '/dashboard/admin' && store.getState().User.perms === 'user') {
        history("/dashboard/status")
      }
    }
  }, [store.getState().User, location.pathname])

  const handleCloseopenMenuTimeTo = () => {
    setanchorElTimeTo(null);
    setopenMenuTimeTo(false)
  }

  const handleCloseopenMenuTimeFrom = () => {
    setanchorElTimeFrom(null);
    setopenMenuTimeFrom(false)
  }

  const handleCloseDialogAddGroup = () => {
    setopenDialogAddGroup(false)
  }

  const handleCloseDialogAddUserToGroup = () => {
    setopenDialogAddUserToGroup(false)
  }

  const handleCloseDialogMangeDV = () => {
    setopenDialogMangeDV(false);
  };

  const handleCloseDialogMangeAdmin = () => {
    setopenDialogMangeAdmin(false);
  };

  const handleClosePersonalProfile = () => {
    setIsOpenPersonalProfile(false)
  }

  const handleCloseChangePassword = () => {
    setIsOpenChangePassword(false)
  }

  const handleSearchEmployee = async () => {

    var query = store.getState().SearchEmployee
    var check = true
    
    if (
      new Date(`2020-01-01 ${query.search.starttime}`).getTime() 
      >= 
      new Date(`2020-01-01 ${query.search.endtime}`).getTime() 

      ||

      new Date(`${query.search.startdate} 00:00`).getTime() 
      >
      new Date(`${query.search.enddate} 00:00`).getTime() 
    ){
      check = false
      setTimeFromValidation(true)
      setTimeToValidation(true)
    } else {
      setTimeFromValidation(false)
      setTimeToValidation(false)
    }

    if (check){
      setOpenBackdrop(true)
      setBackDropOpen(false)
      setGlobalSearchOpen(false)
      query.flag = true
      query.search.offset = 0
      query.search.limit = 10
      store.dispatch(SEARCHEMPLOYEE(query))
      var str_device = ""
      if (query.search.device !== undefined){
        var AccessDoorArrFiltered = query.search.device.filter(function(x) {
          return x !== undefined && x !== "undefined";
        });
        str_device = JSON.stringify(AccessDoorArrFiltered.map((item) => {
          return Number(item)
        }))
      }
      const res_log = await getLog({
        query: `
          {
            GetRecord (
                `+((query.search.name !== "") ? `name: "`+query.search.name+`",`: ``)+`
                `+((query.search.surname !== "") ? `employee_code: "`+query.search.surname+`",`: ``)+`
  
                `+((query.search.card !== "") ? `mifareId: "`+query.search.card+`",`: ``)+`
                `+((query.search.startdate !== "") ? `beginDate: "`+query.search.startdate+`",`: ``)+`
                `+((query.search.starttime !== "") ? `beginTime: "`+query.search.starttime+`",`: ``)+`
                `+((query.search.enddate !== "") ? `endDate: "`+query.search.enddate+`",`: ``)+`
                `+((query.search.endtime !== "") ? `endTime: "`+query.search.endtime+`",`: ``)+`
  
                `+((query.search.device !== "" && query.search.device !== undefined) ? `enterGate: `+str_device+`,`: ``)+`
                `+((query.search.device !== "" && query.search.device !== undefined) ? `exitGate: `+str_device+`,`: ``)+`
                `+((query.search.offset !== "") ? `offset: `+query.search.offset+`,`: ``)+`
                `+((query.search.limit !== "") ? `limit: `+query.search.limit+`,`: ``)+`
              ) {
                results {
                  record_id
                  enter_time
                  enter_method
                  exit_method
                  enter_gate {
                      gate_id
                      name
                      begin_time
                      end_time
                  }
                  exit_time
                  exit_gate {
                      gate_id
                      name
                      begin_time
                      end_time
                  }
                  is_exited
                  employee {
                      employee_id
                      name
                      lastname
                      position
                      mifare_id
                      picture_url
                      face_id
                  }
                }
                count
              }
          }
        `
      }, Token)
      console.log(res_log)
      store.dispatch(LOG(res_log.data.data.GetRecord))
      setOpenBackdrop(false)
    }
  }

  const handleSearchDevice = async () => {
    var query = store.getState().SearchDevice
    query.flag = true
    store.dispatch(SEARCHDEVICE(query))
    const res_device = await getDevice ({
      query: `
      {
        GetDevice (gate_name: "${query.search.device}") {
            results {
                device_id
                gate_id
                device_status_id
                device_config_id
                serial_number
                tablet_enter_serial_number
                tablet_exit_serial_number
                create_at
                update_at
                gate {
                    gate_id
                    name_gate
                    begin_time
                    end_time
                    create_at
                    update_at
                }
                device_config {
                    device_config_id
                    buzzer_enabled
                    door_sensor_enabled
                    break_glass_enabled
                    buzzer_delay
                    two_factor_enabled
                }
                device_status {
                    buzzer
                    iot_box_status
                    tablet_enter_status
                    tablet_exit_status
                    door_status
                    break_glass
                }
            }
            count
        }
      }
      `
    }, Token)
    store.dispatch(DEVICE(res_device.data.data.GetDevice.results))
    setBackDropOpen(false)
    setGlobalSearchOpen(false)
  }

  const getGroupId = () => {
    var garr = location.pathname.split("/")
    var Group_id = garr[4]
    return Group_id
  }

  const getPath = () => {
    if (location.pathname === '/dashboard/status') {
      return "สถานะอุปกรณ์"
    } else if (location.pathname === '/dashboard/setting') {
      return "ตั้งค่าอุปกรณ์"
    } else if (location.pathname === '/dashboard/log') {
      return "ประวัติการเข้าออก"
    } else if (location.pathname.split("/").length === 5) {
      return (<div id={"show-group-name-" + getGroupId()}></div>)
    } else if (location.pathname === '/dashboard/user') {
      return "ตั้งค่าบัญชีผู้ใช้"
    } else if (location.pathname === '/dashboard/admin') {
      return "ระบบแอดมิน"
    }
  }


  const handleAddAdmin = async () => {
    const isEmail = (email) => {
      return (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email))
    }

    if (AddName === "") {
      setAddNameValidation(true)
    } else {
      setAddNameValidation(false)
    }
    if (AddEmployeeCode === "") {
      setAddEmployeeCodeValidation(true)
    } else {
      setAddEmployeeCodeValidation(false)
    }
    if (AddEmail === "" || !isEmail(AddEmail)) {
      setAddEmailValidation(true)
    } else {
      setAddEmailValidation(false)
    }
    if (AddUsername === "") {
      setAddUsernameValidation(true)
    } else {
      setAddUsernameValidation(false)
    }
    if (AddPassword === "") {
      setAddPasswordValidation(true)
    } else {
      setAddPasswordValidation(false)
    }
    if (AddConfirmPass === "") {
      setAddConfirmPassValidation(true)
    } else {
      setAddConfirmPassValidation(false)
    }
    if (AddName !== "" &&
        AddEmployeeCode !== "" &&
        AddEmail !== "" &&
        isEmail(AddEmail) &&
        AddUsername !== "" &&
        AddPassword !== "" &&
        AddConfirmPass !== "") {
          let anyPasswordError = PasswordChecker(AddPassword)
          if (anyPasswordError.length === 0) {
            if (AddPassword === AddConfirmPass) {
              setOpenBackdrop(true)
              setopenDialogMangeAdmin(false)

              setAddPassError(false)
              setAddPassCond([])
              const res = await AddAdmin({
                query: `
                  mutation{
                    CreateUser (
                      username: "${AddUsername}", 
                      password: "${AddPassword}",
                      name: "${AddName}", 
                      email: "${AddEmail}",
                      employee_code: "${AddEmployeeCode}", 
                      role_id : ${AddUserLevel}) 
                    {
                        result
                    }
                  }
                `
              }, Token)
              if (res.data.data.CreateUser.result) {
                const res_getUser = await getAdmin({
                  query: `
                    {
                      GetUserAdmin (orderBy:"create_at" , orderType:"desc") {
                        results {
                          user_id
                          role {
                              role_name
                          }
                          username
                          name
                          email
                          employee_code
                          create_at
                          update_at
                          last_active
                          update_password_at
                        }
                      }
                    }
                  `
                }, Token)
                store.dispatch(LISTUSER(res_getUser.data.data.GetUserAdmin.results))
                setopenDialogMangeAdmin(false)
                setAddUsername("")
                setAddPassword("")
                setAddConfirmPass("")
                setAddName("")
                setAddEmployeeCode("")
                setAddEmail("")
                setAddUserLevel(1001)
                setAddPassError(false)
                setAddPassCond([])
                setIsAddPasswordVisible(false)
                setIsAddConfirmPassVisible(false)

                setOpenBackdrop(false)
                setTitleAction(`เพิ่มบัญชีผู้ใช้ ${AddUsername}`)
                setopenDialogComfirm(true)
                setopenDialogComfirmStatus(true)
              } else {
                setOpenBackdrop(false)
                setopenDialogMangeAdmin(true)

                setTitleAction(res.data.errors[0].message)
                setopenDialogComfirm(true)
                setopenDialogComfirmStatus(false)
              }
            } else {
              setAddPassError(true)
              setAddPassCond([5]) // 5 means AddPassword not equals AddConfirmPass
            }
          } else {
            setAddPassError(true)
            setAddPassCond(anyPasswordError)
          }
    }
  }

  const handleAddGroup = async () => {
    var Token = store.getState().AccessToken
    var AccessDoorArrFiltered = [...new Set(AccessDoorArr.filter(function(x) {
      return x !== undefined && x !== "undefined";
    }))];
    const res = await addGroup({
      query: `
        mutation {
            CreateGroup(name: "`+GroupName+`", gates: `+JSON.stringify(AccessDoorArrFiltered)+`) {
                group_id
                name
                gates {
                    gate_id
                    name
                }
            }
        }
      `
    }, Token)
    if (res.data.errors === undefined){
      const res_group = await getGroup({
        query: `
          query {
            Group {
              group_id
              name
              gates {
                  gate_id
                  name
                  begin_time
                  end_time
              }
              employees {
                  employee_id
                  name
                  lastname
                  position
                  mifare_id
                  picture_url
                  face_id
              }
              group_gates {
                  gate_id
                  group_id
                  override_time
                  begin_time
                  end_time
              }
            }
        }
        `
      }, Token)
      if (res_group.data.errors === undefined){
        store.dispatch(GROUP(res.data.data.Group))
        setopenDialogAddGroup(false)
      }
    } else {
      alert(res.data.errors[0].message)
    }
  }

  const handleAddDevice = async () => {
    var Token = store.getState().AccessToken

    var checkAll = true

    var tablets_enter = AddSerialTabletsEnter
    var tablets_exit = AddSerialTabletsExit
    var iot_box = AddSerialIoTBox
    var doorname = AddDoorName
    var from = TimeFrom
    var to = TimeTo
    var alltime = AddCheckAlltime
    var door = AddDoorSensor
    var glass = AddBreakGlass
    var buzzer = AddBuzzer
    var delay = AddDelayBuzzerEnable

    // AddTimeFromValidation
    // AddTimeToValidation

    if (!alltime){
      if (new Date(`2020-02-02 ${from}`).getTime() >= new Date(`2020-02-02 ${to}`).getTime() ){
        checkAll = false
        setAddTimeFromValidation(true)
        setAddTimeToValidation(true)
      } else {
        setAddTimeFromValidation(false)
        setAddTimeToValidation(false)
      }
    } else {
      from = "00:00"
      to = "23:59"
    }

    if (tablets_enter === ""){
      setAddTabletsEnterValidation(true)
      checkAll = false
    }else {
      setAddTabletsEnterValidation(false)
    }

    if (tablets_exit === ""){
      setAddTabletsExitValidation(true)
      checkAll = false
    }else {
      setAddTabletsExitValidation(false)
    }

    if (iot_box === ""){
      setAddSerialNumberIoTBoxValidation(true)
      checkAll = false
    }else {
      setAddSerialNumberIoTBoxValidation(false)
    }

    if (doorname === ""){
      setAddDoorNameValidation(true)
      checkAll = false
    }else {
      setAddDoorNameValidation(false)
    }
    
    // if (delay === "0"){
    //   setAddDelayValidation(true)
    //   checkAll = false
    // }else {
    //   setAddDelayValidation(false)
    // }

    // if (delay === ""){
    //   setAddDelayValidation(true)
    //   checkAll = false
    // }else {
    //   setAddDelayValidation(false)
    // }


    // if (Number(delay) < 0){
    //   setAddDelayValidation(true)
    //   checkAll = false
    // }else {
    //   setAddDelayValidation(false)
    // }


    if (delay === "-1" || delay === "" || Number(delay) < 0){
      setAddDelayValidation(true)
      checkAll = false
    }else {
      setAddDelayValidation(false)
    }

    //// check
    
    if (
      // tablets_enter !== "" &&
      // tablets_exit !== "" &&
      // iot_box !== "" &&
      // doorname !== "" &&
      // (delay !== "0" && delay !== "" && Number(delay) > 0)
      checkAll
    ) {
      const checkIfDuplicateExists = (w) => {
        return new Set(w).size !== w.length 
      }
      if (!checkIfDuplicateExists([tablets_enter, tablets_exit, iot_box])){
        handleCloseDialogMangeDV()
        setOpenBackdrop(true)
        const res = await AddDeivce({
          query: `
            mutation {
              AddDevice(
                gateName: "${doorname}", 
                iotSerialNumber: "${iot_box}", 
                tabletEnterSerialNumber: "${tablets_enter}",
                tabletExitSerialNumber: "${tablets_exit}",
                doorSensor: ${door}, 
                breakGlass: ${glass}, 
                beginTime: "${ (alltime ? parseTimetoFulltime(new Date(), "00:00"): parseTimetoFulltime(new Date(), from))}",
                endTime: "${(alltime ? parseTimetoFulltime(new Date(), "23:59"): parseTimetoFulltime(new Date(), to))}",
                buzzerEnabled: ${buzzer},
                buzzerDelay: ${delay},
                ) {
                  result
              }
            }
          `
        }, Token)
        if (res.data.errors === undefined && res.data.data.AddDevice.result){
          const res_device = await getDevice({
            query: `
              {
                  GetDevice {
                      results {
                        device_id
                        gate_id
                        device_status_id
                        device_config_id
                        serial_number
                        tablet_enter_serial_number
                        tablet_exit_serial_number
                        create_at
                        update_at
                        gate {
                            gate_id
                            name_gate
                            begin_time
                            end_time
                            create_at
                            update_at
                        }
                        device_config {
                            device_config_id
                            buzzer_enabled
                            door_sensor_enabled
                            break_glass_enabled
                            buzzer_delay
                            two_factor_enabled
                        }
                        device_status {
                            buzzer
                            iot_box_status
                            tablet_enter_status
                            tablet_exit_status
                            door_status
                            break_glass
                        }
                      }
                  }
              }
            `
          }, Token)
          setAddCheckAlltime(false)

          store.dispatch(DEVICE(res_device.data.data.GetDevice.results))
          setOpenBackdrop(false)
          setTitleAction(`เพิ่มอุปกรณ์ ${doorname}`)
          setopenDialogComfirm(true)
          setopenDialogComfirmStatus(true)


          setAddSerialTabletsEnter("")
          setAddSerialTabletsExit("")
          setAddSerialIoTBox("")
          setAddDoorName("")
          setAddDelayBuzzerEnable("0")

        } else {
          alert(res.data.errors[0].message)
          setOpenBackdrop(false)
          setTitleAction(`ไม่สามารถเพิ่มอุปกรณ์ ${doorname}`)
          setopenDialogComfirm(true)
          setopenDialogComfirmStatus(false)
          setMessageError("")
        }
      } else {
        setOpenBackdrop(false)
        setTitleAction(`Serial Number ไม่สามารถซ้ำกันได้`)
        setopenDialogComfirm(true)
        setopenDialogComfirmStatus(false)

        setAddTabletsEnterValidation(true)
        setAddTabletsExitValidation(true)
        setAddSerialNumberIoTBoxValidation(true)

      }
    }

    // result
  }

  const getUnselectDevice = () => {
    return AccessDoorArr.filter((obj) => obj.doorID === undefined)
  }

  const handleAddEmployee = async () => {
    const isEmail = (email) => {
      return (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email))
    }

    // Validation
    if (AddEmployeeName === "") {
      setAddEmployeeNameValidation(true)
    } else {
      setAddEmployeeNameValidation(false)
    }
    if (AddEmployeeSurname === "") {
      setAddEmployeeSurnameValidation(true)
    } else {
      setAddEmployeeSurnameValidation(false)
    }
    if (AddEmployeePostion === "") {
      setAddEmployeePostionValidation(true)
    } else {
      setAddEmployeePostionValidation(false)
    }
    if (AddEmployeeCard === "") {
      setAddEmployeeCardValidation(true)
    } else {
      setAddEmployeeCardValidation(false)
    }
    if (AddEmployeeEmail === "" || !isEmail(AddEmployeeEmail)) {
      setAddEmployeeEmailValidation(true)
    } else {
      setAddEmployeeEmailValidation(false)
    }
    if (AddEmployeePresonalID === "") {
      setAddEmployeePresonalIDValidation(true)
    } else {
      setAddEmployeePresonalIDValidation(false)
    }
    if (AddEmployeeProfile === null) {
      setAddEmployeeProfileValidation(true)
    } else {
      setAddEmployeeProfileValidation(false)
    }
    if (getUnselectDevice().length === 0) {
      setAccessDoorArrValidation(false)
    } else {
      setAccessDoorArrValidation(true)
    }

    if (AddEmployeeName !== "" &&
        AddEmployeeSurname !== "" &&
        AddEmployeePostion !== "" &&
        AddEmployeeCard !== "" &&
        AddEmployeeEmail !== "" && 
        isEmail(AddEmployeeEmail) &&
        AddEmployeePresonalID !== "" &&
        AddEmployeeProfile !== null &&
        getUnselectDevice().length === 0) 
    {
      var name = AddEmployeeName
      var surname = AddEmployeeSurname
      var position = AddEmployeePostion
      var card = AddEmployeeCard
      var email = AddEmployeeEmail
      var personal = AddEmployeePresonalID
      var gates = [] 
      AccessDoorArr.forEach((obj) => {
        gates.push({
          gateId: obj.doorID,
          overrideTime: obj.isTimeInOut,
          beginTime: obj.timeBegin,
          endTime: obj.timeEnd
        })
      })

      let data = new FormData();
      data.append('name', name)
      data.append('lastname', surname)
      data.append('position', position)
      data.append('mifare_id', card)
      data.append('email', email)
      data.append('employee_code', personal)
      data.append('groups', '['+getGroupId()+']')
      data.append('gates', JSON.stringify(gates))
      data.append("picture", AddEmployeeProfile[0])

      try {
        setOpenBackdrop(true)
        setopenDialogAddUserToGroup(false)

        const res = await addEmployee(data, Token)
        if (res.status !== 200){
          alert(res.status)
        } else {
          if (res.data.error_code === undefined) {
            const res = await getEmployeeByGroup({
              query: `
              {
                Employee (groups: [`+getGroupId()+`]) {
                  results {
                      employee_id
                      name
                      lastname
                      position
                      mifare_id
                      employee_code
                      picture_url
                      email
                      groups {
                          group_id
                          name
                      }
                      gates {
                          gate_id,
                          name_gate,
                          begin_time,
                          end_time,
                      }
                      employee_gates {
                          gate_id
                          override_time
                          begin_time
                          end_time
                      }
                  }
                  count
                }     
              } 
              `,
            }, Token)
            store.dispatch(EMPLOYEE(res.data.data.Employee.results))
            setopenDialogAddUserToGroup(false)
            initStateFormAddEmployee()

            setOpenBackdrop(false)
            setTitleAction(`เพิ่มพนักงาน ${name} ${surname}`)
            setopenDialogComfirm(true)
            setopenDialogComfirmStatus(true)
          }
        }
      } catch (error) {
        setOpenBackdrop(false)
        setopenDialogAddUserToGroup(true)
        if (error.response !== undefined) {
          setTitleAction(error.response.data.message)
        } else {
          setTitleAction(error)
        }
        setopenDialogComfirm(true)
        setopenDialogComfirmStatus(false)
      }
    }
    /*var Token = store.getState().AccessToken
    var AccessDoorArrFiltered = [...new Set(AccessDoorArr.filter(function(x) {
      return x !== undefined && x !== "undefined";
    }))];
    var name = AddEmployeeName
    var surname = AddEmployeeSurname
    var postion = AddEmployeePostion
    var card = AddEmployeeCard
    var email = AddEmployeeEmail
    var personal = AddEmployeePresonalID
    
    let data = new FormData();
    data.append('name', name)
    data.append('lastname', surname)
    data.append('position', postion)
    data.append('mifare_id', card)


    data.append('email', email)
    data.append('personal_id', personal)

    data.append('groups', '['+getGroupId()+']')
    data.append('gates', JSON.stringify(AccessDoorArrFiltered))
    data.append("picture", AddEmployeeProfile[0]);

    const res = await addEmployee(data, Token)
    
    if (res.data.name !== undefined){
      const res = await getEmployeeByGroup({
        query: `
          query {
              Group(groupId: `+getGroupId()+`) {
                  group_id
                  name
                  gates {
                      gate_id
                      name
                      begin_time
                      end_time
                  }
                  employees {
                      employee_id
                      name
                      lastname
                      position
                      mifare_id
                      picture_url
                      face_id
                      email
                      personal_id
                  }
              }
          }
        `,
      }, Token)
      if (res.data.errors === undefined){
        store.dispatch(EMPLOYEE(res.data.data.Group))
        setopenDialogAddUserToGroup(false)
      }
    }*/ 
  }

  const handleChangePassword = async () => {
    let anyPasswordError = PasswordChecker(newPasswordChange)
    if (anyPasswordError.length === 0) {
      if (newPasswordChange === confirmNewPasswordChange) {
        setAddPassError(false)
        setAddPassCond([])
        const res = await ChangePassword({
          query: `
            mutation{
              EditMyPassword (
                oldPassword: "${oldPasswordChange}", 
                newPassword: "${newPasswordChange}" ) {
                  result
              }
            }
          `
        }, Token)
        if (res.data.data.EditMyPassword.result === true) {
          alert("เปลี่ยนรหัสผ่านสำเร็จ")
          window.location.href = "/"
          setIsOpenChangePassword(false)
          setOldPasswordChange("")
          setNewPasswordChange("")
          setConfirmNewPasswordChange("")
          setAddPassError(false)
          setAddPassCond([])
        } else {
          if (res.data.errors[0].message === "MATCHED OLD PASSWORD") {
            alert("ไม่สามารถใช้รหัสผ่านเก่าได้")
          } else {
            alert("ไม่สามารถเปลี่ยนรหัสผ่านได้")
          }
          setAddPassError(true)
        }
      } else {
        setAddPassError(true)
        setAddPassCond([5]) // 5 means AddPassword not equals AddConfirmPass
      }
    } else {
      setAddPassError(true)
      setAddPassCond(anyPasswordError)
    }
  }

  const initStateFormAddEmployee = () => {
    setAddEmployeeName("") 
    setAddEmployeeSurname("") 
    setAddEmployeePostion("") 
    setAddEmployeeCard("") 
    setAddEmployeeProfile(null) 
    setAddEmployeeEmail("") 
    setAddEmployeePresonalID("") 
    setAddEmployeeNameValidation(false) 
    setAddEmployeeSurnameValidation(false) 
    setAddEmployeePostionValidation(false) 
    setAddEmployeeCardValidation(false) 
    setAddEmployeeProfileValidation(false) 
    setAddEmployeeEmailValidation(false) 
    setAddEmployeePresonalIDValidation(false) 
    setSelectAll(false)
    setAccessDoorArr([{
      doorID: undefined,
      isTimeInOut: false,
      timeBegin: "00:00",
      timeEnd: "23:59",
      isAllTime: false
    }])
    setAccessDoorArrValidation(false)
    setAddGroupNameValidation(false)
    setImageFile(null)
  }
  
  const handleSetAllTime = (value, index) => {
    let ada = AccessDoorArr
    ada[index].isAllTime = value
    if (value) {
      ada[index].timeBegin = "00:00"
      ada[index].timeEnd = "23:59"
    }
    setAccessDoorArr(Object.assign([], ada))
  }

  const onTimeFrom = (time, index) => {
    let ada = AccessDoorArr
    ada[index].timeBegin = time
    setAccessDoorArr(Object.assign([], ada))
    handleSetAllTime(false, index)
  }

  const onTimeTo = (time, index) => {
    let ada = AccessDoorArr
    ada[index].timeEnd = time
    setAccessDoorArr(Object.assign([], ada))
    handleSetAllTime(false, index)
  }

  const handleSetTimeInOut = (value, index) => {
    let ada = AccessDoorArr
    ada[index].isTimeInOut = value
    setAccessDoorArr(Object.assign([], ada))
  }

  const handleCloseAlert = () => {
    setanchorElAlert(null)
  }

  const updateEmployee = async () => {
    const res = await getEmployeeByGroup({
      query: `
      {
        Employee (groups: [`+getGroupId()+`]) {
          results {
              employee_id
              name
              lastname
              position
              mifare_id
              employee_code
              picture_url
              email
              groups {
                  group_id
                  name
              }
              gates {
                  gate_id,
                  name_gate,
                  begin_time,
                  end_time,
              }
              employee_gates {
                  gate_id
                  override_time
                  begin_time
                  end_time
              }
          }
          count
        }     
      }
        
      `,
    }, Token)
    if (res.data.errors === undefined){
      store.dispatch(EMPLOYEE(res.data.data.Employee.results))
    } 
  }

  const handleUploadFileZip = async () => {
    const Group = getGroupId()

    var data = new FormData();
    data.append('file_zip', ZipFileValue);
    data.append('groupId', Group);

    var res;
    try {
      res = await UploadZip(data, Token)
    }catch(e) {
      res = e.response
    }
    console.log(res)
    if (res.status < 500){
      // success
      setopenDialogComfirm(true)
      setopenDialogComfirmStatus(true)
      setTitleAction(`อัพโหลดไฟล์ ${ZipFileName} สำเร็จแล้ว`)
      setMessageError("")
      updateEmployee()
    }else {
      // unsuccess
      setopenDialogComfirm(true)
      setopenDialogComfirmStatus(false)
      setTitleAction(`อัพโหลดไฟล์ ${ZipFileName} ไม่สำเร็จ`)
      setMessageError(res.data.message)
    }

    setZipFileName("")
    setZipFileValue(null)

    // call api
    // setOpenBackdrop(trueout(() => {
    //   setOpenBackdrop(false)
    //   // success
    //   // setopenDialogComfirm(true)
    //   // setopenDialogComfirmStatus(true)
    //   // setTitleAction(`อัพโหลดไฟล์ ${ZipFileName} สำเร็จแล้ว`)
    //   // setMessageError("")

    //   // unsuccess
    //   setopenDialogComfirm(true)
    //   setopenDialogComfirmStatus(false)
    //   setTitleAction(`อัพโหลดไฟล์ ${ZipFileName} ไม่สำเร็จ`)
    //   setMessageError("กรุณาตรวจสอบข้อมูลภายในไฟล์")


    //   setZipFileName("")
    // }, 2000);)
    // setTime
  }

  const handlesetbackdrop = (value) => {
    setOpenBackdrop(value)
  }

  const MangeReload = () => {
    if (location.pathname === '/dashboard/log') {
      return <ReloadDataLog onBackdrop={handlesetbackdrop} store={store} />
    } else if ((location.pathname).split("/").length === 5) {
      return <ReloadDataEmp onBackdrop={handlesetbackdrop} store={store} group_id={getGroupId()}/>
    }
  }

  return (
    <DashboardNavbarRoot>

      {/* Globle Search */}
      <CustomeBlackdrop 
      className={((BackDropOpen) ? navbarclasses.Visible: navbarclasses.InVisible)}
      onClick={() => {
        var query = store.getState().SearchEmployee
        if (

          new Date(`2020-01-01 ${query.search.starttime}`).getTime() 
          >= 
          new Date(`2020-01-01 ${query.search.endtime}`).getTime() 

          ||

          new Date(`${query.search.startdate} 00:00`).getTime() 
          >
          new Date(`${query.search.enddate} 00:00`).getTime()
          
        ) {

        } else {
          setBackDropOpen(false)
          setGlobalSearchOpen(false)
        }
      }}>
      </CustomeBlackdrop>

      <SearchingForm 
        className={((GlobalSearchOpen) ? navbarclasses.Visible: navbarclasses.InVisible)}
      >

        {/* Title and Exit */}
        <TitleAndExite>
          <Title>
            ค้นหา
          </Title>
          <IconButton 
            style={{
              height: 44
            }}
            onClick={() => {

              var query = store.getState().SearchEmployee
              if (

                new Date(`2020-01-01 ${query.search.starttime}`).getTime() 
                >= 
                new Date(`2020-01-01 ${query.search.endtime}`).getTime() 

                ||

                new Date(`${query.search.startdate} 00:00`).getTime() 
                >
                new Date(`${query.search.enddate} 00:00`).getTime()
                
              ) {

              } else {
                setBackDropOpen(false)
                setGlobalSearchOpen(false)
              }
            }}
          >
            <img src={EXIT} alt={EXIT} />
          </IconButton>

        </TitleAndExite>

        {location.pathname === '/dashboard/log' ?
          <SearchEmployee store={store} /> :
         location.pathname === '/dashboard/setting' ?
          <SearchDevice store={store} /> : null
        }

        {/* Confirm And Exit Search */}
        <CustomeContentBody>
          <CustomeContentSpace />

            {/* <FlexForTimeSearch> */}
            <CustomeContentButton
              style={{
                width: "47.5%",
                height: 60,
                borderColor: "#01A6E6",
                backgroundColor: "#fff",
                color: "#01A6E6",
                marginRight: "5px"
              }}
              onClick={() => {
                var query = store.getState().SearchEmployee
                if (

                  new Date(`2020-01-01 ${query.search.starttime}`).getTime() 
                  >= 
                  new Date(`2020-01-01 ${query.search.endtime}`).getTime() 

                  ||

                  new Date(`${query.search.startdate} 00:00`).getTime() 
                  >
                  new Date(`${query.search.enddate} 00:00`).getTime()
                  
                ) {

                } else {
                  setBackDropOpen(false)
                  setGlobalSearchOpen(false)
                  if (location.pathname === '/dashboard/setting') {
                    store.dispatch(SEARCHDEVICE({
                      flag: false,
                      search: { device: "" }
                    }))
                  }
                }
              }}
            > ยกเลิก</CustomeContentButton>
            <CustomeContentButton
              style={{
                width: "47.5%",
                height: 60,
                borderColor: "#01A6E6",
                backgroundColor: "#01A6E6",
                color: "#fff",
                marginLeft: "5px"
              }}
              onClick={(e) => {
                if (location.pathname === '/dashboard/log') {
                  handleSearchEmployee()
                } else if (location.pathname === '/dashboard/setting') {
                  handleSearchDevice()
                }
              }}
            >
              ค้นหา
            </CustomeContentButton>

          <CustomeContentSpace />
        </CustomeContentBody>


      </SearchingForm>

      <DashboardMenuName >
        <MenuName>{ getPath() }</MenuName>
      </DashboardMenuName>
      <DashboardMenuButton >

        {
          MangeReload()
        }
        
        <IconButton
          aria-controls="customized-menu"
          aria-haspopup="true"
          variant="contained"
          color="primary"
          onClick={(e) => {
            setanchorElAlert(e.currentTarget)
            var EventStatus_ = store.getState().EventStatus
            EventStatus_.old = EventStatus_.new
            store.dispatch(EventStatus(EventStatus_))
            setSEventStatus(Object.assign({}, EventStatus_))
          }}
        >
          {/* <Icon alt="BELL_ALERT" src={BELL_ALERT} /> */}
          {
            // ((store.getState().EventStatus.init) ? ((store.getState().EventStatus.new >= store.getState().EventStatus.old) ? <Icon alt="BELL_NO_ALERT" src={BELL_NO_ALERT} />: <Icon alt="BELL_ALERT" src={BELL_ALERT} />): <Icon alt="BELL_ALERT" src={BELL_ALERT} />)
            ((SEventStatus.init) ? ((SEventStatus.new <= SEventStatus.old) ? <Icon alt="BELL_NO_ALERT" src={BELL_NO_ALERT} />: <Icon alt="BELL_ALERT" src={BELL_ALERT} />): <Icon alt="BELL_NO_ALERT" src={BELL_NO_ALERT} />)

          }
          {/* <Icon alt="BELL_NO_ALERT" src={BELL_NO_ALERT} /> */}
        </IconButton>
        

        {
          (location.pathname === "/dashboard/user" ? 
            <AddGroup 
              style={{display: ((location.pathname !== '/dashboard/setting' && location.pathname !== '/dashboard/admin' && location.pathname !== '/dashboard/user' && ((location.pathname).split("/").length !== 5)) ? "none": "block")}}
              store={store}
            /> 
          :
            <IconButton style={{display: ((location.pathname !== '/dashboard/setting' && location.pathname !== '/dashboard/admin' && location.pathname !== '/dashboard/user' && ((location.pathname).split("/").length !== 5)) ? "none": "block")}} onClick={() => {
              if (location.pathname === '/dashboard/setting') {
                setAddCheckAlltime(true)
                setTimeFrom("00:00")
                setTimeTo("23:59")

                setAddTabletsEnterValidation(false)
                setAddTabletsExitValidation(false)
                setAddSerialNumberIoTBoxValidation(false)

                setopenDialogMangeDV(true)
              } else if (location.pathname === '/dashboard/admin' && store.getState().User.perms === String.fromCharCode(97, 100, 109, 105, 110)) {
                setopenDialogMangeAdmin(true)
              } else if (location.pathname === '/dashboard/user') {
                setopenDialogAddGroup(true)
                // add group access device
              } else if ((location.pathname).split("/").length === 5) {
                setopenDialogAddUserToGroup(true)
                // add user to group device
              }
            }}>
              <Icon alt="ADD" src={ADD} />
            </IconButton>
          )
        }

        <IconButton onClick={() => {
          setBackDropOpen(true)
          setGlobalSearchOpen(true)
        }} style={{display: ((location.pathname === '/dashboard/log') || 
        (location.pathname === '/dashboard/setting') ? "block": "none")}}>
          <Icon alt="SEARCH" src={SEARCH} />
        </IconButton>

        <Deivider></Deivider>

        <IconButton 
        onClick={(event) => {
          setanchorElLogout(event.currentTarget)                                                         
          setopenLogout(true)
        }}>
          <Icon alt="USER_PEOPLE" src={USER_PEOPLE} style={{width: "44px", height: "44px", borderRadius: "15px"}}/>
        </IconButton>

      </DashboardMenuButton>
      

      {
        (anchorElAlert===null ? null: <EventComponent store={store} anchorElAlert={anchorElAlert} handleCloseAlert={handleCloseAlert}/>)
      }


      {/* <StyledMenu
        id="customized-menu"
        anchorEl={anchorElAlert}
        keepMounted
        open={Boolean(anchorElAlert)}
        onClose={handleCloseAlert}
      >

        {
          (Event !== undefined ? Event.results.map((item) => {
            return <StyledMenuItem>
              <ListItemIcon>
                {
                  (item.type === "alert" ? <img alt="UNNORMAL" src={UNNORMAL} />: <img alt="NORMAL" src={NORMAL} />)
                }
              </ListItemIcon>
              <ListItemText >
                <div style={{fontSize: 18}}>{item.topic}</div>
                <div style={{
                  display: "flex",
                  justifyContent: "space-between"
                }}>
                  <div style={{fontSize: 16}}>{item.message}</div>
                  <div style={{
                    paddingLeft: 90
                  }}>{
                    parseDate(item.time) + " " + parseTime(item.time)
                  }</div>
                </div>
              </ListItemText>
            </StyledMenuItem>
          }): null)
        }

            <StyledMenuItem>
              <div style={{
                textAlign: "center", 
                width: "100%", 
                color: "#01a6e6",
                textDecorationLine: "underline",
                cursor: "pointer"
                }}
                onClick={() => {
                  handleRequestEvent10item()
                }}
                >เพิ่มเติม</div>
            </StyledMenuItem>
      </StyledMenu> */}


      {/* Dialog for ManageDevice */}
      <Dialog
        disableBackdropClick={true}
        fullWidth={false}
        maxWidth={"xl"}
        open={openDialogMangeDV}
        onClose={handleCloseDialogMangeDV}
        aria-labelledby="update-dialog-MangeDV"
      >
        <CustomeExitDialogManageDV>
          <IconButton onClick={() => {
            setopenDialogMangeDV(false)
          }}>
            <img src={Exit} alt="Exit" />
          </IconButton>
        </CustomeExitDialogManageDV>
        <CustomeDialogTitleManageDV 
        id="update-dialog-MangeDV"
        children={<div style={{
          fontSize: 28,
          textAlign: "center",
          width: 400
        }}>เพิ่มอุปกรณ์</div>}
        ></CustomeDialogTitleManageDV>
        <CustomeDialogContentManageDV>
        
        <BodyManageDV style={{
          paddingTop: "30px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>Serial No.Tablets Enter</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>
            
            <InputFromId 
              className={classes.InputIdControl}
              variant="outlined" 
              labelwidth={0}
              placeholder="Serial Number Tablets Enter" 
              fullWidth={true}
              autoComplete='off'
              error={AddTabletsEnterValidation}
              onChange={(e) => {
                setAddSerialTabletsEnter(e.target.value)
              }}
            />

          </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>

        {/* Serial Tablet Exit */}
        <BodyManageDV style={{
          paddingTop: "0px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>Serial No.Tablets Exit</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>
            
            <InputFromId 
              className={classes.InputIdControl}
              variant="outlined" 
              labelwidth={0}
              placeholder="Serial Number Tablets Exit" 
              fullWidth={true}
              autoComplete='off'
              error={AddTabletsExitValidation}
              onChange={(e) => {
                setAddSerialTabletsExit(e.target.value)
              }}
            />

          </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>


        {/* Serial No. IoT Box */}
        <BodyManageDV style={{
          paddingTop: "0px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>Serial No.IoT Box</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>
            
            <InputFromId 
              className={classes.InputIdControl}
              variant="outlined" 
              labelwidth={0}
              placeholder="Serial Number IoT Box" 
              fullWidth={true}
              autoComplete='off'
              error={AddSerialNumberIoTBoxValidation}
              onChange={(e) => {
                setAddSerialIoTBox(e.target.value)
              }}
            />

          </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
      </BodyManageDV>


        <BodyManageDV style={{
          paddingTop: "0px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>ตำแหน่งประตู</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>

            <InputFromId 
              className={classes.InputIdControl}
              variant="outlined" 
               
              labelwidth={0}
              placeholder="กรอกตำแหน่งประตู" 
              fullWidth={true}
              autoComplete='off'
              error={AddDoorNameValidation}
              onChange={(e) => {
                setAddDoorName(e.target.value)
              }}
            />

          </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>

        <BodyManageDV style={{
          paddingTop: "5px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>ช่วงเวลาเปิดปิดประตู</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>
            <Fromto>จาก</Fromto>
            <InputFromId 
              className={classes.InputIdControl}
              variant="outlined" 
              labelwidth={0}
              placeholder="00:00" 
              fullWidth={true}
              autoComplete='off'
              value={TimeFrom}
              error={AddTimeFromValidation}
              disabled={AddCheckAlltime}
              onClick={(e) => {
                if (!AddCheckAlltime){
                  setanchorElTimeFrom(e.currentTarget);
                  setopenMenuTimeFrom(true)
                  initTimePicker("2021-01-11T00:00:00.000+00:00", "2021-02-11T00:00:00.000+00:00")
                }
              }}
            />
            <Fromto>ถึง</Fromto>
            <InputFromId 
              className={classes.InputIdControl}
              variant="outlined" 
              error={AddTimeToValidation}
              labelwidth={0}
              placeholder="00:00" 
              fullWidth={true}
              autoComplete='off'
              value={TimeTo}
              disabled={AddCheckAlltime}
              onClick={(e) => {
                if (!AddCheckAlltime) {
                  setanchorElTimeTo(e.currentTarget);
                  setopenMenuTimeTo(true)
                  initTimePicker("2021-01-11T00:00:00.000+00:00", "2021-02-11T00:00:00.000+00:00")
                }
              }}
            />

          </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>

          
          </BodyManageDV>


          {/* select all time */}
          <BodyManageDV style={{
            
            }}>
            <BodySpace style={{
              
            }}></BodySpace>
            <BodyLabel style={{
              textAlign: "left"
            }}></BodyLabel>
            <BodyForm style={{
              
            }}>

            <FromAllTime>
              <Checkbox checked={AddCheckAlltime} color={"primary"} onChange={(e) => {  
                setAddCheckAlltime(e.target.checked )
              }}/>
              <FromAllTimeLabel>
                All Time
              </FromAllTimeLabel>
            </FromAllTime>

            </BodyForm>
            <BodySpace style={{
              
            }}></BodySpace>
            </BodyManageDV>




        {/* Door Sensor */}
        <BodyManageDV style={{
          paddingTop: "5px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>Door Sensor</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>

            <FormControl variant="outlined" className={classes.formControl}>
              <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                input={<BootstrapInput />}
                value={AddDoorSensor}
                onChange={(e) => {
                  setAddDoorSensor(e.target.value)
                }}
              >
                <MenuItem value={true}>Enable</MenuItem>
                <MenuItem value={false}>Disable</MenuItem>
              </Select>
            </FormControl>

          </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>

        {/* Break Glass */}
        <BodyManageDV style={{
          paddingTop: "5px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>Break Glass</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>

            <FormControl variant="outlined" className={classes.formControl}>
              <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                input={<BootstrapInput />}
                value={AddBreakGlass}
                onChange={(e) => {
                  setAddBreakGlass(e.target.value)
                }}
              >
                <MenuItem value={true}>Enable</MenuItem>
                <MenuItem value={false}>Disable</MenuItem>
              </Select>
            </FormControl>

          </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>

        <BodyManageDV style={{
          paddingTop: "5px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>Buzzer</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>

          
            <FormControl variant="outlined" className={classes.formControl}>
              <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                input={<BootstrapInput />}
                value={AddBuzzer}
                onChange={(e) => {
                  setAddBuzzer(e.target.value)
                }}
              >
                <MenuItem value={true}>Enable</MenuItem>
                <MenuItem value={false}>Disable</MenuItem>
              </Select>
            </FormControl>

          </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>

        {/* Option Enable Buzzer */}
        {AddBuzzer ?
                  
          <BodyManageDV style={{
              
          }}>
          <BodySpace style={{
            
          }}></BodySpace>
          <BodyLabel style={{
            textAlign: "left"
          }}></BodyLabel>
          <BodyForm style={{
            paddingTop: "10px"
          }}>

          <FromAllTime>
            <FromAllTimeLabel>
              Delay
            </FromAllTimeLabel>
          </FromAllTime>

          <InputFromId 
            className={classes.InputIdControl}
            variant="outlined" 
            type="number"
            labelwidth={0}
            placeholder="10" 
            fullWidth={true}
            autoComplete='off'
            placeholder="กรอก Delay"
            value={AddDelayBuzzerEnable}
            style={{width: "100%"}}
            error={AddDelayValidation}
            onChange={(e) => {
              let reg = /^\d+$/;

              var value = e.target.value
              if (!reg.test(value) || (Number(value) < 0)){
                alert("กรุณาใส่ delay ให้ถูกต้อง")
              } else {
                if (e.target.value == 0) {
                  setAddDelayBuzzerEnable(e.target.value)
                } else {
                  setAddDelayBuzzerEnable(e.target.value.replace(/^0+/, ''))
                }
              }
            }}
          />
          <FromAllTime>
            <FromAllTimeLabel>
              วินาที
            </FromAllTimeLabel>
          </FromAllTime>

          </BodyForm>
          <BodySpace style={{
            
          }}></BodySpace>
          </BodyManageDV>
        :
          null
        }

        {/* Edit Factor */}
        <BodyManageDV style={{
          paddingTop: "5px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>Factor</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>
            <FormControl variant="outlined" className={classes.formControl}>
              <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                input={<BootstrapInput />}
                value={AddFactor}
                onChange={(e) => {
                  setAddFactor(e.target.value)
                }}
              >
                <MenuItem value={false}>1 Factor Authentication</MenuItem>
                <MenuItem value={true}>2 Factors Authentication</MenuItem>
              </Select>
            </FormControl>
          </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>


        <ActionManageDV>
          <div style={{
            display: "flex",
            justifyContent: "space-between",
            width: "100%",
            padding: "20px calc(10% + 30px) 0px calc(10% + 30px)"
          }}>
            <Choice1Btn style={{
              width: 250, 
              height: 60
            }} onClick={() => {
              handleCloseDialogMangeDV()
              setAddCheckAlltime(false)
            }}>ยกเลิก</Choice1Btn>

            <Choice2Btn 
              style={{
                width: 250, 
                height: 60
              }}
              onClick={() => {
                
                handleAddDevice()
              }}
            > เพิ่มอุปกรณ์ </Choice2Btn>
          </div>
        </ActionManageDV>
        </CustomeDialogContentManageDV>
      </Dialog>

      {/* Dialog for Add Group */}
      <Dialog
        disableBackdropClick={true}
        fullWidth={false}
        maxWidth={"xl"}
        open={openDialogAddGroup}
        onClose={handleCloseDialogAddGroup}
        aria-labelledby="update-dialog-MangeDV"
      >
        <CustomeExitDialogManageDV>
          <IconButton onClick={() => {
            setopenDialogAddGroup(false)
          }}>
            <img src={Exit} alt="Exit" />
          </IconButton>
        </CustomeExitDialogManageDV>
        <CustomeDialogTitleManageDV 
        id="update-dialog-MangeDV"
        children={<div style={{
          fontSize: 28,
          textAlign: "center",
          width: 400
        }}>เพิ่มกลุ่มผู้ใช้</div>}
        ></CustomeDialogTitleManageDV>
        <CustomeDialogContentManageDV>
        


        <BodyManageDV style={{
          paddingTop: "30px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>ชื่อกลุ่มผู้ใช้</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>
            
            <InputFromId 
              className={classes.InputIdControl}
              variant="outlined" 
              error={AddGroupNameValidation}
              labelwidth={0}
              placeholder="ชื่อกลุ่มผู้ใช้" 
              fullWidth={true}
              autoComplete='off'
              value={GroupName}
              onChange={(e) => {
                setGroupName(e.target.value)
              }}
            />

          </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>


        {
          AccessDoorArr.map((item, index) => {
            return <BodyManageDV 
              style={
                ((index === 0) ? {
                  paddingTop: "30px",
                  display: "flex",
                  justifyContent: "center"
                } : {
                  display: "flex",
                  justifyContent: "center"
                })
              }
              key={index.toString()}
            >
              <BodySpace style={{
                padding: "10px 10px 0px 10px"
              }}></BodySpace>
              <BodyLabel style={{
                padding: "10px 10px 0px 10px",
                textAlign: "left"
              }}> {((index === 0) ? "สิทธิ์เข้าออกประตู": null)} </BodyLabel>
              <BodyForm style={{
                padding: "10px 10px 0px 10px"
              }}>
                
    
                <FormControl variant="outlined" className={classes.formControl}>
                  <Select
                    labelId="demo-simple-select-outlined-label"
                    id="demo-simple-select-outlined"
                    input={<BootstrapInput />}
                    value={((AccessDoorArr[index] === undefined) ? "undefined": AccessDoorArr[index])}
                    onChange={(e) => {
                      var ada = AccessDoorArr
                      ada[index] = e.target.value
                      setAccessDoorArr(Object.assign([], ada))
                    }}
                  >
                     <MenuItem value={"undefined"}>{ "กรุณาเลือกสิทธิ์เข้าออกประตู" }</MenuItem>
                    {
                      DeviceList.map((item, index) => {
                        return (
                          <MenuItem key={index.toString()} value={item.device_id}>{ item.device_name }</MenuItem>
                        )
                      })
                    }
                  </Select>
                </FormControl>
                    
              </BodyForm>
              <BodySpace style={{
                // padding: "10px 10px 0px 10px",
                padding: "10px 0px 10px 0px",
                width: "12%"
              }}>

                {
                  ((index === AccessDoorArr.length - 1) ? <div>

                    <img 
                      src={ADDGROUP} 
                      alt="ADDGROUP" 
                      style={{
                        width: 20,
                        heigth: 20,
                        padding: "10px 20px 10px 0px"
                      }}
                      onClick={() => {
                        var ada = AccessDoorArr
                        ada.push(undefined)
                        setAccessDoorArr(Object.assign([], ada))
                      }}
                    />

                    <img 
                      src={REMOVEDOORACCESS} 
                      alt="REMOVEDOORACCESS" 
                      style={{
                        width: 20,
                        heigth: 20,
                        padding: "10px 20px 10px 0px"
                      }}
                      onClick={() => {
                        var ada = AccessDoorArr
                        if (ada.length !== 1){
                          ada.pop()
                          setAccessDoorArr(Object.assign([], ada))
                        }
                      }}
                    />

                  </div>: null)
                }
    
              </BodySpace>
            </BodyManageDV>
    
          })
        }

        <ActionManageDV>
          <div style={{
            display: "flex",
            justifyContent: "space-between",
            width: "100%",
            padding: "20px calc(10% + 30px) 0px calc(10% + 30px)"
          }}>
            <Choice1Btn style={{
              width: 250, 
              height: 60
            }} onClick={() => {
              setopenDialogAddGroup(false)
            }}>ยกเลิก</Choice1Btn>
            <Choice2Btn 
              style={{
                width: 250, 
                height: 60
              }}
              onClick={() => {
                handleAddGroup()
              }}
            >เพิ่มกลุ่มผู้ใช้</Choice2Btn>
          </div>
        </ActionManageDV>

        </CustomeDialogContentManageDV>
      </Dialog>


      {/* Add User to Group */}
      <Dialog
        disableBackdropClick={true}
        fullWidth={false}
        maxWidth={"xl"}
        open={openDialogAddUserToGroup}
        onClose={handleCloseDialogAddUserToGroup}
        aria-labelledby="update-dialog-MangeDV"
        >
        <CustomeExitDialogManageDV>
          <IconButton onClick={() => {
            setopenDialogAddUserToGroup(false)
            initStateFormAddEmployee()
          }}>
            <img src={Exit} alt="Exit" />
          </IconButton>
        </CustomeExitDialogManageDV>
        <CustomeDialogTitleManageDV 
        id="update-dialog-MangeDV"
        children={<div style={{
          fontSize: 28,
          textAlign: "center",
          width: 400
        }}>เพิ่มพนักงาน</div>}
        ></CustomeDialogTitleManageDV>
        <CustomeDialogContentManageDV>

        <div 
          style={{
            paddingTop: 20,
          }}
          onClick={() => {
            inputFile.current.click();
          }}
        >
          <img src={((ImageFile === null) ? UNSELECTEMPY : ImageFile)} alt="UNSELECTEMPY" width={150 } height={150} style={{border: "1px solid " + (AddEmployeeProfileValidation ? "red": "#adb0b13d"), cursor: 'pointer' }}/>
        </div>

        <input 
            type='file' 
            id='file' 
            accept="image/png, image/jpeg"
            ref={inputFile} 
            style={{display: 'none'}}
            onChange={ async (e) => {
              if (e.target.files[0] !== undefined) {
                const profileShow = URL.createObjectURL(e.target.files[0])
                setAddEmployeeProfile([e.target.files[0], e.target.files[0].name])
                setImageFile(profileShow)
              }
              // e.preventDefault()
              // const reader = new FileReader()
              // reader.onload = async (e) => { 
              //   const text = (e.target.result)
              //   console.log(text)
              // };
              // reader.readAsText(e.target.files[0])
            }}
          />
        <BodyManageDV style={{
          paddingTop: "0px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>ชื่อ</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>
            
            <InputFromId 
              className={classes.InputIdControl}
              variant="outlined" 
              value={AddEmployeeName}
              error={AddEmployeeNameValidation}
              onChange={(e) => {
                setAddEmployeeName(e.target.value.split(/[0-9!@#$%^&*( )_+\-={};':"\\|,.<>\/?~]/).join(''))
              }}
              labelwidth={0}
              placeholder="กรอกชื่อ" 
              fullWidth={true}
              autoComplete='off'
            />

          </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>

        <BodyManageDV style={{
          paddingTop: "0px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>นามสกุล</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>
            
            <InputFromId 
              className={classes.InputIdControl}
              variant="outlined" 
              value={AddEmployeeSurname}
              error={AddEmployeeSurnameValidation}
              onChange={(e) => {
                setAddEmployeeSurname(e.target.value.split(/[0-9!@#$%^&*( )_+\-={};':"\\|,.<>\/?~]/).join(''))
              }}
              labelwidth={0}
              placeholder="กรอกนามสกุล" 
              fullWidth={true}
              autoComplete='off'
            />

          </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>


        <BodyManageDV style={{
          paddingTop: "0px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>ตำแหน่ง</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>
            
            <InputFromId 
              className={classes.InputIdControl}
              variant="outlined" 
              value={AddEmployeePostion}
              error={AddEmployeePostionValidation}
              onChange={(e) => {
                setAddEmployeePostion(e.target.value)
              }}
              labelwidth={0}
              placeholder="กรอกตำแหน่ง" 
              fullWidth={true}
              autoComplete='off'
            />

          </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>


        <BodyManageDV style={{
          paddingTop: "0px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>อีเมล</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>
    
    <InputFromId 
      className={classes.InputIdControl}
      variant="outlined" 
      value={AddEmployeeEmail}
      error={AddEmployeeEmailValidation}
      onChange={(e) => {
        setAddEmployeeEmail(e.target.value)
      }}
      labelwidth={0}
      placeholder="กรอกอีเมล" 
      fullWidth={true}
      autoComplete='off'
    />

  </BodyForm>
  <BodySpace style={{
    padding: "10px 10px 0px 10px"
  }}></BodySpace>
</BodyManageDV>

<BodyManageDV style={{
  paddingTop: "5px",
  display: AddEmployeeEmailValidation ? 'flex' : 'none'
}}>
  <BodySpace style={{
    padding: "10px 0px 0px 0px",
    width: "0",
  }}></BodySpace>
  <div>
    <ul style={{ padding: '0', textAlign: 'left', color: 'red' }}>
      <li>
        รูปแบบของอีเมลไม่ถูกต้อง
      </li>
    </ul>
  </div>
</BodyManageDV>

<BodyManageDV style={{
  paddingTop: "0px"
}}>
  <BodySpace style={{
    padding: "10px 10px 0px 10px"
  }}></BodySpace>
  <BodyLabel style={{
    padding: "10px 10px 0px 10px",
    textAlign: "left"
  }}>รหัสพนักงาน</BodyLabel>
  <BodyForm style={{
    padding: "10px 10px 0px 10px"
  }}>
    
    <InputFromId 
      className={classes.InputIdControl}
      variant="outlined" 
      value={AddEmployeePresonalID}
      error={AddEmployeePresonalIDValidation}
      onChange={(e) => {
        setAddEmployeePresonalID(e.target.value.split(/[^a-zA-Z0-9]/).join(''))
      }}
      labelwidth={0}
      placeholder="กรอกรหัสพนักงาน" 
      fullWidth={true}
      autoComplete='off'
    />

  </BodyForm>
  <BodySpace style={{
    padding: "10px 10px 0px 10px"
  }}></BodySpace>
</BodyManageDV>



        <BodyManageDV style={{
          paddingTop: "0px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>Card ID</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>
            
            <InputFromId 
              className={classes.InputIdControl}
              variant="outlined" 
              value={AddEmployeeCard}
              error={AddEmployeeCardValidation}
              onChange={(e) => {
                setAddEmployeeCard(e.target.value.split(/[^a-zA-Z0-9]/).join(''))
              }}
              labelwidth={0}
              placeholder="กรอก Card ID" 
              fullWidth={true}
              autoComplete='off'
            />

          </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>

        {/* Add device to user */}
        {
          AccessDoorArr.map((item, index) => {
            return <div style={{marginBottom: "30px"}}>
              <BodyManageDV 
              style={{
                display: "flex",
                justifyContent: "center"
              }}
              key={index.toString()}
            >
              <BodySpace style={{
                padding: "10px 10px 0px 10px"
              }}></BodySpace>
              <BodyLabel style={{
                padding: "10px 10px 0px 10px",
                textAlign: "left"
              }}> {((index === 0) ? "สิทธิ์เข้าออกประตู": null)} </BodyLabel>
              <BodyForm style={{
                padding: "10px 10px 0px 10px"
              }}>
                
                <FormControl variant="outlined" className={classes.formControl}>
                  <Select
                    style={ AccessDoorArrValidation ? (AccessDoorArr[index].doorID === undefined ? { border: "1px solid red", borderRadius: 6} : null) : null }
                    labelId="demo-simple-select-outlined-label"
                    id="demo-simple-select-outlined"
                    input={<BootstrapInput />}
                    value={((AccessDoorArr[index].doorID === undefined) ? 
                            "undefined": AccessDoorArr[index].doorID)}
                    onChange={(e) => {
                      setSelectAll(false)
                      var ada = AccessDoorArr
                      ada[index].doorID = (e.target.value === "undefined" ? undefined : e.target.value)
                      const checkIfDuplicateExists = (w) => {
                        var neww = w.map((item) => {
                          return item.doorID
                        })
                        return new Set(neww).size !== neww.length 
                      }
                      var check_dup = ada
                      var AccessDoorArrFiltered = check_dup.filter(function(x) {
                        return x.doorID !== undefined && x.doorID !== "undefined";
                      });
                      if (!checkIfDuplicateExists(AccessDoorArrFiltered)){
                        setAccessDoorArr(Object.assign([], ada))
                      } else {
                        const removeDuplicate = AccessDoorArrFiltered.reduce((acc, current) => {
                          const x = acc.find(item => item.doorID === current.doorID)
                          if (!x) {
                            return acc.concat([current])
                          } else {
                            return acc
                          }
                        }, [])
                        setAccessDoorArr(Object.assign([], removeDuplicate))
                      }
                    }}
                  >
                    <MenuItem value={"undefined"}>{ "กรุณาเลือกสิทธิ์เข้าออกประตู" }</MenuItem>
                    {
                      GroupDeviceList.map((device_item, device_index) => {
                        return (
                          <MenuItem key={device_index.toString()} value={device_item.device_id}>{ device_item.device_name }</MenuItem>
                        )
                      })
                    }
                  </Select>
                </FormControl>
                    
              </BodyForm>
              <BodySpace style={{
                // padding: "10px 10px 0px 10px",
                padding: "10px 0px 0px 0px",
                width: "9%"
              }}>

                {
                  ((index === AccessDoorArr.length - 1) ? <div>

                    <img 
                      src={ADDGROUP} 
                      alt="ADDGROUP" 
                      style={{
                        width: 20,
                        heigth: 20,
                        padding: "10px 10px 0px 0px",
                        cursor: "pointer"
                      }}
                      onClick={() => {
                        var ada = AccessDoorArr
                        ada.push({
                          doorID: undefined,
                          isTimeInOut: false,
                          timeBegin: "00:00",
                          timeEnd: "23:59",
                          isAllTime: false
                        })
                        setAccessDoorArr(Object.assign([], ada))
                      }}
                    />

                    <img 
                      src={REMOVEDOORACCESS} 
                      alt="REMOVEDOORACCESS" 
                      style={{
                        width: 20,
                        heigth: 20,
                        padding: "10px 0px 0px 0px",
                        cursor: "pointer"
                      }}
                      onClick={() => {
                        var ada = AccessDoorArr
                        if (ada.length !== 1){
                          ada.pop()
                          setAccessDoorArr(Object.assign([], ada))
                        }
                      }}
                    />

                  </div>: null)
                }
              </BodySpace>
            </BodyManageDV>

            <BodyManageDV>
              <BodySpace style={{
                  padding: "10px 10px 0px 10px"
              }}></BodySpace>
              <BodyForm style={{
                padding: "0px 10px 0px 130px"
              }}>
                <Checkbox color={"primary"} checked={item.isTimeInOut} onChange={(e) => {
                  handleSetTimeInOut(!item.isTimeInOut, index)
                }} />
                <BodyLabel style={{
                  textAlign: "left",
                  width: "180px"
                }}>เพิ่มเวลาเข้าออก</BodyLabel>
              </BodyForm>
            </BodyManageDV>

            <BodyManageDV style={{display: item.isTimeInOut ? 'flex' : 'none'}}>
              <BodySpace style={{
                padding: "10px 10px 0px 10px"
              }}></BodySpace>
              <BodyForm style={{
                padding: "0px 10px 0px 190px"
              }}>
                <Fromto>จาก</Fromto>
                <div style={{width: "120px"}}><TimePicker index={index} disabled={item.isAllTime} onTime={onTimeFrom} init={item.timeBegin} tag={"time-add-group-from"}/></div>
                <Fromto>ถึง</Fromto>
                <div style={{width: "120px"}}><TimePicker index={index} disabled={item.isAllTime} onTime={onTimeTo} init={item.timeEnd} tag={"time-add-group-to"} /></div>
              </BodyForm>
              <BodySpace style={{
                padding: "10px 10px 0px 10px"
              }}></BodySpace>
            </BodyManageDV>

            <BodyManageDV style={{display: item.isTimeInOut ? 'flex' : 'none'}}>
              <BodySpace style={{
                  padding: "10px 10px 0px 10px"
              }}></BodySpace>
              <BodyForm style={{
                padding: "0px 10px 0px 130px"
              }}>
                <Checkbox color={"primary"} checked={item.isAllTime} onChange={(e) => {
                  handleSetAllTime(!item.isAllTime, index)
                }} />
                <BodyLabel style={{
                  textAlign: "left",
                  width: "180px"
                }}>All Time</BodyLabel>
              </BodyForm>
            </BodyManageDV>

            </div>
          })
        }

        <BodyManageDV>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyForm 
            style={{
              alignItems: "center",
              padding: "0px 10px 0px 175px"
            }}
          >
            <Checkbox
              color="primary"
              checked={SelectAll}
              onClick={(e) => {
                var checked = e.target.checked
                if (checked) {
                  // AccessDoorArr
                  var device = GroupDeviceList
                  var all = device.map((item) => {
                    return {
                      doorID: item.device_id,
                      isTimeInOut: false,
                      timeBegin: "00:00",
                      timeEnd: "23:59",
                      isAllTime: false
                    }
                  })
                  setAccessDoorArr(Object.assign([], all))
                } else {
                  setAccessDoorArr(Object.assign([], [{
                    doorID: undefined,
                    isTimeInOut: false,
                    timeBegin: "00:00",
                    timeEnd: "23:59",
                    isAllTime: false
                  }]))
                }
                setSelectAll(checked)
              }}
            />
            <BodyLabel style={{
              textAlign: "left"
            }}>เลือกทุกประตู</BodyLabel>
          </BodyForm>
          <BodySpace></BodySpace>
        </BodyManageDV>


        {/* update image */}
        {/* Select Access Door */}
        <BodyManageDV style={{
          paddingTop: "10px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>

          <input 
            accept=".zip"
            type='file' 
            id='file' 
            ref={inputFileZip} 
            style={{display: 'none'}}
            onChange={ async (e) => {
              if (e.target.files[0] !== undefined) {
                setopenDialogAddUserToGroup(false)
                setZipFileName(e.target.files[0].name)
                setopenDialogComfirmUpload(true)
                setZipFileValue(e.target.files[0])
              }
              inputFileZip.current.value = ""

              //   const profileShow = URL.createObjectURL(e.target.files[0])
              //   setAddEmployeeProfile([e.target.files[0], e.target.files[0].name])
              //   setImageFile(profileShow)
              // }
              // e.preventDefault()
              // const reader = new FileReader()
              // reader.onload = async (e) => { 
              //   const text = (e.target.result)
              //   console.log(text)
              // };
              // reader.readAsText(e.target.files[0])
            }}
          />


          <div
            style={{
              width: "80%"
            }}
          >
            <div
              style={{
                padding: "10px 0px",
              }}
            >หรือ</div>
            <hr></hr>
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                alignContent: "center",
                alignItems: "center",
                justifyContent: "space-between",
                padding: "20px 0px",
              }}
            >
              <div>Upload ข้อมูลพนักงาน { (ZipFileName !== "" ? `[ ${ZipFileName} ]`: "") }</div>
              <div onClick={() => {
                inputFileZip.current.click();
              }}><img src={EXPORTEMPTY} style={{cursor: "pointer"}} alt="EXPORTEMPTY" /></div>
            </div>
            <hr></hr>
          </div>

          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>


        <ActionManageDV>
          <div style={{
            display: "flex",
            justifyContent: "space-between",
            width: "100%",
            padding: "20px calc(10% + 30px) 0px calc(10% + 30px)"
          }}>
            <Choice1Btn style={{
              width: 250, 
              height: 60
            }} onClick={() => {
              setopenDialogAddUserToGroup(false)
              initStateFormAddEmployee()
            }}>ยกเลิก</Choice1Btn>
            <Choice2Btn style={{
              width: 250, 
              height: 60
            }} onClick={() => {
              handleAddEmployee()
            }}>เพิ่มพนักงาน</Choice2Btn>
          </div>
        </ActionManageDV>

        </CustomeDialogContentManageDV>
      </Dialog>


      {/* Dialog for MangeAdin */}
      <Dialog
        disableBackdropClick={true}
        fullWidth={false}
        maxWidth={"xl"}
        open={openDialogMangeAdmin}
        onClose={handleCloseDialogMangeAdmin}
        aria-labelledby="update-dialog-MangeDV"
      >
        
        <CustomeExitDialogManageDV>
          <IconButton onClick={() => {
            setopenDialogMangeAdmin(false)
            setAddConfirmPass("")
            setAddPassword("")
            setAddUsername("")
            setAddName("")
            setAddEmployeeCode("")
            setAddEmail("")
            setAddUserLevel(1001)
            setAddPassError(false)
            setAddPassCond([])
            setAddNameValidation(false)
            setAddEmployeeCodeValidation(false)
            setAddEmailValidation(false)
            setAddUsernameValidation(false)
            setAddPasswordValidation(false)
            setAddConfirmPassValidation(false)
            setIsAddPasswordVisible(false)
            setIsAddConfirmPassVisible(false)
          }}>
            <img src={Exit} alt="Exit" />
          </IconButton>
        </CustomeExitDialogManageDV>
        <CustomeDialogTitleManageDV 
        id="update-dialog-MangeDV"
        children={<div style={{
          fontSize: 28,
          textAlign: "center",
          width: 400
        }}>เพิ่มบัญชีผู้ใช้</div>}
        ></CustomeDialogTitleManageDV>
        <CustomeDialogContentManageDV>

        <BodyManageDV style={{
          paddingTop: "30px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>ชื่อ-นามสกุล</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>
            
            <InputFromId 
              className={classes.InputIdControl}
              variant="outlined" 
              error={AddNameValidation} 
              labelwidth={0}
              placeholder="ชื่อ-นามสกุล" 
              fullWidth={true}
              autoComplete='off'
              value={AddName}
              onChange={(e) => {
                setAddName(e.target.value.split(/[0-9!@#$%^&*()_+\-={};':"\\|,.<>\/?~]/).join(''))
              }}
            />

        </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>

        <BodyManageDV style={{
          paddingTop: "5px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>รหัสพนักงาน</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>
            
            <InputFromId 
              className={classes.InputIdControl}
              variant="outlined" 
              error={AddEmployeeCodeValidation}
              labelwidth={0}
              placeholder="รหัสพนักงาน" 
              fullWidth={true}
              autoComplete='off'
              value={AddEmployeeCode}
              onChange={(e) => {
                setAddEmployeeCode(e.target.value.split(/[^a-zA-Z0-9]/).join(''))
              }}
            />

        </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>

        <BodyManageDV style={{
          paddingTop: "5px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>อีเมล</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>
            
            <InputFromId 
              className={classes.InputIdControl}
              variant="outlined" 
              error={AddEmailValidation}
              labelwidth={0}
              placeholder="อีเมล" 
              fullWidth={true}
              autoComplete='off'
              value={AddEmail}
              onChange={(e) => {
                setAddEmail(e.target.value)
              }}
            />

        </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>

        <BodyManageDV style={{
          paddingTop: "5px",
          display: AddEmailValidation ? 'flex' : 'none'
        }}>
          <BodySpace style={{
            padding: "10px 0px 0px 0px",
            width: "0",
          }}></BodySpace>
          <div>
            <ul style={{ padding: '0', textAlign: 'left', color: 'red' }}>
              <li>
                รูปแบบของอีเมลไม่ถูกต้อง
              </li>
            </ul>
          </div>
        </BodyManageDV>
        
        <BodyManageDV style={{
          paddingTop: "5px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>Username</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>
            
            <InputFromId 
              className={classes.InputIdControl}
              variant="outlined" 
              error={AddUsernameValidation}
              labelwidth={0}
              placeholder="Username" 
              fullWidth={true}
              autoComplete='off'
              value={AddUsername}
              onChange={(e) => {
                setAddUsername(e.target.value.split(/[^a-zA-Z0-9]/).join(''))
              }}
            />

        </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>


        <BodyManageDV style={{
          paddingTop: "5px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>Password</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>

            <InputFromId 
              className={ clsx(classes.InputIdControl) }
              error={AddPassError || AddPasswordValidation}
              variant="outlined" 
              labelwidth={0}
              placeholder="Password" 
              type={isAddPasswordVisible ? "text" : "password"}
              fullWidth={true}
              autoComplete='off'
              value={AddPassword}
              onChange={(e) => {
                setAddPassword(e.target.value)
              }}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      edge="end"
                      onClick={() => {
                        setIsAddPasswordVisible(!isAddPasswordVisible)
                      }}
                    >
                      {
                        isAddPasswordVisible ?
                        <VisibilityOffIcon style={{color: "#BABABA"}} /> :
                        <VisibilityIcon style={{color: "#BABABA"}} />
                      }
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />

          </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>


        <BodyManageDV style={{
          paddingTop: '5px',
          display: (AddPassCond.length === 0 || AddPassCond.includes(5)) ? 'none' : 'flex'
        }}>
          <BodySpace style={{
            padding: "10px 120px 0px 10px"
          }}></BodySpace>
          <div>
            <ul style={{ padding: '0', textAlign: 'left', color: 'red' }}>
              <li style={{display: AddPassCond.includes(0) ? 'list-item' : 'none' }}>
                รหัสผ่านต้องมีความยาวมากกว่า 8 ตัวอักษร
              </li>
              <li style={{display: AddPassCond.includes(1) ? 'list-item' : 'none' }}>
                รหัสผ่านต้องประกอบไปด้วยตัวพิมพ์ใหญ่อย่างน้อย 1 ตัว
              </li>
              <li style={{display: AddPassCond.includes(2) ? 'list-item' : 'none' }}>
                รหัสผ่านต้องประกอบไปด้วยตัวพิมพ์เล็กอย่างน้อย 1 ตัว
              </li>
              <li style={{display: AddPassCond.includes(3) ? 'list-item' : 'none' }}>
                รหัสผ่านต้องประกอบไปด้วยตัวเลขอย่างน้อย 1 ตัว
              </li>
              <li style={{display: AddPassCond.includes(4) ? 'list-item' : 'none' }}>
                รหัสผ่านต้องประกอบไปด้วยอักขระพิเศษอย่างน้อย 1 ตัว
              </li>
            </ul>
          </div>
        </BodyManageDV>


        <BodyManageDV style={{
          paddingTop: "5px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>Comfirm Password</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>

            <InputFromId 
              className={classes.InputIdControl}
              type={isAddConfirmPassVisible ? "text" : "password"}
              variant="outlined" 
              error={AddPassError || AddConfirmPassValidation}
              labelwidth={0}
              placeholder="Comfirm Password" 
              fullWidth={true}
              autoComplete='off'
              value={AddConfirmPass}
              onChange={(e) => {
                setAddConfirmPass(e.target.value)
              }}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      edge="end"
                      onClick={() => {
                        setIsAddConfirmPassVisible(!isAddConfirmPassVisible)
                      }}
                    >
                      {
                        isAddConfirmPassVisible ?
                        <VisibilityOffIcon style={{color: "#BABABA"}} /> :
                        <VisibilityIcon style={{color: "#BABABA"}} />
                      }
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />

          </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>


        <BodyManageDV style={{
          paddingTop: "5px",
          display: (AddPassCond.length === 0) ? 'none' : 'flex'
        }}>
          <BodySpace style={{
            padding: "10px 80px 0px 10px"
          }}></BodySpace>
          <div>
            <ul style={{ padding: '0', textAlign: 'left', color: 'red' }}>
              <li style={{display: AddPassCond.includes(5) ? 'list-item' : 'none' }}>
                โปรดตรวจสอบ Confirm Password ให้ถูกต้อง
              </li>
            </ul>
          </div>
        </BodyManageDV>

        <BodyManageDV style={{
          paddingTop: "5px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>User Level</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 0px"
          }}>
            
            <Radio color={"primary"} checked={AddUserLevel === 1000} onChange={(e) => {
              setAddUserLevel(1000)
            }}/>
            <BodyLabel style={{
              textAlign: "left",
              width: "70px"
            }}>Admin</BodyLabel>

            <Radio color={"primary"} checked={AddUserLevel === 1001} onChange={(e) => {
              setAddUserLevel(1001)
            }}/>
            <BodyLabel style={{
              textAlign: "left",
              width: "70px"
            }}>User</BodyLabel>

          </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>

        <ActionManageDV>
          <div style={{
            display: "flex",
            justifyContent: "space-between",
            width: "100%",
            padding: "30px calc(10% + 30px) 0px calc(10% + 30px)"
          }}>
            <Choice1Btn style={{
              width: 250, 
              height: 60
            }} onClick={() => {
              setopenDialogMangeAdmin(false)
              setAddConfirmPass("")
              setAddPassword("")
              setAddUsername("")
              setAddName("")
              setAddEmployeeCode("")
              setAddEmail("")
              setAddUserLevel(1001)
              setAddPassError(false)
              setAddPassCond([])
              setAddNameValidation(false)
              setAddEmployeeCodeValidation(false)
              setAddEmailValidation(false)
              setAddUsernameValidation(false)
              setAddPasswordValidation(false)
              setAddConfirmPassValidation(false)
              setIsAddPasswordVisible(false)
              setIsAddConfirmPassVisible(false)
            }}>ยกเลิก</Choice1Btn>
            <Choice2Btn style={{
              width: 250, 
              height: 60
            }} onClick={() => {
              handleAddAdmin()
            }}>เพิ่มบัญชีผู้ใช้</Choice2Btn>
          </div>
        </ActionManageDV>

        </CustomeDialogContentManageDV>
      
      </Dialog>





















      <CustomeMenu
          anchorEl={anchorElTimeFrom}
          keepMounted
          open={openMenuTimeFrom}
          onClose={handleCloseopenMenuTimeFrom}
          style={{
            zIndex: 30000
          }}
        >

          <Container  >

            <div style={{
              display: "flex",
              alignContent: "center",
              alignItems: "center",
              justifyContent: "center",
            }}>

              <div style={{
                width: 40,
                height: 30,
                display: "flex",
                alignContent: "center",
                alignItems: "center",
                justifyContent: "center",
                marginBottom: "10px"
              }}>
                <IconButton onClick={(e) => {
                  var tNumber = t1
                  var elmnt = document.getElementById("scroll-hours-from-" + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));
                  elmnt.scrollIntoView();
                  if (Number(t1) > 0){
                    t1--
                    setHoursFrom(((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()))
                    setTimeFrom(
                      ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                      ":" + 
                      ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                    )
                  }
                }}>
                  <img src={UpTime} alt="UpTime" />
                </IconButton>
              </div>
              <div style={{
                width: 40,
              }}></div>
              <div style={{
                width: 40,
                height: 30,
                display: "flex",
                alignContent: "center",
                alignItems: "center",
                justifyContent: "center",
                marginBottom: "10px"
              }}>
                <IconButton onClick={() => {
                  var tNumber = t2
                  var elmnt = document.getElementById("scroll-minutese-from-" + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));
                  elmnt.scrollIntoView();
                  if (Number(t2) > 0){
                    t2--
                    setMinuteseFrom(((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString()))
                    setTimeFrom(
                      ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                      ":" + 
                      ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                    )

                  }
                }}>
                  <img src={UpTime} alt="UpTime" />
                </IconButton>
              </div>
            </div>

            <div style={{
              display: "flex"
            }}>
              <div style={{
                height: "200px",
                width: "40px",
                overflow: "scroll",
              }} 
              ref={ScorllHoursFrom}
              className="remove-scroll"
              >
                {
                  Time.Hours.map((item, index) => {
                    return (
                      <div
                          id={"scroll-hours-from-" + Number(index)}
                          key={index}
                          style={{
                          width: "40px",
                          height: "40px",
                          display: "flex",
                          alignContent: "center",
                          justifyContent: "center",
                        }}
                        >
                          <IconButton 
                            onClick={() => {
                              setHoursFrom(item)
                              t1 = item
                              setTimeFrom(
                        ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                        ":" + 
                        ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                      )
                          }}
                          className={((item === HoursFrom) ? classestime.Select: classestime.Unselect)}
                        >{item}</IconButton>
                      </div>  
                    )
                  })
                }
              </div>
              <div style={{
                height: "200px",
                width: "40px",
                fontSize: "28px",
                display: "flex",
                alignContent: "center",
                alignItems: "center",
                justifyContent: "center",
              }} >:</div>
              <div style={{
                height: "200px",
                width: "40px",
                overflow: "scroll",
              }} 
              ref={ScorllMinuteseFrom}
              className="remove-scroll"
              >
                {
                  Time.Minutese.map((item, index) => {
                    return (
                      <div
                        key={index}
                        id={"scroll-minutese-from-" + Number(index)}
                        style={{
                        width: "40px",
                        height: "40px",
                        display: "flex",
                        alignContent: "center",
                        justifyContent: "center",
                      }}>
                        <IconButton 
                          className={((item === MinuteseFrom) ? classestime.Select: classestime.Unselect)} 
                          onClick={() => {
                            setMinuteseFrom(item)
                            t2 = item
                            setTimeFrom(
                      ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                      ":" + 
                      ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                    )
                          }}
                        >{item}</IconButton>
                      </div>  
                    )
                  })
                }
              </div>
            </div>

            <div style={{
              display: "flex",
              alignContent: "center",
              alignItems: "center",
              justifyContent: "center",
            }}>

              <div style={{
                width: 40,
                height: 30,
                display: "flex",
                alignContent: "center",
                alignItems: "center",
                justifyContent: "center",
                marginTop: "10px"
              }}>
                <IconButton onClick={() => {
                  var tNumber = t1
                  var elmnt = document.getElementById("scroll-hours-from-" + ((Number(tNumber) !== 23) ? (Number(tNumber) + 1): Number(tNumber)));
                  elmnt.scrollIntoView();
                  if (Number(t1) < 23){
                    t1++
                    setHoursFrom(((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()))
                    setTimeFrom(
                      ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                      ":" + 
                      ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                    )
                  }
                }}>
                  <img src={DownTime} alt="DownTime" />
                </IconButton>
              </div>
              <div style={{
                width: 40,
              }}></div>
              <div style={{
                width: 40,
                height: 30,
                display: "flex",
                alignContent: "center",
                alignItems: "center",
                justifyContent: "center",
                marginTop: "10px"
              }}>
                <IconButton onClick={() => {
                  var tNumber = t2
                  var elmnt = document.getElementById("scroll-minutese-from-" + ((Number(tNumber) !== 59) ? (Number(tNumber) + 1): Number(tNumber)));
                  elmnt.scrollIntoView();
                  if (Number(t2) < 59){
                    t2++
                    setMinuteseFrom(((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString()))
                    setTimeFrom(
                      ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                      ":" + 
                      ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                    )
                  }
                }}>
                  <img src={DownTime} alt="DownTime" />
                </IconButton>
              </div>
            </div>

          </Container>

        </CustomeMenu>


        <CustomeMenu
          anchorEl={anchorElTimeTo}
          keepMounted
          open={openMenuTimeTo}
          onClose={handleCloseopenMenuTimeTo}
          style={{
            zIndex: 30000
          }}
        >

          <Container  >

          <div style={{
            display: "flex",
            alignContent: "center",
            alignItems: "center",
            justifyContent: "center",
          }}>

            <div style={{
              width: 40,
              height: 30,
              display: "flex",
              alignContent: "center",
              alignItems: "center",
              justifyContent: "center",
              marginBottom: "10px"
            }}>
              <IconButton 
                onClick={() => {
                  var tNumber = t3
                  var elmnt = document.getElementById("scroll-hours-to-" + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));                                                                                      elmnt.scrollIntoView();
                  if (Number(t3) > 0){
                    t3--
                    setHoursTo(((Number(t3) < 10) ? ("0" + Number(t3).toString()): Number(t3).toString()))
                    setTimeTo(
                      ((Number(t3) < 10) ? ("0" + Number(t3).toString()): Number(t3).toString()) + 
                      ":" + 
                      ((Number(t4) < 10) ? ("0" + Number(t4).toString()): Number(t4).toString())
                    )
                  }
                }}
              >
                <img src={UpTime} alt="UpTime" />
              </IconButton>
            </div>
            <div style={{
              width: 40,
            }}></div>
            <div style={{
              width: 40,
              height: 30,
              display: "flex",
              alignContent: "center",
              alignItems: "center",
              justifyContent: "center",
              marginBottom: "10px"
            }}>
              <IconButton
                onClick={() => {
                  var tNumber = t4
                  var elmnt = document.getElementById("scroll-minutese-to-" + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));
                  elmnt.scrollIntoView();
                  if (Number(t4) > 0){
                    t4--
                    setMinuteseTo(((Number(t4) < 10) ? ("0" + Number(t4).toString()): Number(t4).toString()))
                    setTimeTo(
                      ((Number(t3) < 10) ? ("0" + Number(t3).toString()): Number(t3).toString()) + 
                      ":" + 
                      ((Number(t4) < 10) ? ("0" + Number(t4).toString()): Number(t4).toString())
                    )
                  }
                }}
              >
                <img src={UpTime} alt="UpTime" />
              </IconButton>
            </div>
          </div>

          <div style={{
            display: "flex"
          }}>
            <div style={{
              height: "200px",
              width: "40px",
              overflow: "scroll",
            }} 
            ref={ScorllHoursTo}
            className="remove-scroll"
            >
              {
                Time.Hours.map((item, index) => {
                  return (
                    <div
                    id={"scroll-hours-to-" + Number(index)}
                    key={index}
                    style={{
                      width: "40px",
                      height: "40px",
                      display: "flex",
                      alignContent: "center",
                      justifyContent: "center",
                    }}
                    >
                      <IconButton 
                        onClick={() => {
                          setHoursTo(item)
                          t3 = item
                          setTimeTo(
                            ((Number(t3) < 10) ? ("0" + Number(t3).toString()): Number(t3).toString()) + 
                            ":" + 
                            ((Number(t4) < 10) ? ("0" + Number(t4).toString()): Number(t4).toString())
                          )
                        }}
                        className={((item === HoursTo) ? classestime.Select: classestime.Unselect)}
                      >{item}</IconButton>
                    </div>  
                  )
                })
              }
            </div>
            <div style={{
              height: "200px",
              width: "40px",
              fontSize: "28px",
              display: "flex",
              alignContent: "center",
              alignItems: "center",
              justifyContent: "center",
            }} >:</div>
            <div style={{
              height: "200px",
              width: "40px",
              overflow: "scroll",
            }} 
            ref={ScorllMinuteseTo}
            className="remove-scroll"
            >
              {
                Time.Minutese.map((item, index) => {
                  return (
                    <div
                    id={"scroll-minutese-to-" + Number(index)}
                      key={index}
                      style={{
                      width: "40px",
                      height: "40px",
                      display: "flex",
                      alignContent: "center",
                      justifyContent: "center",
                    }}>
                      <IconButton 
                        className={((item === MinuteseTo) ? classestime.Select: classestime.Unselect)} 
                        onClick={() => {
                          setMinuteseTo(item)
                          t4 = item
                          setTimeTo(
                            ((Number(t3) < 10) ? ("0" + Number(t3).toString()): Number(t3).toString()) + 
                            ":" + 
                            ((Number(t4) < 10) ? ("0" + Number(t4).toString()): Number(t4).toString())
                          )
                        }}
                      >{item}</IconButton>
                    </div>  
                  )
                })
              }
            </div>
          </div>

          <div style={{
            display: "flex",
            alignContent: "center",
            alignItems: "center",
            justifyContent: "center",
          }}>

            <div style={{
              width: 40,
              height: 30,
              display: "flex",
              alignContent: "center",
              alignItems: "center",
              justifyContent: "center",
              marginTop: "10px"
            }}>
              <IconButton
                onClick={() => {
                  var tNumber = t3
                  var elmnt = document.getElementById("scroll-hours-to-" + ((Number(tNumber) !== 23) ? (Number(tNumber) + 1): Number(tNumber)));
                  elmnt.scrollIntoView();
                  if (Number(t3) < 23){
                    t3++
                    setHoursTo(((Number(t3) < 10) ? ("0" + Number(t3).toString()): Number(t3).toString()))
                    setTimeTo(
                      ((Number(t3) < 10) ? ("0" + Number(t3).toString()): Number(t3).toString()) + 
                      ":" + 
                      ((Number(t4) < 10) ? ("0" + Number(t4).toString()): Number(t4).toString())
                    )
                  }
                }}
              >
                <img src={DownTime} alt="DownTime" />
              </IconButton>
            </div>
            <div style={{
              width: 40,
            }}></div>
            <div style={{
              width: 40,
              height: 30,
              display: "flex",
              alignContent: "center",
              alignItems: "center",
              justifyContent: "center",
              marginTop: "10px"
            }}>
              <IconButton
                onClick={() => {
                  var tNumber = t4
                  var elmnt = document.getElementById("scroll-minutese-to-" + ((Number(tNumber) !== 59) ? (Number(tNumber) + 1): Number(tNumber)));
                  elmnt.scrollIntoView();
                  if (Number(t4) < 59){
                    t4++
                    setMinuteseTo(((Number(t4) < 10) ? ("0" + Number(t4).toString()): Number(t4).toString()))
                    setTimeTo(
                      ((Number(t3) < 10) ? ("0" + Number(t3).toString()): Number(t3).toString()) + 
                      ":" + 
                      ((Number(t4) < 10) ? ("0" + Number(t4).toString()): Number(t4).toString())
                    )
                  }
                }}
              >
                <img src={DownTime} alt="DownTime" />
              </IconButton>
            </div>
          </div>

        </Container>

        </CustomeMenu>









        <CustomeMenu
          id="customized-menu"
          anchorEl={anchorElLogout}
          keepMounted
          open={openLogout}
          onClose={handleCloseLogout}
        >
          <CustomeMenuItem onClick={() => {
            setIsOpenPersonalProfile(true)
          }}>
            <ListItemText primary="Profile" />
          </CustomeMenuItem>
          <CustomeMenuItem onClick={() => {
            // For dowload manual
          }}>
            <ListItemText primary="Manual" />
          </CustomeMenuItem>
          <CustomeMenuItem style={{color: 'red'}} onClick={() => {
            window.location.href = "/"
          }}>
            <ListItemText primary="Logout" />
          </CustomeMenuItem>
        </CustomeMenu>

        {/* // active
        setopenDialogComfirm(true)
        setopenDialogComfirmStatus(true)
        setTitleAction("")
        setMessageError("ทำรายการสำเร็จ")

        // inactive
        setopenDialogComfirm(true)
        setopenDialogComfirmStatus(false)
        setTitleAction("กรุณาตรวจสอบข้อมูลภายในไฟล์")
        setMessageError("ทำรายการไม่สำเร็จ") */}

      <Dialog
        disableBackdropClick={true}
        fullWidth={false}
        maxWidth={"xl"}
        open={openDialogComfirmUpload}
        aria-labelledby="update-dialog-MangeDV"
      >
          <Confirm Message={<div>คุณต้องการอัพโหลดไฟล์ไหม <span style={{color: "#01A6E6"}}>{ ZipFileName }</span> หรือไม่</div>} title={"ลบอุปกรณ์"}/>

          <ComfirmAction>

          <ComfirmAction>
          <Choice2Btn 
              style={{
                width: 380, 
                height: 60,
                marginRight: 20
              }}
              onClick={() => {
                handleUploadFileZip()
                setopenDialogComfirmUpload(false)
              }}
            >
              ยืนยัน
            </Choice2Btn>

            <Choice2Btn 
              style={{
                width: 380, 
                height: 60,
                backgroundColor: "#fff",
                color: "#01A6E6",
                marginLeft: 20
              }}
              onClick={() => {
                setZipFileName("")
                setopenDialogComfirmUpload(false)
              }}
            >
              ยกเลิก
            </Choice2Btn>
          </ComfirmAction>
            
          </ComfirmAction>

      </Dialog>


        <Dialog
          disableBackdropClick={true}
          fullWidth={false}
          maxWidth={"xl"}
          open={openDialogComfirm}
          aria-labelledby="update-dialog-MangeDV"
        >
            {
              (openDialogComfirmStatus ? <Success title={TitleAction}/> : <Unsuccess title={TitleAction} msgerror={MessageError}/>)
            }
            <ComfirmAction>

              {
                (openDialogComfirmStatus ? <Choice2Btn 
                  style={{
                    width: 800, 
                    height: 60
                  }}
                  onClick={() => {
                    setopenDialogComfirm(false)
                  }}
                >
                  ยืนยัน
                </Choice2Btn> : <ComfirmAction>
                  <Choice2Btn 
                    style={{
                      width: 800, 
                      height: 60,
                      backgroundColor: "#e46f6f",
                      border: "red"
                    }}
                    onClick={() => {
                      setopenDialogComfirm(false)
                    }}
                  >
                    ยืนยัน
                  </Choice2Btn>
                </ComfirmAction>)
              }
              
            </ComfirmAction>

        </Dialog>

        {/* Personal Profile */}
        <Dialog
          disableBackdropClick={true}
          fullWidth={false}
          maxWidth={"xl"}
          open={isOpenPersonalProfile}
          onClose={handleClosePersonalProfile}
          aria-labelledby="personal-profile-dialog"
        >
          {
            mustChangePass ?
            <div style={{marginTop: '30px'}}></div> : 
            <CustomeExitDialogManageDV>
              <IconButton onClick={() => {
                setIsOpenPersonalProfile(false)
              }}>
                <img src={Exit} alt="Exit" />
              </IconButton>
            </CustomeExitDialogManageDV>
          }

          <CustomeDialogTitleManageDV 
          id="update-dialog-MangeDV"
          children={<div style={{
            fontSize: 28,
            textAlign: "center",
            width: 400
          }}>Personal Profile</div>}
          ></CustomeDialogTitleManageDV>

          <CustomeDialogContentManageDV>
            <BodyManageDV style={{
              paddingTop: "0px"
            }}>
              <BodySpace style={{
                padding: "10px 10px 0px 10px"
              }}></BodySpace>
              <BodyLabel style={{
                padding: "10px 10px 0px 10px",
                textAlign: "left"
              }}>ชื่อ</BodyLabel>
              <BodyLabel style={{
                padding: "10px 10px 0px 10px",
                textAlign: "left"
              }}>{profileData.name}</BodyLabel>
            </BodyManageDV>
            <BodyManageDV style={{
              paddingTop: "0px"
            }}>
              <BodySpace style={{
                padding: "10px 10px 0px 10px"
              }}></BodySpace>
              <BodyLabel style={{
                padding: "10px 10px 0px 10px",
                textAlign: "left"
              }}>รหัสพนักงาน</BodyLabel>
              <BodyLabel style={{
                padding: "10px 10px 0px 10px",
                textAlign: "left"
              }}>{profileData.employee_code}</BodyLabel>
            </BodyManageDV>
            <BodyManageDV style={{
              paddingTop: "0px"
            }}>
              <BodySpace style={{
                padding: "10px 10px 0px 10px"
              }}></BodySpace>
              <BodyLabel style={{
                padding: "10px 10px 0px 10px",
                textAlign: "left"
              }}>อีเมล</BodyLabel>
              <BodyLabel style={{
                padding: "10px 10px 0px 10px",
                textAlign: "left"
              }}>
              { profileData.email !== undefined ?
                profileData.email.length > 18 ?
                  <div style={{cursor: 'help'}}>
                    <CustomTooltip
                    title={
                      <div 
                      className={classes.sizeFont}
                      style={{
                        padding: 6,
                        whiteSpace: 'nowrap',
                        lineHeight: 1.4,
                      }}>{profileData.email}</div>
                    } 
                    arrow
                    >
                      <span>{profileData.email.substring(0, 18)}...</span>
                    </CustomTooltip>
                  </div>
                  :
                  <span>{profileData.email}</span> :
                  null
              }
              </BodyLabel>
            </BodyManageDV>
            <BodyManageDV style={{
              paddingTop: "0px"
            }}>
              <BodySpace style={{
                padding: "10px 10px 0px 10px"
              }}></BodySpace>
              <BodyLabel style={{
                padding: "10px 10px 0px 10px",
                textAlign: "left"
              }}>Username</BodyLabel>
              <BodyLabel style={{
                padding: "10px 10px 0px 10px",
                textAlign: "left"
              }}>{profileData.username}</BodyLabel>
            </BodyManageDV>
            <BodyManageDV style={{
              paddingTop: "0px"
            }}>
              <BodySpace style={{
                padding: "10px 10px 0px 10px"
              }}></BodySpace>
              <BodyLabel style={{
                padding: "10px 10px 0px 10px",
                textAlign: "left"
              }}>User Level</BodyLabel>
              <BodyLabel style={{
                padding: "10px 10px 0px 10px",
                textAlign: "left"
              }}>{
                profileData.role !== undefined ?
                profileData.role.role_name : ''
              }</BodyLabel>
            </BodyManageDV>
            <BodyManageDV style={{
              paddingTop: "0px"
            }}>
              <BodySpace style={{
                padding: "10px 10px 0px 10px"
              }}></BodySpace>
              <BodyLabel style={{
                padding: "10px 10px 0px 10px",
                textAlign: "left",
              }}>วันที่เปลี่ยนรหัสผ่านล่าสุด</BodyLabel>
              <BodyLabel style={{
                padding: "10px 10px 0px 10px",
                textAlign: "left"
              }}>{
                profileData.update_password_at !== undefined ? 
                profileData.update_password_at.slice(0, 10) : ''}</BodyLabel>
            </BodyManageDV>

            <ActionManageDV>
              <div style={{
                display: "flex",
                justifyContent: "center",
                width: "100%",
                padding: "20px calc(10% + 30px) 0px calc(10% + 30px)"
              }}>
                <Choice2Btn style={{
                  width: 250, 
                  height: 60
                }} onClick={() => {
                  setIsOpenPersonalProfile(false)
                  setIsOpenChangePassword(true)
                }}>
                  {
                    (store.getState().User === undefined) ? 'เปลี่ยนรหัสผ่าน' :
                    (store.getState().User.update_password) ? 
                    'รหัสผ่านคุณหมดอายุ, กรุณาเปลี่ยนรหัสผ่าน' : 
                    'เปลี่ยนรหัสผ่าน'
                  }
                </Choice2Btn>
              </div>
            </ActionManageDV>
          </CustomeDialogContentManageDV>
        </Dialog>

        <Dialog
          disableBackdropClick={true}
          fullWidth={false}
          maxWidth={"xl"}
          open={isOpenChangePassword}
          onClose={handleCloseChangePassword}
          aria-labelledby="change-password-dialog">
          
          {
            mustChangePass ?
            <div style={{marginTop: '30px'}}></div> :
            <CustomeExitDialogManageDV>
              <IconButton onClick={() => {
                setIsOpenChangePassword(false)
                setOldPasswordChange("")
                setNewPasswordChange("")
                setConfirmNewPasswordChange("")
                setAddPassError(false)
                setAddPassCond([])
                setIsOldPasswordChangeVisible(false)
                setIsNewPasswordChangeVisible(false)
                setIsConfirmNewPasswordChangeVisible(false)
              }}>
                <img src={Exit} alt="Exit" />
              </IconButton>
            </CustomeExitDialogManageDV>
          }

          <CustomeDialogTitleManageDV 
            id="update-dialog-MangeDV"
            children={<div style={{
              fontSize: 28,
              textAlign: "center",
              width: 400
            }}>เปลี่ยนรหัสผ่าน</div>}
          ></CustomeDialogTitleManageDV>

          <CustomeDialogContentManageDV>
            <BodyManageDV style={{
              paddingTop: "0px"
            }}>
              <BodySpace style={{
                padding: "10px 10px 0px 10px"
              }}></BodySpace>
              <BodyLabel style={{
                padding: "10px 10px 0px 10px",
                textAlign: "left"
              }}>Old Password</BodyLabel>
              <BodyForm style={{
                padding: "10px 10px 0px 10px"
              }}>
                
                <InputFromId 
                  className={ clsx(classes.InputIdControl) }
                  error={AddPassError}
                  variant="outlined" 
                  labelwidth={0}
                  placeholder="Old Password" 
                  type={isOldPasswordChangeVisible ? "text" : "password"}
                  fullWidth={true}
                  autoComplete='off'
                  value={oldPasswordChange}
                  onChange={(e) => {
                    setOldPasswordChange(e.target.value)
                  }}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          edge="end"
                          onClick={() => {
                            setIsOldPasswordChangeVisible(!isOldPasswordChangeVisible)
                          }}
                        >
                          {
                            isOldPasswordChangeVisible ?
                            <VisibilityOffIcon style={{color: "#BABABA"}} /> :
                            <VisibilityIcon style={{color: "#BABABA"}} />
                          }
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />

              </BodyForm>
              <BodySpace style={{
                padding: "10px 10px 0px 10px"
              }}></BodySpace>
            </BodyManageDV>

            <BodyManageDV style={{
              paddingTop: "0px"
            }}>
              <BodySpace style={{
                padding: "10px 10px 0px 10px"
              }}></BodySpace>
              <BodyLabel style={{
                padding: "10px 10px 0px 10px",
                textAlign: "left"
              }}>New Password</BodyLabel>
              <BodyForm style={{
                padding: "10px 10px 0px 10px"
              }}>
                
                <InputFromId 
                  className={ clsx(classes.InputIdControl) }
                  error={AddPassError}
                  variant="outlined" 
                  labelwidth={0}
                  placeholder="New Password" 
                  type={isNewPasswordChangeVisible ? "text" : "password"}
                  fullWidth={true}
                  autoComplete='off'
                  value={newPasswordChange}
                  onChange={(e) => {
                    setNewPasswordChange(e.target.value)
                  }}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          edge="end"
                          onClick={() => {
                            setIsNewPasswordChangeVisible(!isNewPasswordChangeVisible)
                          }}
                        >
                          {
                            isNewPasswordChangeVisible ?
                            <VisibilityOffIcon style={{color: "#BABABA"}} /> :
                            <VisibilityIcon style={{color: "#BABABA"}} />
                          }
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />

              </BodyForm>
              <BodySpace style={{
                padding: "10px 10px 0px 10px"
              }}></BodySpace>
            </BodyManageDV>

            <BodyManageDV style={{
              paddingTop: "5px",
              display: (AddPassCond.length === 0) ? 'none' : 'flex'
            }}>
              <BodySpace style={{
                padding: "10px 120px 0px 10px"
              }}></BodySpace>
              <div>
                <ul style={{ padding: '0', textAlign: 'left', color: 'red' }}>
                  <li style={{display: AddPassCond.includes(0) ? 'list-item' : 'none' }}>
                    รหัสผ่านต้องมีความยาวมากกว่า 8 ตัวอักษร
                  </li>
                  <li style={{display: AddPassCond.includes(1) ? 'list-item' : 'none' }}>
                    รหัสผ่านต้องประกอบไปด้วยตัวพิมพ์ใหญ่อย่างน้อย 1 ตัว
                  </li>
                  <li style={{display: AddPassCond.includes(2) ? 'list-item' : 'none' }}>
                    รหัสผ่านต้องประกอบไปด้วยตัวพิมพ์เล็กอย่างน้อย 1 ตัว
                  </li>
                  <li style={{display: AddPassCond.includes(3) ? 'list-item' : 'none' }}>
                    รหัสผ่านต้องประกอบไปด้วยตัวเลขอย่างน้อย 1 ตัว
                  </li>
                  <li style={{display: AddPassCond.includes(4) ? 'list-item' : 'none' }}>
                    รหัสผ่านต้องประกอบไปด้วยอักขระพิเศษอย่างน้อย 1 ตัว
                  </li>
                </ul>
              </div>
            </BodyManageDV>

            <BodyManageDV style={{
              paddingTop: "0px"
            }}>
              <BodySpace style={{
                padding: "10px 10px 0px 10px"
              }}></BodySpace>
              <BodyLabel style={{
                padding: "10px 10px 0px 10px",
                textAlign: "left"
              }}>Confirm Password</BodyLabel>
              <BodyForm style={{
                padding: "10px 10px 0px 10px"
              }}>
                
                <InputFromId 
                  className={ clsx(classes.InputIdControl) }
                  error={AddPassError}
                  variant="outlined" 
                  labelwidth={0}
                  placeholder="Confirm New Password" 
                  type={isConfirmNewPasswordChangeVisible ? "text" : "password"}
                  fullWidth={true}
                  autoComplete='off'
                  value={confirmNewPasswordChange}
                  onChange={(e) => {
                    setConfirmNewPasswordChange(e.target.value)
                  }}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          edge="end"
                          onClick={() => {
                            setIsConfirmNewPasswordChangeVisible(!isConfirmNewPasswordChangeVisible)
                          }}
                        >
                          {
                            isConfirmNewPasswordChangeVisible ?
                            <VisibilityOffIcon style={{color: "#BABABA"}} /> :
                            <VisibilityIcon style={{color: "#BABABA"}} />
                          }
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />

              </BodyForm>
              <BodySpace style={{
                padding: "10px 10px 0px 10px"
              }}></BodySpace>
            </BodyManageDV>

            <BodyManageDV style={{
              paddingTop: "5px",
              display: (AddPassCond.length === 0) ? 'none' : 'flex'
            }}>
              <BodySpace style={{
                padding: "10px 80px 0px 10px"
              }}></BodySpace>
              <div>
                <ul style={{ padding: '0', textAlign: 'left', color: 'red' }}>
                  <li style={{display: AddPassCond.includes(5) ? 'list-item' : 'none' }}>
                    โปรดตรวจสอบ Confirm Password ให้ถูกต้อง
                  </li>
                </ul>
              </div>
            </BodyManageDV>

            <ActionManageDV>
              <div style={{
                display: "flex",
                justifyContent: "space-between",
                width: "100%",
                padding: "20px calc(10% + 30px) 0px calc(10% + 30px)"
              }}>
                <Choice1Btn style={{
                  width: 250, 
                  height: 60
                }} onClick={() => {
                  if (mustChangePass) {
                    setIsOpenPersonalProfile(true)
                  }
                  setIsOpenChangePassword(false)
                  setOldPasswordChange("")
                  setNewPasswordChange("")
                  setConfirmNewPasswordChange("")
                  setAddPassError(false)
                  setAddPassCond([])
                  setIsOldPasswordChangeVisible(false)
                  setIsNewPasswordChangeVisible(false)
                  setIsConfirmNewPasswordChangeVisible(false)
                }}>ยกเลิก</Choice1Btn>
                <Choice2Btn style={{
                  width: 250, 
                  height: 60
                }} onClick={() => {
                  handleChangePassword()
                }}>ยืนยัน</Choice2Btn>
              </div>
            </ActionManageDV>

          </CustomeDialogContentManageDV>
        </Dialog>


        <Backdrop className={navbarclasses.backdrop} open={OpenBackdrop}>
          <CircularProgress color="inherit" />
        </Backdrop>


    </DashboardNavbarRoot>
  )
}

export default DashboardNavbar
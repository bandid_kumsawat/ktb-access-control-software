import React from 'react'

import ADDGROUP from "src/asset/img/sidebar/ADDGROUP.svg"
import REMOVEDOORACCESS from "src/asset/img/sidebar/REMOVEDOORACCESS.svg"

import styled from 'styled-components'

import "./employee.css"

import Dialog from '@material-ui/core/Dialog'
import Alert from 'src/components/ComfirmAction/Alert'

// button component
import Choice2Btn from 'src/components/material/button/Choice2Btn'
import Checkbox from '@material-ui/core/Checkbox';

// TimePicker
// sub Component
import Container from 'src/components/TimePicker/Container'
// Pseudo Style
import "src/components/TimePicker/style.css"
// setting style import TimeStyles from 'src/components/TimePicker/style'
import TimeStyles from 'src/components/TimePicker/style'
// time format
import Time from 'src/components/TimePicker/time.json'
// icon
import UpTime from "src/asset/img/timepicker/up.svg";
import DownTime from "src/asset/img/timepicker/down.svg";

// commont lib
import parseToday from 'src/commons/parseToday'

// Search Form
import CustomeContentBody from 'src/components/material/formsearch/Body/Form'
import CustomeContentSpace from 'src/components/material/formsearch/Body/Space'
import CustomeContentTextBox from 'src/components/material/formsearch/Form/InputDate'

import IconButton from '@material-ui/core/IconButton';

// menu component
import CustomeMenu from 'src/components/material/menu/CustomeMenu'

// setting style 
import useStyles from 'src/components/device/setting/style'

// store
import { 
  SEARCHEMPLOYEE
} from "src/stores/actions"

import FormControl from "@material-ui/core/FormControl"
import Select from "@material-ui/core/Select"


const ContainerSearchFlow = styled("div")`
  overflow-y: scroll;
  height: calc(100vh - 200px);
`
const ComfirmAction = styled("div")`
  display: flex;
  align-content: center;
  justify-content: space-around;
  margin: 20px;
`

// scrollbar position
var t1 = "00"
var t2 = "00"
var mode = ""

export default function SearchEmployee(props) {
  const classes = useStyles();
  const classestime = TimeStyles()
  const [store] = React.useState(props.store)
  const ScorllHoursFrom = React.createRef();
  const ScorllMinuteseFrom = React.createRef();
  
  // form control
  const [ Name, setName ] = React.useState("")
  const [ Surname, setSurname ] = React.useState("")
  const [ Card, setCard ] = React.useState("")
  const [ DateStart, setDateStart ] = React.useState(parseToday(1))
  const [ DateEnd, setDateEnd ] = React.useState(parseToday(0))
  const [ TimeStart, setTimeStart ] = React.useState("00:00")
  const [ TimeEnd, setTimeEnd ] = React.useState("23:59")

  const [anchorElTimeSelect, setanchorElTimeSelect] = React.useState(null);
  const [ openMenuTimeFrom, setopenMenuTimeFrom ] = React.useState(false)

  const [ HoursFrom, setHoursFrom ] = React.useState("00") // true
  const [ MinuteseFrom, setMinuteseFrom ] = React.useState("00") // false

  // const [ OpenFrom, setOpenFrom ] = React.useState("") // false

  const [ TimeFrom , setTimeFrom ] = React.useState("00:00")

  const [ AccessDoorArr, setAccessDoorArr ] = React.useState([undefined])

  const [ DeviceList, setDeviceList ] = React.useState([])

  const [ openDialogComfirm, setopenDialogComfirm ] = React.useState(false)

  const [ SelectAll, setSelectAll ] = React.useState(false)


  const [ TimeFromValidation, setTimeFromValidation ] = React.useState(false)
  const [ TimeToValidation, setTimeToValidation ] = React.useState(false)

  const [ DateFromValidation, setDateFromValidation ] = React.useState(false)
  const [ DateToValidation, setDateToValidation ] = React.useState(false)


  React.useEffect(() => {
    const LoadData = () => {
      setName(store.getState().SearchEmployee.search.name)
      setSurname(store.getState().SearchEmployee.search.surname)
      setCard(store.getState().SearchEmployee.search.card)
      setTimeStart(store.getState().SearchEmployee.search.starttime)
      setTimeEnd(store.getState().SearchEmployee.search.endtime)

      var list = []
      var device = store.getState().Device
      if (device !== undefined){
        device.forEach((item, index) => {
          list.push({
            device_id: item.gate_id,
            device_name: item.gate.name_gate
          })
        })
      }
      setDeviceList(list)
    }

    var interval_ = setInterval(() => {
      LoadData()
    })
    return () => {
      clearInterval(interval_)
    }
  }, [store])

  const handleCloseopenMenuTimeFrom = () => {
    setanchorElTimeSelect(null);
    setopenMenuTimeFrom(false);
    var searche = store.getState().SearchEmployee
    var check = true
    console.log(TimeFrom)

    var SDT = mode
    if (SDT === "tst"){
      // start time

      if (
        new Date(`2020-01-01 ${TimeFrom}`).getTime() 
        >= 
        new Date(`2020-01-01 ${searche.search.endtime}`).getTime() 
      ){
        check = false
        setTimeFromValidation(true)
        setTimeToValidation(true)
        // setDateFromValidation(true)
        // setDateToValidation(true)
      } else {
        setTimeFromValidation(false)
        setTimeToValidation(false)
        // setDateFromValidation(false)
        // setDateToValidation(false)
      }
      setTimeStart(TimeFrom)
      searche.search.starttime = TimeFrom
      store.dispatch(SEARCHEMPLOYEE(searche))
    } else if (SDT === "tsp"){
      // stop time

      if (
        new Date(`2020-01-01 ${searche.search.starttime}`).getTime() 
        >= 
        new Date(`2020-01-01 ${TimeFrom}`).getTime() 
      ){
        check = false
        setTimeFromValidation(true)
        setTimeToValidation(true)
        // setDateFromValidation(true)
        // setDateToValidation(true)
      } else {
        setTimeFromValidation(false)
        setTimeToValidation(false)
        // setDateFromValidation(false)
        // setDateToValidation(false)
      }
      setTimeEnd(TimeFrom)
      searche.search.endtime = TimeFrom
      store.dispatch(SEARCHEMPLOYEE(searche))
    }
  }

  const handleChangeName = (e) => {
    setName(e.target.value)
    // eslint-disable-next-line
    var searche = store.getState().SearchEmployee
    searche.search.name = e.target.value
    store.dispatch(SEARCHEMPLOYEE(searche))
  }
  const handleChangeSurname = (e) => {
    setSurname(e.target.value)
    // eslint-disable-next-line
    var searche = store.getState().SearchEmployee
    searche.search.surname = e.target.value
    store.dispatch(SEARCHEMPLOYEE(searche))
  }
  const handleChangeCard = (e) => {
    setCard(e.target.value)
    // eslint-disable-next-line
    var searche = store.getState().SearchEmployee
    searche.search.card = e.target.value
    store.dispatch(SEARCHEMPLOYEE(searche))
  }

  const handleChangeDateStart = (e) => {

    var searche = store.getState().SearchEmployee
    var date = e.target.value
    var check = true

    if (
      new Date(`${date} 00:00`).getTime() 
      >
      new Date(`${searche.search.enddate} 00:00`).getTime() 
    ){
      check = false
      // setTimeFromValidation(true)
      // setTimeToValidation(true)
      setDateFromValidation(true)
      setDateToValidation(true)
    } else {
      // setTimeFromValidation(false)
      // setTimeToValidation(false)
      setDateFromValidation(false)
      setDateToValidation(false)
    }

    setDateStart(e.target.value)
    searche.search.startdate = e.target.value
    store.dispatch(SEARCHEMPLOYEE(searche))
  }

  const handleChangeDateEnd = (e) => {
    var searche = store.getState().SearchEmployee
    var date = e.target.value
    var check = true

    if (
      new Date(`${searche.search.startdate} 00:00`).getTime() 
      >
      new Date(`${date} 00:00`).getTime() 
    ){
      check = false
      // setTimeFromValidation(true)
      // setTimeToValidation(true)
      setDateFromValidation(true)
      setDateToValidation(true)
    } else {
      // setTimeFromValidation(false)
      // setTimeToValidation(false)
      setDateFromValidation(false)
      setDateToValidation(false)
    }

    setDateEnd(e.target.value)
    // eslint-disable-next-line
    searche.search.enddate = e.target.value
    store.dispatch(SEARCHEMPLOYEE(searche))
  }

  const setListDoorSearch = (device) => {
    // eslint-disable-next-line
    var searche = store.getState().SearchEmployee
    searche.search.device = device
    store.dispatch(SEARCHEMPLOYEE(searche))
  }

  return (
    <ContainerSearchFlow id="custome-scroll-bar">


      {/* ชื่อ-นามสกุล */}
      <CustomeContentBody>
        <CustomeContentSpace />
        <CustomeContentTextBox 
          className={classes.InputIdControl}
          variant="outlined" 
          labelwidth={0}
          placeholder="ชื่อ-นามสกุล" 
          fullWidth={true}
          autoComplete='off'
          value={Name}
          onChange={handleChangeName}
        />
        <CustomeContentSpace />
      </CustomeContentBody>

      {/* รหัสพนักงาน */}
      <CustomeContentBody>
        <CustomeContentSpace />
        <CustomeContentTextBox 
          className={classes.InputIdControl}
          variant="outlined" 
          labelwidth={0}
          placeholder="รหัสพนักงาน" 
          fullWidth={true}
          autoComplete='off'
          value={Surname}
          onChange={handleChangeSurname}
        />
        <CustomeContentSpace />
      </CustomeContentBody>

      {/* หมายเลข Card ID */}
      <CustomeContentBody>
        <CustomeContentSpace />
        <CustomeContentTextBox 
          className={classes.InputIdControl}
          variant="outlined" 
          labelwidth={0}
          placeholder="Card ID" 
          fullWidth={true}
          autoComplete='off'
          value={Card}
          onChange={handleChangeCard}
        />
        <CustomeContentSpace />
      </CustomeContentBody>




      {/* Form Search Start Date */}
      <CustomeContentBody style={{
        width: "100%",
        display: "flex",
        alignContent: "center",
        justifyContent: "center",
        flexDirection: "column",
      }}>
        เลือกชื่อประตู
        <div style={{
          borderBottom: "1px solid #c0c0c0",
          width: "74%"
        }}></div>
      </CustomeContentBody>


      {/* <Select
        labelId="demo-simple-select-outlined-label"
        id="demo-simple-select-outlined"
        input={<BootstrapInput />}
      >
          <MenuItem value={"undefined"}>{ "กรุณาเลือกสิทธิ์เข้าออกประตู" }</MenuItem>
        {
          DeviceList.map((item, index) => {
            return (
              <MenuItem key={index.toString()} value={item.device_id}>{ item.device_name }</MenuItem>
            )
          })
        }
      </Select> */}


      {/* Deivce Name */}
      {
        AccessDoorArr.map((select_item, select_index) => {
          return (
            <CustomeContentBody key={ select_index.toString() } style={{marginTop: "10px"}}>
              <CustomeContentSpace />

                <FormControl variant="outlined" className={classes.formControl}>

                <Select
                  style={{marginTop: "10px"}}
                  native
                  value={((AccessDoorArr[select_index] === undefined) ? "undefined": AccessDoorArr[select_index])}
                  onChange={(e) => {
                    var ada = Object.assign([], AccessDoorArr)
                    ada[select_index] = (e.target.value === "undefined" ? undefined: e.target.value)
                    const checkIfDuplicateExists = (w) => {
                      return new Set(w).size !== w.length 
                    }
                    var check_dup = ada
                    var AccessDoorArrFiltered = check_dup.filter(function(x) {
                      return x !== undefined && x !== "undefined";
                    });
                    if (!checkIfDuplicateExists(AccessDoorArrFiltered)){
                      setAccessDoorArr(Object.assign([], ada))
                      setListDoorSearch(ada)
                    } else {
                      setopenDialogComfirm(true)
                      setAccessDoorArr(Object.assign([], [...new Set(AccessDoorArrFiltered)]))
                      setListDoorSearch([...new Set(AccessDoorArrFiltered)])
                    }
                  }}
                >
                  <option aria-label="กรุณาเลือกชื่อประตู" value={"undefined"}>กรุณาเลือกชื่อประตู</option>
                  {
                    DeviceList.map((item, index) => {
                      return (
                        <option key={`${select_item}-${item.device_id}-${item.device_name}`.toString()} aria-label={item.device_name} value={item.device_id} >{item.device_name}</option>
                      )
                    })
                  }
                </Select>

                </FormControl>
              <CustomeContentSpace >

              {
                (select_index === AccessDoorArr.length - 1 ? <div>
                  <img 
                    src={ADDGROUP} 
                    alt="ADDGROUP" 
                    style={{
                      width: 20,
                      heigth: 20,
                      padding: "10px 5px 10px 10px"
                    }}
                    onClick={() => {
                      var ada = AccessDoorArr
                      ada.push(undefined)
                      setAccessDoorArr(Object.assign([], ada))
                      setListDoorSearch(ada)
                    }}
                  />

                  <img 
                    src={REMOVEDOORACCESS} 
                    alt="REMOVEDOORACCESS" 
                    style={{
                      width: 20,
                      heigth: 20,
                      padding: "10px 0px 10px 0px"
                    }}
                    onClick={() => {
                      var ada = AccessDoorArr
                      if (ada.length !== 1){
                        ada.pop()
                        setAccessDoorArr(Object.assign([], ada))
                        setListDoorSearch(ada)
                      } else {
                        setAccessDoorArr(Object.assign([], [undefined]))
                        setListDoorSearch([undefined])
                      }
                    }}
                  />
                </div>: null)
              }

              </CustomeContentSpace>
            </CustomeContentBody>
          )
        })
      }

      <CustomeContentBody style={{marginTop: "10px"}}>
        <CustomeContentSpace />
          <Checkbox
            color="primary"
            checked={SelectAll}
            onClick={(e) => {
              if (e.target.checked){
                var alldevice = DeviceList.map((item) => {
                  return item.device_id
                })
                setAccessDoorArr(Object.assign([], alldevice))
                setListDoorSearch(alldevice)
              } else {
                setAccessDoorArr(Object.assign([], [undefined]))
                setListDoorSearch([undefined])
              }
              setSelectAll(e.target.checked )
            }}
          /> เลือกทุกประตู
        <CustomeContentSpace />
      </CustomeContentBody>


      {/* Form Search Start Date */}
      <CustomeContentBody style={{
        width: "100%",
        display: "flex",
        alignContent: "center",
        justifyContent: "center",
        flexDirection: "column",
      }}>
        วันและเวลาเริ่มต้น
        <div style={{
          borderBottom: "1px solid #c0c0c0",
          width: "74%"
        }}></div>
      </CustomeContentBody>
      <CustomeContentBody>
        <CustomeContentSpace />
        <CustomeContentTextBox 
          className={classes.formControlDate}
          variant="outlined" 
          labelwidth={0}
          placeholder="เลือกวันที่เริ่มต้นค้นหา" 
          fullWidth={true}
          autoComplete='off'
          type="date"
          value={DateStart}
          onChange={handleChangeDateStart}
          error={DateFromValidation}
          InputLabelProps={{
            shrink: true,
          }}

        />
        <CustomeContentSpace />
      </CustomeContentBody>
      <CustomeContentBody>
        <CustomeContentSpace />
        <CustomeContentTextBox 
          className={classes.InputIdControl}
          variant="outlined" 
          labelwidth={0}
          placeholder="เลือกวันที่ที่ต้องการ" 
          fullWidth={true}
          autoComplete='off'
          style={{width: "100%"}}
          value={TimeStart}
          error={TimeFromValidation}
          onClick={(e) => {
            mode = "tst"
            setTimeFrom(TimeStart)
            setHoursFrom(TimeStart.split(":")[0])
            setMinuteseFrom(TimeStart.split(":")[1])
            t1 = TimeStart.split(":")[0]
            t2 = TimeStart.split(":")[1]
            setanchorElTimeSelect(e.currentTarget);
            setopenMenuTimeFrom(true)
            try{
              var tNumber = t1
              // eslint-disable-next-line
              var elmnt = document.getElementById("scroll-hours-search-from-" + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));
              elmnt.scrollIntoView();
              tNumber = t2
              // eslint-disable-next-line
              var elmnt = document.getElementById("scroll-minutese-search-from-" + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));
              elmnt.scrollIntoView();  
            }catch (e) {
              console.log(e)
            }
          }}
        />

        <CustomeContentSpace />
      </CustomeContentBody>

      {/* Form Search End Date */}
      <CustomeContentBody style={{
        width: "100%",
        display: "flex",
        alignContent: "center",
        justifyContent: "center",
        flexDirection: "column",
      }}>
        วันและเวลาสิ้นสุด
        <div style={{
          borderBottom: "1px solid #c0c0c0",
          width: "74%"
        }}></div>
      </CustomeContentBody>
      <CustomeContentBody>
        <CustomeContentSpace />
        <CustomeContentTextBox 
          className={classes.InputIdControl}
          variant="outlined" 
          labelwidth={0}
          placeholder="เลือกวันที่ที่ต้องการ" 
          fullWidth={true}
          autoComplete='off'
          type="date"
          value={DateEnd}
          onChange={handleChangeDateEnd}
          error={DateToValidation}
          InputLabelProps={{
            shrink: true,
          }}
        />
        <CustomeContentSpace />
      </CustomeContentBody>
      <CustomeContentBody>
        <CustomeContentSpace />

        <CustomeContentTextBox 
          className={classes.InputIdControl}
          variant="outlined" 
          labelwidth={0}
          placeholder="เลือกวันที่ที่ต้องการ" 
          fullWidth={true}
          autoComplete='off'
          style={{width: "100%"}}
          value={TimeEnd}
          error={TimeToValidation}
          onClick={(e) => {
            mode = "tsp"
            setTimeFrom(TimeEnd)
            setHoursFrom(TimeEnd.split(":")[0])
            setMinuteseFrom(TimeEnd.split(":")[1])
            t1 = TimeEnd.split(":")[0]
            t2 = TimeEnd.split(":")[1]
            setanchorElTimeSelect(e.currentTarget);
            setopenMenuTimeFrom(true)
            try{
              var tNumber = t1
              // eslint-disable-next-line
              var elmnt = document.getElementById("scroll-hours-search-from-" + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));
              elmnt.scrollIntoView();
              tNumber = t2
              // eslint-disable-next-line
              var elmnt = document.getElementById("scroll-minutese-search-from-" + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));
              elmnt.scrollIntoView(); 
            }catch (e) {
              console.log(e)
            }
          }}
        />
      
        <CustomeContentSpace />







        <CustomeMenu
          anchorEl={anchorElTimeSelect}
          keepMounted
          open={openMenuTimeFrom}
          onClose={handleCloseopenMenuTimeFrom}
          style={{
            zIndex: 30000
          }}
        >

          <Container  >

            <div style={{
              display: "flex",
              alignContent: "center",
              alignItems: "center",
              justifyContent: "center",
            }}>

              <div style={{
                width: 40,
                height: 30,
                display: "flex",
                alignContent: "center",
                alignItems: "center",
                justifyContent: "center",
                marginBottom: "10px"
              }}>
                <IconButton onClick={(e) => {
                  var tNumber = t1
                  // eslint-disable-next-line
                  var elmnt = document.getElementById("scroll-hours-search-from-" + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));
                  elmnt.scrollIntoView();
                  if (Number(t1) > 0){
                    t1--
                    setHoursFrom(((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()))
                    setTimeFrom(
                      ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                      ":" + 
                      ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                    )
                  }
                }}>
                  <img src={UpTime} alt="UpTime" />
                </IconButton>
              </div>
              <div style={{
                width: 40,
              }}></div>
              <div style={{
                width: 40,
                height: 30,
                display: "flex",
                alignContent: "center",
                alignItems: "center",
                justifyContent: "center",
                marginBottom: "10px"
              }}>
                <IconButton onClick={() => {
                  console.log(t2)
                  var tNumber = t2
                  // eslint-disable-next-line
                  var elmnt = document.getElementById("scroll-minutese-search-from-" + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));
                  elmnt.scrollIntoView();
                  if (Number(t2) > 0){
                    t2--
                    setMinuteseFrom(((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString()))
                    setTimeFrom(
                      ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                      ":" + 
                      ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                    )
                  }
                }}>
                  <img src={UpTime} alt="UpTime" />
                </IconButton>
              </div>
            </div>

            <div style={{
              display: "flex"
            }}>
              <div style={{
                height: "200px",
                width: "40px",
                overflow: "scroll",
              }} 
              ref={ScorllHoursFrom}
              className="remove-scroll"
              >
                {
                  Time.Hours.map((item, index) => {
                    return (
                      <div
                          id={"scroll-hours-search-from-" + Number(index)}
                          key={index}
                          style={{
                          width: "40px",
                          height: "40px",
                          display: "flex",
                          alignContent: "center",
                          justifyContent: "center",
                        }}
                        >
                          <IconButton 
                            onClick={() => {
                              setHoursFrom(item)
                              t1 = item
                              setTimeFrom(
                                ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                                ":" + 
                                ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                              )
                          }}
                          className={((item === HoursFrom) ? classestime.Select: classestime.Unselect)}
                        >{item}</IconButton>
                      </div>  
                    )
                  })
                }
              </div>
              <div style={{
                height: "200px",
                width: "40px",
                fontSize: "28px",
                display: "flex",
                alignContent: "center",
                alignItems: "center",
                justifyContent: "center",
              }} >:</div>
              <div style={{
                height: "200px",
                width: "40px",
                overflow: "scroll",
              }} 
              ref={ScorllMinuteseFrom}
              className="remove-scroll"
              >
                {
                  Time.Minutese.map((item, index) => {
                    return (
                      <div
                        key={index}
                        id={"scroll-minutese-search-from-" + Number(index)}
                        style={{
                        width: "40px",
                        height: "40px",
                        display: "flex",
                        alignContent: "center",
                        justifyContent: "center",
                      }}>
                        <IconButton 
                          className={((item === MinuteseFrom) ? classestime.Select: classestime.Unselect)} 
                          onClick={() => {
                            setMinuteseFrom(item)
                            t2 = item
                            setTimeFrom(
                              ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                              ":" + 
                              ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                            )
                          }}
                        >{item}</IconButton>
                      </div>  
                    )
                  })
                }
              </div>
            </div>

            <div style={{
              display: "flex",
              alignContent: "center",
              alignItems: "center",
              justifyContent: "center",
            }}>

              <div style={{
                width: 40,
                height: 30,
                display: "flex",
                alignContent: "center",
                alignItems: "center",
                justifyContent: "center",
                marginTop: "10px"
              }}>
                <IconButton onClick={() => {
                  var tNumber = t1
                  // eslint-disable-next-line
                  var elmnt = document.getElementById("scroll-hours-search-from-" + ((Number(tNumber) !== 23) ? (Number(tNumber) + 1): Number(tNumber)));
                  elmnt.scrollIntoView();
                  if (Number(t1) < 23){
                    t1++
                    setHoursFrom(((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()))
                    setTimeFrom(
                      ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                      ":" + 
                      ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                    )
                  }
                }}>
                  <img src={DownTime} alt="DownTime" />
                </IconButton>
              </div>
              <div style={{
                width: 40,
              }}></div>
              <div style={{
                width: 40,
                height: 30,
                display: "flex",
                alignContent: "center",
                alignItems: "center",
                justifyContent: "center",
                marginTop: "10px"
              }}>
                <IconButton onClick={() => {
                  var tNumber = t2
                  // eslint-disable-next-line
                  var elmnt = document.getElementById("scroll-minutese-search-from-" + ((Number(tNumber) !== 59) ? (Number(tNumber) + 1): Number(tNumber)));
                  elmnt.scrollIntoView();
                  if (Number(t2) < 59){
                    t2++
                    setMinuteseFrom(((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString()))
                    setTimeFrom(
                      ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                      ":" + 
                      ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                    )
                  }
                }}>
                  <img src={DownTime} alt="DownTime" />
                </IconButton>
              </div>
            </div>

          </Container>

        </CustomeMenu>




      </CustomeContentBody>


      <Dialog
        disableBackdropClick={true}
        fullWidth={false}
        maxWidth={"xl"}
        open={openDialogComfirm}
        aria-labelledby="update-dialog-MangeDV"
        style={{zIndex: 30000}}
      >
        <Alert Message={"กรุณาอย่าเลือกชื่อประตูซ้ำ"}/>
        <ComfirmAction>

          <ComfirmAction>
            <Choice2Btn 
              style={{
                width: 800, 
                height: 60,
                backgroundColor: "#01A6E6",
                border: "#01A6E6",
                color: "#fff"
              }}
              onClick={() => {
                setopenDialogComfirm(false)
              }}
            >
              ยืนยัน
            </Choice2Btn>
          </ComfirmAction>
          
        </ComfirmAction>

      </Dialog>
      

    </ContainerSearchFlow>
  )
}
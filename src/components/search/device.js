import React from 'react'

// Search Form
import CustomeContentBody from 'src/components/material/formsearch/Body/Form'
import CustomeContentSpace from 'src/components/material/formsearch/Body/Space'
import CustomeContentTextBox from 'src/components/material/formsearch/Form/InputId'

// setting style 
import useStyles from 'src/components/device/setting/style'
import { SEARCHDEVICE } from 'src/stores/actions';

export default function SearchDevice(props) {
  const classes = useStyles();
  const [store] = React.useState(props.store)

  const [ gateName, setGateName ] = React.useState("")

  const handleChangeGateName = (e) => {
    setGateName(e.target.value)
    var searche = store.getState().SearchDevice
    searche.search.device = e.target.value
    store.dispatch(SEARCHDEVICE(searche))
  }

  React.useEffect(() => {
    const LoadData = () => {
      setGateName(store.getState().SearchDevice.search.device)
    }
    var interval_ = setInterval(() => {
      LoadData()
    })
    return () => {
      clearInterval(interval_)
    }
  }, [store])

  return (
    <div>

      {/* ตำแหน่งประตู */}
      <CustomeContentBody>
        <CustomeContentSpace />
        <CustomeContentTextBox 
          className={classes.InputIdControl}
          variant="outlined" 
            
          labelwidth={0}
          placeholder="ตำแหน่งประตู" 
          fullWidth={true}
          autoComplete='off'
          value={gateName}
          onChange={handleChangeGateName}
        />
        <CustomeContentSpace />
      </CustomeContentBody>

    </div>
  )
}
import React from 'react';
import PropTypes from 'prop-types';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import clsx from 'clsx'
import { makeStyles } from '@material-ui/core/styles';

import "src/components/TablePaging/Admin/TableAdmin.css"

// common lib
import pasesFullDate from "src/commons/pasesFullDate"

// import TimePicker from '@material-ui/lab/TimePicker';
import Paper from '@material-ui/core/Paper';
import { IconButton } from '@material-ui/core';
import styled from 'styled-components';

// store listuser
import { LISTUSER } from 'src/stores/actions'

// setting style 
import useStyles from './style'

// svg image
import Edit from 'src/asset/img/setting/EDIT.svg'

// Confirm dialog
import Success from 'src/components/ComfirmAction/Success'
import Unsuccess from 'src/components/ComfirmAction/Unsuccess'
import Backdrop from '@material-ui/core/Backdrop'
import CircularProgress from '@material-ui/core/CircularProgress'
import Confirm from 'src/components/ComfirmAction/Confirm'  

// dialog remove admin
import Dialog from '@material-ui/core/Dialog'
import CustomeDialogContentUpdateFWTextUpdateFW from "src/components/material/dialog/UpdateFirmware/CustomeDialogContent"
import CustomeDialogContentUpdateFW from 'src/components/material/dialog/UpdateFirmware/CustomeDialogContent'
import CustomeDialogTitleUpdateFW from 'src/components/material/dialog/UpdateFirmware/CustomeDialogTitle'
import CustomeExitDialogUpdateFW from 'src/components/material/dialog/UpdateFirmware/CustomeExitDialog'
import CustomeDialogContentManageDV from 'src/components/material/dialog/MangeDevice/CustomeDialogContent'
import CustomeDialogTitleManageDV from 'src/components/material/dialog/MangeDevice/CustomeDialogTitle'
import CustomeExitDialogManageDV from 'src/components/material/dialog/MangeDevice/CustomeExitDialog'
import ActionManageDV from "src/components/material/dialog/MangeDevice/content/Action"
import BodyManageDV from "src/components/material/dialog/MangeDevice/content/Body"
import BodyForm from 'src/components/material/dialog/MangeDevice/content/Body/Form'
import BodyLabel from 'src/components/material/dialog/MangeDevice/content/Body/Label'
import BodySpace from 'src/components/material/dialog/MangeDevice/content/Body/Space'
import InputFromId from 'src/components/material/dialog/MangeDevice/content/Form/InputId'
import InputAdornment from '@material-ui/core/InputAdornment';
// img
import Exit from 'src/asset/img/setting/EXIT.svg'

// button component
import Choice1Btn from 'src/components/material/button/Choice1Btn'
import Choice2Btn from 'src/components/material/button/Choice2Btn'

// api remove admin
import RemoveAdmin from 'src/apis/removeUser'
import getAdmin from 'src/apis/getUser'
import ChangePasswordAdmin from 'src/apis/changePasswordAdmin'
import unlockuser from 'src/apis/unlockuser';

// commons
import PasswordChecker from 'src/commons/passwordChecker'

// icons
import UnlockIcon from '@material-ui/icons/LockOpen'
import DeleteForeverIcon from '@material-ui/icons/DeleteForever'
import VisibilityOffIcon from '@material-ui/icons/Visibility'
import VisibilityIcon from '@material-ui/icons/VisibilityOff'

const StutusContrainer = styled('div')`
  padding: 20px 20px 20px 30px;
`
const ComfirmAction = styled("div")`
  display: flex;
  align-content: center;
  justify-content: space-around;
  margin: 20px;
`

const useStylesBackdrop = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff"
  }
}));

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

function TablePagingTableHead(props) {
  const [ headCells ] = React.useState(props.headCells)
  const { classes, order, orderBy, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        <TableCell align={'left'} style={{width: 34, height: 34}} />
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={'left'}
            padding={headCell.disablePadding ? 'none' : 'default'}
            sortDirection={orderBy === headCell.id ? order : false}
            style={{fontSize: 18}}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
              style={{fontSize: 18}}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
        <TableCell align={'left'} style={{width: "10px"}} />
      </TableRow>
    </TableHead>
  );
}

TablePagingTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
  headCells: PropTypes.array.isRequired,
};

export default function AdminManange(props) {

  const classes = useStyles();
  const classesbd = useStylesBackdrop()

  const [ store ] = React.useState(props.store)

  const [ openDialogFW, setopenDialogFW ]  = React.useState(false)

  const [ AdminID, setAdminID ] = React.useState("")

  const [order, setOrder] = React.useState('desc');
  const [orderBy, setOrderBy] = React.useState('name');
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [ rows, setrows] = React.useState(undefined)

  const [headCells, setHeadCells] = React.useState([])

  const [canEditPassword, setCanEditPassword] = React.useState(false)
  const [isOpenAdminChangePassword, setIsOpenAdminChangePassword] = React.useState(false)

  const [changePassUserID, setChangePassUserID] = React.useState('')
  const [changePassUsername, setChangePassUsername] = React.useState('')

  const [newPassword, setNewPassword] = React.useState('')
  const [confirmNewPassword, setConfirmNewPassword] = React.useState('')
  const [ AddPassError, setAddPassError ] = React.useState(false)
  const [ AddPassCond, setAddPassCond ] = React.useState([])
  const [ isNewPasswordVisible, setIsNewPasswordVisible ] = React.useState(false)
  const [ isConfirmNewPasswordVisible, setIsConfirmNewPasswordVisible ] = React.useState(false)

  const [Perms, setPerms] = React.useState("admin")

  const [ openDialogComfirm, setopenDialogComfirm ] = React.useState(false)
  const [ openDialogComfirmStatus, setopenDialogComfirmStatus ] = React.useState(false)
  const [ TitleAction, setTitleAction ] = React.useState("")
  const [ MessageError, setMessageError ] = React.useState("")
  const [ OpenBackdrop, setOpenBackdrop ] = React.useState(false)
  const [ openDialogComfirm2, setopenDialogComfirm2 ] = React.useState(false)
  const [ openDialogComfirmStatus2, setopenDialogComfirmStatus2 ] = React.useState(false)

  const [ LockUserName, setLockUserName ] = React.useState("")
  const [ LockUserId, setLockUserId ] = React.useState("")

  React.useEffect(() => {
    const updateListUser = () => {
      if (store.getState().User !== undefined){
        setPerms(store.getState().User.perms)
      }
      if (store.getState().ListUser !== undefined){
        var users = store.getState().ListUser
        var newusers = []
        users.forEach((row) => {
          newusers.push({
            id: row.user_id,
            name: row.username,
            lastactive: row.last_active,
            role: row.role.role_name,
            is_locked: row.is_locked
          })
        })
        setrows(newusers)

      }
    }
    const interval_ = setInterval(() => {
      updateListUser()
    }, 200);

    return () => {
      clearInterval(interval_)
    }
  }, [store])

  React.useEffect(() => {
    if (store.getState().User !== undefined &&
        store.getState().User.perms === 'admin') {
      setHeadCells(
        [
          { id: 'unlock', numeric: true, disablePadding: false, label: '' },
          { id: 'name', numeric: true, disablePadding: false, label: 'Username' },
          { id: 'level', numeric: true, disablePadding: false, label: 'User Level'},
          { id: 'lastactive', numeric: true, disablePadding: false, label: 'Last Login' },
          { id: 'changepassword', numeric: true, disablePadding: false, label: 'Password' }
        ]
      )
      setCanEditPassword(true)
    } else {
      setHeadCells(
        [
          { id: 'name', numeric: true, disablePadding: false, label: 'Username' },
          { id: 'lastactive', numeric: true, disablePadding: false, label: 'Last Login' }
        ]
      )
      setCanEditPassword(false)
    }
  }, [store])

  const handleCloseDialogFW = () => {
    setopenDialogFW(false)
  }

  
  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = rows.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleOpenAdminChangePassword = (id, username) => {
    setChangePassUserID(id)
    setChangePassUsername(username)
    setIsOpenAdminChangePassword(true)
  }

  const handleCloseAdminChangePassword = () => {
    setChangePassUserID('')
    setChangePassUsername('')
    setIsOpenAdminChangePassword(false)
    setNewPassword('')
    setConfirmNewPassword('')
    setAddPassError(false)
    setAddPassCond([])
  }

  const handleAdminChangePassword = async () => {
    let anyPasswordError = PasswordChecker(newPassword)
    if (anyPasswordError.length === 0) {
      if (newPassword === confirmNewPassword) {
        setAddPassError(false)
        setAddPassCond([])
        const Token = store.getState().AccessToken
        const res = await ChangePasswordAdmin({
          query: `
            mutation{
              EditPasswordByAdmin (
                userId: ${changePassUserID} ,
                newPassword:"${newPassword}") {
                  result
              }
            }
          `
        }, Token)
        if (res.data.data.EditPasswordByAdmin.result === true) {
          alert("เปลี่ยนรหัสผ่านสำเร็จ")
          setChangePassUserID('')
          setChangePassUsername('')
          setIsOpenAdminChangePassword(false)
          setNewPassword('')
          setConfirmNewPassword('')
          setAddPassError(false)
          setAddPassCond([])
          setIsNewPasswordVisible(false)
          setIsConfirmNewPasswordVisible(false)
        } else {
          if (res.data.errors[0].message === "MATCHED OLD PASSWORD") {
            alert("ไม่สามารถใช้รหัสผ่านเก่าได้")
          } else {
            alert("ไม่สามารถเปลี่ยนรหัสผ่านได้")
          }
          setAddPassError(true)
        }
      } else {
        setAddPassError(true)
        setAddPassCond([5]) // 5 means AddPassword not equals AddConfirmPass
      }
    } else {
      setAddPassError(true)
      setAddPassCond(anyPasswordError)
    }
  }

  const handleUnlockUser = async () => {
    setOpenBackdrop(true)
    var user_id = LockUserId
    var token = store.getState().AccessToken
    console.log(token)
    const res = await unlockuser({
      query: `
        mutation{
            UnlockUser (userId: ${Number(user_id)}) {
                result
            }
        }
      `
    }, token)
    if (res.data.errors !== undefined){
      alert(res.data.errors[0].message)
    }
    setTimeout(() => {
      setOpenBackdrop(false)
    }, 1000);
  }

  const notFoundData = (colSpan) => {
    return <TableRow >
      <TableCell colSpan={colSpan} style={{textAlign: "center"}}>ไม่พบข้อมูล</TableCell >
    </TableRow>
  }

  return (
    <StutusContrainer>

      {
        ((rows === undefined) ? "ไม่พบข้อมูล": <Paper className={classes.paper}>
          <TableContainer className={classes.container} id="table-containner-custom-scroll-log">
            <Table
              className={classes.table}

              // aria-labelledby="tableTitle"
              // size={'medium'}
              // aria-label="TablePaging table"

              stickyHeader 
              aria-label="sticky table"
            >
              <TablePagingTableHead
                classes={classes}
                numSelected={selected.length}
                order={order}
                orderBy={orderBy}
                onSelectAllClick={handleSelectAllClick}
                onRequestSort={handleRequestSort}
                rowCount={rows.length}
                headCells={headCells}
              />
              <TableBody>
                {
                  (rows.length === 0 ? notFoundData(7) : stableSort(rows, getComparator(order, orderBy))
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => {

                    return (

                      <TableRow
                        hover
                        // onClick={() => console.log(row.id)}
                        role="checkbox"
                        tabIndex={-1}
                        key={row.id}
                      >
                        <TableCell style={{ width: '0%' }} />
                        <TableCell align="left" width={10} style={{fontSize: 18}}>
                          {/* {row.is_locked ?  */}
                          {row.is_locked && Perms === String.fromCharCode(97, 100, 109, 105, 110) ? 
                            <UnlockIcon
                            style={{ 
                              color: '#01A6E6', 
                              cursor: 'pointer' 
                            }}
                            onClick={async () => {
                              // var user_id = row.id
                              // var token = store.getState().AccessToken
                              // await unlockuser({
                              //   query: `
                              //     mutation{
                              //         UnlockUser (userId: ${Number(user_id)}) {
                              //             result
                              //         }
                              //     }
                              //   `,
                              //   token
                              // })
                              setLockUserId(row.id)
                              setLockUserName(row.name)
                              setopenDialogComfirm(true)
                              setTitleAction("คุณต้องการปลดล๊อกผู้ใช้นี้ไหม")              
                              setopenDialogComfirmStatus(true)
                              setMessageError("")

                            }}
                            /> : null
                          }
                        </TableCell>
                        <TableCell align="left" style={{fontSize: 18}}>

                          {
                            row.name
                          }
                          
                        </TableCell>
                        <TableCell align="left" style={{fontSize: 18}}>
                          {
                            row.role.charAt(0).toUpperCase() + row.role.slice(1)
                          }
                        </TableCell>
                        <TableCell align="left" className={classes.sizeFont}>
                          {pasesFullDate(row.lastactive)}
                        </TableCell>
                        {
                          canEditPassword ?
                          <TableCell align="left" 
                          style={{
                            fontSize: 18,
                            textDecoration: "underline",
                            cursor: "pointer",
                            color: "#4472C4"}}
                            onClick={() => handleOpenAdminChangePassword(row.id, row.name)}>
                            แก้ไข
                          </TableCell> : null
                        }
                        <TableCell align="right">
                          <IconButton style={{
                            width: "10",
                            height: "10",
                          }}
                          aria-controls="customized-menu"
                          aria-haspopup="true"
                          variant="contained"
                          color="secondary"
                          onClick={(e) => {
                            setopenDialogFW(true)
                            setAdminID(row.id)
                          }}
                          >
                            <DeleteForeverIcon 
                            style={{ 
                              fontSize: '20px',
                              color: '#01A6E6'
                            }}/>
                          </IconButton>
                        </TableCell>
                        
                      </TableRow>
                    );
                  }))
                }
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[5, 10, 25, 50, 100, 1000]}
            component="div"
            count={rows.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
          />
        </Paper>)
      }
      {/* {
        ((location.pathname.split("/").length === 5) ? <Outlet />: 
          <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="customized table">
              <TableHead>
                <TableRow>
                  <CustomeTableCell align="left" style={{ width: 10 }} ></CustomeTableCell>
                  <CustomeTableCell align="left">Username</CustomeTableCell>
                  <CustomeTableCell align="left">Last Active</CustomeTableCell>
                  <CustomeTableCell align="right"></CustomeTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {ListUser.map((row, index) => (

                    ((row.username === 'admin') ? null : <CustomeTableRow key={row.user_id}>

                    <CustomeTableCell align="left"></CustomeTableCell>
                      
                    <CustomeTableCell align="left" >
                      
                      <ContentName>
                        { row.username }
                      </ContentName>
                      
                    </CustomeTableCell>
    
    
                    <CustomeTableCell align="left" >
                      
                      <ContentName>
                        {
                          pasesFullDate(row.last_active)
                        }
                      </ContentName>
                      
                    </CustomeTableCell>
    
                    <CustomeTableCell aling="right" style={{
                        display: "flex",
                        alignContent: "center",
                        alignItems: "center",
                        justifyContent: "flex-end",
    
                    }}>
                      <IconButton style={{
                        width: "10",
                        height: "10",
                      }}
                      aria-controls="customized-menu"
                      aria-haspopup="true"
                      variant="contained"
                      color="secondary"
                      onClick={(e) => {
                        setopenDialogFW(true)
                        setAdminID(row.user_id)
                      }}
                      >
                        <img src={Edit} alt="Edit" />
                      </IconButton>
                    </CustomeTableCell>
    
                  </CustomeTableRow>)

                ))}
              </TableBody>
            </Table>
          </TableContainer>
        )
      } */}

      {/* Dialog for Admin change password */}
      <Dialog
        disableBackdropClick={true}
        fullWidth={false}
        maxWidth={"xl"}
        open={isOpenAdminChangePassword}
        onClose={handleCloseAdminChangePassword}
        aria-labelledby="admin-change-password-dialog">
        <CustomeExitDialogManageDV>
          <IconButton onClick={() => {
            setIsOpenAdminChangePassword(false)
            setChangePassUserID("")
            setChangePassUsername("")
            setNewPassword("")
            setConfirmNewPassword("")
            setAddPassError(false)
            setAddPassCond([])
            setIsNewPasswordVisible(false)
            setIsConfirmNewPasswordVisible(false)
          }}>
            <img src={Exit} alt="Exit" />
          </IconButton>
        </CustomeExitDialogManageDV>
        <CustomeDialogTitleManageDV 
          id="admin-change-password-dialog"
          children={<div style={{
            fontSize: 28,
            textAlign: "center",
            width: 400
          }}>แก้ไข Password {changePassUsername}</div>}
        ></CustomeDialogTitleManageDV>
        <CustomeDialogContentManageDV>
          <BodyManageDV style={{
            paddingTop: "0px"
          }}>
            <BodySpace style={{
              padding: "10px 10px 0px 10px"
            }}></BodySpace>
            <BodyLabel style={{
              padding: "10px 10px 0px 10px",
              textAlign: "left"
            }}>New Password</BodyLabel>
            <BodyForm style={{
              padding: "10px 10px 0px 10px"
            }}>
                  
              <InputFromId 
                className={ clsx(classes.InputIdControl) }
                error={AddPassError}
                variant="outlined" 
                labelwidth={0}
                placeholder="New Password" 
                type={isNewPasswordVisible ? "text" : "password"}
                fullWidth={true}
                autoComplete='off'
                value={newPassword}
                onChange={(e) => {
                  setNewPassword(e.target.value)
                }}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        edge="end"
                        onClick={() => {
                          setIsNewPasswordVisible(!isNewPasswordVisible)
                        }}
                      >
                        {
                          isNewPasswordVisible ?
                          <VisibilityOffIcon style={{color: "#BABABA"}} /> :
                          <VisibilityIcon style={{color: "#BABABA"}} />
                        }
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
              />
            </BodyForm>
            <BodySpace style={{
              padding: "10px 10px 0px 10px"
            }}></BodySpace>
          </BodyManageDV>
          
          <BodyManageDV style={{
            paddingTop: "5px",
            display: (AddPassCond.length === 0) ? 'none' : 'flex'
          }}>
            <BodySpace style={{
              padding: "10px 120px 0px 10px"
            }}></BodySpace>
            <div>
              <ul style={{ padding: '0', textAlign: 'left', color: 'red' }}>
                <li style={{display: AddPassCond.includes(0) ? 'list-item' : 'none' }}>
                  รหัสผ่านต้องมีความยาวมากกว่า 8 ตัวอักษร
                </li>
                <li style={{display: AddPassCond.includes(1) ? 'list-item' : 'none' }}>
                  รหัสผ่านต้องประกอบไปด้วยตัวพิมพ์ใหญ่อย่างน้อย 1 ตัว
                </li>
                <li style={{display: AddPassCond.includes(2) ? 'list-item' : 'none' }}>
                  รหัสผ่านต้องประกอบไปด้วยตัวพิมพ์เล็กอย่างน้อย 1 ตัว
                </li>
                <li style={{display: AddPassCond.includes(3) ? 'list-item' : 'none' }}>
                  รหัสผ่านต้องประกอบไปด้วยตัวเลขอย่างน้อย 1 ตัว
                </li>
                <li style={{display: AddPassCond.includes(4) ? 'list-item' : 'none' }}>
                  รหัสผ่านต้องประกอบไปด้วยอักขระพิเศษอย่างน้อย 1 ตัว
                </li>
              </ul>
            </div>
          </BodyManageDV>

          <BodyManageDV style={{
            paddingTop: "0px"
          }}>
            <BodySpace style={{
              padding: "10px 10px 0px 10px"
            }}></BodySpace>
            <BodyLabel style={{
              padding: "10px 10px 0px 10px",
              textAlign: "left"
            }}>Confirm Password</BodyLabel>
            <BodyForm style={{
              padding: "10px 10px 0px 10px"
            }}>
                
              <InputFromId 
                className={ clsx(classes.InputIdControl) }
                error={AddPassError}
                variant="outlined" 
                labelwidth={0}
                placeholder="Confirm New Password" 
                type={isConfirmNewPasswordVisible ? "text" : "password"}
                fullWidth={true}
                autoComplete='off'
                value={confirmNewPassword}
                onChange={(e) => {
                  setConfirmNewPassword(e.target.value)
                }}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        edge="end"
                        onClick={() => {
                          setIsConfirmNewPasswordVisible(!isConfirmNewPasswordVisible)
                        }}
                      >
                        {
                          isConfirmNewPasswordVisible ?
                          <VisibilityOffIcon style={{color: "#BABABA"}} /> :
                          <VisibilityIcon style={{color: "#BABABA"}} />
                        }
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
              />

            </BodyForm>
            <BodySpace style={{
              padding: "10px 10px 0px 10px"
            }}></BodySpace>
          </BodyManageDV>

          <BodyManageDV style={{
            paddingTop: "5px",
            display: (AddPassCond.length === 0) ? 'none' : 'flex'
          }}>
            <BodySpace style={{
              padding: "10px 80px 0px 10px"
            }}></BodySpace>
            <div>
              <ul style={{ padding: '0', textAlign: 'left', color: 'red' }}>
                <li style={{display: AddPassCond.includes(5) ? 'list-item' : 'none' }}>
                  โปรดตรวจสอบ Confirm Password ให้ถูกต้อง
                </li>
              </ul>
            </div>
          </BodyManageDV>

          <ActionManageDV>
            <div style={{
              display: "flex",
              justifyContent: "space-between",
              width: "100%",
              padding: "20px calc(10% + 30px) 0px calc(10% + 30px)"
            }}>
              <Choice1Btn style={{
                width: 250, 
                height: 60
              }} onClick={() => {
                setIsOpenAdminChangePassword(false)
                setChangePassUserID("")
                setChangePassUsername("")
                setNewPassword("")
                setConfirmNewPassword("")
                setAddPassError(false)
                setAddPassCond([])
                setIsNewPasswordVisible(false)
                setIsConfirmNewPasswordVisible(false)
              }}>ยกเลิก</Choice1Btn>
              <Choice2Btn style={{
                width: 250, 
                height: 60
              }} onClick={() => {
                handleAdminChangePassword()
              }}>ยืนยัน</Choice2Btn>
            </div>
          </ActionManageDV>

        </CustomeDialogContentManageDV>
      </Dialog>

      {/* Dialog for Update Software */}
      <Dialog
        disableBackdropClick={true}
        fullWidth={false}
        maxWidth={"xl"}
        open={openDialogFW}
        onClose={handleCloseDialogFW}
        aria-labelledby="update-dialog-fw-fw"
      >
        <CustomeExitDialogUpdateFW>
          <IconButton onClick={() => {
            setopenDialogFW(false)
          }}>
            <img src={Exit} alt="Exit" />
          </IconButton>
        </CustomeExitDialogUpdateFW>
        <CustomeDialogTitleUpdateFW id="update-dialog-fw-fw" style={{textAlign: "center"}}>ลบข้อมูล</CustomeDialogTitleUpdateFW>
        <CustomeDialogContentUpdateFW>
          <CustomeDialogContentUpdateFWTextUpdateFW>
            คุณยืนยันที่จะลบข้อมูล ใช่หรือไม่?
          </CustomeDialogContentUpdateFWTextUpdateFW>
          <div style={{
            display: "flex",  
            justifyContent: "space-between"
          }}>
            <Choice1Btn
            style={{
              width: 250, 
              height: 60,
              marginRight: 20,
              border: "1px solid #CC1503",
              color: '#CC1503'
            }}
            onClick={() => {
              setopenDialogFW(false)
            }}
            >ไม่</Choice1Btn>
            <Choice2Btn 
              style={{
                width: 250, 
                height: 60,
                border: "1px solid #CC1503",
                color: '#fff',
                backgroundColor: "#CC1503"
              }}
              onClick={async () => {
                console.log(AdminID)
                var token = store.getState().AccessToken
                setOpenBackdrop(true)
                setopenDialogFW(false)
                const res = await RemoveAdmin({
                  query: `
                    mutation{
                      RemoveUser(userId: ${AdminID}) {
                          result
                      }
                    }
                  `
                }, token)
                if (res.data.errors === undefined){
                  // alert("แอดมินถูกลบเรียบร้อยแล้ว")
                  // load user data
                  const res_getUser = await getAdmin({
                  query: `
                    {
                      GetUserAdmin (orderBy:"create_at" , orderType:"desc") {
                        results {
                          user_id
                          role {
                              role_name
                          }
                          username
                          name
                          email
                          employee_code
                          create_at
                          update_at
                          last_active
                          update_password_at
                        }
                      }
                    }
                  `
                  }, token)
                  store.dispatch(LISTUSER(res_getUser.data.data.GetUserAdmin.results))

                  setOpenBackdrop(false)
                  setTitleAction(`ลบบัญชีผู้ใช้`)
                  setopenDialogComfirm2(true)
                  setopenDialogComfirmStatus2(true)
                }else {
                  setOpenBackdrop(false)
                  setTitleAction(res.data.errors[0].message)
                  setopenDialogComfirm2(true)
                  setopenDialogComfirmStatus2(false)
                }
                setopenDialogFW(false)
              }}
            >ยืนยัน</Choice2Btn>
          </div>
        </CustomeDialogContentUpdateFW>
      </Dialog>

      <Dialog
          disableBackdropClick={true}
          fullWidth={false}
          maxWidth={"xl"}
          open={openDialogComfirm}
          aria-labelledby="update-dialog-MangeDV"
        >
            <Confirm Message={<div>คุณต้องการปลดล็อคผู้ใช้ <span style={{color: "#01A6E6"}}>{LockUserName}</span> หรือไม่</div>} title={"ลบอุปกรณ์"}/>
            <ComfirmAction>

              <Choice2Btn 
                style={{
                  width: 380, 
                  height: 60,
                  marginRight: 20
                }}
                onClick={() => {
                  setopenDialogComfirm(false)
                  handleUnlockUser()
                }}
              >
                ยืนยัน
              </Choice2Btn>

              <Choice2Btn 
                style={{
                  width: 380, 
                  height: 60,
                  backgroundColor: "#fff",
                  color: "#01A6E6",
                  marginLeft: 20
                }}
                onClick={() => {
                  setopenDialogComfirm(false)
                }}
              >
                ยกเลิก
              </Choice2Btn>
              
            </ComfirmAction>

        </Dialog>
        
        
        <Dialog
          disableBackdropClick={true}
          fullWidth={false}
          maxWidth={"xl"}
          open={openDialogComfirm2}
          aria-labelledby="update-dialog-MangeDV"
        >
            {
              (openDialogComfirmStatus2 ? <Success title={TitleAction}/> : <Unsuccess title={TitleAction} msgerror={MessageError}/>)
            }
            <ComfirmAction>

              {
                (openDialogComfirmStatus2 ? <Choice2Btn 
                  style={{
                    width: 800, 
                    height: 60
                  }}
                  onClick={() => {
                    setopenDialogComfirm2(false)
                  }}
                >
                  ยืนยัน
                </Choice2Btn> : <ComfirmAction>
                  <Choice2Btn 
                    style={{
                      width: 800, 
                      height: 60,
                      backgroundColor: "#e46f6f",
                      border: "red"
                    }}
                    onClick={() => {
                      setopenDialogComfirm2(false)
                    }}
                  >
                    ยืนยัน
                  </Choice2Btn>
                </ComfirmAction>)
              }
              
            </ComfirmAction>

        </Dialog>

        <Backdrop className={classesbd.backdrop} open={OpenBackdrop}>
          <CircularProgress color="inherit" />
        </Backdrop>


    </StutusContrainer>

  );
}

 
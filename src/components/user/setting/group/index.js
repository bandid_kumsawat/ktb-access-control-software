import React from 'react';
import { useParams } from 'react-router-dom';
import PropTypes from 'prop-types';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Tooltip from '@material-ui/core/Tooltip';

import "src/components/TablePaging/Employee/TableEmployee.css"
import { makeStyles } from '@material-ui/core/styles';

import server from "src/apis/config"

// image
import Edit from 'src/asset/img/setting/EDIT.svg'
import Exit from 'src/asset/img/setting/EXIT.svg'
import ADDGROUP from "src/asset/img/sidebar/ADDGROUP.svg"
import REMOVEDOORACCESS from "src/asset/img/sidebar/REMOVEDOORACCESS.svg"
import EXPORTEMPTY from "src/asset/img/employee/exportemployee.svg"
import EditIcon from '@material-ui/icons/Edit';

// import TimePicker from '@material-ui/lab/TimePicker';
import Paper from '@material-ui/core/Paper';
import styled from 'styled-components';

// apis
import getEmployeeByGroup from 'src/apis/getEmployeeByGroup';
import editEmployee from 'src/apis/editEmployee';
import removeEmployee from 'src/apis/removeEmployee'

import Fromto from 'src/components/material/dialog/MangeDevice/content/Body/Fromto'
import TimePicker from 'src/components/GlobalDialog/TimePicker';

// dialog update Device component
import Dialog from '@material-ui/core/Dialog'
import CustomeDialogContentManageDV from 'src/components/material/dialog/MangeDevice/CustomeDialogContent'
import CustomeDialogTitleManageDV from 'src/components/material/dialog/MangeDevice/CustomeDialogTitle'
import CustomeExitDialogManageDV from 'src/components/material/dialog/MangeDevice/CustomeExitDialog'
import CustomeExitDialogUpdateFW from 'src/components/material/dialog/UpdateFirmware/CustomeExitDialog'
import CustomeDialogContentUpdateFWTextUpdateFW from "src/components/material/dialog/UpdateFirmware/CustomeDialogContent"
import CustomeDialogContentUpdateFW from 'src/components/material/dialog/UpdateFirmware/CustomeDialogContent'
import CustomeDialogTitleUpdateFW from 'src/components/material/dialog/UpdateFirmware/CustomeDialogTitle'
import ActionManageDV from "src/components/material/dialog/MangeDevice/content/Action"
import BodyManageDV from "src/components/material/dialog/MangeDevice/content/Body"
import HeaderManageDV from "src/components/material/dialog/MangeDevice/content/Header"
import BodyManageDVAddDevice from 'src/components/material/dialog/MangeDevice/content/Header/Adddevice'
import BodyManageDVAddEmployer from 'src/components/material/dialog/MangeDevice/content/Header/AddEmployer'
import BodyForm from 'src/components/material/dialog/MangeDevice/content/Body/Form'
import BodyLabel from 'src/components/material/dialog/MangeDevice/content/Body/Label'
import BodySpace from 'src/components/material/dialog/MangeDevice/content/Body/Space'
import InputFromId from 'src/components/material/dialog/MangeDevice/content/Form/InputId'
import Checkbox from '@material-ui/core/Checkbox';

// button component
import Choice1Btn from 'src/components/material/button/Choice1Btn'
import Choice2Btn from 'src/components/material/button/Choice2Btn'

import { withStyles } from '@material-ui/core/styles';

// icon button
import { IconButton } from '@material-ui/core';

// setting style 
import useStyles from './style'

import MenuItem from '@material-ui/core/MenuItem';

// store listuser
import { EMPLOYEE } from 'src/stores/actions'

import Success from 'src/components/ComfirmAction/Success'
import Unsuccess from 'src/components/ComfirmAction/Unsuccess'
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";

// From Select
import FormControl from "@material-ui/core/FormControl"
import Select from "@material-ui/core/Select"
import InputBase from '@material-ui/core/InputBase'
const BootstrapInput = withStyles((theme) => ({
  root: {
    'label + &': {
      marginTop: theme.spacing(3),
    },
  },
  input: {
    textAlign: "left",
    borderRadius: 4,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #ccc',
    fontSize: 16,
    padding: '10px 47px 13px 12px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:focus': {
      borderRadius: 4,
      // borderColor: '#80bdff',
      // boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
      border: '1px solid #337AB7',
    },
  },
}))(InputBase);


const GroupUserContrainer = styled('div')`
  padding: 0px 0px 0px 0px;
`

const ContentName = styled('div')`
  display: flex;
  align-items: center;
  align-content: center;
`

const GroupName = styled('div')`
padding: 0px 0px 0px 6px;
font-size: 18px;
`

const CustomTooltip = withStyles({
  tooltip: {
    color: 'black',
    backgroundColor: 'white',
    boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)",
    textAlign: "center",
    maxWidth: "none"
  },
  arrow: {
    color: 'white',
  },
})(Tooltip);

const ComfirmAction = styled("div")`
  display: flex;
  align-content: center;
  justify-content: space-around;
  margin: 20px;
`

const useStylesNavbar = makeStyles((theme) => ({
  Visible: {
    display: "block",
  },
  InVisible: {
    display: "none",
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff"
  }
}));


function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

function TablePagingTableHead(props) {
  const [ headCells ] = React.useState(props.headCells)
  const { classes, order, orderBy, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        <TableCell align={'left'} style={{width: 34, height: 34}} />
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={'left'}
            padding={headCell.disablePadding ? 'none' : 'default'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
              style={{fontSize: 18}}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
        <TableCell align={'left'} style={{width: 34, height: 34}} />
      </TableRow>
    </TableHead>
  );
}

TablePagingTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
  headCells: PropTypes.array.isRequired,
};
// ASC (Default) เรียงจาก น้อยไปมาก
// DESC เรียงจาก มากไปน้อย

export default function UserGroup(props) {
  const classes = useStyles();
  const navbarclasses = useStylesNavbar()
  let { group_id } = useParams();
  let { group_name } = useParams();

  const inputFile = React.createRef()

  const [ ImageFile, setImageFile ] = React.useState([null])

  const [ store ] = React.useState(props.store)
  const [ UserGroups, setUserGroups ] = React.useState(undefined)
  const [ Token, setToken ] = React.useState([])
  const [ openDialogEditUserToGroup, setopenDialogEditUserToGroup ] = React.useState(false)

  // add group
  const [ AccessDoorArr, setAccessDoorArr ] = React.useState([undefined])

  const [ EditEmployeeName, setEditEmployeeName ] = React.useState("") 
  const [ EditEmployeeSurname, setEditEmployeeSurname ] = React.useState("") 
  const [ EditEmployeePostion, setEditEmployeePostion ] = React.useState("") 
  const [ EditEmployeeCard, setEditEmployeeCard ] = React.useState("") 
  const [ EditEmployeeProfile, setEditEmployeeProfile ] = React.useState(null) 

  const [ TimeToValidation, setTimeToValidation ] = React.useState(false)
  const [ TimeFromValidation, setTimeFromValidation ] = React.useState(false)

  const [ DeviceList, setDeviceList ] = React.useState([])
  const [ EmployeeId, setEmployeeId] = React.useState([])
  const [ GroupDeviceList, setGroupDeviceList ] = React.useState([])

  const [ EditEmployeeEmail, setEditEmployeeEmail ] = React.useState([])
  const [ EditEmployeePresonalID, setEditEmployeePresonalID ] = React.useState([])

  const [ EditAccessDoorArr, setEditAccessDoorArr ] = React.useState([{
    doorID: undefined,
    isTimeInOut: false,
    timeBegin: "00:00",
    timeEnd: "23:59",
    isAllTime: false,
    TimeFromValidation: false,
    TimeToValidation: false,
  }])

  const [ EditEmployeeNameValidation, setEditEmployeeNameValidation ] = React.useState(false)
  const [ EditEmployeeSurnameValidation, setEditEmployeeSurnameValidation ] = React.useState(false)
  const [ EditEmployeePostionValidation, setEditEmployeePostionValidation ] = React.useState(false)
  const [ EditEmployeeCardValidation, setEditEmployeeCardValidation ] = React.useState(false)
  const [ EditEmployeeEmailValidation, setEditEmployeeEmailValidation ] = React.useState(false)
  const [ EditEmployeePresonalIDValidation, setEditEmployeePresonalIDValidation ] = React.useState(false)
  const [ EditEmployeeProfileValidation, setEditEmployeeProfileValidation ] = React.useState(false)
  const [ EditAccessDoorArrValidation, setEditAccessDoorArrValidation ] = React.useState(false)
  const [ SelectAll, setSelectAll ] = React.useState(false)

  const [ isOpenDeleteEmployeeDialog, setIsOpenDeleteEmployeeDialog ] = React.useState(false)

  const [ openDialogComfirm, setopenDialogComfirm ] = React.useState(false)
  const [ TitleAction, setTitleAction ] = React.useState("")
  const [ OpenBackdrop, setOpenBackdrop ] = React.useState(false)
  const [ openDialogComfirmStatus, setopenDialogComfirmStatus ] = React.useState(true)
  const [ MessageError, setMessageError ] = React.useState("")

  const [order, setOrder] = React.useState('desc');
  const [orderBy, setOrderBy] = React.useState('name');
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [ rows, setrows] = React.useState(undefined)

  const headCells = [
    { id: 'name', numeric: true, disablePadding: false, label: 'ชื่อ-นามสกุล' },
    { id: 'position', numeric: true, disablePadding: false, label: 'ตำแหน่ง' },
    { id: 'personal_id', numeric: true, disablePadding: false, label: 'รหัสพนักงาน' },
    { id: 'email', numeric: true, disablePadding: false, label: 'อีเมล' },
    { id: 'mifare_id', numeric: true, disablePadding: false, label: 'Card ID' },
    { id: 'gates', numeric: true, disablePadding: false, label: 'สิทธิ์เข้าออกประตู' },
  ];
  const handleCloseDialogEditUserToGroup = () => {
    setopenDialogEditUserToGroup(false)
  }

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = rows.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const reloadEmployee = async () => {
    const res = await getEmployeeByGroup({
      query: `
      {
        Employee (groups: [`+group_id+`]) {
          results {
              employee_id
              name
              lastname
              position
              mifare_id
              employee_code
              picture_url
              email
              groups {
                  group_id
                  name
              }
              gates {
                  gate_id,
                  name_gate,
                  begin_time,
                  end_time,
              }
              employee_gates {
                  gate_id
                  override_time
                  begin_time
                  end_time
              }
          }
          count
        }   
      }
      `,
    }, Token)
    if (res.data.errors === undefined){
      setopenDialogEditUserToGroup(false)
      setUserGroups(Object.assign([], [{
        employees: [],
        gates: [],
        group_id: "",
        name: ""
      }]))
      setUserGroups(Object.assign([], []))
      store.dispatch(EMPLOYEE([]))
      setTimeout(() => {
        setUserGroups(Object.assign([], res.data.data.Employee.results))
        store.dispatch(EMPLOYEE(res.data.data.Employee.results))
      }, 500);
    }
  }

  React.useEffect(() => {
    const loaaaa = async () => {
      const Token = store.getState().AccessToken
      const Group = store.getState().Group
      setToken(Token)
      const res = await getEmployeeByGroup({
        query: `
          {
            Employee (groups: [`+group_id+`]) {
              results {
                  employee_id
                  name
                  lastname
                  position
                  mifare_id
                  employee_code
                  picture_url
                  email
                  groups {
                      group_id
                      name
                  }
                  gates {
                      gate_id,
                      name_gate,
                      begin_time,
                      end_time,
                  }
                  employee_gates {
                      gate_id
                      override_time
                      begin_time
                      end_time
                  }
              }
              count
            }   
          }
        `,
      }, Token)
      try {
        var Group_name = Group.filter(item => Number(item.group_id) === Number(group_id));
        if (res.data.errors === undefined) {
          // eslint-disable-next-line
          if (res.data.data.Employee.count === 0) {
            store.dispatch(EMPLOYEE(res.data.data.Employee.results))
            document.getElementById(`show-group-name-${group_id}`).innerHTML  = "<a href=\"#/dashboard/user\" />ตั้งค่าบัญชีผู้ใช้</a> > " + (Group_name.length > 0 ? Group_name[0].name : "")
            setUserGroups(Object.assign([], res.data.data.Employee.results))
          } else {
            store.dispatch(EMPLOYEE(res.data.data.Employee.results))
            document.getElementById(`show-group-name-${group_id}`).innerHTML  = "<a href=\"#/dashboard/user\" />ตั้งค่าบัญชีผู้ใช้</a> > " + (Group_name.length > 0 ? Group_name[0].name : "")
            setUserGroups(Object.assign([], res.data.data.Employee.results))
          }
        }else {
          // eslint-disable-next-line
          document.getElementById(`show-group-name-${Token}`).innerHTML  = "<a href=\"#/dashboard/user\" />ตั้งค่าบัญชีผู้ใช้</a> > " + "ไอดีกรุ๊ปไม่สามารถใช้งานได้"
        }
      } catch (e) {
        console.log(e)
      }
    }
    loaaaa()
    const loadData = () => {
      var list = []
      var device = store.getState().Device
      var employee_ = store.getState().Employee
      if (device !== undefined && employee_ !== undefined){
        device.forEach((item, index) => {
          list.push({
            device_id: item.device_id,
            device_name: item.gate.name_gate
          })
        })
        setDeviceList(Object.assign([], list))
        var newemployee_ = []
        //console.log('Employee', employee_)
        employee_.forEach((row) => {
          var emp = {
            gid: row.groups.group_id,
            gname: row.groups.name,
            id: row.employee_id,
            face_id: row.face_id,
            lastname: row.lastname,
            mifare_id: row.mifare_id,
            name: row.name,
            picture_url: row.picture_url,
            position: row.position,
            email: row.email,
            personal_id: row.employee_code,
            gates: row.employee_gates,
            gates_info: row.gates
          }
          newemployee_.push(emp)
        })
        setrows(Object.assign([], newemployee_))
      }
    }
    loadData()
    var interval_ = setInterval(() => {
      loadData()
    }, 200);

    return () => {
      clearInterval(interval_)
    }

  }, [store, group_id])

  React.useEffect(() => {
    if (group_id !== undefined) {
      if (store.getState().Group !== undefined) {
        let list = []
        let g_id = parseInt(group_id)
        let availableGate = store.getState().Group.filter(obj => obj.group_id === g_id)
        availableGate = availableGate[0].gates
        availableGate.forEach((item, index) => {
          list.push({
            device_id: item.gate_id,
            device_name: item.name
          })
        })
        setGroupDeviceList(list)
      }
    }
  }, [group_id])

  const parseGroupUser = (accgroups) => {
    var str = ""
    if (accgroups !== undefined){
      accgroups.forEach((item, index) => {
        str += item.name_gate
        if (index !== accgroups.length - 1){
          str +=  '\n'
        }
      }) 
    }
    return str
  }

  const getUnselectDevice = () => {
    return EditAccessDoorArr.filter((obj) => obj.doorID === undefined)
  }

  const handleEditEmployee = async () => {

    var check = true

    const isEmail = (email) => {
      return (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email))
    }

    // Validation
    if (EditEmployeeName === "") {
      check = false
      setEditEmployeeNameValidation(true)
    } else {
      setEditEmployeeNameValidation(false)
    }
    if (EditEmployeeSurname === "") {
      check = false
      setEditEmployeeSurnameValidation(true)
    } else {
      setEditEmployeeSurnameValidation(false)
    }
    if (EditEmployeePostion === "") {
      check = false
      setEditEmployeePostionValidation(true)
    } else {
      setEditEmployeePostionValidation(false)
    }
    if (EditEmployeeCard === "") {
      check = false
      setEditEmployeeCardValidation(true)
    } else {
      setEditEmployeeCardValidation(false)
    }
    if (EditEmployeeEmail === "" || !isEmail(EditEmployeeEmail)) {
      check = false
      setEditEmployeeEmailValidation(true)
    } else {
      setEditEmployeeEmailValidation(false)
    }
    if (EditEmployeePresonalID === "") {
      check = false
      setEditEmployeePresonalIDValidation(true)
    } else {
      setEditEmployeePresonalIDValidation(false)
    }
    /*if (EditEmployeeProfile === null) {
      setEditEmployeeProfileValidation(true)
    } else {
      setEditEmployeeProfileValidation(false)
    }*/
    // if (getUnselectDevice().length === 0) {
    //   setEditAccessDoorArrValidation(false)
    // } else {
    //   setEditAccessDoorArrValidation(true)
    // }
    console.log(TimeToValidation)
    console.log(TimeFromValidation)
    if (TimeToValidation || TimeFromValidation){
      check = false
    }

    EditAccessDoorArr.forEach((obj) => {
      if (obj.TimeFromValidation || obj.TimeToValidation){
        check = false
      }
    })

    // ada[index].TimeFromValidation = false
    // ada[index].TimeToValidation = false
    // setTimeToValidation(false)
    // setTimeFromValidation(false)

    if (check
        // EditEmployeeName !== "" &&
        // EditEmployeeSurname !== "" &&
        // EditEmployeePostion !== "" &&
        // EditEmployeeCard !== "" &&
        // EditEmployeeEmail !== "" &&
        // isEmail(EditEmployeeEmail) &&
        // EditEmployeePresonalID !== ""
        // getUnselectDevice().length === 0
      ) 
    {
      var id = EmployeeId
      var name = EditEmployeeName
      var surname = EditEmployeeSurname
      var postion = EditEmployeePostion
      var card = EditEmployeeCard
      var profile = EditEmployeeProfile
      var email = EditEmployeeEmail
      var personal = EditEmployeePresonalID
      var gates = [] 
      EditAccessDoorArr.forEach((obj) => {
        gates.push({
          gateId: obj.doorID,
          overrideTime: obj.isTimeInOut,
          beginTime: obj.timeBegin,
          endTime: obj.timeEnd
        })
      })

      let data = new FormData();
      data.append('name', name)
      data.append('lastname', surname)
      data.append('position', postion)
      data.append('mifare_id', card)
      data.append('email', email)
      data.append('employee_code', personal)
      data.append('groups', '['+group_id+']')
      data.append('gates', JSON.stringify(gates))
      if (profile !== null){
        data.append("picture", profile[0]);
      }

      try {
        setOpenBackdrop(true)
        setopenDialogEditUserToGroup(false)

        const res = await editEmployee(data, Token, id)
        if (res.status >= 400) {
          alert(res.data.message)
        }
        if (res.data.error_code !== undefined){
          setUserGroups(Object.assign([], [{
            employees: [],
            group_id: "",
            gates: [],
            name: "",
          }]))
        }

        setOpenBackdrop(false)
        setTitleAction(`แก้ไขพนักงาน ${name} ${surname}`)
        setopenDialogComfirm(true)
        setopenDialogComfirmStatus(true)
        reloadEmployee()
        setEditEmployeeProfile(null)
      } catch (error) {
        setOpenBackdrop(false)
        setopenDialogEditUserToGroup(true)
        if (error.response !== undefined) {
          setTitleAction(error.response.data.message)
        } else {
          setTitleAction(error)
        }
        setopenDialogComfirm(true)
        setopenDialogComfirmStatus(false)
      }
    }
  }

  const handleRemoveEmployee = async () => {
    const Token = store.getState().AccessToken
    const id = EmployeeId
    setOpenBackdrop(true)
    const res = await removeEmployee({
      query: `
      mutation{
          RemoveEmployee(employeeId: `+id+`) {
              result
          }
      }
      `
    }, Token)
    setOpenBackdrop(false)
    if (res.data.errors !== undefined){
      setTitleAction(res.data.errors[0].message)
      setopenDialogComfirm(true)
      setopenDialogComfirmStatus(false)
    } else {
      setTitleAction(`ลบพนักงาน ${id}`)
      setopenDialogComfirm(true)
      setopenDialogComfirmStatus(true)
    }
    reloadEmployee()
  }
  
  const handleSetAllTime = (value, index) => {
    let ada = EditAccessDoorArr
    ada[index].isAllTime = value
    if (value) {
      ada[index].timeBegin = "00:00"
      ada[index].timeEnd = "23:59"
    }

    ada[index].TimeFromValidation = false
    ada[index].TimeToValidation = false
    setTimeToValidation(false)
    setTimeFromValidation(false)

    setEditAccessDoorArr(Object.assign([], [{
      doorID: undefined,
      isTimeInOut: false,
      timeBegin: "00:00",
      timeEnd: "23:59",
      isAllTime: false,
    }]))
    // setTimeout(() => {
      setEditAccessDoorArr(Object.assign([], ada))
    // }, 200);
  }

  const onTimeFrom = (time, index) => {
    let ada = EditAccessDoorArr
    var check = true

    if (new Date(`2020-02-02 ${time}`).getTime() >= new Date(`2020-02-02 ${ada[index].timeEnd}`).getTime() ){
      check = false
      ada[index].TimeFromValidation = true
      ada[index].TimeToValidation = true
      setTimeToValidation(true)
      setTimeFromValidation(true)
    } else {
      ada[index].TimeFromValidation = false
      ada[index].TimeToValidation = false
      setTimeToValidation(false)
      setTimeFromValidation(false)
    }

    ada[index].timeBegin = time
    ada[index].isAllTime = false
    setEditAccessDoorArr(Object.assign([], []))
    // setTimeout(() => {
      setEditAccessDoorArr(Object.assign([], ada))
    // }, 100);
  }

  const onTimeTo = (time, index) => {
    let ada = EditAccessDoorArr
    var check = true

    if (new Date(`2020-02-02 ${ada[index].timeBegin}`).getTime() >= new Date(`2020-02-02 ${time}`).getTime() ){
      check = false
      ada[index].TimeFromValidation = true
      ada[index].TimeToValidation = true
      setTimeToValidation(true)
      setTimeFromValidation(true)
    } else {
      ada[index].TimeFromValidation = false
      ada[index].TimeToValidation = false
      setTimeToValidation(false)
      setTimeFromValidation(false)
    }

    ada[index].timeEnd = time
    ada[index].isAllTime = false
    setEditAccessDoorArr(Object.assign([], []))
    // setTimeout(() => {
      setEditAccessDoorArr(Object.assign([], ada))
    // }, 100);
  }

  const handleSetTimeInOut = (value, index) => {
    let ada = EditAccessDoorArr
    ada[index].isTimeInOut = value
    setEditAccessDoorArr(Object.assign([{
      doorID: undefined,
      isTimeInOut: false,
      timeBegin: "00:00",
      timeEnd: "23:59",
      isAllTime: false,
    }], ada))
  }

  const handleCloseDeleteEmployeeDialog = () => {
    setIsOpenDeleteEmployeeDialog(false)
  }

  const notFoundData = (colSpan) => {
    return <TableRow >
      <TableCell colSpan={colSpan} style={{textAlign: "center"}}>ไม่พบข้อมูล</TableCell >
    </TableRow>
  }

  return (

    <GroupUserContrainer>
      {
        ((rows === undefined) ? "ไม่พบข้อมูล": <Paper className={classes.paper}>
          <TableContainer className={classes.container} id="table-containner-custom-scroll-employee">
            <Table
              className={classes.table}

              // aria-labelledby="tableTitle"
              // size={'medium'}
              // aria-label="TablePaging table"

              stickyHeader 
              aria-label="sticky table"
            >
              <TablePagingTableHead
                classes={classes}
                numSelected={selected.length}
                order={order}
                orderBy={orderBy}
                onSelectAllClick={handleSelectAllClick}
                onRequestSort={handleRequestSort}
                rowCount={rows.length}
                headCells={headCells}
              />
              <TableBody>
                {
                  (rows.length === 0 ? notFoundData(8): stableSort(rows, getComparator(order, orderBy))
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => {

                    return (

                      <TableRow
                        hover
                        onClick={() => console.log(row.id)}
                        role="checkbox"
                        tabIndex={-1}
                        key={row.id}
                      >
                        <TableCell style={{width: '1%'}} />
                        <TableCell align="left">

                          <ContentName>
                            <GroupName style={{whiteSpace: 'nowrap'}}>{ row.name + " " + row.lastname }</GroupName>
                          </ContentName>
                          
                        </TableCell>
                        <TableCell align="left" className={classes.sizeFont}>
                          {row.position}
                        </TableCell>

                        <TableCell align="left" className={classes.sizeFont}>{ row.personal_id }</TableCell>
                        <TableCell align="left" className={classes.sizeFont}>
                          { row.email.length > 14 ?
                          <div style={{cursor: 'help'}}>
                            <CustomTooltip
                            title={
                              <div 
                              className={classes.sizeFont}
                              style={{
                                padding: 6,
                                whiteSpace: 'nowrap',
                                lineHeight: 1.4,
                              }}>{row.email}</div>
                            } 
                            arrow
                            >
                              <span>{row.email.substring(0, 14)}...</span>
                            </CustomTooltip>
                          </div>
                          :
                            <span>{row.email}</span>
                          }
                        </TableCell>

                        <TableCell align="left" className={classes.sizeFont}>{row.mifare_id}</TableCell>

                        <TableCell align="left" className={classes.sizeFont}>
                          {row.gates_info.length > 1 ? 
                          <div style={{cursor: 'help'}}>
                            <CustomTooltip
                            title={
                              <div 
                              className={classes.sizeFont}
                              style={{
                                padding: 6,
                                whiteSpace: 'pre-wrap',
                                lineHeight: 1.4,
                              }}>{parseGroupUser(row.gates_info)}</div>
                            } 
                            arrow>
                              <span>{parseGroupUser(row.gates_info).split('\n')[0]}...</span>
                            </CustomTooltip>
                          </div> 
                          : 
                          <span>{parseGroupUser(row.gates_info)}</span>
                          }
                        </TableCell>

                        <TableCell align="left">
                          <IconButton 
                            style={{
                              height: 44
                            }}
                            onClick={() => {
                              setEmployeeId(row.id)
                              setEditEmployeeName(row.name)
                              setEditEmployeeSurname(row.lastname)
                              setEditEmployeePostion(row.position)
                              setEditEmployeeCard(row.mifare_id)
                              setEditEmployeeEmail(row.email)
                              setEditEmployeePresonalID(row.personal_id)
                              setImageFile([null])
                              var gate = []
                              row.gates.forEach((item, index) => {
                                gate.push({
                                  doorID: item.gate_id,
                                  isTimeInOut: item.override_time,
                                  timeBegin: item.begin_time.slice(11, 16),
                                  timeEnd: item.end_time.slice(11, 16),
                                  isAllTime: (item.begin_time.slice(11, 16) === "00:00" && item.end_time.slice(11, 16) === "23:59"),
                                })
                              })
                              setEditAccessDoorArr(Object.assign([{
                                doorID: undefined,
                                isTimeInOut: false,
                                timeBegin: "00:00",
                                timeEnd: "23:59",
                                isAllTime: false,
                              }], gate))
                              setopenDialogEditUserToGroup(true)
                            }}
                          >
                            <EditIcon color="primary" style={{fontSize: "18px"}}/>
                          </IconButton>
                        </TableCell>
                      </TableRow>
                    );
                  }))
                }
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[5, 10, 25, 50, 100, 1000]}
            component="div"
            count={rows.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
          />
        </Paper>
        )
      }
      {/* <TableContainer component={Paper}>


        <Table className={classes.table} aria-label="customized table">
          <TableHead>
            <TableRow>
              <CustomeTableCell align="left" style={{ width: 10 }} ></CustomeTableCell>
              <CustomeTableCell align="left">ชื่อ - นามสกุล</CustomeTableCell>
              <CustomeTableCell align="left">ตำแหน่งงาน</CustomeTableCell>
              <CustomeTableCell align="left">Personal ID</CustomeTableCell>
              <CustomeTableCell align="left">Email</CustomeTableCell>
              <CustomeTableCell align="left">รหัสบัตร</CustomeTableCell>
              <CustomeTableCell align="left">สิทธิ์เข้าออกประตู</CustomeTableCell>
              <CustomeTableCell align="left"></CustomeTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {
              ((UserGroups !== undefined) ? UserGroups[0].employees.map((row) => (
                <CustomeTableRow key={row.employee_id}>
                  <CustomeTableCell align="left"></CustomeTableCell>
                    
                  <CustomeTableCell align="left" >
                    
                    <ContentName>
                      <img src={"${server.url}" + "/employee/"+row.employee_id+"/picture?authorization=" + Token} alt={"IMG/" + row.employee_id} width={32} height={32} style={{ borderRadius: "30px" }}
                      key={row.employee_id.toString()}
                      onLoad={() => {
                        console.log('loaded image!')
                      }}
                      onLoadStart={() => {
                        console.log('load starting')
                      }}
                      
                      />
                      <GroupName>{ row.name + " " + row.lastname }</GroupName>
                    </ContentName>
                    
                  </CustomeTableCell>
  
  
                  <CustomeTableCell align="left" >
                    
                      {
                        row.position
                      }
                    
                  </CustomeTableCell>

                  <CustomeTableCell align="left" >
                    
                      {
                        row.personal_id
                      }
                    
                  </CustomeTableCell>
                  <CustomeTableCell align="left" >
                      
                      {
                        row.email
                      }
                    
                  </CustomeTableCell>

                  <CustomeTableCell align="left" >
                    
                    {
                      row.mifare_id
                    }
                  
                </CustomeTableCell>

  
                  <CustomeTableCell align="left" >
                    
                    {
                      parseGroupUser(UserGroups[0].gates)
                    }
                  
                </CustomeTableCell>

                <CustomeTableCell align="left" style={{
                  display: "flex",
                  alignContent: "center",
                  alignItems: "center",
                  justifyContent: "flex-end",
                }}>
                  <IconButton 
                    style={{
                      height: 44
                    }}
                    onClick={() => {
                      setEmployeeId(row.employee_id)
                      setEditEmployeeName(row.name)
                      setEditEmployeeSurname(row.lastname)
                      setEditEmployeePostion(row.position)
                      setEditEmployeeCard(row.mifare_id)

                      setEditEmployeeEmail(row.email)
                      setEditEmployeePresonalID(row.personal_id)

                      setImageFile([null])

                      var gate = []
                      UserGroups[0].gates.forEach((item, index) => {
                        gate.push(item.gate_id)
                      })
                      setAccessDoorArr(Object.assign([], gate))
                      setopenDialogEditUserToGroup(true)
                    }}
                  >
                    <img src={Edit} alt={Edit} />
                  </IconButton>
                  
                </CustomeTableCell>
  
                </CustomeTableRow>
              )): null)
            }
          </TableBody>
        </Table>
      </TableContainer> */}

      <Dialog
        disableBackdropClick={true}
        fullWidth={false}
        maxWidth={"xl"}
        open={openDialogEditUserToGroup}
        onClose={handleCloseDialogEditUserToGroup}
        aria-labelledby="update-dialog-MangeDV"
        >

        <CustomeExitDialogManageDV>
          <IconButton onClick={() => {
            setopenDialogEditUserToGroup(false)
            setEditEmployeeNameValidation(false)
            setEditEmployeeSurnameValidation(false)
            setEditEmployeePostionValidation(false)
            setEditEmployeeCardValidation(false)
            setEditEmployeeEmailValidation(false)
            setEditEmployeePresonalIDValidation(false)
            setEditEmployeeProfileValidation(false)
            setEditAccessDoorArrValidation(false)
          }}>
            <img src={Exit} alt="Exit" />
          </IconButton>
        </CustomeExitDialogManageDV>
        <CustomeDialogTitleManageDV 
        id="update-dialog-MangeDV"
        children={<div style={{
          fontSize: 28,
          textAlign: "center",
          width: 400
        }}>แก้ไขข้อมูล</div>}
        ></CustomeDialogTitleManageDV>
        <CustomeDialogContentManageDV>

          <div 
            style={{
              paddingTop: 20
            }}
            onClick={() => {
              inputFile.current.click();
            }}
          >
            <img src={((ImageFile[0] === null) ? `${server.url}/employee/${EmployeeId}/picture?authorization=${Token}` : ImageFile[0])} alt="UNSELECTEMPY" width={150} height={150} style={{borderRadius: 4, border: "1px solid " + (EditEmployeeProfileValidation ? "red": "#adb0b13d"), cursor: 'pointer'}} accept="image/png,image/jpg"/>
          </div>
          <div 
            style={{
              paddingTop: "0px",
              width: "100%",
              display: "flex",
              alignContent: "center",
              paddingBottom: "10px",
              justifyContent: "flex-end",
              cursor: "pointer"
            }}
          >
            {
              (ImageFile[0] ? <span 
                style={{
                  paddingRight: "20px",
                  textDecoration: "underline",
                  color: "#387bf5",
                }}
                onClick={() => {
                  setImageFile(Object.assign([], [null]))
                }}
              >reset</span>: null)
            }
          </div>
          <input 
            type='file' 
            id='file' 
            ref={inputFile} 
            style={{display: 'none'}}
            onChange={ async (e) => {
              console.log(e.target.files[0])
              if (e.target.files[0] !== undefined) {
                const profileShow = URL.createObjectURL(e.target.files[0])
                setEditEmployeeProfile([e.target.files[0], e.target.files[0].name])
                setImageFile(Object.assign([], [profileShow]))
              }
            }}
          />

        <BodyManageDV style={{
          paddingTop: "0px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>ชื่อ</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>
            
            <InputFromId 
              className={classes.InputIdControl}
              variant="outlined" 
              value={EditEmployeeName}
              error={EditEmployeeNameValidation}
              onChange={(e) => {
                setEditEmployeeName(e.target.value.split(/[0-9!@#$%^&*( )_+\-={};':"\\|,.<>\/?~]/).join(''))
              }}
              labelwidth={0}
              placeholder="กรอกชื่อ" 
              fullWidth={true}
              autoComplete='off'
            />

          </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>

        <BodyManageDV style={{
          paddingTop: "0px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>นามสกุล</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>
            
            <InputFromId 
              className={classes.InputIdControl}
              variant="outlined" 
              value={EditEmployeeSurname}
              error={EditEmployeeSurnameValidation}
              onChange={(e) => {
                setEditEmployeeSurname(e.target.value.split(/[0-9!@#$%^&*( )_+\-={};':"\\|,.<>\/?~]/).join(''))
              }}
              labelwidth={0}
              placeholder="กรอกนามสกุล" 
              fullWidth={true}
              autoComplete='off'
            />

          </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>


        <BodyManageDV style={{
          paddingTop: "0px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>ตำแหน่ง</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>
            
            <InputFromId 
              className={classes.InputIdControl}
              variant="outlined" 
              value={EditEmployeePostion}
              error={EditEmployeePostionValidation}
              onChange={(e) => {
                setEditEmployeePostion(e.target.value)
              }}
              labelwidth={0}
              placeholder="กรอกตำแหน่ง" 
              fullWidth={true}
              autoComplete='off'
            />

          </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>

        <BodyManageDV style={{
  paddingTop: "0px"
}}>
  <BodySpace style={{
    padding: "10px 10px 0px 10px"
  }}></BodySpace>
  <BodyLabel style={{
    padding: "10px 10px 0px 10px",
    textAlign: "left"
  }}>อีเมล</BodyLabel>
  <BodyForm style={{
    padding: "10px 10px 0px 10px"
  }}>
    
    <InputFromId 
      className={classes.InputIdControl}
      variant="outlined" 
      value={EditEmployeeEmail}
      error={EditEmployeeEmailValidation}
      onChange={(e) => {
        setEditEmployeeEmail(e.target.value)
      }}
      labelwidth={0}
      placeholder="กรอกอีเมล" 
      fullWidth={true}
      autoComplete='off'
    />

  </BodyForm>
  <BodySpace style={{
    padding: "10px 10px 0px 10px"
  }}></BodySpace>
</BodyManageDV>

<BodyManageDV style={{
  paddingTop: "5px",
  display: EditEmployeeEmailValidation ? 'flex' : 'none'
}}>
  <BodySpace style={{
    padding: "10px 0px 0px 0px",
    width: "0",
  }}></BodySpace>
  <div>
    <ul style={{ padding: '0', textAlign: 'left', color: 'red' }}>
      <li>
        รูปแบบของอีเมลไม่ถูกต้อง
      </li>
    </ul>
  </div>
</BodyManageDV>

<BodyManageDV style={{
  paddingTop: "0px"
}}>
  <BodySpace style={{
    padding: "10px 10px 0px 10px"
  }}></BodySpace>
  <BodyLabel style={{
    padding: "10px 10px 0px 10px",
    textAlign: "left"
  }}>รหัสพนักงาน</BodyLabel>
  <BodyForm style={{
    padding: "10px 10px 0px 10px"
  }}>
    
    <InputFromId 
      className={classes.InputIdControl}
      variant="outlined" 
      value={EditEmployeePresonalID}
      error={EditEmployeePresonalIDValidation}
      onChange={(e) => {
        setEditEmployeePresonalID(e.target.value.split(/[^a-zA-Z0-9]/).join(''))
      }}
      labelwidth={0}
      placeholder="กรอกรหัสพนักงาน" 
      fullWidth={true}
      autoComplete='off'
      disabled={true}
    />

  </BodyForm>
  <BodySpace style={{
    padding: "10px 10px 0px 10px"
  }}></BodySpace>
</BodyManageDV>


        <BodyManageDV style={{
          paddingTop: "0px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>Card ID</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>
            
            <InputFromId 
              className={classes.InputIdControl}
              variant="outlined" 
              value={EditEmployeeCard}
              error={EditEmployeeCardValidation}
              onChange={(e) => {
                setEditEmployeeCard(e.target.value.split(/[^a-zA-Z0-9]/).join(''))
              }}
              labelwidth={0}
              placeholder="กรอก Card ID" 
              fullWidth={true}
              autoComplete='off'
            />

          </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>

        {/* update image */}
        {/* Select Access Door */}
        {
          EditAccessDoorArr.map((item, index) => {
            return <div style={{marginBottom: "30px"}}>
              <BodyManageDV 
              style={{
                display: "flex",
                justifyContent: "center"
              }}
              key={index.toString()}
            >
              <BodySpace style={{
                padding: "10px 10px 0px 10px"
              }}></BodySpace>
              <BodyLabel style={{
                padding: "10px 10px 0px 10px",
                textAlign: "left"
              }}> {((index === 0) ? "สิทธิ์เข้าออกประตู": null)} </BodyLabel>
              <BodyForm style={{
                padding: "10px 10px 0px 10px"
              }}>
                
                <FormControl variant="outlined" className={classes.formControl}>
                  <Select
                    style={ EditAccessDoorArrValidation ? (EditAccessDoorArr[index].doorID === undefined ? { border: "1px solid red", borderRadius: 6} : null) : null }
                    labelId="demo-simple-select-outlined-label"
                    id="demo-simple-select-outlined"
                    input={<BootstrapInput />}
                    value={((EditAccessDoorArr[index].doorID === undefined) ? 
                            "undefined": EditAccessDoorArr[index].doorID)}
                    onChange={(e) => {
                      setSelectAll(false)
                      var ada = EditAccessDoorArr
                      ada[index].doorID = (e.target.value === "undefined" ? undefined : e.target.value)
                      const checkIfDuplicateExists = (w) => {
                        var neww = w.map((item) => {
                          return item.doorID
                        })
                        return new Set(neww).size !== neww.length 
                      }
                      var check_dup = ada
                      var EditAccessDoorArrFiltered = check_dup.filter(function(x) {
                        return x.doorID !== undefined && x.doorID !== "undefined";
                      });
                      if (!checkIfDuplicateExists(EditAccessDoorArrFiltered)){
                        setEditAccessDoorArr(Object.assign([{
                          doorID: undefined,
                          isTimeInOut: false,
                          timeBegin: "00:00",
                          timeEnd: "23:59",
                          isAllTime: false,
                        }], ada))
                      } else {
                        const removeDuplicate = EditAccessDoorArrFiltered.reduce((acc, current) => {
                          const x = acc.find(item => item.doorID === current.doorID)
                          if (!x) {
                            return acc.concat([current])
                          } else {
                            return acc
                          }
                        }, [])
                        setEditAccessDoorArr(Object.assign([{
                          doorID: undefined,
                          isTimeInOut: false,
                          timeBegin: "00:00",
                          timeEnd: "23:59",
                          isAllTime: false,
                        }], removeDuplicate))
                      }
                    }}
                  >
                    <MenuItem value={"undefined"}>{ "กรุณาเลือกสิทธิ์เข้าออกประตู" }</MenuItem>
                    {
                      GroupDeviceList.map((device_item, device_index) => {
                        return (
                          <MenuItem key={device_index.toString()} value={device_item.device_id}>{ device_item.device_name }</MenuItem>
                        )
                      })
                    }
                  </Select>
                </FormControl>
                    
              </BodyForm>
              <BodySpace style={{
                // padding: "10px 10px 0px 10px",
                padding: "10px 0px 0px 0px",
                width: "9%"
              }}>

                {
                  ((index === EditAccessDoorArr.length - 1) ? <div>

                    <img 
                      src={ADDGROUP} 
                      alt="ADDGROUP" 
                      style={{
                        width: 20,
                        heigth: 20,
                        padding: "10px 10px 0px 0px",
                        cursor: "pointer"
                      }}
                      onClick={() => {
                        var ada = EditAccessDoorArr
                        ada.push({
                          doorID: undefined,
                          isTimeInOut: false,
                          timeBegin: "00:00",
                          timeEnd: "23:59",
                          isAllTime: false
                        })
                        setEditAccessDoorArr(Object.assign([{
                          doorID: undefined,
                          isTimeInOut: false,
                          timeBegin: "00:00",
                          timeEnd: "23:59",
                          isAllTime: false,
                        }], ada))
                      }}
                    />

                    <img 
                      src={REMOVEDOORACCESS} 
                      alt="REMOVEDOORACCESS" 
                      style={{
                        width: 20,
                        heigth: 20,
                        padding: "10px 0px 0px 0px",
                        cursor: "pointer"
                      }}
                      onClick={() => {
                        var ada = EditAccessDoorArr
                        if (ada.length !== 1){
                          ada.pop()
                          setEditAccessDoorArr(Object.assign([{
                            doorID: undefined,
                            isTimeInOut: false,
                            timeBegin: "00:00",
                            timeEnd: "23:59",
                            isAllTime: false,
                          }], ada))
                        }
                      }}
                    />

                  </div>: null)
                }
              </BodySpace>
            </BodyManageDV>

            <BodyManageDV>
              <BodySpace style={{
                  padding: "10px 10px 0px 10px"
              }}></BodySpace>
              <BodyForm style={{
                padding: "0px 10px 0px 130px"
              }}>
                <Checkbox color={"primary"} checked={item.isTimeInOut} onChange={(e) => {
                  handleSetTimeInOut(!item.isTimeInOut, index)
                }} />
                <BodyLabel style={{
                  textAlign: "left",
                  width: "180px"
                }}>เพิ่มเวลาเข้าออก</BodyLabel>
              </BodyForm>
            </BodyManageDV>

            <BodyManageDV style={{display: item.isTimeInOut ? 'flex' : 'none'}}>
              <BodySpace style={{
                padding: "10px 10px 0px 10px"
              }}></BodySpace>
              <BodyForm style={{
                padding: "0px 10px 0px 190px"
              }}>
                <Fromto>จาก</Fromto>
                <div style={{width: "120px"}}><TimePicker index={index} disabled={item.isAllTime} onTime={onTimeFrom} init={item.timeBegin} tag={"time-add-group-from"} error={item.TimeFromValidation}/></div>
                <Fromto>ถึง</Fromto>
                <div style={{width: "120px"}}><TimePicker index={index} disabled={item.isAllTime} onTime={onTimeTo} init={item.timeEnd} tag={"time-add-group-to"}  error={item.TimeToValidation}/></div>
              </BodyForm>
              <BodySpace style={{
                padding: "10px 10px 0px 10px"
              }}></BodySpace>
            </BodyManageDV>

            <BodyManageDV style={{display: item.isTimeInOut ? 'flex' : 'none'}}>
              <BodySpace style={{
                  padding: "10px 10px 0px 10px"
              }}></BodySpace>
              <BodyForm style={{
                padding: "0px 10px 0px 130px"
              }}>
                <Checkbox color={"primary"} checked={item.isAllTime} onChange={(e) => {
                  handleSetAllTime(!item.isAllTime, index)
                }} />
                <BodyLabel style={{
                  textAlign: "left",
                  width: "180px"
                }}>All Time</BodyLabel>
              </BodyForm>
            </BodyManageDV>

            </div>
          })
        }

        <BodyManageDV>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyForm 
            style={{
              alignItems: "center",
              padding: "0px 10px 0px 175px"
            }}
          >
            <Checkbox
              color="primary"
              checked={SelectAll}
              onClick={(e) => {
                var checked = e.target.checked
                if (checked) {
                  // AccessDoorArr
                  var device = GroupDeviceList
                  var all = device.map((item) => {
                    return {
                      doorID: item.device_id,
                      isTimeInOut: false,
                      timeBegin: "00:00",
                      timeEnd: "23:59",
                      isAllTime: false
                    }
                  })
                  setEditAccessDoorArr(Object.assign([{
                    doorID: undefined,
                    isTimeInOut: false,
                    timeBegin: "00:00",
                    timeEnd: "23:59",
                    isAllTime: false,
                  }], all))
                } else {
                  setEditAccessDoorArr(Object.assign([], [{
                    doorID: undefined,
                    isTimeInOut: false,
                    timeBegin: "00:00",
                    timeEnd: "23:59",
                    isAllTime: false
                  }]))
                }
                setSelectAll(checked)
              }}
            />
            <BodyLabel style={{
              textAlign: "left"
            }}>เลือกทุกประตู</BodyLabel>
          </BodyForm>
          <BodySpace></BodySpace>
        </BodyManageDV>

        <BodyManageDV style={{
          paddingTop: "10px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>

          {/* <div
            style={{
              width: "80%"
            }}
          >
            <div
              style={{
                padding: "10px 0px",
              }}
            >หรือ</div>
            <hr></hr>
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                alignContent: "center",
                alignItems: "center",
                justifyContent: "space-between",
                padding: "20px 0px",
              }}
            >
              <div>Upload ข้อมูลพนักงาน</div>
              <div><img src={EXPORTEMPTY} alt="EXPORTEMPTY" /></div>
            </div>
            <hr></hr>
          </div> */}

          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
        </BodyManageDV>

        <ActionManageDV>
          <div style={{
            display: "flex",
            justifyContent: "space-between",
            width: "100%",
            padding: "20px calc(10% + 30px) 0px calc(10% + 30px)"
          }}>
            <Choice1Btn style={{
              width: 250, 
              height: 60,
              border: "1px solid red",
              color: "red"
            }} onClick={() => {
              setIsOpenDeleteEmployeeDialog(true)
            }}>ลบพนักงาน</Choice1Btn>
            <Choice2Btn style={{
              width: 250, 
              height: 60
            }} onClick={() => {
              handleEditEmployee()
            }}>แก้ไขข้อมูลพนักงาน</Choice2Btn>
          </div>
        </ActionManageDV>

        </CustomeDialogContentManageDV>

      </Dialog>


      {/* Confirm delete employee */}
      <Dialog
        disableBackdropClick={true}
        fullWidth={false}
        maxWidth={"xl"}
        open={isOpenDeleteEmployeeDialog}
        onClose={handleCloseDeleteEmployeeDialog}
        aria-labelledby="update-dialog-fw-fw"
      >
        <CustomeExitDialogUpdateFW>
          <IconButton onClick={() => {
            setIsOpenDeleteEmployeeDialog(false)
          }}>
            <img src={Exit} alt="Exit" />
          </IconButton>
        </CustomeExitDialogUpdateFW>
        <CustomeDialogTitleUpdateFW id="update-dialog-fw-fw" style={{textAlign: "center"}}>ลบข้อมูลพนักงาน?</CustomeDialogTitleUpdateFW>
        <CustomeDialogContentUpdateFW>
          <CustomeDialogContentUpdateFWTextUpdateFW>
            {`คุณแน่ใจว่าจะลบพนักงาน ${EditEmployeeName} ${EditEmployeeSurname} ใช่หรือไม่`}
          </CustomeDialogContentUpdateFWTextUpdateFW>
          <div style={{
            display: "flex",  
            justifyContent: "space-between"
          }}>
            <Choice1Btn
            style={{
              width: 250, 
              height: 60,
              marginRight: 20,
              border: "1px solid #CC1503",
              color: '#CC1503'
            }}
            onClick={() => {
              setIsOpenDeleteEmployeeDialog(false)
            }}
            >ไม่ลบแล้ว</Choice1Btn>
            <Choice2Btn 
              style={{
                width: 250, 
                height: 60,
                border: "1px solid #CC1503",
                color: '#fff',
                backgroundColor: "#CC1503"
              }}
              onClick={() => {
                handleRemoveEmployee()
                setIsOpenDeleteEmployeeDialog(false)
                setopenDialogEditUserToGroup(false)
                setEditEmployeeNameValidation(false)
                setEditEmployeeSurnameValidation(false)
                setEditEmployeePostionValidation(false)
                setEditEmployeeCardValidation(false)
                setEditEmployeeEmailValidation(false)
                setEditEmployeePresonalIDValidation(false)
                setEditEmployeeProfileValidation(false)
                setEditAccessDoorArrValidation(false)
              }}
            >ยืนยันลบพนักงาน</Choice2Btn>
          </div>
        </CustomeDialogContentUpdateFW>
      </Dialog>

      <Dialog
        disableBackdropClick={true}
        fullWidth={false}
        maxWidth={"xl"}
        open={openDialogComfirm}
        aria-labelledby="update-dialog-MangeDV"
      >
          {
            (openDialogComfirmStatus ? <Success title={TitleAction}/> : <Unsuccess title={TitleAction} msgerror={MessageError}/>)
          }
          <ComfirmAction>

            {
              (openDialogComfirmStatus ? <Choice2Btn 
                style={{
                  width: 800, 
                  height: 60
                }}
                onClick={() => {
                  setopenDialogComfirm(false)
                }}
              >
                ยืนยัน
              </Choice2Btn> : <ComfirmAction>
                <Choice2Btn 
                  style={{
                    width: 800, 
                    height: 60,
                    backgroundColor: "#e46f6f",
                    border: "red"
                  }}
                  onClick={() => {
                    setopenDialogComfirm(false)
                  }}
                >
                  ยืนยัน
                </Choice2Btn>
              </ComfirmAction>)
            }
            
          </ComfirmAction>
      </Dialog>

      <Backdrop className={navbarclasses.backdrop} open={OpenBackdrop}>
        <CircularProgress color="inherit" />
      </Backdrop>

    </GroupUserContrainer>

  );
}


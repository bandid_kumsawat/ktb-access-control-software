import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  online: {
    color: "#11AB42",
  },
  offline: {
    color: "#CC1503"
  },
  active: {
    borderBottom: "5px solid #01A6E6",
    color: "#01a6e6"
  },
  formControl: {
    // margin: theme.spacing(1),
    width: "100%",
  },
  root: {
    width: '100%',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  container: {
    // "full height" - "height navbar" - "space footter" - "padding container top" - "padding container bottom" - "height result search"
    maxHeight: "calc(100vh - 8vh - 30px - 40px - 40px - 70px - 20px)",
  },
  sizeFont: {
    fontSize: 18
  }
}));

export default useStyles
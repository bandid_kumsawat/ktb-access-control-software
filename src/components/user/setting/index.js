import React from 'react';
import { Outlet, useNavigate, useLocation} from 'react-router-dom';

// import AddGroup from 'src/components/GlobalDialog/AddGroup'
import TimePicker from 'src/components/GlobalDialog/TimePicker';
import { withStyles, makeStyles } from '@material-ui/core/styles';

import Chip from '@material-ui/core/Chip';
import MeetingRoomIcon from '@material-ui/icons/MeetingRoom';

import parseTime from 'src/commons/parseTime'

import PropTypes from 'prop-types';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Paper from '@material-ui/core/Paper';
import styled from 'styled-components';
import Checkbox from '@material-ui/core/Checkbox';

import EditIcon from '@material-ui/icons/Edit';

// Confirm dialog
import Confirm from 'src/components/ComfirmAction/Confirm'  

// Confirm dialog
import Success from 'src/components/ComfirmAction/Success'
import Unsuccess from 'src/components/ComfirmAction/Unsuccess'
import Backdrop from '@material-ui/core/Backdrop'
import CircularProgress from '@material-ui/core/CircularProgress'

// import TimePicker from '@material-ui/lab/TimePicker';
import { IconButton } from '@material-ui/core';

// setting style 
import useStyles from './style'

// svg image
import Edit from 'src/asset/img/setting/EDIT.svg'
import Exit from 'src/asset/img/search/EXIT.svg'
import ADDGROUP from "src/asset/img/sidebar/ADDGROUP.svg"
import REMOVEDOORACCESS from "src/asset/img/sidebar/REMOVEDOORACCESS.svg"


// dialog update Device component
import Dialog from '@material-ui/core/Dialog'
import CustomeDialogContentManageDV from 'src/components/material/dialog/MangeDevice/CustomeDialogContent'
import CustomeDialogTitleManageDV from 'src/components/material/dialog/MangeDevice/CustomeDialogTitle'
import CustomeExitDialogManageDV from 'src/components/material/dialog/MangeDevice/CustomeExitDialog'
import ActionManageDV from "src/components/material/dialog/MangeDevice/content/Action"
import BodyManageDV from "src/components/material/dialog/MangeDevice/content/Body"
import BodyForm from 'src/components/material/dialog/MangeDevice/content/Body/Form'
import BodyLabel from 'src/components/material/dialog/MangeDevice/content/Body/Label'
import BodySpace from 'src/components/material/dialog/MangeDevice/content/Body/Space'
import InputFromId from 'src/components/material/dialog/MangeDevice/content/Form/InputId'
import Fromto from 'src/components/material/dialog/MangeDevice/content/Body/Fromto'

import MenuItem from '@material-ui/core/MenuItem';

// button component
import Choice1Btn from 'src/components/material/button/Choice1Btn'
import Choice2Btn from 'src/components/material/button/Choice2Btn'

// apis
import removeGroup from 'src/apis/removeGroup'
import getGroup from 'src/apis/getGroup'
import updateGroup from 'src/apis/updateGroup'
// store
import { GROUP} from 'src/stores/actions'

// From Select
import FormControl from "@material-ui/core/FormControl"
import Select from "@material-ui/core/Select"
import InputBase from '@material-ui/core/InputBase'
const BootstrapInput = withStyles((theme) => ({
  root: {
    'label + &': {
      marginTop: theme.spacing(3),
    },
  },
  input: {
    textAlign: "left",
    borderRadius: 4,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #ccc',
    fontSize: 16,
    padding: '10px 47px 13px 12px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:focus': {
      borderRadius: 4,
      // borderColor: '#80bdff',
      // boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
      border: '1px solid #337AB7',
    },
  },
}))(InputBase);

const useStylesBackdrop = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff"
  }
}));


const StutusContrainer = styled('div')`
  padding: 20px 20px 20px 30px;
`

const ContentName = styled('div')`
  display: flex;
  align-items: center;
  align-content: center;
`

const GroupName = styled('div')`
  text-decoration: underline;
  cursor: pointer;
  color: #337AB7;

`

const ComfirmAction = styled("div")`
  display: flex;
  align-content: center;
  justify-content: space-around;
  margin: 20px;
`

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

function TablePagingTableHead(props) {
  const [ headCells ] = React.useState(props.headCells)
  const { classes, order, orderBy, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {/* <TableCell align={'left'} style={{width: 34, height: 34}} /> */}
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={'left'}
            // padding={headCell.disablePadding ? 'none' : 'default'}
            // sortDirection={orderBy === headCell.id ? order : false}
            style={{fontSize: 18, whiteSpace: "nowrap"}}
          >
            {/* {headCell.label} */}
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
              style={{fontSize: 18}}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
        <TableCell align={'left'} style={{width: 34, height: 34}} />
      </TableRow>
    </TableHead>
  );
}

TablePagingTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
  headCells: PropTypes.array.isRequired,
};

export default function UserSetting(props) {

  const classes = useStyles();
  let history = useNavigate();
  const location = useLocation();

  const [ store ] = React.useState(props.store)
  const [ Token, setToken ] = React.useState('')
  const [ Groups, setGroups ] = React.useState([])
  const [ openDialogAddGroup, setopenDialogAddGroup ] = React.useState(false)

  const [ openDialogComfirmDeleteGroup, setopenDialogComfirmDeleteGroup ] = React.useState(false)

  // Remove Group
  const [ RemoveGroupName, setRemoveGroupName ] = React.useState("") 

  // Edit group
  const [ GroupId, setGroupId ] = React.useState("") 
  const [ EditGroupName, setEditGroupName ] = React.useState("") 

  const [ EditAccessDoorArr, setEditAccessDoorArr ] = React.useState([{
    doorID: undefined,
    isTimeInOut: false,
    timeBegin: "00:00",
    timeEnd: "23:59",
    isAllTime: false,

    TimeFromValidation: false,
    TimeToValidation: false,
  }])
  const [ DeviceList, setDeviceList ] = React.useState([])

  const [ EditGroupNameValidation, setEditGroupNameValidation ] = React.useState(false)
  const [ EditAccessDoorArrValidation, setEditAccessDoorArrValidation ] = React.useState(false)

  const [ TimeToValidation, setTimeToValidation ] = React.useState(false)
  const [ TimeFromValidation, setTimeFromValidation ] = React.useState(false)

  const [order, setOrder] = React.useState('desc');
  const [orderBy, setOrderBy] = React.useState('name');
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [ rows, setrows] = React.useState(undefined)
  
  const [ SelectAll, setSelectAll ] = React.useState(false)

  const classesbd = useStylesBackdrop()
  const [ OpenBackdrop, setOpenBackdrop ] = React.useState(false);

  const [ openDialogComfirmStatus, setopenDialogComfirmStatus ] = React.useState(false)
  const [ TitleAction, setTitleAction ] = React.useState(false)
  const [ MessageError ] = React.useState(false)
  const [ openDialogComfirm, setopenDialogComfirm ] = React.useState(false)

  const headCells = [
    { id: 'name', numeric: true, disablePadding: false, label: 'ชื่อกลุ่ม' },
    { id: 'door_access', numeric: true, disablePadding: false, label: 'สิทธิ์เข้าออกประตู' },
  ];

  React.useEffect(() => {
    const updateLog = () => {
      var device = store.getState().Device
      var group = store.getState().Group
      var list = []
      setToken(store.getState().AccessToken)
      if (group !== undefined && device !== undefined){
        var newgroup = []
        group.forEach((row) => {
          newgroup.push( {
            id: row.group_id,
            name: row.name,
            gate: row.gates,
            door_access: row.gates,
          } )
        })
        setGroups(group)
        setrows(newgroup)
        
        if (device !== undefined){
          device.forEach((item, index) => {
            list.push({
              device_id: item.gate_id,
              device_name: item.gate.name_gate
            })
          })
        }
        setDeviceList(list)
      }
    }
    const interval_ = setInterval(() => {
      updateLog()
    }, 200);

    return () => {
      clearInterval(interval_)
    }
  }, [store])

  const FindDataGroup = (id_index) => {
    var id_ = 0
    Groups.forEach((item, index) => {
      if (Number(item.group_id) === Number(id_index)){
        id_ = index
      }
    })
    if (Groups !== undefined){
      var tempGroup = []
      setEditGroupName(Groups[id_].name)
      setRemoveGroupName(Groups[id_].name)
      if (Groups[id_].group_gates.length !== 0){
        Groups[id_].group_gates.forEach((item) => {
          var checkAllTime = (parseTime(item.begin_time) === "00:00" && parseTime(item.end_time) === "23:59" ? true: false)
          tempGroup.push({
            doorID: item.gate_id,
            isTimeInOut: item.override_time,
            timeBegin: parseTime(item.begin_time),
            timeEnd: parseTime(item.end_time),
            isAllTime: checkAllTime
          })
        })
        console.log(tempGroup)
        setEditAccessDoorArr(Object.assign([], tempGroup))
      } else {
        setEditAccessDoorArr(Object.assign([], [{
          doorID: undefined,
          isTimeInOut: false,
          timeBegin: "00:00",
          timeEnd: "23:59",
          isAllTime: false
        }]))
      }
      // if (Groups[id_].gates.length === 0){
      //   uigroup.push({
      //     doorID: undefined,
      //     isTimeInOut: false,
      //     timeBegin: "00:00",
      //     timeEnd: "23:59",
      //     isAllTime: false
      //   })
      // } else {
      //   Groups[id_].gates.forEach((item) => {
      //     uigroup.push(item.gate_id)
      //   })
      //   if (Groups[id_].group_gates.length === 0){
      //     tempGroup.push({
      //       doorID: undefined,
      //       isTimeInOut: false,
      //       timeBegin: "00:00",
      //       timeEnd: "23:59",
      //       isAllTime: false
      //     })
      //   } else {
      //     Groups[id_].group_gates.forEach((item) => {
      //       var checkAllTime = (parseTime(item.begin_time) === "00:00" && parseTime(item.end_time) === "23:59" ? true: false)
      //       tempGroup.push({
      //         doorID: item.gate_id,
      //         isTimeInOut: item.override_time,
      //         timeBegin: parseTime(item.begin_time),
      //         timeEnd: parseTime(item.end_time),
      //         isAllTime: checkAllTime
      //       })
      //     })
      //   }
      //   console.log("tempGroup")
      //   console.log(tempGroup)
      //   // setEditAccessDoorArr(Object.assign([], tempGroup))
      // }
    }
  }

  const handleCloseDialogAddGroup = () => {
    setopenDialogAddGroup(false)
  }

  const parseGroupUser = (accgroups) => {
    var str = ""
    if (accgroups !== undefined){
      accgroups.forEach((item, index) => {
        str += item.name
        if (index !== accgroups.length - 1){
          str +=  ", "
        }
      }) 
    }
    return str
  }

  const getUnselectDevice = () => {
    return EditAccessDoorArr.filter((obj) => obj.doorID === undefined)
  }

  const handleUpdateGroup = async () => {
    var update = true
    if (EditGroupName === "") {
      setEditGroupNameValidation(true)
      update = false
    } else {
      setEditGroupNameValidation(false)
    }

    if (TimeToValidation && TimeFromValidation) {
      update = false
    }

    // if (getUnselectDevice().length === 0) {
    //   setEditAccessDoorArrValidation(false)
    // } else {
    //   setEditAccessDoorArrValidation(true)
    //   update = false
    // }
    if (update){
      if (EditAccessDoorArr.length > 0){

        var door = `[]`
        var check_from = true, check_to = true
        var FilterDoorUndefined = EditAccessDoorArr.filter(function(x) {

          if (x.TimeFromValidation && x.TimeToValidation){
            check_from = false
            check_to = false
          }
          
          return x.doorID !== undefined && x.doorID !== "undefined";
        });

        if (check_from && check_to) {

          setopenDialogAddGroup(false)
          setOpenBackdrop(true)

          door = FilterDoorUndefined.map((item) => {
            return `{
              gateId: ${Number(item.doorID)}, 
              overrideTime: ${item.isTimeInOut}, 
              beginTime: "${item.timeBegin}", 
              endTime: "${item.timeEnd}"
            }`
          })
          var query = `
            mutation {
                EditGroup(
                  groupId: ${GroupId}, 
                  name: "${EditGroupName}",
                  gates: [${door}]
                ) {
                  group_id
                  name
                }
            }
          `
          const res_updateGroup = await updateGroup({query: query}, Token)
          if (res_updateGroup.data.errors !== undefined){
            alert(res_updateGroup.data.errors[0].message)
          }
          const res_group = await getGroup({
            query: `
                query {
                  Group {
                      group_id
                      name
                      gates {
                          gate_id
                          name
                          begin_time
                          end_time
                      }
                      employees {
                          employee_id
                          name
                          lastname
                          position
                          mifare_id
                          picture_url
                          face_id
                      }
                      group_gates {
                          gate_id
                          group_id
                          override_time
                          begin_time
                          end_time
                      }
                  }
              }
            `
          }, Token)
          if (res_group.data.errors === undefined){
            setOpenBackdrop(false)
            setopenDialogComfirmStatus(true)
            setopenDialogComfirm(true)
            setTitleAction(`แก้ไขกลุ่ม ${EditGroupName} สำเร็จ`)
          }else {
            setOpenBackdrop(false)
            setopenDialogComfirmStatus(false)
            setopenDialogComfirm(true)
            setTitleAction(`แก้ไขกลุ่ม ${EditGroupName} ไม่สำเร็จสำเร็จ`)
          }
          setopenDialogAddGroup(false)
        }
      }
    }



    // if (update){
    //   const res = await updateGroup({
    //     query: `
    //       mutation {
    //           EditGroup(
    //             groupId: ${GroupId}, 
    //             name: "${EditGroupName}",
    //             gates: [
    //               {
    //                 gateId: 10102, overrideTime: true, beginTime: "08:00", endTime: "17:00"
    //               },
    //               {
    //                 gateId: 10103, overrideTime: false, beginTime: "08:00", endTime: "17:00"
    //               },
    //             ]
    //           ) {
    //             group_id
    //             name
    //           }
    //       }
    //     `
    //   }, Token)
    // }

    /*(var AccessDoorArrFiltered = [...new Set(EditAccessDoorArr.filter(function(x) {
      return x !== undefined && x !== "undefined";
    }))];
    const res = await updateGroup({
      query: `
        mutation {
            EditGroup(groupId: `+GroupId+`, name: "`+EditGroupName+`", gates: `+JSON.stringify(AccessDoorArrFiltered)+`) {
                group_id
                name
                gates {
                    gate_id
                    name
                }
            }
        }
      `
    }, Token)
    if (res.data.errors === undefined){

      const res_group = await getGroup({
        query: `
          query {
              Group {
                  group_id
                  name
                  gates {
                      gate_id
                      name
                      begin_time
                      end_time
                  }
              }
          }
        `
      }, Token)
      if (res_group.data.errors === undefined){
        store.dispatch(GROUP(res_group.data.data.Group))
        handleCloseDialogAddGroup()
      }
    }*/
  }

  const handleRemoveGroup = async () => {
    setOpenBackdrop(true)
    const res = await removeGroup({
      query: `
        mutation{
            RemoveGroup(groupId: `+GroupId+`) {
                result
            }
        }
      `
    }, Token)
    if (res.data.errors === undefined){
      const res_group = await getGroup({
        query: `
            query {
              Group {
                  group_id
                  name
                  gates {
                      gate_id
                      name
                      begin_time
                      end_time
                  }
                  employees {
                      employee_id
                      name
                      lastname
                      position
                      mifare_id
                      picture_url
                      face_id
                  }
                  group_gates {
                      gate_id
                      group_id
                      override_time
                      begin_time
                      end_time
                  }
              }
          }
        `
      }, Token)
      if (res_group.data.errors === undefined){
        store.dispatch(GROUP(res_group.data.data.Group))
        handleCloseDialogAddGroup()

        setOpenBackdrop(false)
        setopenDialogComfirmStatus(true)
        setopenDialogComfirm(true)
        setTitleAction(`ลบกลุ่ม ${RemoveGroupName} สำเร็จ`)
      } else {
        setOpenBackdrop(false)
        setopenDialogComfirmStatus(false)
        setopenDialogComfirm(true)
        setTitleAction(`ลบกลุ่ม ${RemoveGroupName} ไม่สำเร็จ`)
      }
    } else {
      alert(res.data.errors[0].message)
    }
  }

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = rows.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const onTimeFrom = (time, index) => {
    let ada = EditAccessDoorArr
    var check = true
    
    if (new Date(`2020-02-02 ${time}`).getTime() >= new Date(`2020-02-02 ${ada[index].timeEnd}`).getTime() ){
      check = false
      ada[index].TimeFromValidation = true
      ada[index].TimeToValidation = true
      setTimeToValidation(true)
      setTimeFromValidation(true)
    } else {
      ada[index].TimeFromValidation = false
      ada[index].TimeToValidation = false
      setTimeToValidation(false)
      setTimeFromValidation(false)
    }

    ada[index].timeBegin = time
    ada[index].isAllTime = false

    setEditAccessDoorArr(Object.assign([], []))
    // setTimeout(() => {
      setEditAccessDoorArr(Object.assign([], ada))
    // }, 100);
  }

  const onTimeTo = (time, index) => {
    let ada = EditAccessDoorArr
    var check = true

    if (new Date(`2020-02-02 ${ada[index].timeBegin}`).getTime() >= new Date(`2020-02-02 ${time}`).getTime() ){
      check = false
      ada[index].TimeFromValidation = true
      ada[index].TimeToValidation = true
      setTimeToValidation(true)
      setTimeFromValidation(true)
    } else {
      ada[index].TimeFromValidation = false
      ada[index].TimeToValidation = false
      setTimeToValidation(false)
      setTimeFromValidation(false)
    }

    ada[index].timeEnd = time
    ada[index].isAllTime = false

    setEditAccessDoorArr(Object.assign([], []))
    // setTimeout(() => {
      setEditAccessDoorArr(Object.assign([], ada))
    // }, 100);
  }

  const handleSetTimeInOut = (value, index) => {
    let ada = EditAccessDoorArr
    ada[index].isTimeInOut = value
    setEditAccessDoorArr(Object.assign([], ada))
  }

  const handleSetAllTime = (value, index) => {
    if (value === true){
      let ada = EditAccessDoorArr
      ada[index].timeBegin = "00:00"
      ada[index].timeEnd = "23:59"
      ada[index].TimeFromValidation = false
      ada[index].TimeToValidation = false
      setTimeToValidation(false)
      setTimeFromValidation(false)
      ada[index].isAllTime = value
      setEditAccessDoorArr(Object.assign([], []))
      setTimeout(() => {
        setEditAccessDoorArr(Object.assign([], ada))
      }, 100);
    }else {
      let ada = EditAccessDoorArr
      ada[index].isAllTime = value
      setEditAccessDoorArr(Object.assign([], []))
      setTimeout(() => {
        setEditAccessDoorArr(Object.assign([], ada))
      }, 100);
    }
  }

  const notFoundData = (colSpan) => {
    return <TableRow >
      <TableCell colSpan={colSpan} style={{textAlign: "center"}}>ไม่พบข้อมูล</TableCell >
    </TableRow>
  }

  return (
    <StutusContrainer>
      
      {
        ((location.pathname.split("/").length === 5) ? <Outlet />: ((rows === undefined) ? "ไม่พบข้อมูล": (
          <Paper className={classes.paper}>
            <TableContainer className={classes.container} id="table-containner-custom-scroll-log">
              <Table
                className={classes.table}
                stickyHeader 
                aria-label="sticky table"
              >
                <TablePagingTableHead
                  classes={classes}
                  numSelected={selected.length}
                  order={order}
                  orderBy={orderBy}
                  onSelectAllClick={handleSelectAllClick}
                  onRequestSort={handleRequestSort}
                  rowCount={rows.length}
                  headCells={headCells}
                />
                <TableBody>
                  {
                    (rows.length === 0 ? notFoundData(8) : stableSort(rows, getComparator(order, orderBy))
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row, index) => {

                      return (

                        <TableRow
                          hover
                          onClick={() => console.log(row.id)}
                          tabIndex={-1}
                          key={row.id}
                        >
                          {/* <TableCell style={{width: 34, height: 34}} /> */}
                          <TableCell align="left" className={classes.sizeFont} style={{whiteSpace: "nowrap"}}>
                           <ContentName>
                             <GroupName onClick={() => {
                               history(`/dashboard/user/group/${row.id}`)
                             }}>{ row.name }</GroupName>
                           </ContentName>
                          </TableCell>
                          {/* <TableCell style={{fontSize: 18, whiteSpace: "nowrap"}}> */}
                          <TableCell style={{maxWidth: "500px"}}>
                            {/* {
                              parseGroupUser(row.door_access)
                            } */}
                            {
                              row.door_access.map((row) => {
                                return <Chip
                                  icon={<MeetingRoomIcon />}
                                  label={row.name}
                                  clickable
                                  color="primary"
                                  variant="outlined"
                                  style={{marginRight: "4px", marginTop: "2px", marginBottom: "2px"}}
                                />
                              })
                            }
                          </TableCell>
                          <TableCell align="left" key={"tttt" + row.id}>
                            <IconButton
                              style={{
                                width: "10",
                                height: "10",
                              }}
                              aria-controls="customized-menu"
                              aria-haspopup="true"
                              variant="contained"
                              color="secondary"
                              onClick={() => {
                                setGroupId(row.id)
                                // FindDataGroup(((rowsPerPage * page) + index)) // note
                                FindDataGroup(row.id)
                                setopenDialogAddGroup(true)
                              }}
                            >
                              <EditIcon color="primary" style={{fontSize: "18px"}}/>
                            </IconButton>
                          </TableCell>

                        </TableRow>
                      );
                    }))
                  }
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              rowsPerPageOptions={[5, 10, 25, 50, 100, 1000]}
              component="div"
              count={rows.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
            />
          </Paper>
        )))
        
          // <TableContainer component={Paper}>
          //   <Table className={classes.table} aria-label="customized table">
          //     <TableHead>
          //       <TableRow>
          //         <CustomeTableCell align="left" style={{ width: 10 }} ></CustomeTableCell>
          //         <CustomeTableCell align="left">ชื่อกลุ่มผู้ใช้</CustomeTableCell>
          //         <CustomeTableCell align="left">สิทธิ์เข้าออกประตู</CustomeTableCell>
          //         <CustomeTableCell align="right"></CustomeTableCell>
          //       </TableRow>
          //     </TableHead>
          //     <TableBody>
          //       {Groups.map((row, index) => (
          //         <CustomeTableRow key={row.group_id}>
          //           <CustomeTableCell align="left"></CustomeTableCell>
                      
          //           <CustomeTableCell align="left" >
                      
          //             <ContentName>
          //               <GroupName onClick={() => {
          //                 history("/dashboard/user/group/" + row.group_id)
          //               }}>{ row.name }</GroupName>
          //             </ContentName>
                      
          //           </CustomeTableCell>
    
          //           <CustomeTableCell align="left" >
                      
          //             <ContentName>
          //               {
          //                 parseGroupUser(row.gates)
          //               }
          //             </ContentName>
                      
          //           </CustomeTableCell>
    
          //           <CustomeTableCell 
          //             aling="right" 
          //             style={{
          //                 display: "flex",
          //                 alignContent: "center",
          //                 alignItems: "center",
          //                 justifyContent: "flex-end",
      
          //             }}>
                      // <IconButton
                      //   style={{
                      //     width: "10",
                      //     height: "10",
                      //   }}
                      //   aria-controls="customized-menu"
                      //   aria-haspopup="true"
                      //   variant="contained"
                      //   color="secondary"
                      //   onClick={() => {
                      //     setGroupId(row.group_id)
                      //     FindDataGroup(index)
                      //     setopenDialogAddGroup(true)
                      //   }}
                      // >
                      //   <img src={Edit} alt="Edit" />
                      // </IconButton>
          //           </CustomeTableCell>
    
          //         </CustomeTableRow>
          //       ))}
          //     </TableBody>
          //   </Table>
          // </TableContainer>
        
      //   )
      }

      
      {/* Dialog for Add Group */}
      <Dialog
        disableBackdropClick={true}
        fullWidth={false}
        maxWidth={"xl"}
        open={openDialogAddGroup}
        onClose={handleCloseDialogAddGroup}
        aria-labelledby="update-dialog-MangeDV"
      >
        <CustomeExitDialogManageDV>
          <IconButton onClick={() => {
            setopenDialogAddGroup(false)

            setEditAccessDoorArrValidation(false)
            setEditGroupNameValidation(false)
            
            setSelectAll(false)
          }}>
            <img src={Exit} alt="Exit" />
          </IconButton>
        </CustomeExitDialogManageDV>
        <CustomeDialogTitleManageDV 
        id="update-dialog-MangeDV"
        children={<div style={{
          fontSize: 28,
          textAlign: "center",
          width: 400
        }}>แก้ไขกลุ่มผู้ใช้</div>}
        ></CustomeDialogTitleManageDV>
        <CustomeDialogContentManageDV>
        

        <BodyManageDV style={{
          paddingTop: "30px"
        }}>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyLabel style={{
            padding: "10px 10px 0px 10px",
            textAlign: "left"
          }}>ชื่อกลุ่มผู้ใช้</BodyLabel>
          <BodyForm style={{
            padding: "10px 10px 0px 10px"
          }}>
            
            <InputFromId 
              className={classes.InputIdControl}
              variant="outlined" 
              error={EditGroupNameValidation}
              labelwidth={0}
              placeholder="กรอกชื่อกลุ่มผู้ใช้" 
              fullWidth={true}
              autoComplete='off'
              value={EditGroupName}
              onChange={(e) => {
                setEditGroupName(e.target.value)
              }}
            />

          </BodyForm>
          <BodySpace style={{
            padding: "10px 10px 0px 30px"
          }}></BodySpace>
        </BodyManageDV>
        <div style={{marginTop: "30px"}}></div>
        {
          EditAccessDoorArr.map((item, index) => {
            return <div style={{marginBottom: "30px"}}>
              <BodyManageDV 
              style={{
                display: "flex",
                justifyContent: "center"
              }}
              key={index.toString()}
            >
              <BodySpace style={{
                padding: "10px 10px 0px 10px"
              }}></BodySpace>
              <BodyLabel style={{
                padding: "10px 10px 0px 10px",
                textAlign: "left"
              }}> {((index === 0) ? "สิทธิ์เข้าออกประตู": null)} </BodyLabel>
              <BodyForm style={{
                padding: "10px 10px 0px 10px"
              }}>
                
                <FormControl variant="outlined" className={classes.formControl}>
                  <Select
                    style={ EditAccessDoorArrValidation ? (EditAccessDoorArr[index].doorID === undefined ? { border: "1px solid red", borderRadius: 6} : null) : null }
                    labelId="demo-simple-select-outlined-label"
                    id="demo-simple-select-outlined"
                    input={<BootstrapInput />}
                    value={((EditAccessDoorArr[index].doorID === undefined) ? 
                            "undefined": EditAccessDoorArr[index].doorID)}
                    onChange={(e) => {
                      setSelectAll(false)
                      var ada = EditAccessDoorArr
                      ada[index].doorID = (e.target.value === "undefined" ? undefined : e.target.value)
                      const checkIfDuplicateExists = (w) => {
                        var neww = w.map((item) => {
                          return item.doorID
                        })
                        return new Set(neww).size !== neww.length 
                      }
                      var check_dup = ada
                      var EditAccessDoorArrFiltered = check_dup.filter(function(x) {
                        return x.doorID !== undefined && x.doorID !== "undefined";
                      });
                      if (!checkIfDuplicateExists(EditAccessDoorArrFiltered)){
                        setEditAccessDoorArr(Object.assign([], ada))
                      } else {
                        const removeDuplicate = EditAccessDoorArrFiltered.reduce((acc, current) => {
                          const x = acc.find(item => item.doorID === current.doorID)
                          if (!x) {
                            return acc.concat([current])
                          } else {
                            return acc
                          }
                        }, [])
                        setEditAccessDoorArr(Object.assign([], removeDuplicate))
                      }
                    }}
                  >
                    <MenuItem value={"undefined"}>{ "กรุณาเลือกสิทธิ์เข้าออกประตู" }</MenuItem>
                    {
                      DeviceList.map((device_item, device_index) => {
                        return (
                          <MenuItem key={device_index.toString()} value={device_item.device_id}>{ device_item.device_name }</MenuItem>
                        )
                      })
                    }
                  </Select>
                </FormControl>
                    
              </BodyForm>
              <BodySpace style={{
                padding: "10px 0px 0px 0px",
                width: "12%"
              }}>

                {
                  ((index === EditAccessDoorArr.length - 1) ? <div>

                    <img 
                      src={ADDGROUP} 
                      alt="ADDGROUP" 
                      style={{
                        width: 20,
                        heigth: 20,
                        padding: "10px 20px 0px 0px",
                        cursor: "pointer"
                      }}
                      onClick={() => {
                        var ada = EditAccessDoorArr
                        ada.push({
                          doorID: undefined,
                          isTimeInOut: false,
                          timeBegin: "00:00",
                          timeEnd: "23:59",
                          isAllTime: false
                        })
                        setEditAccessDoorArr(Object.assign([], ada))
                      }}
                    />

                    <img 
                      src={REMOVEDOORACCESS} 
                      alt="REMOVEDOORACCESS" 
                      style={{
                        width: 20,
                        heigth: 20,
                        padding: "10px 20px 0px 0px",
                        cursor: "pointer"
                      }}
                      onClick={() => {
                        var ada = EditAccessDoorArr
                        if (ada.length !== 1){
                          ada.pop()
                          setEditAccessDoorArr(Object.assign([], ada))
                        }
                      }}
                    />

                  </div>: null)
                }
              </BodySpace>
            </BodyManageDV>

            <BodyManageDV>
              <BodySpace style={{
                  padding: "10px 10px 0px 10px"
              }}></BodySpace>
              <BodyForm style={{
                padding: "0px 10px 0px 130px"
              }}>
                <Checkbox color={"primary"} checked={item.isTimeInOut} onChange={(e) => {
                  handleSetTimeInOut(!item.isTimeInOut, index)
                }} />
                <BodyLabel style={{
                  textAlign: "left",
                  width: "180px"
                }}>เพิ่มเวลาเข้าออก</BodyLabel>
              </BodyForm>
            </BodyManageDV>

            <BodyManageDV style={{display: item.isTimeInOut ? 'flex' : 'none'}}>
              <BodySpace style={{
                padding: "10px 10px 0px 10px"
              }}></BodySpace>
              <BodyForm style={{
                padding: "0px 10px 0px 190px"
              }}>
                <Fromto>จาก</Fromto>
                <div style={{width: "120px"}}><TimePicker index={index} disabled={item.isAllTime} onTime={onTimeFrom} init={item.timeBegin} tag={"time-edit-group-from"} error={item.TimeFromValidation} /></div>
                <Fromto>ถึง</Fromto>
                <div style={{width: "120px"}}><TimePicker index={index} disabled={item.isAllTime} onTime={onTimeTo} init={item.timeEnd} tag={"time-edit-group-to"}  error={item.TimeToValidation} /></div>
              </BodyForm>
              <BodySpace style={{
                padding: "10px 10px 0px 10px"
              }}></BodySpace>
            </BodyManageDV>

            <BodyManageDV style={{display: item.isTimeInOut ? 'flex' : 'none'}}>
              <BodySpace style={{
                  padding: "10px 10px 0px 10px"
              }}></BodySpace>
              <BodyForm style={{
                padding: "0px 10px 0px 130px"
              }}>
                <Checkbox color={"primary"} checked={item.isAllTime} onChange={(e) => {
                  handleSetAllTime(!item.isAllTime, index)
                }} />
                <BodyLabel style={{
                  textAlign: "left",
                  width: "180px"
                }}>All Time</BodyLabel>
              </BodyForm>
            </BodyManageDV>

            </div>
          })
        }

        <BodyManageDV>
          <BodySpace style={{
            padding: "10px 10px 0px 10px"
          }}></BodySpace>
          <BodyForm 
            style={{
              alignItems: "center",
              padding: "0px 10px 0px 175px"
            }}
          >
            <Checkbox
              color="primary"
              checked={SelectAll}
              onClick={(e) => {
                var checked = e.target.checked
                if (checked) {
                  // AccessDoorArr
                  var device = DeviceList
                  var all = device.map((item) => {
                    return {
                      doorID: item.device_id,
                      isTimeInOut: false,
                      timeBegin: "00:00",
                      timeEnd: "23:59",
                      isAllTime: false
                    }
                  })
                  setEditAccessDoorArr(Object.assign([], all))
                } else {
                  setEditAccessDoorArr(Object.assign([], [{
                    doorID: undefined,
                    isTimeInOut: false,
                    timeBegin: "00:00",
                    timeEnd: "23:59",
                    isAllTime: false
                  }]))
                }
                setSelectAll(checked)
              }}
            />
            <BodyLabel style={{
              textAlign: "left"
            }}>เลือกทุกประตู</BodyLabel>
          </BodyForm>
          <BodySpace></BodySpace>
        </BodyManageDV>


        <ActionManageDV>
          <div style={{
            display: "flex",
            justifyContent: "space-between",
            width: "100%",
            padding: "20px calc(10% + 30px) 0px calc(10% + 30px)"
          }}>
            <Choice1Btn style={{
              width: 250, 
              height: 60,
              borderColor: "red",
              color: "red"
            }} onClick={() => {
              setEditAccessDoorArrValidation(false)
              setEditGroupNameValidation(false)
              setopenDialogAddGroup(false)
              setopenDialogComfirmDeleteGroup(true)
              setSelectAll(false)
            }}>ลบ</Choice1Btn>
            <Choice2Btn style={{
              width: 250, 
              height: 60
            }} onClick={() => {
              setEditAccessDoorArrValidation(false)
              setEditGroupNameValidation(false)
              handleUpdateGroup()
              setSelectAll(false)
            }}>แก้ไขกลุ่มผู้ใช้</Choice2Btn>
          </div>
        </ActionManageDV>

        </CustomeDialogContentManageDV>
      </Dialog>

      <Dialog
        disableBackdropClick={true}
        fullWidth={false}
        maxWidth={"xl"}
        open={openDialogComfirmDeleteGroup}
        aria-labelledby="update-dialog-MangeDV"
      >
          <Confirm Message={<div>คุณต้องการลบ <span style={{color: "#01A6E6"}}>{RemoveGroupName}</span> หรือไม่</div>} title={"ลบอุปกรณ์"}/>

          <ComfirmAction>

          <ComfirmAction>
          <Choice2Btn 
              style={{
                width: 380, 
                height: 60,
                marginRight: 20
              }}
              onClick={() => {
                setopenDialogComfirmDeleteGroup(false)
                handleRemoveGroup()
              }}
            >
              ยืนยัน
            </Choice2Btn>

            <Choice2Btn 
              style={{
                width: 380, 
                height: 60,
                backgroundColor: "#fff",
                color: "#01A6E6",
                marginLeft: 20
              }}
              onClick={() => {
                setopenDialogComfirmDeleteGroup(false)
              }}
            >
              ยกเลิก
            </Choice2Btn>
          </ComfirmAction>
            
          </ComfirmAction>

      </Dialog>

      <Backdrop className={classesbd.backdrop} open={OpenBackdrop}>
        <CircularProgress color="inherit" />
      </Backdrop>

        <Dialog
          disableBackdropClick={true}
          fullWidth={false}
          maxWidth={"xl"}
          open={openDialogComfirm}
          aria-labelledby="update-dialog-MangeDV"
        >
            {
              (openDialogComfirmStatus ? <Success title={TitleAction}/> : <Unsuccess title={TitleAction} msgerror={MessageError}/>)
            }
            <ComfirmAction>

              {
                (openDialogComfirmStatus ? <Choice2Btn 
                  style={{
                    width: 800, 
                    height: 60
                  }}
                  onClick={() => {
                    setopenDialogComfirm(false)
                  }}
                >
                  ยืนยัน
                </Choice2Btn> : <ComfirmAction>
                  <Choice2Btn 
                    style={{
                      width: 800, 
                      height: 60,
                      backgroundColor: "#e46f6f",
                      border: "red"
                    }}
                    onClick={() => {
                      setopenDialogComfirm(false)
                    }}
                  >
                    ยืนยัน
                  </Choice2Btn>
                </ComfirmAction>)
              }
              
            </ComfirmAction>

        </Dialog>

    </StutusContrainer>

  );
}


import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { makeStyles } from '@material-ui/core/styles';

import TimePicker from 'src/components/GlobalDialog/TimePicker';

import Checkbox from '@material-ui/core/Checkbox';

// custom dialog
import CustomeDialogContentManageDV from 'src/components/material/dialog/MangeDevice/CustomeDialogContent'
import CustomeDialogTitleManageDV from 'src/components/material/dialog/MangeDevice/CustomeDialogTitle'
import CustomeExitDialogManageDV from 'src/components/material/dialog/MangeDevice/CustomeExitDialog'
import ActionManageDV from "src/components/material/dialog/MangeDevice/content/Action"
import BodyManageDV from "src/components/material/dialog/MangeDevice/content/Body"
import BodyForm from 'src/components/material/dialog/MangeDevice/content/Body/Form'
import BodyLabel from 'src/components/material/dialog/MangeDevice/content/Body/Label'
import InputFromId from 'src/components/material/dialog/MangeDevice/content/Form/InputId'
import Fromto from 'src/components/material/dialog/MangeDevice/content/Body/Fromto'

import Backdrop from '@material-ui/core/Backdrop'
import CircularProgress from '@material-ui/core/CircularProgress'

// Confirm dialog
import Success from 'src/components/ComfirmAction/Success'
import Unsuccess from 'src/components/ComfirmAction/Unsuccess'

// material ui
import {
  IconButton,
  Dialog
} from '@material-ui/core'

// image 
import ADD from 'src/asset/img/navbar/ADD.svg'
import Exit from 'src/asset/img/setting/EXIT.svg'
import ADDGROUP from "src/asset/img/sidebar/ADDGROUP.svg"
import REMOVEDOORACCESS from "src/asset/img/sidebar/REMOVEDOORACCESS.svg"

// button component
import Choice1Btn from 'src/components/material/button/Choice1Btn'
import Choice2Btn from 'src/components/material/button/Choice2Btn'

// setting style 
import useStyles from 'src/components/device/setting/style'
import { withStyles } from '@material-ui/core/styles';

import addGroup from 'src/apis/addGroup'
import getGroup from 'src/apis/getGroup'

// From Select
import FormControl from "@material-ui/core/FormControl"
import Select from "@material-ui/core/Select"
import InputBase from '@material-ui/core/InputBase'
import MenuItem from '@material-ui/core/MenuItem';
const BootstrapInput = withStyles((theme) => ({
  root: {
    'label + &': {
      marginTop: theme.spacing(3),
    },
  },
  input: {
    textAlign: "left",
    borderRadius: 4,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #ccc',
    fontSize: 16,
    padding: '10px 47px 13px 12px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:focus': {
      borderRadius: 4,
      // borderColor: '#80bdff',
      // boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
      border: '1px solid #337AB7',
    },
  },
}))(InputBase);

const useStylesBackdrop = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff"
  }
}));

// create style to element
const Container = styled("div")`
  width: 100%,
  height: 100%
`
const ComfirmAction = styled("div")`
  display: flex;
  align-content: center;
  justify-content: space-around;
  margin: 20px;
`

const BodySpace = styled("div")`
  width: 9%;
`


const AddGroupDialog = (props) => {
  const { onClose, open, onConfirm, store } = props
  const classes = useStyles()
  
  const [ GroupName, setGroupName ] = React.useState("")
  const [ AccessDoorArr, setAccessDoorArr ] = React.useState([{
    doorID: undefined,
    isTimeInOut: false,
    timeBegin: "00:00",
    timeEnd: "23:59",
    isAllTime: false
  }])

  const [ DeviceList, setDeviceList ] = React.useState([])

  const [ SelectAll, setSelectAll ] = React.useState(false)

  const [ AddGroupNameValidation, setAddGroupNameValidation ] = React.useState(false)
  // const [ AddTimeFromValidation, setAddTimeFromValidation ] = React.useState(false)
  // const [ AddTimeToValidation, setAddTimeToValidation ] = React.useState(false)
  const [ AccessDoorArrValidation, setAccessDoorArrValidation ] = React.useState(false)

  const [ TimeToValidation, setTimeToValidation ] = React.useState(false)
  const [ TimeFromValidation, setTimeFromValidation ] = React.useState(false)

  // const [anchorElTimeFrom, setanchorElTimeFrom] = React.useState(null);
  // const [anchorElTimeTo, setanchorElTimeTo] = React.useState(null);

  // const [ ElementMenu, setElementMenu ]  = React.useState(null);
  // const [ OpneMenu, setOpneMenu ]  = React.useState(false);

  const handleAddGroup = () => {
    var check = true
    if (GroupName === ""){
      setAddGroupNameValidation(true)
      check = false
    } else {
      setAddGroupNameValidation(false)
    }

    if (getUnselectDevice().length === 0) {
      setAccessDoorArrValidation(false)
    } else {
      setAccessDoorArrValidation(true)
      check = false
    }
    if (check) {
      setAddGroupNameValidation(false)
      setAccessDoorArrValidation(false)
      onConfirm({
        GroupName: GroupName,
        DeviceList: AccessDoorArr,
      })
      setGroupName("")
      setAccessDoorArr([{
        doorID: undefined,
        isTimeInOut: false,
        timeBegin: "00:00",
        timeEnd: "23:59",
        isAllTime: false
      }])

    }
    /*if (GroupName === ""){
      setAddGroupNameValidation(true)
    }else {
      setAddGroupNameValidation(false)
    }

    if (AccessDoorArr.length !== 0 && AccessDoorArr[0] !== undefined){
      setAccessDoorArrValidation(true)
    }else {
      setAccessDoorArrValidation(false)
    }
    if (GroupName !== "" && AccessDoorArr.length !== 0 && AccessDoorArr[0] !== undefined){
      onConfirm({
        GroupName: GroupName,
        DeviceList: AccessDoorArr,
        TimeBegin: TimeBegin,
        TimeEnd: TimeEnd,
      })
    }*/
  }

  React.useEffect(() => {
    const initDevice = () => {
      // setDeviceList
      const device = store.getState().Device
      if (device !== undefined){
        var temp = []
        device.forEach((item ,index) => {
          temp.push( {
            device_id: item.gate_id,
            device_name: item.gate.name_gate
          } )
        })
        setDeviceList(temp)
      }
    }
    initDevice()
  }, [store])


  const initStateForm = () => {
    setGroupName("")
    setAccessDoorArr([{
      doorID: undefined,
      isTimeInOut: false,
      timeBegin: "00:00",
      timeEnd: "23:59",
      isAllTime: false
    }])
    setAddGroupNameValidation(false)
  }

  const onTimeFrom = (time, index) => {
    let ada = AccessDoorArr
    var check = true

    if (new Date(`2020-02-02 ${time}`).getTime() >= new Date(`2020-02-02 ${ada[index].timeEnd}`).getTime() ){
      check = false
      ada[index].TimeFromValidation = true
      ada[index].TimeToValidation = true
      setTimeToValidation(true)
      setTimeFromValidation(true)
    } else {
      ada[index].TimeFromValidation = false
      ada[index].TimeToValidation = false
      setTimeToValidation(false)
      setTimeFromValidation(false)
    }

    ada[index].timeBegin = time
    ada[index].isAllTime = false
    setAccessDoorArr(Object.assign([], []))
    // setTimeout(() => {
      setAccessDoorArr(Object.assign([], ada))
    // }, 100);
  }

  const onTimeTo = (time, index) => {
    let ada = AccessDoorArr
    var check = true

    if (new Date(`2020-02-02 ${ada[index].timeBegin}`).getTime() >= new Date(`2020-02-02 ${time}`).getTime() ){
      check = false
      ada[index].TimeFromValidation = true
      ada[index].TimeToValidation = true
      setTimeToValidation(true)
      setTimeFromValidation(true)
    } else {
      ada[index].TimeFromValidation = false
      ada[index].TimeToValidation = false
      setTimeToValidation(false)
      setTimeFromValidation(false)
    }

    ada[index].timeEnd = time
    ada[index].isAllTime = false
    setAccessDoorArr(Object.assign([], []))
    // setTimeout(() => {
      setAccessDoorArr(Object.assign([], ada))
    // }, 100);
  }

  const handleSetTimeInOut = (value, index) => {
    let ada = AccessDoorArr
    ada[index].isTimeInOut = value
    setAccessDoorArr(Object.assign([], ada))
  }

  const handleSetAllTime = (value, index) => {
    let ada = AccessDoorArr
    ada[index].isAllTime = value
    ada[index].timeBegin = "00:00"
    ada[index].timeEnd = "23:59"
    ada[index].TimeFromValidation = false
    ada[index].TimeToValidation = false
    setTimeToValidation(false)
    setTimeFromValidation(false)
    setAccessDoorArr(Object.assign([], []))
    // setTimeout(() => {
      setAccessDoorArr(Object.assign([], ada))
    // }, 100);
  }

  const getUnselectDevice = () => {
    return AccessDoorArr.filter((obj) => obj.doorID === undefined)
  }

  return (
    <div>

      <Dialog
        disableBackdropClick={true}
        fullWidth={false}
        maxWidth={"xl"}
        open={open}
        onClose={onClose}
        // onEnter={onConfirm}
        aria-labelledby="update-dialog-MangeDV"
      >
        <CustomeExitDialogManageDV>
          <IconButton onClick={() => {
            onClose(false)
            initStateForm()
            setAddGroupNameValidation(false)
            setAccessDoorArrValidation(false)
          }}>
            <img src={Exit} alt="Exit" />
          </IconButton>
        </CustomeExitDialogManageDV>
        <CustomeDialogTitleManageDV 
        id="update-dialog-MangeDV"
        children={<div style={{
          fontSize: 28,
          textAlign: "center",
          width: 400
        }}>เพิ่มกลุ่มผู้ใช้</div>}
        ></CustomeDialogTitleManageDV>
        <CustomeDialogContentManageDV>
        
          
          <BodyManageDV style={{
            paddingTop: "30px"
          }}>
            <BodySpace style={{
              padding: "10px 10px 0px 10px"
            }}></BodySpace>
            <BodyLabel style={{
              padding: "10px 10px 0px 10px",
              textAlign: "left"
            }}>ชื่อกลุ่มผู้ใช้</BodyLabel>
            <BodyForm style={{
              padding: "10px 10px 0px 10px"
            }}>
              
              <InputFromId 
                className={classes.InputIdControl}
                variant="outlined" 
                error={AddGroupNameValidation}
                labelwidth={0}
                placeholder="ชื่อกลุ่มผู้ใช้" 
                fullWidth={true}
                autoComplete='off'
                value={GroupName}
                onChange={(e) => {
                  setGroupName(e.target.value)
                }}
              />

            </BodyForm>
            <BodySpace style={{
              padding: "10px 10px 0px 10px"
            }}></BodySpace>
          </BodyManageDV>

          <div style={{marginTop: "30px"}}></div>
          {
            AccessDoorArr.map((item, index) => {
              return <div style={{marginBottom: "30px"}}>
                <BodyManageDV 
                style={{
                  display: "flex",
                  justifyContent: "center"
                }}
                key={index.toString()}
              >
                <BodySpace style={{
                  padding: "10px 10px 0px 10px"
                }}></BodySpace>
                <BodyLabel style={{
                  padding: "10px 10px 0px 10px",
                  textAlign: "left"
                }}> {((index === 0) ? "สิทธิ์เข้าออกประตู": null)} </BodyLabel>
                <BodyForm style={{
                  padding: "10px 10px 0px 10px"
                }}>
                  
                  <FormControl variant="outlined" className={classes.formControl}>
                    <Select
                      style={ AccessDoorArrValidation ? (AccessDoorArr[index].doorID === undefined ? { border: "1px solid red", borderRadius: 6} : null) : null }
                      labelId="demo-simple-select-outlined-label"
                      id="demo-simple-select-outlined"
                      input={<BootstrapInput />}
                      value={((AccessDoorArr[index].doorID === undefined) ? 
                              "undefined": AccessDoorArr[index].doorID)}
                      onChange={(e) => {
                        setSelectAll(false)
                        var ada = AccessDoorArr
                        ada[index].doorID = (e.target.value === "undefined" ? undefined : e.target.value)
                        const checkIfDuplicateExists = (w) => {
                          var neww = w.map((item) => {
                            return item.doorID
                          })
                          return new Set(neww).size !== neww.length 
                        }
                        var check_dup = ada
                        var AccessDoorArrFiltered = check_dup.filter(function(x) {
                          return x.doorID !== undefined && x.doorID !== "undefined";
                        });
                        if (!checkIfDuplicateExists(AccessDoorArrFiltered)){
                          setAccessDoorArr(Object.assign([], ada))
                        } else {
                          const removeDuplicate = AccessDoorArrFiltered.reduce((acc, current) => {
                            const x = acc.find(item => item.doorID === current.doorID)
                            if (!x) {
                              return acc.concat([current])
                            } else {
                              return acc
                            }
                          }, [])
                          setAccessDoorArr(Object.assign([], removeDuplicate))
                        }
                      }}
                    >
                      <MenuItem value={"undefined"}>{ "กรุณาเลือกสิทธิ์เข้าออกประตู" }</MenuItem>
                      {
                        DeviceList.map((device_item, device_index) => {
                          return (
                            <MenuItem key={device_index.toString()} value={device_item.device_id}>{ device_item.device_name }</MenuItem>
                          )
                        })
                      }
                    </Select>
                  </FormControl>
                      
                </BodyForm>
                <BodySpace style={{
                  // padding: "10px 10px 0px 10px",
                  padding: "10px 0px 0px 0px",
                  width: "12%"
                }}>

                  {
                    ((index === AccessDoorArr.length - 1) ? <div>

                      <img 
                        src={ADDGROUP} 
                        alt="ADDGROUP" 
                        style={{
                          width: 20,
                          heigth: 20,
                          padding: "10px 20px 0px 0px",
                          cursor: "pointer"
                        }}
                        onClick={() => {
                          var ada = AccessDoorArr
                          ada.push({
                            doorID: undefined,
                            isTimeInOut: false,
                            timeBegin: "00:00",
                            timeEnd: "23:59",
                            isAllTime: false
                          })
                          setAccessDoorArr(Object.assign([], ada))
                        }}
                      />

                      <img 
                        src={REMOVEDOORACCESS} 
                        alt="REMOVEDOORACCESS" 
                        style={{
                          width: 20,
                          heigth: 20,
                          padding: "10px 20px 0px 0px",
                          cursor: "pointer"
                        }}
                        onClick={() => {
                          var ada = AccessDoorArr
                          if (ada.length !== 1){
                            ada.pop()
                            setAccessDoorArr(Object.assign([], ada))
                          }
                        }}
                      />

                    </div>: null)
                  }
                </BodySpace>
              </BodyManageDV>

              <BodyManageDV>
                <BodySpace style={{
                    padding: "10px 10px 0px 10px"
                }}></BodySpace>
                <BodyForm style={{
                  padding: "0px 10px 0px 130px"
                }}>
                  <Checkbox color={"primary"} checked={item.isTimeInOut} onChange={(e) => {
                    handleSetTimeInOut(!item.isTimeInOut, index)
                  }} />
                  <BodyLabel style={{
                    textAlign: "left",
                    width: "180px"
                  }}>เพิ่มเวลาเข้าออก</BodyLabel>
                </BodyForm>
              </BodyManageDV>

              <BodyManageDV style={{display: item.isTimeInOut ? 'flex' : 'none'}}>
                <BodySpace style={{
                  padding: "10px 10px 0px 10px"
                }}></BodySpace>
                <BodyForm style={{
                  padding: "0px 10px 0px 190px"
                }}>
                  <Fromto>จาก</Fromto>
                  <div style={{width: "120px"}}><TimePicker index={index} disabled={item.isAllTime} onTime={onTimeFrom} init={item.timeBegin} tag={"time-add-group-from"} error={ item.TimeFromValidation }/></div>
                  <Fromto>ถึง</Fromto>
                  <div style={{width: "120px"}}><TimePicker index={index} disabled={item.isAllTime} onTime={onTimeTo} init={item.timeEnd} tag={"time-add-group-to"}  error={ item.TimeToValidation }/></div>
                </BodyForm>
                <BodySpace style={{
                  padding: "10px 10px 0px 10px"
                }}></BodySpace>
              </BodyManageDV>

              <BodyManageDV style={{display: item.isTimeInOut ? 'flex' : 'none'}}>
                <BodySpace style={{
                    padding: "10px 10px 0px 10px"
                }}></BodySpace>
                <BodyForm style={{
                  padding: "0px 10px 0px 130px"
                }}>
                  <Checkbox color={"primary"} checked={item.isAllTime} onChange={(e) => {
                    handleSetAllTime(!item.isAllTime, index)
                  }} />
                  <BodyLabel style={{
                    textAlign: "left",
                    width: "180px"
                  }}>All Time</BodyLabel>
                </BodyForm>
              </BodyManageDV>

              </div>
            })
          }

          <BodyManageDV>
            <BodySpace style={{
              padding: "10px 10px 0px 10px"
            }}></BodySpace>
            <BodyForm 
              style={{
                alignItems: "center",
                padding: "0px 10px 0px 188px"
              }}
            >
              <Checkbox
                color="primary"
                checked={SelectAll}
                onClick={(e) => {
                  var checked = e.target.checked
                  if (checked) {
                    // AccessDoorArr
                    var device = DeviceList
                    var all = device.map((item) => {
                      return {
                        doorID: item.device_id,
                        isTimeInOut: false,
                        timeBegin: "00:00",
                        timeEnd: "23:59",
                        isAllTime: false
                      }
                    })
                    setAccessDoorArr(Object.assign([], all))
                  } else {
                    setAccessDoorArr(Object.assign([], [{
                      doorID: undefined,
                      isTimeInOut: false,
                      timeBegin: "00:00",
                      timeEnd: "23:59",
                      isAllTime: false
                    }]))
                  }
                  setSelectAll(checked)
                }}
              />
              <BodyLabel style={{
                textAlign: "left"
              }}>เลือกทุกประตู</BodyLabel>
            </BodyForm>
            <BodySpace></BodySpace>
          </BodyManageDV>


          <ActionManageDV>
            <div style={{
              display: "flex",
              justifyContent: "space-between",
              width: "100%",
              padding: "20px calc(10% + 30px) 0px calc(10% + 30px)"
            }}>
              <Choice1Btn style={{
                width: 250, 
                height: 60
              }} onClick={() => {
                onClose(false)
                setAddGroupNameValidation(false)
                setAccessDoorArrValidation(false)
                initStateForm()
              }}>ยกเลิก</Choice1Btn>
              <Choice2Btn 
                style={{
                  width: 250, 
                  height: 60
                }}
                onClick={() => {
                  handleAddGroup()
                }}
              >เพิ่มกลุ่มผู้ใช้</Choice2Btn>
            </div>
          </ActionManageDV>

        </CustomeDialogContentManageDV>

      </Dialog>

    </div>
  )
}

AddGroupDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  onConfirm: PropTypes.func.isRequired,
  store: PropTypes.object.isRequired,
};

export default function AddGroup (props) {

  const [ store ] = React.useState(props.store)
  const [ Open, setOpen ] = React.useState(false)
  const [ Token ] = React.useState(store.getState().AccessToken)

  const classesbd = useStylesBackdrop()
  const [ OpenBackdrop, setOpenBackdrop ] = React.useState(false);

  const [ openDialogComfirmStatus, setopenDialogComfirmStatus ] = React.useState(false)
  const [ TitleAction, setTitleAction ] = React.useState(false)
  const [ MessageError ] = React.useState(false)
  const [ openDialogComfirm, setopenDialogComfirm ] = React.useState(false)
  
  const handleDialogAddGroupClose = (close) => {
    console.log(close)
    setOpen(false)
  }

  const handleDialogAddGroupSubmit = async (data) => {
    var query = data
    var doorQuery = []
    var check_from = true, check_to = true
    if (query.DeviceList.length > 0){
      var FilterDoorUndefined = query.DeviceList.filter(function(x) {
        if (x.TimeFromValidation && x.TimeToValidation){
          check_from = false
          check_to = false
        }
        return x.doorID !== undefined && x.doorID !== "undefined";
      });
      if (check_from && check_to){
        setOpenBackdrop(true)
        setOpen(false)
        doorQuery = FilterDoorUndefined.map((item) => {
          return `{
            gateId: ${Number(item.doorID)}, 
            overrideTime: ${item.isTimeInOut}, 
            beginTime: "${item.timeBegin}", 
            endTime: "${item.timeEnd}"
          }`
        })


        var Query = `
        mutation {
            CreateGroup(
                name: "${query.GroupName}", 
                gates: [${doorQuery.map((item) => {return item})}]
            ) {
                group_id
                name
            }
        }
      `
      const res_addgroup_ = await addGroup({
        query: Query
      }, Token)
      if (res_addgroup_.data.errors !== undefined){
        alert(res_addgroup_.data.errors[0].message)
      }
  
  
      const res_group = await getGroup({
        query: `
            query {
              Group {
                  group_id
                  name
                  gates {
                      gate_id
                      name
                      begin_time
                      end_time
                  }
                  employees {
                      employee_id
                      name
                      lastname
                      position
                      mifare_id
                      picture_url
                      face_id
                  }
                  group_gates {
                      gate_id
                      group_id
                      override_time
                      begin_time
                      end_time
                  }
              }
          }
        `
      }, Token)
      if (res_group.data.errors === undefined){
        setOpenBackdrop(false)
  
        setopenDialogComfirmStatus(true)
        setopenDialogComfirm(true)
        setTitleAction(`สร้างกลุ่ม ${query.GroupName} สำเร็จ`)
      }else {
        setOpenBackdrop(false)
  
        setopenDialogComfirmStatus(false)
        setopenDialogComfirm(true)
        setTitleAction(`สร้างกลุ่ม ${query.GroupName} ไม่สำเร็จ`)
      }

      }
    }

  }

  return (
    <Container>
      <IconButton
        aria-controls="customized-menu"
        aria-haspopup="true"
        variant="contained"
        color="primary"
        onClick={() => {
          setOpen(true)
        }}
      >
        <img src={ADD} alt={ADD} />
      </IconButton> 

      <AddGroupDialog 
        open={Open}
        onClose={handleDialogAddGroupClose}
        onConfirm={handleDialogAddGroupSubmit}
        store={store}
      />

      <Backdrop className={classesbd.backdrop} open={OpenBackdrop}>
        <CircularProgress color="inherit" />
      </Backdrop>

        <Dialog
          disableBackdropClick={true}
          fullWidth={false}
          maxWidth={"xl"}
          open={openDialogComfirm}
          aria-labelledby="update-dialog-MangeDV"
        >
            {
              (openDialogComfirmStatus ? <Success title={TitleAction}/> : <Unsuccess title={TitleAction} msgerror={MessageError}/>)
            }
            <ComfirmAction>

              {
                (openDialogComfirmStatus ? <Choice2Btn 
                  style={{
                    width: 800, 
                    height: 60
                  }}
                  onClick={() => {
                    setopenDialogComfirm(false)
                  }}
                >
                  ยืนยัน
                </Choice2Btn> : <ComfirmAction>
                  <Choice2Btn 
                    style={{
                      width: 800, 
                      height: 60,
                      backgroundColor: "#e46f6f",
                      border: "red"
                    }}
                    onClick={() => {
                      setopenDialogComfirm(false)
                    }}
                  >
                    ยืนยัน
                  </Choice2Btn>
                </ComfirmAction>)
              }
              
            </ComfirmAction>

        </Dialog>

    </Container>
  )
}
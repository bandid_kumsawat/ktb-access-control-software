import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { makeStyles } from '@material-ui/core/styles';

// customscroll
import "./style.css"
import FileCopyIcon from '@material-ui/icons/FileCopy';

// custom dialog
import CustomeDialogContentManageDV from 'src/components/material/dialog/MangeDevice/CustomeDialogContent'
import CustomeDialogTitleManageDV from 'src/components/material/dialog/MangeDevice/CustomeDialogTitle'
import CustomeExitDialogManageDV from 'src/components/material/dialog/MangeDevice/CustomeExitDialog'
import ActionManageDV from "src/components/material/dialog/MangeDevice/content/Action"
import BodyManageDV from "src/components/material/dialog/MangeDevice/content/Body"
import BodyForm from 'src/components/material/dialog/MangeDevice/content/Body/Form'
import BodyLabel from 'src/components/material/dialog/MangeDevice/content/Body/Label'
import InputFromId from 'src/components/material/dialog/MangeDevice/content/Form/InputId'
import Fromto from 'src/components/material/dialog/MangeDevice/content/Body/Fromto'

import Backdrop from '@material-ui/core/Backdrop'
import CircularProgress from '@material-ui/core/CircularProgress'

// Confirm dialog
import Success from 'src/components/ComfirmAction/Success'
import Unsuccess from 'src/components/ComfirmAction/Unsuccess'

// material ui
import {
  IconButton,
  Dialog
} from '@material-ui/core'

// image 
import Exit from 'src/asset/img/setting/EXIT.svg'

// button component
import Choice1Btn from 'src/components/material/button/Choice1Btn'
import Choice2Btn from 'src/components/material/button/Choice2Btn'

// setting style 
import useStyles from 'src/components/device/setting/style'
import { withStyles } from '@material-ui/core/styles';

import VpnKeyIcon from '@material-ui/icons/VpnKey';

const useStylesBackdrop = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff"
  }
}));

// create style to element
const Container = styled("div")`
  width: 100%,
  height: 100%
`
const ComfirmAction = styled("div")`
  display: flex;
  align-content: center;
  justify-content: space-around;
  margin: 20px;
`

const BodySpace = styled("div")`
  width: 9%;
`

const BoxShowSecretKey = styled("div")`
  width: 100%;
  display: flex;
  align-content: center;
  justify-content: flex-start;
  align-items: center;
  border: 1px solid #a9a9a9;
  border-radius: 3px;
  overflow-x: scroll;
  padding-right: 14px;
  padding-left: 14px;
`
const Copyed = styled("div")`
  white-space: nowrap;
  display: flex;
  align-content: center;
  align-items: center;
  justify-content: flex-start;
  text-decoration: underline;
`
const SecretKeyRequestDialog = (props) => {
  const { onClose, open, onConfirm, store } = props
  const classes = useStyles()
  
  return (
    <div>

      <Dialog
        disableBackdropClick={true}
        fullWidth={false}
        maxWidth={"xl"}
        open={open}
        onClose={onClose}
        // onEnter={onConfirm}
        aria-labelledby="update-dialog-MangeDV"
      >
        <CustomeExitDialogManageDV>
          <IconButton onClick={() => {
            onClose(false)
          }}>
            <img src={Exit} alt="Exit" />
          </IconButton>
        </CustomeExitDialogManageDV>
        <CustomeDialogTitleManageDV 
        id="update-dialog-MangeDV"
        children={<div style={{
          fontSize: 28,
          textAlign: "center",
          width: 400
        }}>ขอรับ Secret Key</div>}
        ></CustomeDialogTitleManageDV>
        <CustomeDialogContentManageDV>
        
          
          <BodyManageDV style={{
            paddingTop: "30px"
          }}>
            <BodySpace style={{
              padding: "10px 10px 0px 10px"
            }}></BodySpace>
            <BodyLabel style={{
              padding: "10px 10px 0px 10px",
              textAlign: "left"
            }}>Secret Key</BodyLabel>
            <BodyForm style={{
              padding: "10px 10px 0px 10px"
            }}>
              
              <BoxShowSecretKey id={"BoxShowSecretKey-scorll"}>{"FFGGIDIDIDIID445DIID445EIID445DIID445EIID445DIID445EDFE555554877DFDDC"}</BoxShowSecretKey>
              <IconButton>
                <FileCopyIcon color={"primary"} />
              </IconButton>
              <Copyed >
                Copyed !
              </Copyed>

            </BodyForm>
            <BodySpace style={{
              padding: "10px 10px 0px 10px"
            }}></BodySpace>
          </BodyManageDV>


          <ActionManageDV>
            <div style={{
              display: "flex",
              justifyContent: "space-between",
              width: "100%",
              padding: "20px calc(10% + 30px) 0px calc(10% + 30px)"
            }}>
              <Choice1Btn style={{
                width: 250, 
                height: 60
              }} onClick={() => {
                onClose(false)
              }}>ยกเลิก</Choice1Btn>
              <Choice2Btn 
                style={{
                  width: 250, 
                  height: 60
                }}
                onClick={() => {
                  navigator.clipboard.writeText("k2k2k2k2k2k2kkkkkkkkkkkkkkkk2jsjsjsjs")
                }}
              >ขอรับ Secret Key</Choice2Btn>
            </div>
          </ActionManageDV>

        </CustomeDialogContentManageDV>

      </Dialog>

    </div>
  )
}

SecretKeyRequestDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  onConfirm: PropTypes.func.isRequired,
  store: PropTypes.object.isRequired,
};

export default function AddGroup (props) {

  const [ store ] = React.useState(props.store)
  const [ Open, setOpen ] = React.useState(false)
  const [ Token ] = React.useState(store.getState().AccessToken)

  const classesbd = useStylesBackdrop()
  const [ OpenBackdrop, setOpenBackdrop ] = React.useState(false);

  const [ openDialogComfirmStatus, setopenDialogComfirmStatus ] = React.useState(false)
  const [ TitleAction, setTitleAction ] = React.useState(false)
  const [ MessageError ] = React.useState(false)
  const [ openDialogComfirm, setopenDialogComfirm ] = React.useState(false)
  
  const handleDialogAddGroupClose = (close) => {
    console.log(close)
    setOpen(false)
  }

  const handleDialogAddGroupSubmit = async (data) => {
    console.log(data)
  }

  return (
    <Container>
      <IconButton
        aria-controls="customized-menu"
        aria-haspopup="true"
        variant="contained"
        color="primary"
        onClick={() => {
          setOpen(true)
        }}
      >
        <VpnKeyIcon color={"primary"} />
      </IconButton> 

      <SecretKeyRequestDialog 
        open={Open}
        onClose={handleDialogAddGroupClose}
        onConfirm={handleDialogAddGroupSubmit}
        store={store}
      />

      <Backdrop className={classesbd.backdrop} open={OpenBackdrop}>
        <CircularProgress color="inherit" />
      </Backdrop>

      <Dialog
        disableBackdropClick={true}
        fullWidth={false}
        maxWidth={"xl"}
        open={openDialogComfirm}
        aria-labelledby="update-dialog-MangeDV"
      >
          {
            (openDialogComfirmStatus ? <Success title={TitleAction}/> : <Unsuccess title={TitleAction} msgerror={MessageError}/>)
          }
          <ComfirmAction>

            {
              (openDialogComfirmStatus ? <Choice2Btn 
                style={{
                  width: 800, 
                  height: 60
                }}
                onClick={() => {
                  setopenDialogComfirm(false)
                }}
              >
                ยืนยัน
              </Choice2Btn> : <ComfirmAction>
                <Choice2Btn 
                  style={{
                    width: 800, 
                    height: 60,
                    backgroundColor: "#e46f6f",
                    border: "red"
                  }}
                  onClick={() => {
                    setopenDialogComfirm(false)
                  }}
                >
                  ยืนยัน
                </Choice2Btn>
              </ComfirmAction>)
            }
            
          </ComfirmAction>

      </Dialog>

    </Container>
  )
}
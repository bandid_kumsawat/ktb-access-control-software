import React from "react";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";

import Login from './pages/login'
import DeviceStatus from './components/device/status'
import DeviceSetting from './components/device/setting'
import DashboardLayout from "./components/DashboardLayout"
import Log from "./components/device/log"
import UserSetting from './components/user/setting'
import Admin from './components/user/admin'
import GroupUser from './components/user/setting/group'

import { useRoutes } from 'react-router-dom';

import { store } from './stores/store'

const theme = createMuiTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 450,
      md: 600,
      lg: 900,
      xl: 1200
    }
  },
  palette: {
    primary: {
      main: "#01A6E6",
    },
    secondary: {
      main: "#FFFFFF",
    },
  },
});

function App() {
  const routing = useRoutes([
    {
      path: '/',
      element: <Login store={ store } />,
    }, {
      path: '/Login',
      element: <Login store={ store } />,
    }, {
      path: "/dashboard",
      element: <DashboardLayout store={ store } />,
      children: [
        { path: '/status', element: <DeviceStatus store={ store } /> },
        { path: '/log', element: <Log store={ store } /> },
        { path: '/setting', element: <DeviceSetting store={ store } />, },
        { path: '/user', element: <UserSetting store={ store } />, children: [
          { path: '/group/:group_id', element: <GroupUser store={ store } />, },
        ]},
        { path: '/admin', element: <Admin store={ store } />, },
      ]
    }
  ]);

  return (
    <MuiThemeProvider theme={theme}>
      { routing }
    </MuiThemeProvider>
  );
}

export default App;

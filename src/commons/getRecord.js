import { ContactSupportOutlined } from '@material-ui/icons';
import getLog from 'src/apis/getLog';

var AccessDoorArrFiltered = (device) => {
  return device.filter(function(x) {
    return x !== undefined && x !== "undefined";
  });
}

export default function getRecord (query_, Token) {
  var str_device = "[]"
  var orderby = ""
  if (query_.search.orderType === "date_in" || query_.search.orderType === "time_in"){
    orderby = "enter_time"
  } else if (query_.search.orderType === "date_out" || query_.search.orderType === "time_out"){
    orderby = "exit_time"
  } else if (query_.search.orderType === "enter_in" ) {
    orderby = "enter_method"
  } else if (query_.search.orderType === "enter_out" ) {
    orderby = "exit_method"
  }
  if (query_.flag){
    if (query_.search.device !== undefined){
      var filterdata = AccessDoorArrFiltered(query_.search.device)
      str_device = JSON.stringify(filterdata)
      return getLog({
        query: `
          {
              GetRecord (
                `+((query_.search.name !== "") ? `name: "`+query_.search.name+`",`: ``)+`
                `+((query_.search.surname !== "") ? `employee_code: "`+query_.search.surname+`",`: ``)+`
                `+((query_.search.card !== "") ? `mifareId: "`+query_.search.card+`",`: ``)+`
                `+((query_.search.startdate !== "") ? `beginDate: "`+query_.search.startdate+`",`: ``)+`
                `+((query_.search.enddate !== "") ? `endDate: "`+query_.search.enddate+`",`: ``)+`
                `+((query_.search.starttime !== "") ? `beginTime: "`+query_.search.starttime+`",`: ``)+`
                `+((query_.search.endtime !== "") ? `endTime: "`+query_.search.endtime+`",`: ``)+`
    
                `+((query_.search.device !== "" && query_.search.device !== undefined) ? `enterGate: `+str_device+`,`: ``)+`
                `+((query_.search.device !== "" && query_.search.device !== undefined) ? `exitGate: `+str_device+`,`: ``)+`
                `+((query_.search.offset !== "") ? `offset: `+query_.search.offset+`,`: ``)+`
                `+((query_.search.limit !== "") ? `limit: `+query_.search.limit+`,`: ``)+`
                


                `+((query_.search.orderType !== "") ? `orderType: "`+query_.search.orderBy+`",`: ``)+`
                `+((query_.search.orderBy !== "") ? `orderBy: "`+orderby+`",`: ``)+`
                
              ) {
                results {
                  record_id
                  enter_time
                  enter_method
                  exit_method
                  enter_gate {
                      gate_id
                      name
                      begin_time
                      end_time
                  }
                  exit_time
                  exit_gate {
                      gate_id
                      name
                      begin_time
                      end_time
                  }
                  is_exited
                  employee {
                      employee_id
                      name
                      lastname
                      position
                      mifare_id
                      picture_url
                      face_id
                  }
                }
                count
              }
          }
        `
      }, Token)
    } else {
      return getLog({
        query: `
          {
              GetRecord (
                `+((query_.search.name !== "") ? `name: "`+query_.search.name+`",`: ``)+`
                `+((query_.search.surname !== "") ? `employee_code: "`+query_.search.surname+`",`: ``)+`
                `+((query_.search.card !== "") ? `mifareId: "`+query_.search.card+`",`: ``)+`
                `+((query_.search.startdate !== "") ? `beginDate: "`+query_.search.startdate+`",`: ``)+`
                `+((query_.search.enddate !== "") ? `endDate: "`+query_.search.enddate+`",`: ``)+`
                `+((query_.search.starttime !== "") ? `beginTime: "`+query_.search.starttime+`",`: ``)+`
                `+((query_.search.endtime !== "") ? `endTime: "`+query_.search.endtime+`",`: ``)+`
                enterGate: [],
                exitGate: [],
                `+((query_.search.offset !== "") ? `offset: `+query_.search.offset+`,`: ``)+`
                `+((query_.search.limit !== "") ? `limit: `+query_.search.limit+`,`: ``)+`
                
                `+((query_.search.orderType !== "") ? `orderType: "`+query_.search.orderBy+`",`: ``)+`
                `+((query_.search.orderBy !== "") ? `orderBy: "`+orderby+`",`: ``)+`
              ) {
                results {
                  record_id
                  enter_time
                  enter_method
                  exit_method
                  enter_gate {
                      gate_id
                      name
                      begin_time
                      end_time
                  }
                  exit_time
                  exit_gate {
                      gate_id
                      name
                      begin_time
                      end_time
                  }
                  is_exited
                  employee {
                      employee_id
                      name
                      lastname
                      position
                      mifare_id
                      picture_url
                      face_id
                  }
                }
                count
              }
          }
        `
      }, Token)
    }
  } else {
    if (query_.search.device !== undefined){
      var filterdata = AccessDoorArrFiltered(query_.search.device)
      str_device = JSON.stringify(filterdata)
      console.log(`
      {
          GetRecord (
            `+((query_.search.offset !== "") ? `offset: `+query_.search.offset+`,`: ``)+`
            `+((query_.search.limit !== "") ? `limit: `+query_.search.limit+`,`: ``)+`

            
            `+((query_.search.orderType !== "") ? `orderType: "`+query_.search.orderBy+`",`: ``)+`
            `+((query_.search.orderBy !== "") ? `orderBy: "`+orderby+`",`: ``)+`

          ) {
            results {
              record_id
              enter_time
              enter_method
              exit_method
              enter_gate {
                  gate_id
                  name
                  begin_time
                  end_time
              }
              exit_time
              exit_gate {
                  gate_id
                  name
                  begin_time
                  end_time
              }
              is_exited
              employee {
                  employee_id
                  name
                  lastname
                  position
                  mifare_id
                  picture_url
                  face_id
              }
            }
            count
          }
      }
    `)
      return getLog({
        query: `
          {
              GetRecord (
                `+((query_.search.offset !== "") ? `offset: `+query_.search.offset+`,`: ``)+`
                `+((query_.search.limit !== "") ? `limit: `+query_.search.limit+`,`: ``)+`

                
                `+((query_.search.orderType !== "") ? `orderType: "`+query_.search.orderBy+`",`: ``)+`
                `+((query_.search.orderBy !== "") ? `orderBy: "`+orderby+`",`: ``)+`

              ) {
                results {
                  record_id
                  enter_time
                  enter_method
                  exit_method
                  enter_gate {
                      gate_id
                      name
                      begin_time
                      end_time
                  }
                  exit_time
                  exit_gate {
                      gate_id
                      name
                      begin_time
                      end_time
                  }
                  is_exited
                  employee {
                      employee_id
                      name
                      lastname
                      position
                      mifare_id
                      picture_url
                      face_id
                  }
                }
                count
              }
          }
        `
      }, Token)
    } else {
      return getLog({
        query: `
          {
              GetRecord (
                `+((query_.search.name !== "") ? `name: "`+query_.search.name+`",`: ``)+`
                `+((query_.search.surname !== "") ? `employee_code: "`+query_.search.surname+`",`: ``)+`
                `+((query_.search.card !== "") ? `mifareId: "`+query_.search.card+`",`: ``)+`
                `+((query_.search.startdate !== "") ? `beginDate: "`+query_.search.startdate+`",`: ``)+`
                `+((query_.search.enddate !== "") ? `endDate: "`+query_.search.enddate+`",`: ``)+`
                `+((query_.search.starttime !== "") ? `beginTime: "`+query_.search.starttime+`",`: ``)+`
                `+((query_.search.endtime !== "") ? `endTime: "`+query_.search.endtime+`",`: ``)+`
                enterGate: [],
                `+((query_.search.offset !== "") ? `offset: `+query_.search.offset+`,`: ``)+`
                `+((query_.search.limit !== "") ? `limit: `+query_.search.limit+`,`: ``)+`
                
                `+((query_.search.orderType !== "") ? `orderType: "`+query_.search.orderBy+`",`: ``)+`
                `+((query_.search.orderBy !== "") ? `orderBy: "`+orderby+`",`: ``)+`


              ) {
                results {
                  record_id
                  enter_time
                  enter_method
                  exit_method
                  enter_gate {
                      gate_id
                      name
                      begin_time
                      end_time
                  }
                  exit_time
                  exit_gate {
                      gate_id
                      name
                      begin_time
                      end_time
                  }
                  is_exited
                  employee {
                      employee_id
                      name
                      lastname
                      position
                      mifare_id
                      picture_url
                      face_id
                  }
                }
                count
              }
          }
        `
      }, Token)
    }
  }
}
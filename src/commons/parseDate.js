const addDigiNubmerStr = (snum) => {
  return ((Number(snum) < 10) ? "0" + snum.toString(): snum.toString())
}

export default function parseDate (time) {
  var str = ''
  if (time !== ""){
    var dd = new Date(time)
    str =  addDigiNubmerStr(dd.getDate())  + "/" + addDigiNubmerStr(dd.getMonth() + 1)  + "/"+ dd.getFullYear()
  } else  {
    str = time
  }
  return str
}
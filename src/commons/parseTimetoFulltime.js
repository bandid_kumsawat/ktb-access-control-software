const addDigiNubmerStr = (snum) => {
  return ((Number(snum) < 10) ? "0" + snum.toString(): snum.toString())
}

export default function parseTimetoFulltime (full, time) {
  var Full = new Date(full)
  var h = time.split(":")[0]
  var m = time.split(":")[1]
  Full.setHours(h)
  Full.setMinutes(m)
  return `${addDigiNubmerStr(Full.getFullYear())}-${addDigiNubmerStr(Full.getMonth() + 1 )}-${addDigiNubmerStr(Full.getDay() + 1 )}T${addDigiNubmerStr(Full.getHours()  )}:${addDigiNubmerStr(Full.getMinutes())}:00.000Z`
  
  
  // addDigiNubmerStr(Full.getFullYear()) + "-" +
  // ( addDigiNubmerStr(Full.getMonth() + 1 ) ) + "-" +
  // ( addDigiNubmerStr(Full.getDay() + 1 ) ) + "T" +
  // ( addDigiNubmerStr(Full.getHours() ) ) + ":" +
  // ( addDigiNubmerStr(Full.getMinutes()) ) + ":" +
  // ( "00" ) + ".000Z"
  // 2017-01-27T13:19:53.000Z
}
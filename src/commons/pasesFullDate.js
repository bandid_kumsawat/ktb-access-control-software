const addDigiNubmerStr = (snum) => {
  return ((Number(snum) < 10) ? "0" + snum.toString(): snum.toString())
}

export default function pasesFullDate (date) {
  var dd = new Date(date)
  return dd.getFullYear() + "/" + addDigiNubmerStr(dd.getMonth() + 1)  + "/" + addDigiNubmerStr(dd.getDate()) + " " + addDigiNubmerStr(dd.getHours()) + ":" + addDigiNubmerStr(dd.getMinutes())
}
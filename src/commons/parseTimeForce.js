const addDigiNubmerStr = (snum) => {
  return ((Number(snum) < 10) ? "0" + snum.toString(): snum.toString())
}

function startTimer(duration) {
  var timer = duration, hours, minutes, seconds;
  hours = parseInt(timer / 60 / 60, 10);
  minutes = parseInt(timer / 60, 10);
  seconds = parseInt(timer % 60, 10);

  hours = hours < 10 ? "0" + hours : hours;
  minutes = minutes < 10 ? "0" + minutes : minutes;
  seconds = seconds < 10 ? "0" + seconds : seconds;

  if (--timer < 0) {
      timer = duration;
  }
  return [hours, minutes, seconds,]
}

export default function parseTimeForce (time) {
  // 2021-05-31T06:00:00.00007Z
  var force = new Date(time)
  // console.log(force)
  // var now = new Date()
  // var diff = force.getTime() - now.getTime()
  // return startTimer(diff/1000)


  var now = new Date().getTime();
  var distance = force - now;

  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
  return [
    (days < 0 ? 0 : days ),
    (hours < 0 ? 0 : hours ),
    (minutes < 0 ? 0 : minutes ),
    (seconds < 0 ? 0 : seconds ),
  ]
}
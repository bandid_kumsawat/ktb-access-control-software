const hasUppercase = (str) => {
    return (/[A-Z]/.test(str))
}

const hasLowercase = (str) => {
    return (/[a-z]/.test(str))
}

const hasDigit = (str) => {
    return (/[0-9]/.test(str))
}

const hasSpecialCharacter = (str) => {
    return (/[!@#$%^&*()_+\-={};':"\\|,.<>\/?~]/.test(str))
}

export default function PasswordChecker (password) {
    const error_list = []
    /* Password length must not less than 8 character */
    if (password.length < 8) {
        error_list.push(0)
    }
    /* Password must contains at least 1 uppercase */
    if (!hasUppercase(password)) {
        error_list.push(1)
    }
    /* Password must contains at least 1 lowercase */
    if (!hasLowercase(password)) {
        error_list.push(2)
    }
    /* Password must contains at least 1 digit [0-9] */
    if (!hasDigit(password)) {
        error_list.push(3)
    }
    /* Password must contains at least 1 special character*/
    if (!hasSpecialCharacter(password)) {
        error_list.push(4)
    }
    return error_list
}


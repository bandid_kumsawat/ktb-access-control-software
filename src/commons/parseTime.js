const addDigiNubmerStr = (snum) => {
  return ((Number(snum) < 10) ? "0" + snum.toString(): snum.toString())
}

export default function pasesTime (time) {
  // 0000-01-01T15:20:53Z
  var str = ''
  if (time !== ""){
    var h = time.split("T")[1].split(":")[0]
    var m = time.split("T")[1].split(":")[1]
    var Full = new Date()
    Full.setHours(h)
    Full.setMinutes(m)
    
    str =  ( addDigiNubmerStr(Full.getHours() ) ) + ":" + ( addDigiNubmerStr(Full.getMinutes()) )
  } else  {
    str = time
  }
  return str
}
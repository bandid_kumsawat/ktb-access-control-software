const addDigiNubmerStr = (snum) => {
  return ((Number(snum) < 10) ? "0" + snum.toString(): snum.toString())
}

export default function pasesToday (offset) {
  var dd = new Date()
  dd.setDate(dd.getDate() - offset)
  return dd.getFullYear() + "-" + addDigiNubmerStr(dd.getMonth() + 1)  + "-" + addDigiNubmerStr(dd.getDate()) 
}
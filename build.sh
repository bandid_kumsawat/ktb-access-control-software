set -e

npm run build

IMAGE_TAG=$(git log --format="%H" -n 1)
VERSION_TAG=$(git tag --contains)

if [ ! -z "$VERSION_TAG" ]
then
    IMAGE_TAG="$VERSION_TAG"
fi

echo "version : $VERSION_TAG"
echo "IMAGE_TAG : $IMAGE_TAG"

mkdir -p docker-images

docker build -t ktb-acs-nginx:$IMAGE_TAG .
docker save -o ./docker-images/ktb-acs-nginx.tar ktb-acs-nginx:$IMAGE_TAG


FROM nginx:1.12.0-alpine

EXPOSE 80 443
COPY build /usr/share/nginx/html
COPY acs.conf /etc/nginx/conf.d/
COPY cert/accesscontrol.inno.ktb.crt /etc/ssl/
COPY cert/accesscontrol.inno.ktb.key /etc/ssl/
